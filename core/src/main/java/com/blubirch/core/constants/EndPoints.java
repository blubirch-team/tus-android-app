package com.blubirch.core.constants;

public interface EndPoints {

    String URL_LOGIN = "login";

    /** {@link com.blubirch.core.data.api.interfaces.ForgotPasswordAPI } */
    String GET_PASSWORD_OTP = "password_resets/send_otp";
    String GET_PASSWORD_TOKEN = "password_resets/edit";
    String RESET_PASSWORD = "password_resets/reset";


}
