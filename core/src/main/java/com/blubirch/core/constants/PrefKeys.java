package com.blubirch.core.constants;

public interface PrefKeys {
    /**
     * Header keys
     */
    String HEADER_HASH_KEY = "hash";
    String HEADER_APP_VERSION = "app_version";
    String HEADER_ACCESS_TOKEN = "Authorization";
    /**
     * User keys
     */
    String USER_NAME_KEY="user_name";
    String USER_EMAIL_KEY="user_email";
    String USER_TITLE_KEY ="user_title";
}
