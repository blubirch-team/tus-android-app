package com.blubirch.core.di.modules;

import com.blubirch.core.data.api.interfaces.UserAPI;
import com.blubirch.core.data.repository.UserRepo;
import com.blubirch.core.data.repository.UserRepoImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class LoginDataModule {

    @Singleton
    @Provides
    public UserAPI providesUserAPI(Retrofit retrofit) {
        return retrofit.create(UserAPI.class);
    }

    @Singleton
    @Provides
    public UserRepo providesUserRepo(UserAPI userAPI) {
        return new UserRepoImpl(userAPI);
    }
}
