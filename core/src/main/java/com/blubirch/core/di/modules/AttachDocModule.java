package com.blubirch.core.di.modules;

import com.blubirch.core.presentation.TakePictureActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AttachDocModule {
    
    @ContributesAndroidInjector
    abstract TakePictureActivity bindTakePictureActivity();

}
