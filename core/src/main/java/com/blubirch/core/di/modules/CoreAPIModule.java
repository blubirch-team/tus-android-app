package com.blubirch.core.di.modules;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.blubirch.core.BuildConfig;
import com.blubirch.core.constants.PrefKeys;
import com.blubirch.core.data.api.adapters.LongTypeAdapter;
import com.blubirch.core.utility.AppPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class CoreAPIModule {

    private static final String TAG = "CoreAPIModule";

    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_PRAGMA = "Pragma";

    @Singleton
    @Provides
    Retrofit provideRetrofitClient(Context context) {

        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Long.class, new LongTypeAdapter());
        gsonBuilder.registerTypeAdapter(long.class, new LongTypeAdapter());
//        gsonBuilder.registerTypeAdapterFactory(new NullStringToDefaultAdapterFactory<>());
//        gsonBuilder.registerTypeAdapterFactory(new PostProcessingTypeAdapterFactory(MissingStringPostProcessor.getInstance()));
        Gson gson = gsonBuilder.serializeNulls().setLenient().create();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(BuildConfig.SERVER_URL)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson));

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if(BuildConfig.DEBUG) {
            httpClient.addInterceptor(provideOfflineCacheInterceptor(context))
                    .addNetworkInterceptor(provideCacheInterceptor(context))
                    .cache(provideCache(context));
        }
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(30, TimeUnit.SECONDS);

        httpClient.addNetworkInterceptor(chain -> {
            Request request = chain.request();

            final String accessToken = AppPreferences.getString(PrefKeys.HEADER_ACCESS_TOKEN, "");
            final String appVersionCode = String.valueOf(AppPreferences.getInt(PrefKeys.HEADER_APP_VERSION));
            final String hash = "";

            request = request.newBuilder()
                    .addHeader(PrefKeys.HEADER_ACCESS_TOKEN, accessToken)
                    .addHeader(PrefKeys.HEADER_APP_VERSION, appVersionCode)
                    .addHeader(PrefKeys.HEADER_HASH_KEY, hash).build();

            Response response = chain.proceed(request);
            String token = response.header(PrefKeys.HEADER_ACCESS_TOKEN, "");
            if (token != null && !token.isEmpty())
                AppPreferences.putString(PrefKeys.HEADER_ACCESS_TOKEN, token);
            return response;

        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.level(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }

        builder.client(httpClient.build());
        return builder.build();
    }


    private Cache provideCache(Context context) {
        Cache cache = null;

        try {
            cache = new Cache(new File(context.getApplicationContext().getCacheDir(), "http-cache"),
                    10 * 1024 * 1024); // 10 MB
        } catch (Exception e) {
            Log.e(TAG, "Could not create Cache!");
        }

        return cache;
    }

    private Interceptor provideCacheInterceptor(Context context) {
        return chain -> {
            Response response = chain.proceed(chain.request());

            CacheControl cacheControl;

            if (isConnected(context)) {
                cacheControl = new CacheControl.Builder()
                        .maxAge(0, TimeUnit.SECONDS)
                        .build();
            } else {
                cacheControl = new CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build();
            }

            return response.newBuilder()
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                    .build();

        };
    }

    private Interceptor provideOfflineCacheInterceptor(Context context) {
        return chain -> {
            Request request = chain.request();

            if (!isConnected(context)) {
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build();

                request = request.newBuilder()
                        .removeHeader(HEADER_PRAGMA)
                        .removeHeader(HEADER_CACHE_CONTROL)
                        .cacheControl(cacheControl)
                        .build();
            }

            return chain.proceed(request);
        };
    }

    public boolean isConnected(Context context) {
        try {
            ConnectivityManager e = (ConnectivityManager) context.getApplicationContext().getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = e.getActiveNetworkInfo();
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        } catch (Exception e) {
            Log.w(TAG, e.toString());
        }

        return false;
    }
}
