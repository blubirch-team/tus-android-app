package com.blubirch.core.di.modules;

import com.blubirch.core.data.api.interfaces.ForgotPasswordAPI;
import com.blubirch.core.data.repository.ForgotPasswordRepo;
import com.blubirch.core.data.repository.ForgotPasswordRepoImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ForgotPasswordDataModule {

    @Singleton
    @Provides
    public ForgotPasswordAPI providesForgotPasswordAPI(Retrofit retrofit) {
        return retrofit.create(ForgotPasswordAPI.class);
    }

    @Singleton
    @Provides
    public ForgotPasswordRepo providesForgotPasswordRepo(ForgotPasswordAPI forgotPasswordAPI) {
        return new ForgotPasswordRepoImpl(forgotPasswordAPI);
    }

}
