package com.blubirch.core.di.modules;

import androidx.lifecycle.ViewModel;

import com.blubirch.core.di.ViewModelKey;
import com.blubirch.core.presentation.WebViewActivity;
import com.blubirch.core.presentation.login.resetpassword.ForgotPasswordEmailActivity;
import com.blubirch.core.presentation.login.resetpassword.ForgotPasswordEmailViewModel;
import com.blubirch.core.presentation.login.resetpassword.ForgotPasswordOTPActivity;
import com.blubirch.core.presentation.login.resetpassword.ForgotPasswordOTPViewModel;
import com.blubirch.core.presentation.login.resetpassword.ForgotPasswordResetActivity;
import com.blubirch.core.presentation.login.resetpassword.ForgotPasswordResetViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class ForgotPasswordModule {

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordEmailViewModel.class)
    abstract ViewModel bindForgotPasswordEmailViewModel(ForgotPasswordEmailViewModel forgotPasswordEmailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordOTPViewModel.class)
    abstract ViewModel bindForgotPasswordOTPViewModel(ForgotPasswordOTPViewModel forgotPasswordOTPViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordResetViewModel.class)
    abstract ViewModel bindForgotPasswordResetViewModel(ForgotPasswordResetViewModel forgotPasswordResetViewModel);

    @ContributesAndroidInjector()
    abstract ForgotPasswordEmailActivity bindForgotPasswordEmailActivity();

    @ContributesAndroidInjector()
    abstract ForgotPasswordOTPActivity bindForgotPasswordOTPActivity();

    @ContributesAndroidInjector()
    abstract ForgotPasswordResetActivity bindForgotPasswordResetActivity();

    @ContributesAndroidInjector()
    abstract WebViewActivity bindWebViewActivity();

}
