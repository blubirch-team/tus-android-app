package com.blubirch.core.di.modules;

import androidx.lifecycle.ViewModel;

import com.blubirch.core.di.ViewModelKey;
import com.blubirch.core.presentation.AboutUsFragment;
import com.blubirch.core.presentation.login.LogoutDialog;
import com.blubirch.core.presentation.login.LoginActivity;
import com.blubirch.core.presentation.login.LoginViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class LoginModule {

    @ContributesAndroidInjector()
    abstract LoginActivity bindLoginActivity();

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindLoginViewModel(LoginViewModel loginViewModel);

    @ContributesAndroidInjector
    abstract LogoutDialog bindLogoutDialog();

    @ContributesAndroidInjector
    abstract AboutUsFragment bindAboutUsFragment();


}
