package com.blubirch.core.utility;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

public class AppUtils {

    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1221;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (cm != null) {
            networkInfo = cm.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.isConnected();
    }

    public static int getAppVersionCode(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }

    public static String getAppVersionName(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }


    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void orientationChange(Activity activity) {
        int orientation = activity.getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                break;
        }
    }


    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public static void showSoftKeyboard(Context ctx, EditText et) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Service.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(et, 0);
        }
    }


    public static String getRandomOrderNo() {
        Random ran = new Random();
        return String.valueOf((1000 + ran.nextInt(9000)));
    }


    public static void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    public static boolean validatePinCode(String pin) {
        if (TextUtils.isEmpty(pin))
            return false;
        return pin.length() >= 6;
    }

    public static String getCurrencyValue(long val, boolean currency) {
        if (currency) {
            return "₹ " + getFormattedAmount(val);
        }
        return getFormattedAmount(val);
    }

    public static String getCurrencyValue(double val, boolean currency) {
        if (currency) {
            return "₹ " + getFormattedAmount(Math.round(val));
        }

        return getFormattedAmount(Math.round(val));
    }

    public static String getMimeType(String url) {
        Log.e("UTILS", "UPLOAD FILE ITEM " + url);
        String type = "";
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        if (type == null) {
            type = "";
        }
        return type;
    }

    public static String getMimeTypeForFile(Context context, File file) {
        Uri uri = Uri.fromFile(file);
        ContentResolver cR = context.getContentResolver();
        return cR.getType(uri);
    }

    public static Date getDateFromString(String strDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(strDate);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static Date getEarlyDate(Date date1, Date date2) {
        if(date1.compareTo(date2) > 0) {
            return date2;
        } else {
            return date1;
        }
    }

    @Nullable
    public static Date getLaterDate(List<String> dates) {
        Date toReturnDate = null;
        for (String date : dates) {
            if (date == null) {
                continue;
            }
            if (toReturnDate == null) {
                toReturnDate = getDateFromString(date);
            } else {
                toReturnDate = getLaterDate(toReturnDate, getDateFromString(date));
            }
        }
        return toReturnDate;
    }

    @Nullable
    public static Date getLaterDateFromDate(List<Date> dates) {
        Date toReturnDate = null;
        for (Date date : dates) {
            if (date == null) {
                continue;
            }
            if (toReturnDate == null) {
                toReturnDate = date;
            } else {
                toReturnDate = getLaterDate(toReturnDate, date);
            }
        }
        return toReturnDate;
    }

    @Nullable
    public static Date getLaterDate(String... dates) {
        return getLaterDate(dates);
    }

    public static Date getLaterDate(Date date1, Date date2) {
        if (date1.compareTo(date2) > 0) {
            return date1;
        } else {
            return date2;
        }
    }

    private static String getFormattedAmount(long amount) {
        DecimalFormat formatter = new DecimalFormat("##,##,##,##,###");
        return formatter.format(amount);
//        return NumberFormat.getNumberInstance(Locale.getDefault()).format(amount);
    }

    public static String dateEpochToZoneDateSir(long epoh) {
        Date date = new Date(epoh * 1000L);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");
        format.setTimeZone(TimeZone.getDefault());
        String formatted = format.format(date);
        return formatted;
    }

    public static String dateEpochToDateSir(long epoh) {
        Date date = new Date(epoh * 1000L);
        DateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm aa");
        format.setTimeZone(TimeZone.getDefault());
        String formatted = format.format(date);
        return formatted;
    }

    public static String dateEpochToShortDateSir(long epoh) {
        Date date = new Date(epoh * 1000L);
        DateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getDefault());
        String formatted = format.format(date);
        return formatted;
    }

    public static String dateEpochToShortDate(long epoh) {
        Date date = new Date(epoh * 1000L);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setTimeZone(TimeZone.getDefault());
        return format.format(date);
    }

    public static String dateStringToShortDate(String strDate) {
        Date date = getDateFromString(strDate);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setTimeZone(TimeZone.getDefault());
        return format.format(date);
    }

    public static String dateStringToShortDate(Date date) {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setTimeZone(TimeZone.getDefault());
        return format.format(date);
    }

    public static String dateStringToShortDateTime(Date date) {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
        format.setTimeZone(TimeZone.getDefault());
        return format.format(date);
    }



    public static void launchInstallingExcelApp(Context context) {
        final String appPackageName = "com.google.android.apps.docs.editors.sheets";
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static String toCommaSeparatedString(ArrayList<String> strings) {
        if (strings == null)
            return "Any";
        StringBuilder stringBuilder = new StringBuilder();
        for (String str : strings) {
            stringBuilder.append(str);
            stringBuilder.append(",");
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        if (stringBuilder.length() == 0)
            stringBuilder.append("Any");
        return stringBuilder.toString();
    }

    public static String toCommaSeparatedString(String[] strings) {
        if (strings == null)
            return "-";
        StringBuilder stringBuilder = new StringBuilder();
        for (String str : strings) {
            stringBuilder.append(str);
            stringBuilder.append(",");
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        String requiredString = stringBuilder.toString().trim();
        if (TextUtils.isEmpty(requiredString))
            requiredString = "-";
        return requiredString;
    }

    public static ArrayList<String> fromCommaSeparatedString(String string) {
        if (TextUtils.isEmpty(string)) {
            return null;
        } else {
            ArrayList<String> list = new ArrayList(Arrays.asList(string.split(",")));
            return list;
        }
    }


    public static void copyToClipBoard(Context context, String text) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("text label", text);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
    }


    public static ArrayList<String> getArray(List<List<String>> lists) {
        ArrayList<String> values = new ArrayList<>();
        if (lists == null || lists.isEmpty())
            return values;
        for (List<String> strings : lists) {
            values.add(strings.get(0));
        }
        return values;
    }

    /**
     * Returns true if s contains any character other than
     * letters, numbers, or spaces.  Returns false otherwise.
     */

    public static boolean containsSpecialCharacter(String s) {
        return (s != null) && s.matches("[^A-Za-z0-9 ]");
    }


    public static void getStorageAccessPermission(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        AppUtils.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }
    }

    public static SpannableString setSpan(SpannableString spannableString, final String spanText, ClickableSpan clickableSpan) {
        int lastIndexOf = spannableString.toString().lastIndexOf(spanText);
        if (lastIndexOf > 0) {
            spannableString.setSpan(new UnderlineSpan(), lastIndexOf, lastIndexOf + spanText.length(), 0);
            if (clickableSpan != null)
                spannableString.setSpan(clickableSpan, lastIndexOf, lastIndexOf + spanText.length(), 0);
        }
        return spannableString;
    }

    /**
     * @param activity Activity object
     * @return true if activity is running
     */
    public static boolean isActivityRunning(Activity activity) {
        return !activity.isFinishing() && !activity.isDestroyed();
    }


    public static File getFileForImage(Context context, String imageName) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File path = new File(context.getFilesDir(), "MyAppName" + File.separator + "Images");
        if (!path.exists()) {
            path.mkdirs();
        }
        return new File(path, imageName + "_" + timeStamp + ".jpeg");
    }


    public static File getFileForImage(Context context, String child, String imageName, String extension) {
        long timestamp = System.currentTimeMillis();
        File path = new File(context.getFilesDir(), child);
        if (!path.exists()) {
            path.mkdirs();
        }
        return new File(path, imageName + "_" + timestamp + "_" + generateSafeToken() + "." + extension);
    }

    public static String generateSafeToken() {
        char[] acceptableChars = new char[]{
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        };
        SecureRandom random = new SecureRandom();
        StringBuilder randomStr = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            int index = random.nextInt(acceptableChars.length);
            randomStr.append(acceptableChars[index]);
        }
        return randomStr.toString();
    }

    public static Boolean isImageOrientationNormal(String filePath) throws IOException {
        ExifInterface exif;
        exif = new ExifInterface(filePath);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);
        return !(orientation == 3 || orientation == 6 || orientation == 8);
    }

    public static long calculateMinutes(Date dateEarly, Date dateLater) {
        return (dateLater.getTime() - dateEarly.getTime()) / (60 * 1000);
    }

    public static int calculateDays(Date dateEarly, Date dateLater) {
        return (int) ((dateLater.getTime() - dateEarly.getTime()) / (24 * 60 * 60 * 1000));
    }

    public static int ageInDays(Date date) {
        return calculateDays(date == null ? new Date() : date, new Date());
    }
}
