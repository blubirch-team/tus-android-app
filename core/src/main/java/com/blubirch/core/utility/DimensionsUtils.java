package com.blubirch.core.utility;

import android.content.Context;
import android.content.res.Resources;

import androidx.annotation.DimenRes;

public final class DimensionsUtils {
    private DimensionsUtils() throws InstantiationException {
        throw new InstantiationException("This utility class is created for instantiation");
    }

    public static float getDimension(@DimenRes int resourceId) {
        return Resources.getSystem().getDimension(resourceId);
    }

    static int getDimensionPixelSize(@DimenRes int resourceId) {
        return Resources.getSystem().getDimensionPixelSize(resourceId);
    }

    public static int dpToPx(float dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static float getDimension(Context context, @DimenRes int resourceId) {
        return context.getResources().getDimension(resourceId);
    }

    static int getDimensionPixelSize(Context context, @DimenRes int resourceId) {
        return context.getResources().getDimensionPixelSize(resourceId);
    }

    public static int dpToPx(Context context, float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }
}