package com.blubirch.core.utility

import android.content.Context
import android.view.View
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.files.fileChooser
import java.io.File

class KotlinJavaDelegation {
    companion object {
        fun makeBottomSheet(view: View): MaterialDialog {
            val bottomSheetDialog = MaterialDialog(view.context, BottomSheet(LayoutMode.WRAP_CONTENT))
            bottomSheetDialog.apply {
                customView(view = view, noVerticalPadding = false, horizontalPadding = true)
            }
            return bottomSheetDialog
        }

        fun createFileChooserDialog(context: Context, fileChooserListener: FileChooserListener): MaterialDialog {
            var externalFilesDir = context.getExternalFilesDir(null)
            try {
                externalFilesDir = externalFilesDir?.parentFile?.parentFile?.parentFile?.parentFile
            } catch (ignored : Exception) {  }

            val materialDialog = MaterialDialog(context);
            materialDialog.apply {
                fileChooser(context, initialDirectory = externalFilesDir){
                        dialog, file -> fileChooserListener.onFileSelected(file)
                }
            }
            return materialDialog
        }

        fun createDialog(view: View) : MaterialDialog{
            val materialDialog = MaterialDialog(view.context)
            materialDialog.apply {
                customView(view = view, noVerticalPadding = false, horizontalPadding = true)
            }
            return materialDialog
        }

    }




    interface FileChooserListener{
        fun onFileSelected(file: File)
    }
}