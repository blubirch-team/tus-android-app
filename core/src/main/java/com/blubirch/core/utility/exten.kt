package com.blubirch.core.utility

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.print.PrintHelper
import com.afollestad.materialdialogs.DialogCallback
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.blubirch.core.R
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.journeyapps.barcodescanner.BarcodeEncoder
import java.io.File
import java.util.*


fun Fragment.materialDialog(
    message: String, title: String = "",
    positiveText: String, positiveClickListener: DialogCallback,
    negativeText: String, negativeClickListener: DialogCallback
): MaterialDialog? {
    this.context?.let {
        return MaterialDialog(it)
            .show {
                lifecycleOwner(this@materialDialog)
                if (title.isNotBlank()) {
                    title(text = title)
                }
                message(text = message)
                negativeButton(text = negativeText, click = negativeClickListener)
                positiveButton(text = positiveText, click = positiveClickListener)
                noAutoDismiss()
            }

    }
    return null
}

fun Fragment.materialDialog(
    message: String, title: String = "",
    positiveText: String = "ok", positiveClickListener: DialogCallback? = null
): MaterialDialog? {
    this.context?.let { context ->
        return MaterialDialog(context)
            .show {
                lifecycleOwner(this@materialDialog)
                if (title.isNotBlank()) {
                    title(text = title)
                }
                message(text = message)
                if (positiveClickListener != null) {
                    positiveButton(text = positiveText, click = positiveClickListener)
                    noAutoDismiss()
                } else {
                    positiveButton(text = positiveText, click = {
                        it.dismiss()
                    })
                }
            }

    }
    return null
}

fun Fragment.materialDialog(
    @StringRes message: Int,
    @StringRes title: Int = R.string.alert,
    @StringRes positiveText: Int = R.string.ok,
    positiveClickListener: DialogCallback = { it.dismiss() },
    @StringRes negativeText: Int = R.string.cancel,
    negativeClickListener: DialogCallback? = { it.dismiss() }
): MaterialDialog? {
    requireContext().apply {
        return MaterialDialog(this)
            .show {
                lifecycleOwner(this@materialDialog)
                title(title)
                message(message)
                    positiveButton(text = resources.getString(positiveText), click = positiveClickListener)
                    negativeButton (text = resources.getString(negativeText), click = negativeClickListener)
            }
    }
}


fun Context.navigate(kClass: Class<out AppCompatActivity>) {
    startActivity(Intent(this, kClass))
}

fun Context.navigateWithExtra(kClass: Class<out AppCompatActivity>, extra: Any) {
    if (extra is String) {
        startActivity(Intent(this, kClass).putExtra("EXTRA", extra))

    } else {
        startActivity(Intent(this, kClass).putExtra("EXTRA", extra.toJson()))
    }
}


fun <T> Activity.getExtra(clazz: Class<T>): T? {
    return try {
        intent.extras?.getString("EXTRA")?.fromJson(clazz)
    } catch (e: Exception) {
        null
    }
}

fun Activity.getExtra(): String? {
    return try {
        intent.extras?.getString("EXTRA")
    } catch (e: Exception) {
        null
    }
}

fun Activity.getIntExtra(): Int? {
    return try {
        intent.extras?.getInt("EXTRA", -1)
    } catch (e: Exception) {
        null
    }
}

fun View.setBackGroundColorExt(@ColorRes color: Int) {
    setBackgroundColor(ContextCompat.getColor(context, color))
}

fun View.getColor(@ColorRes color: Int) = ContextCompat.getColor(context, color)

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) =
    liveData.observe(this, androidx.lifecycle.Observer(body))


fun Context.navigateClearStack(kClass: Class<out Activity>) {
    startActivity(Intent(this, kClass)
        .apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        })
}

fun Any.toJson() = Gson().toJson(this)
fun <T> String.fromJson(clazz: Class<T>) = Gson().fromJson(this, clazz)

fun EditText.stringText() = this.text.toString()

fun Context.dp2Pixel(dp: Int): Float {
    return dp * (resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)

}

fun View.gone() {
    visibility = View.GONE
}

fun View.inVisible() {
    visibility = View.INVISIBLE
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.visible(boolean: Boolean) {
    if (boolean) {
        visible()
    } else {
        gone()
    }
}

fun ImageView.loadImage(imageUrl: String, width: Int = 0, height: Int = 0) {
    val img = if (imageUrl.startsWith("http")) {
        Glide.with(this)
            .load(imageUrl)
            .centerInside()
    } else {
        Glide.with(this)
            .load(File(imageUrl))
            .centerInside()
    }
    if (width != 0 && height != 0) {
        img.override(width, height)
    }
    img.into(this)
}

fun AppCompatImageView.loadImage(imageUrl: String, width: Int = 0, height: Int = 0) {
    val img = if (imageUrl.startsWith("http")) {
        Glide.with(this)
            .load(imageUrl)
            .centerInside()
    } else {
        Glide.with(this)
            .load(File(imageUrl))
            .centerInside()
    }
    if (width != 0 && height != 0) {
        img.override(width, height)
    }
    img.into(this)
}

fun AppCompatTextView.copyTextToClipboard(){
    AppUtils.copyToClipBoard(this.context, this.text.toString())
}

fun AppCompatTextView.copyTextToClipboardOnClick(){
    this.setOnClickListener {
        AppUtils.copyToClipBoard(this.context, this.text.toString())
        Toast.makeText(this.context, "Text copied!", Toast.LENGTH_SHORT).show()
    }
}

fun <E> MutableList<E>.filterWith(filter: CustomPredicate<in E>): Boolean {
    Objects.requireNonNull(filter)
    var removed = false
    val listIterator = this.listIterator()
    while (listIterator.hasNext()) {
        if (filter.test(listIterator.next())) {
            listIterator.remove()
            removed = true
        }
    }
    return removed
}


fun String?.or(anotherString : String?) : String? {
    if (this.isNullOrBlank()) {
        return anotherString
    }
    return this
}

fun AppCompatActivity.materialDialog(
    message: String, title: String = "",
    positiveText: String, positiveClickListener: DialogCallback,
    negativeText: String, negativeClickListener: DialogCallback
): MaterialDialog? {

    return MaterialDialog(this)
        .show {
            lifecycleOwner(this@materialDialog)
            if (title.isNotBlank()) {
                title(text = title)
            }
            message(text = message)
            negativeButton(text = negativeText, click = negativeClickListener)
            positiveButton(text = positiveText, click = positiveClickListener)
            noAutoDismiss()
        }

}

fun Context.dipToPixels(dipValue: Float) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, resources.displayMetrics)

fun doPhotoPrint(context: Context, bitmap: Bitmap, fileName: String = "test") {
    PrintHelper(context).apply {
        scaleMode = PrintHelper.SCALE_MODE_FIT
    }.also { printHelper ->
        printHelper.printBitmap(
            TextUtils.concat("barcode.jpg", fileName, " - print").toString(),
            bitmap
        )
    }
}

fun generateBarcodeImage(text: String, width: Int = 800, height: Int = 200): Bitmap? {
    return try {
        val bitMatrix: BitMatrix =
            MultiFormatWriter().encode(text, BarcodeFormat.CODE_128, width, height)
        BarcodeEncoder().createBitmap(bitMatrix)
    } catch (e: WriterException) {
        e.printStackTrace()
        null
    }
}

fun loadBitmapFromView(v: View): Bitmap {
    val b =
        Bitmap.createBitmap(v.layoutParams.width, v.layoutParams.height, Bitmap.Config.ARGB_8888)
    val c = Canvas(b)
    v.layout(v.left, v.top, v.right, v.bottom)
    v.draw(c)
    return b
}

fun disableEditText(editText: EditText) {
    editText.isCursorVisible = false
    editText.isClickable = false
    editText.isFocusable = false
    editText.isFocusableInTouchMode = false
}