package com.blubirch.core.utility

import android.util.Base64
import io.reactivex.Single
import java.io.*
import kotlin.Throws

fun getFileBase64String(filePath: String): Single<String> {
    return Single.fromCallable {
        "data:image/png;base64," + Base64.encodeToString(
            read(File(filePath)), Base64.NO_CLOSE
        )
    }
}

@Throws(IOException::class)
fun read(file: File): ByteArray? {
    val ous = ByteArrayOutputStream()
    var ios: InputStream? = null
    val buffer = ByteArray(4096)
    try {
        ios = FileInputStream(file)
        var read: Int
        while (ios.read(buffer).also { read = it } != -1) {
            ous.write(buffer, 0, read)
        }
    } finally {
        try { ous.close() } catch (e: IOException) { }
        try { ios?.close() } catch (e: IOException) { }
    }
    return ous.toByteArray()
}