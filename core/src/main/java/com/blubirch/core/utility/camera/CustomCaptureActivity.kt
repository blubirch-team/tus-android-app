package com.blubirch.core.utility.camera

import com.blubirch.core.R
import com.journeyapps.barcodescanner.CaptureActivity
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import kotlinx.android.synthetic.main.custom_zxing_capture.*

class CustomCaptureActivity : CaptureActivity() {

    private var isTorchOn = false

    override fun initializeContent(): DecoratedBarcodeView {
        setContentView(R.layout.custom_zxing_capture)
        decoratedBarcodeView.setTorchListener(object : DecoratedBarcodeView.TorchListener {
            override fun onTorchOff() {
                imgTorch.setBackgroundResource(R.drawable.ic_torch_off)
            }

            override fun onTorchOn() {
                imgTorch.setBackgroundResource(R.drawable.ic_torch_on)
            }
        })
        imgTorch.setOnClickListener {
            if(isTorchOn) setTorchOff() else setTorchOn()
        }
        return decoratedBarcodeView
    }

    fun setTorchOn() {
        isTorchOn = true
        decoratedBarcodeView.setTorchOn()
    }

    fun setTorchOff() {
        isTorchOn = false
        decoratedBarcodeView.setTorchOff()
    }

}