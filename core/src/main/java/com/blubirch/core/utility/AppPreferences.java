package com.blubirch.core.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Class is used for save and retrieve Shared preference data.
 *
 * @author Prashant
 */
public class AppPreferences {
    private static final String MSG_ILLEGAL_ARGS = "The argument should be the application context!";
    private static final String VALUE_NOT_SET = null;
    private static SharedPreferences sharedPreference;

    /**
     * This method should be called at least once to set the SharedPreferences,
     * preferably at application launch. Once called, it need not be called
     * again by subsequent Activities
     *
     * @param applicationContext : Must be the application context and not an Activity context
     */
    public static void initPreferences(Context applicationContext) {
        if (applicationContext instanceof Activity) {
            throw new IllegalArgumentException(MSG_ILLEGAL_ARGS);
        } else if (sharedPreference == null) {
            sharedPreference = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        }
    }

    /**
     * Saves the specified key value pair in the application preferences
     *
     * @param key
     * @param value
     */
    public synchronized static void putString(String key,
                                              String value) {
        sharedPreference.edit().putString(key, value).commit();

    }

    /**
     * Gets the application preference identified by the argument key. Returns
     * null if the specified preference does not exist
     *
     * @param key
     * @return Value
     */
    public static String getString(String key) {
        if (sharedPreference == null) {
            return null;
        }
        return sharedPreference.getString(key, VALUE_NOT_SET);
    }


    public static String getString(String key,
                                   String defaultVal) {
        if (sharedPreference == null) {
            return null;
        }
        return sharedPreference.getString(key, defaultVal);
    }


    public synchronized static void putInt(String key,
                                           int value) {
        sharedPreference.edit().putInt(key, value).commit();
    }

    public static int getInt(String key) {
        if (sharedPreference == null) {
            return -1;
        }
        return sharedPreference.getInt(key, -1);
    }


    public static int getInt(String key, int defValue) {
        if (sharedPreference == null) {
            return defValue;
        }
        return sharedPreference.getInt(key, defValue);
    }

    public synchronized static void putLong(String key,
                                            Long value) {
        sharedPreference.edit().putLong(key, value).commit();

    }

    public static long getLong(String key) {
        if (sharedPreference == null) {
            return -1;
        }
        return sharedPreference.getLong(key, -1);

    }

    public static long getLong(String key, long defValue) {
        if (sharedPreference == null) {
            return defValue;
        }
        return sharedPreference.getLong(key, defValue);

    }

    public synchronized static void putFloat(String key,
                                             float value) {
        sharedPreference.edit().putFloat(key, value).commit();

    }

    public static float getFloat(String key) {
        if (sharedPreference == null) {
            return -1;
        }
        return sharedPreference.getFloat(key, -1);
    }

    public static float getFloat(String key, float defValue) {
        if (sharedPreference == null) {
            return defValue;
        }
        return sharedPreference.getFloat(key, defValue);
    }

    public synchronized static void putBoolean(String key,
                                               boolean value) {
        sharedPreference.edit().putBoolean(key, value).commit();

    }

    public static boolean getBoolean(String key) {
        if (sharedPreference == null)
            return false;
        return sharedPreference.getBoolean(key, false);
    }

    public static boolean getBoolean(String key, boolean defValue) {
        if (sharedPreference == null)
            return false;
        return sharedPreference.getBoolean(key, false);
    }

    /**
     * Multiple preferences can be saved by passing the key value pairs in a Map
     *
     * @param preferenceMap Map containing Keys and Values to be stored.
     */
    public synchronized static void putPreferences(
            Map<String, String> preferenceMap) {
        if (sharedPreference == null)
            return;
        SharedPreferences.Editor editor = sharedPreference.edit();
        Set<String> preferenceKeySet = preferenceMap.keySet();
        for (Iterator<String> index = preferenceKeySet.iterator(); index
                .hasNext(); ) {
            String key = index.next();
            String value = preferenceMap.get(key);
            editor.putString(key, value);
        }
        editor.commit();
    }

    public synchronized static void removePreferences(String[] keys) {
        if (sharedPreference == null)
            return;
        SharedPreferences.Editor editor = sharedPreference.edit();
        for (String key : keys) {
            editor.remove(key);
        }
        editor.commit();
    }

    public synchronized static void removePreference(String key) {
        if (sharedPreference == null)
            return;
        if (sharedPreference.contains(key)) {
            sharedPreference.edit().remove(key).commit();
        }
    }

    public synchronized static void removeAllPreferences() {
        if (sharedPreference == null)
            return;
        sharedPreference.edit().clear().commit();
    }

    public synchronized static boolean contains(String key) {
        if (sharedPreference == null)
            return false;
        return sharedPreference.contains(key);
    }

    public synchronized static void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        if (sharedPreference == null)
            return;
        sharedPreference.registerOnSharedPreferenceChangeListener(listener);
    }

    public synchronized static void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        if (sharedPreference == null)
            return;
        sharedPreference.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public Map<String, ?> getAll() {
        if (sharedPreference == null)
            return null;
        return (Map<String, String>) sharedPreference.getAll();
    }
}
