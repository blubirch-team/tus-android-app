package com.blubirch.core.data.repository;

import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.core.data.api.dto.user.ForgotPasswordOTPResDTO;

import io.reactivex.Single;

public interface ForgotPasswordRepo {

    Single<ForgotPasswordOTPResDTO> getOTP(String email);

    Single<String> getToken(String email, Integer otp);

    Single<BaseDTO> resetPassword(String email, String token, String password);

}
