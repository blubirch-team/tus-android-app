package com.blubirch.core.data

sealed class MyException(val throwable: Throwable, var msg: String = "", var code: Int = -1) :
    Exception(throwable) {

    class UnKnownError(throwable: Throwable) : MyException(throwable)
    class AutoApiErrorError(throwable: Throwable) : MyException(throwable)
    class ApiError(throwable: Throwable) : MyException(throwable)
    class NetworkErrorError(throwable: Throwable) : MyException(throwable)
    class TimeoutErrorError(throwable: Throwable) : MyException(throwable)
    class UnAuthenticate(throwable: Throwable) : MyException(throwable)
    class ValidationError(throwable: Throwable) : MyException(throwable, throwable.message ?: "", 0)
}