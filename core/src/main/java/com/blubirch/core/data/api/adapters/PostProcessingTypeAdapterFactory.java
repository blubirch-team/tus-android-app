package com.blubirch.core.data.api.adapters;

import androidx.core.util.Consumer;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Arrays;

public class PostProcessingTypeAdapterFactory implements TypeAdapterFactory {

    private final Iterable<? extends Consumer<Object>> postProcessors;

    public PostProcessingTypeAdapterFactory(final Iterable<? extends Consumer<Object>> postProcessors) {
        this.postProcessors = postProcessors;
    }

    public PostProcessingTypeAdapterFactory(final Consumer<Object>... postProcessors) {
        this(Arrays.asList(postProcessors.clone()));
    }

    @Override
    public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> typeToken) {
        final TypeAdapter<T> delegateAdapter = gson.getDelegateAdapter(this, typeToken);
        return new TypeAdapter<T>() {
            @Override
            public void write(final JsonWriter out, final T value)
                    throws IOException {
                delegateAdapter.write(out, value);
            }

            @Override
            public T read(final JsonReader in)
                    throws IOException {
                final T value = delegateAdapter.read(in);
                for (final Consumer<Object> postProcessor : postProcessors) {
                    postProcessor.accept(value);
                }
                return value;
            }
        };
    }

}