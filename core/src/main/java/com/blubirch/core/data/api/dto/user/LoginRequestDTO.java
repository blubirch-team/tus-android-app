package com.blubirch.core.data.api.dto.user;

import com.google.gson.annotations.SerializedName;

public class LoginRequestDTO {

    @SerializedName("user")
    private Params user;

    public LoginRequestDTO(final String login, final String password) {
        user = new Params();
        user.setLogin(login);
        user.setPassword(password);
    }

    public Params getUser() {
        return user;
    }

    public void setUser(Params user) {
        this.user = user;
    }

    static class Params {

        @SerializedName("login")
        private String login;
        @SerializedName("password")
        private String password;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
