package com.blubirch.core.data.database;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class DBExecutor {

    public static Scheduler dbScheduler;

    static {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        dbScheduler = Schedulers.from(executorService);
    }

}
