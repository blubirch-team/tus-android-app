package com.blubirch.core.data.api.dto.user

data class ForgotPasswordOTPResDTO(
    val otp: Int?,
    val status: String?,
    val message: String?
)