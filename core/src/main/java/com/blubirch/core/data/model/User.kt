package com.blubirch.core.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

class User {

    @SerializedName("id")
    var id: Long = 0

    @SerializedName("username")
    var username: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    @SerializedName("contact_no")
    var contactNo: String? = null

    @SerializedName("deleted_at")
    var deletedAt: Date? = null

    @SerializedName("roles")
    var roles: ArrayList<String>? = null
}