package com.blubirch.core.data.database.converters;

import androidx.room.TypeConverter;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONTypeConverter {

    @TypeConverter
    public static JSONObject fromString(String strJSONObject) {
        try {
            return new JSONObject(strJSONObject);
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }

    @TypeConverter
    public static String jsonToString(JSONObject jsonObject) {
        return jsonObject.toString();
    }
}
