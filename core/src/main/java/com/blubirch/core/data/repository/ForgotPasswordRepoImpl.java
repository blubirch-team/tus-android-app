package com.blubirch.core.data.repository;

import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.core.data.api.dto.user.ForgotPasswordOTPResDTO;
import com.blubirch.core.data.api.interfaces.ForgotPasswordAPI;

import javax.inject.Inject;

import io.reactivex.Single;

public class ForgotPasswordRepoImpl implements ForgotPasswordRepo{

    private final ForgotPasswordAPI forgotPasswordAPI;

    @Inject
    public ForgotPasswordRepoImpl(ForgotPasswordAPI forgotPasswordAPI) {
        this.forgotPasswordAPI = forgotPasswordAPI;
    }

    @Override
    public Single<ForgotPasswordOTPResDTO> getOTP(String email) {
        return forgotPasswordAPI.getOTP(email);
    }

    @Override
    public Single<String> getToken(String email, Integer otp){
        return forgotPasswordAPI.getToken(email, otp);
    }

    @Override
    public Single<BaseDTO> resetPassword(String email, String token, String password) {
        return forgotPasswordAPI.resetPassword(email, token, password);
    }

}
