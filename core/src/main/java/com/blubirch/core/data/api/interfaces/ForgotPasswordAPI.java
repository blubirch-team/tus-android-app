package com.blubirch.core.data.api.interfaces;


import com.blubirch.core.constants.EndPoints;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.core.data.api.dto.user.ForgotPasswordOTPResDTO;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ForgotPasswordAPI {

    @FormUrlEncoded
    @POST(EndPoints.GET_PASSWORD_OTP)
    Single<ForgotPasswordOTPResDTO> getOTP(@Field("email") String email);

    @FormUrlEncoded
    @POST(EndPoints.GET_PASSWORD_TOKEN)
    Single<String> getToken(@Field("email") String email, @Field("otp") Integer otp);

    @FormUrlEncoded
    @POST(EndPoints.RESET_PASSWORD)
    Single<BaseDTO> resetPassword(@Field("email") String email, @Field("token") String token, @Field("password") String password);

}
