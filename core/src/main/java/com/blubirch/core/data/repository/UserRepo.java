package com.blubirch.core.data.repository;



import com.blubirch.core.data.api.dto.user.LoginResponseDTO;

import io.reactivex.Single;

public interface UserRepo {

    Single<LoginResponseDTO> login(final String email, final String password);
}
