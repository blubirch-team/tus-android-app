package com.blubirch.core.data.api.interfaces;


import com.blubirch.core.constants.EndPoints;
import com.blubirch.core.data.api.dto.user.LoginRequestDTO;
import com.blubirch.core.data.api.dto.user.LoginResponseDTO;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserAPI {

    @POST(EndPoints.URL_LOGIN)
    Single<LoginResponseDTO> login(@Body LoginRequestDTO loginRequestDTO);


}
