package com.blubirch.core.data.api.dto

open class BaseDTO(var message : String = "", var status : Int = 0, var error : String = "")