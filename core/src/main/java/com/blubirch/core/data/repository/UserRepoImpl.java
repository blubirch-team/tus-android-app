package com.blubirch.core.data.repository;



import com.blubirch.core.data.api.dto.user.LoginRequestDTO;
import com.blubirch.core.data.api.dto.user.LoginResponseDTO;
import com.blubirch.core.data.api.interfaces.UserAPI;

import javax.inject.Inject;

import io.reactivex.Single;

public class UserRepoImpl implements UserRepo {

    private UserAPI userAPI;

    @Inject
    public UserRepoImpl(UserAPI userAPI) {
        this.userAPI = userAPI;
    }


    @Override
    public Single<LoginResponseDTO> login(final String email, final String password) {
        LoginRequestDTO loginRequestDTO = new LoginRequestDTO(email, password);
        return userAPI.login(loginRequestDTO);
    }
}
