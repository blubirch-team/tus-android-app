package com.blubirch.core.data.api.adapters;

import androidx.core.util.Consumer;

import java.lang.reflect.Field;

public class MissingStringPostProcessor
        implements Consumer<Object> {

    //	private static final String DEFAULT_VALUE = "Field not in response";
    private static final String DEFAULT_VALUE = "-";
    private static Consumer<Object> missingStringPostProcessor;


    private MissingStringPostProcessor() {
    }

    public static Consumer<Object> getInstance() {
        if (missingStringPostProcessor == null) {
            synchronized (MissingStringPostProcessor.class) {
                if (missingStringPostProcessor == null) {
                    missingStringPostProcessor = new MissingStringPostProcessor();
                }
            }
        }
        return missingStringPostProcessor;
    }

    @Override
    public void accept(final Object o) {
        try {
            if (o == null) {
                return;
            }
            final Field[] declaredFields = o.getClass().getDeclaredFields();
            for (final Field field : declaredFields) {
                field.setAccessible(true);
                if (field.getType() == String.class) {
                    if (field.get(o) == null) {
                        field.set(o, DEFAULT_VALUE);
                    }
                }
            }
        } catch (final IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

}