package com.blubirch.core.data.api.dto.user;

import com.blubirch.core.data.model.User;
import com.google.gson.annotations.SerializedName;

public class LoginResponseDTO {

    @SerializedName("user")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
