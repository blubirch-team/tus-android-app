package com.blubirch.core.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

open class BaseEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0L

    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    var createdAt: Date? = null

    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    var updatedAt: Date? = null
}