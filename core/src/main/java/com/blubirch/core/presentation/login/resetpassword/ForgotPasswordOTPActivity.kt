package com.blubirch.core.presentation.login.resetpassword

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.core.BuildConfig
import com.blubirch.core.data.MyException.*
import com.blubirch.core.databinding.ActivityForgotPasswordOtpBinding
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.customview.listeners.PrettyTextWatcher
import com.blubirch.core.presentation.login.LoginActivity
import com.blubirch.core.utility.AppUtils
import com.github.loadingview.LoadingDialog
import com.github.loadingview.LoadingDialog.Companion.get
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class ForgotPasswordOTPActivity : DaggerAppCompatActivity() {

    companion object{
        const val EXTRA_OTP : String = "OTP"
        const val EXTRA_EMAIL : String = "EMAIL"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var mViewModel: ForgotPasswordOTPViewModel
    private var snackbar: Snackbar? = null
    private var loadingDialog: LoadingDialog? = null
    private lateinit var binding: ActivityForgotPasswordOtpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProvider(this, viewModelFactory).get(
            ForgotPasswordOTPViewModel::class.java
        )

        if(mViewModel.setData(intent).not()){
            finish()
            return
        }


        binding = ActivityForgotPasswordOtpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.edtOTP.addTextChangedListener(object : PrettyTextWatcher() {
            override fun afterTextChanged(text: String) {
                binding.edtOTP.error = null
            }
        })

        binding.txtResendOTP.setOnClickListener {
            showLoader()
            mViewModel.sendOTP()
        }

        binding.txtReturnToLogin.setOnClickListener {
            Intent(this@ForgotPasswordOTPActivity, LoginActivity::class.java).apply {
                startActivity(this)
            }
        }

        binding.btnVerify.setOnClickListener { v: View? ->
            if (validate()) {
                AppUtils.hideSoftKeyboard(this@ForgotPasswordOTPActivity)
                getToken()
            }
        }

        attachLiveData()
    }

    private fun getToken(){
        showLoader()
        mViewModel.getToken()
    }

    private fun validate(): Boolean {
        binding.edtOTP.let {
            if (TextUtils.isEmpty(it.text)) {
                it.requestFocus()
                it.post {
                    AppUtils.hideSoftKeyboard(this)
                    it.error = "Enter OTP"
                }
                return false
            } else{
                try {
                    if (mViewModel.otpLiveData.value != it.text.toString().toInt()) {
                        it.post {
                            AppUtils.hideSoftKeyboard(this)
                            it.error = "Invalid OTP"
                        }
                        return false
                    }
                } catch (e: Exception) {
                    it.post {
                        AppUtils.hideSoftKeyboard(this)
                        it.error = "Invalid OTP"
                    }
                    return false
                }
            }
            return true
        }
    }

    private fun showSnackBar(text: String?) {
        hideSnackBar()
        text?.let {
            snackbar = Snackbar.make(binding.consMain, it, Snackbar.LENGTH_LONG).apply {
                show()
            }
        }
    }

    private fun hideSnackBar() {
        snackbar?.dismiss()
    }

    private fun attachLiveData() {
        mViewModel.tokenLiveData.observe(this, Observer {
            hideLoader()
            Intent(this@ForgotPasswordOTPActivity, ForgotPasswordResetActivity::class.java).apply {
                putExtra(ForgotPasswordResetActivity.EXTRA_TOKEN, it)
                putExtra(ForgotPasswordResetActivity.EXTRA_EMAIL, mViewModel.email)
                startActivity(this)
            }
        })

        mViewModel.otpLiveData.observe(this, Observer {
            hideLoader()
            if (BuildConfig.DEBUG) {
                binding.edtOTP.setText(it.toString())
            }
            showSnackBar("OTP sent to registered email")
        })

        mViewModel.errorLiveData.observe(this, Observer {
            hideLoader()
            handleError(it)
        })
    }

    private fun showLoader() {
        if (loadingDialog == null) {
            loadingDialog = get(this)
        }
        loadingDialog!!.show()
    }

    private fun hideLoader() {
        if (loadingDialog != null) loadingDialog!!.hide()
    }

    private fun handleError(throwable: Throwable) {
        if (throwable is UnKnownError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is TimeoutErrorError) {
            showSnackBar("Timeout error!")
        } else if (throwable is NetworkErrorError) {
            showSnackBar("Connection error!")
        } else if (throwable is ApiError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is ValidationError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is UnAuthenticate) {
            showSnackBar(throwable.cause!!.message)
        } else {
            Log.d("IAgreeException", throwable.message)
            showSnackBar(throwable.message)
        }
    }
}