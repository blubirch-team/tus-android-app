package com.blubirch.core.presentation.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blubirch.core.data.model.User;
import com.blubirch.core.data.repository.UserRepo;
import com.blubirch.core.presentation.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class LoginViewModel extends BaseViewModel {

    private static final String TAG = LoginViewModel.class.getSimpleName();
    @Inject
    UserRepo userRepo;
    private MutableLiveData<User> userMutableLiveData = new MutableLiveData<>();

    @Inject
    public LoginViewModel() {
    }

    public void login(final String email, final String password) {
        getCompositeDisposable().add(
                userRepo.login(email, password)
                        .subscribeOn(Schedulers.io())
                        .subscribe((loginResponseDTO, throwable) -> {
                            if (throwable != null) {
                                if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
                                    postAPIError(new Throwable("Invalid Username/Email or Password"));
                                } else {
                                    postAPIError(throwable);
                                }
                            } else {
                                userMutableLiveData.postValue(loginResponseDTO.getUser());
                            }
                        })
        );
    }

    public LiveData<User> userLiveData() {
        return userMutableLiveData;
    }
}
