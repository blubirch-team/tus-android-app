package com.blubirch.core.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import com.blubirch.core.R
import com.blubirch.core.utility.AppUtils
import com.blubirch.core.utility.loadImage
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.CameraOptions
import com.otaliastudios.cameraview.PictureResult
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_take_picture.*
import kotlinx.android.synthetic.main.activity_take_picture.grpControls
import kotlinx.android.synthetic.main.activity_take_picture.ivCancel
import kotlinx.android.synthetic.main.activity_take_picture.ivCapture
import kotlinx.android.synthetic.main.activity_take_picture.ivConfirm
import kotlinx.android.synthetic.main.activity_take_picture.ivRePick
import kotlinx.android.synthetic.main.camera_image_fragment.*
import java.io.File

class TakePictureActivity : DaggerAppCompatActivity() {

    companion object{
        const val INTENT_REQUEST_CODE_TAKE_PICTURE = 1000
        const val INTENT_RESULT_TAKE_PICTURE = "RESULT_PICTURE"
    }

    private var filePath: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_take_picture)

        ivConfirm.setOnClickListener { onConfirmClicked() }
        ivCapture.setOnClickListener { takePhoto() }
        ivRePick.setOnClickListener { onRePickClicked() }
        ivCancel.setOnClickListener { onCancelClicked() }

        cameraView.addCameraListener(object : CameraListener() {
            override fun onCameraOpened(options: CameraOptions) {
                showCamera()
            }
        })
    }

    private fun takePhoto() {
        if (!cameraView.isOpened) {
            cameraView.open()
        }
        cameraView.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                grpControls.isEnabled = false
                result.toFile( AppUtils.getFileForImage(baseContext, "") ) { file: File? ->
                    file?.absolutePath?.let {
                        imgPicPreview.visibility = View.VISIBLE
                        hideCamera()
                        imgPicPreview.loadImage(it)
                        filePath = it
                    }
                }
            }
        })
        cameraView.takePicture()
    }


    private fun onConfirmClicked() {
        if (!filePath.isNullOrBlank()) {
            setResult(Activity.RESULT_OK, Intent().also {
                it.putExtra(INTENT_RESULT_TAKE_PICTURE, filePath)
            })
        }
        finish()
    }

    override fun onResume() {
        super.onResume()
        if (cameraView.isVisible && cameraView.isOpened.not()) {
            cameraView.open()
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, Intent().also {
            it.putExtra(INTENT_RESULT_TAKE_PICTURE, filePath)
        })
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        if(cameraView.isOpened){
            cameraView.close()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraView.destroy()
    }

    private fun onRePickClicked() {
        cameraView.open()
        showCamera()
    }

    private fun showCamera() {
        Log.d("TakePicture", "showCamera: ")
        imgPicPreview.visibility = View.GONE
        cameraView.visibility = View.VISIBLE
        grpControls.visibility = View.GONE
        ivCapture.visibility = View.VISIBLE
    }

    private fun hideCamera() {
        Log.d("TakePicture", "hideCamera: ")
        imgPicPreview.visibility = View.VISIBLE
        cameraView.visibility = View.GONE
        grpControls.visibility = View.VISIBLE
        ivCapture.visibility = View.GONE
    }

    private fun onCancelClicked() {
        hideCamera()
        finish()
    }


}