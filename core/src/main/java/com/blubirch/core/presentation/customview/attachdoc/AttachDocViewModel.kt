package com.blubirch.core.presentation.customview.attachdoc

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject

open class AttachDocViewModel @Inject constructor() : BaseViewModel() {

    private var docTypes = ArrayList<ArrayList<String>>()

    val docsToFillRef: List<String> = arrayListOf("obd")
    private var refValueToFill : String? = null

    private val attachedDocs = ArrayList<AttachedDoc>()
    val attachedDocLiveData : LiveData<ArrayList<AttachedDoc>> by lazy {
        MutableLiveData<ArrayList<AttachedDoc>>()
    }

    fun addDoc(attachedDoc: AttachedDoc) {
        attachedDocs.add(0, attachedDoc)
        (attachedDocLiveData as MutableLiveData).postValue(attachedDocs)
    }

    fun addDoc(attachedDocs: ArrayList<AttachedDoc>) {
        AttachDocViewModel@this.attachedDocs.addAll(attachedDocs)
        (attachedDocLiveData as MutableLiveData).postValue(AttachDocViewModel@this.attachedDocs)
    }

    fun delete(attachedDoc: AttachedDoc?, position: Int) {
        attachedDocs.remove(attachedDoc)
        (attachedDocLiveData as MutableLiveData).postValue(attachedDocs)
    }

    fun setDocTypes(docTypes: Array<Array<String>>) {
        for (docType in docTypes) {
            val doc = ArrayList<String>()
            doc.addAll(docType)
            this.docTypes.add(doc)
        }
    }

    fun setDocTypes(docTypes: ArrayList<ArrayList<String>>) {
        this.docTypes.clear()
        this.docTypes.addAll(docTypes)
    }

    fun setDocTypes(fileTypes: List<List<String?>?>?) {
        if (fileTypes != null) {
            docTypes.clear()
            for (index in fileTypes.indices) {
                fileTypes[index]?.let { it ->
                    val docType = ArrayList<String>()
                    if (it[0].isNullOrEmpty().not() && it[1].isNullOrEmpty().not()) {
                        docType.add(it[0]!!)
                        docType.add(it[1]!!)
                        docTypes.add(docType)
                    }
                }
            }
        }
    }

    fun setRefFillValue(refFillValue: String?) {
        refValueToFill = refFillValue
    }

    fun getRefFillValue() : String? {
        return refValueToFill
    }

    fun getDocList(): java.util.ArrayList<String> {
        val docTypes = java.util.ArrayList<String>()
        for (docTypeList in AttachDocViewModel@this.docTypes) {
            docTypes.add(docTypeList[1])
        }
        return docTypes
    }

    fun getDocTypeId(selectedDoc: String) : String{
        for (docType in docTypes) {
            if(docType[1].equals(selectedDoc)){
                return docType[0]
            }
        }
        return ""
    }

}