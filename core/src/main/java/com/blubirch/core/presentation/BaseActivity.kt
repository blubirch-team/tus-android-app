package com.blubirch.core.presentation

import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.blubirch.core.R
import com.blubirch.core.databinding.ActivityBaseBinding
import com.blubirch.core.databinding.ToastMessgaeLayoutBinding
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.utility.materialDialog
import com.github.loadingview.LoadingDialog
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import java.lang.reflect.ParameterizedType


abstract class BaseActivity<T : BaseViewModel> : DaggerAppCompatActivity() {

    private lateinit var binding: ActivityBaseBinding
    private lateinit var baseViewModel: T
    private var mViewModelFactory: ViewModelFactory? = null

    private lateinit var appBarConfiguration: AppBarConfiguration
    protected lateinit var navController: NavController


    open fun getViewModel(): T {
        if (!::baseViewModel.isInitialized) {
            baseViewModel = ViewModelProvider(this, mViewModelFactory!!)
                .get((this.javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>)
        }
        return baseViewModel
    }


    abstract fun attachLiveData()
    abstract fun setupNavGraph(navController: NavController)
    abstract fun getViewModelFactory(): ViewModelFactory

    private var loadingDialog: LoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }
        binding = ActivityBaseBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navController = findNavController(R.id.navHostFragment)
        setupNavGraph(navController)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController,appBarConfiguration)
        initPrimaryComponents()
    }


    override fun onResume() {
        super.onResume()

        baseViewModel.progressLiveData.value = false

        baseViewModel.progressLiveData.observe(this, Observer {
            if (it) {
                if (loadingDialog == null)
                    loadingDialog = LoadingDialog[this].show()
                else
                    loadingDialog?.show()
            } else {
                loadingDialog?.hide()
            }

        })

        baseViewModel.errorLiveData.observe(this, Observer {
            baseViewModel.progressLiveData.value = false
            loadingDialog?.hide()
            it?.apply {
                baseViewModel.errorLiveData.value = null
            }
        })
        attachLiveData()
    }

    private fun initPrimaryComponents() {
        mViewModelFactory = getViewModelFactory()
        baseViewModel = getViewModel()
    }

    private fun showExitDialog() {
        materialDialog("Are you sure you want to exit?", "Exiting Application", "Yes", {
            this@BaseActivity.finish()
        }, "No", {
            it.dismiss()
        })
    }

    override fun onPause() {
        baseViewModel.errorLiveData.value = null
        baseViewModel.progressLiveData.value = false
        super.onPause()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        val first =
            supportFragmentManager.primaryNavigationFragment?.childFragmentManager?.fragments?.first()
        first?.let {
            if(first is BaseFragment<*>){
                if (first.onBackPressed()) {
                    return
                }
            }
        }
        super.onBackPressed()
    }

    fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    fun setToolbarTitle(@StringRes title: Int) {
        supportActionBar?.setTitle(title)
    }

    private var snackbar: Snackbar? = null
    fun showSnackBar(text: String, duration: Int = Snackbar.LENGTH_SHORT, view: View? = null) {
        hideSnackBar()
        snackbar = Snackbar.make(binding.coordinatorLayout, text, duration)
        if (view != null)
            snackbar!!.anchorView = view
        snackbar?.show()
    }

    fun hideSnackBar() {
        snackbar?.dismiss()
    }

    var toast: Toast? = null
    fun showToast(text: String) {
        hideToast()
        toast = Toast.makeText(this, text, Toast.LENGTH_SHORT)
        toast!!.show()
    }

    fun showToastLonger(text: String) {
        hideToast()
        toast = Toast(this)
        val binding = ToastMessgaeLayoutBinding.inflate(layoutInflater)
        toast!!.view = binding.root
        toast!!.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0, 40)
        binding.message.text = text
        toast!!.duration = Toast.LENGTH_LONG
        toast!!.show()
    }

    fun hideToast() {
        toast?.cancel()
    }
}