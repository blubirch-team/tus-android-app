package com.blubirch.core.presentation.customview.attachdoc

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class AttachedDoc constructor(
    val docName: String,
    val docKey: String,
    val fileAbsolutePath: String?,
    var referenceNo: String = "",
    var isSyncedWithServer: Boolean = false
) : Parcelable
