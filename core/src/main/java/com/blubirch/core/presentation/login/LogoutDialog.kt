package com.blubirch.core.presentation.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.blubirch.core.BuildConfig
import com.blubirch.core.constants.PrefKeys
import com.blubirch.core.databinding.DialogLogoutBinding
import com.blubirch.core.di.customview.DaggerBottomSheetDialogFragment
import com.blubirch.core.utility.AppPreferences
import kotlinx.android.synthetic.main.dialog_logout.*

class LogoutDialog : DaggerBottomSheetDialogFragment() {

    fun newInstance(): LogoutDialog {
        return LogoutDialog()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DialogLogoutBinding.inflate(LayoutInflater.from(requireContext()))
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        txtUserIcon.text = AppPreferences.getString(PrefKeys.USER_TITLE_KEY, "")
        txtUsername.text = AppPreferences.getString(PrefKeys.USER_NAME_KEY, "")
        txtEmail.text = AppPreferences.getString(PrefKeys.USER_EMAIL_KEY, "")
//        txtAppVersion.text = "-v" + BuildConfig.VERSION_NAME
        txtAboutWMS.setOnClickListener {
            try {
                dismiss()
                findNavController().navigate(LogoutDialogDirections.actionBsfLogoutToAboutUsFragment())
            } catch (exception: Exception) {
            }
        }
        txtLogout.setOnClickListener {
            dismiss()
            AppPreferences.removePreference(PrefKeys.HEADER_ACCESS_TOKEN)
            requireActivity().finish()
            startActivity(Intent(requireActivity(), LoginActivity::class.java))
        }
    }

}