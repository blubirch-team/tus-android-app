package com.blubirch.core.presentation.customview.listeners;

import android.text.Editable;
import android.text.TextWatcher;


public abstract class PrettyTextWatcher implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String text = "";
        if (s != null) {
            text = s.toString();
        }
        afterTextChanged(text);
    }

    public abstract void afterTextChanged(String text);
}
