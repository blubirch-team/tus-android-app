package com.blubirch.core.presentation.login.resetpassword

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.core.data.api.dto.user.ForgotPasswordOTPResDTO
import com.blubirch.core.data.repository.ForgotPasswordRepo
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.utility.or
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ForgotPasswordOTPViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var forgotPasswordRepo: ForgotPasswordRepo


    lateinit var email : String
        private set

    val tokenLiveData : LiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val otpLiveData : LiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    fun getToken() {
        if (email.trim().isEmpty()) {
            postAPIError(Throwable("Enter OTP"))
            return
        }

        compositeDisposable.add(
            forgotPasswordRepo.getToken(email, otpLiveData.value)
                .subscribeOn(Schedulers.io())
                .subscribe { t1: String?, throwable: Throwable? ->
                    throwable?.let {
                        postAPIError(it)
                        it.printStackTrace()
                    }

                    t1?.let {
                        (tokenLiveData as MutableLiveData).postValue(it)
                    }
                }
        )
    }

    fun sendOTP() {
        compositeDisposable.add(
            forgotPasswordRepo.getOTP(email)
                .subscribeOn(Schedulers.io())
                .subscribe { forgotPasswordOTPResDTO: ForgotPasswordOTPResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        postAPIError(it)
                        it.printStackTrace()
                    }

                    forgotPasswordOTPResDTO?.let {
                        if (it.otp != null) {
                            (otpLiveData as MutableLiveData).postValue(it.otp)
                        } else {
                            postAPIError(Throwable(it.message.or("Unknown error")))
                        }
                    }
                }
        )
    }

    fun setData(intent: Intent?): Boolean {
        val otp = intent?.extras?.getInt(ForgotPasswordOTPActivity.EXTRA_OTP) ?: -1
        email = intent?.extras?.getString(ForgotPasswordOTPActivity.EXTRA_EMAIL) ?: ""
        val toReturn = otp != -1 && email.isEmpty().not()
        if(toReturn){
            (otpLiveData as MutableLiveData).postValue(otp)
        }
        return toReturn
    }


}