package com.blubirch.core.presentation.login.resetpassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.core.data.api.dto.user.ForgotPasswordOTPResDTO
import com.blubirch.core.data.repository.ForgotPasswordRepo
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.utility.or
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ForgotPasswordEmailViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var forgotPasswordRepo: ForgotPasswordRepo

    var email: String = ""
        private set

    val otpLiveData : LiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    fun getOTP(email: String) {
        this@ForgotPasswordEmailViewModel.email = email
        compositeDisposable.add(
            forgotPasswordRepo.getOTP(email)
                .subscribeOn(Schedulers.io())
                .subscribe { forgotPasswordOTPResDTO: ForgotPasswordOTPResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        postAPIError(it)
                        it.printStackTrace()
                    }

                    forgotPasswordOTPResDTO?.let {
                        if (it.otp != null) {
                            (otpLiveData as MutableLiveData).postValue(it.otp)
                        } else {
                            postAPIError(Throwable(it.message.or("Unknown error")))
                        }
                    }
                }
        )
    }

}