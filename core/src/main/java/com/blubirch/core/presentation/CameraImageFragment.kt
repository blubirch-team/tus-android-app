package com.blubirch.core.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.blubirch.core.R
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.utility.*
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.CameraOptions
import com.otaliastudios.cameraview.PictureResult
import kotlinx.android.synthetic.main.camera_image_fragment.*
import javax.inject.Inject


abstract class CameraImageFragment : BaseFragment<CameraImageViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    private lateinit var viewModel: CameraImageViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.camera_image_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        camera.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CameraImageViewModel::class.java)

        ivConfirm.setOnClickListener {
            onConfirmClicked()
        }
        ivCapture.setOnClickListener {
            takePhoto()
        }
        ivRePick.setOnClickListener {
            onRePickClicked()
        }
        ivCancel.setOnClickListener {
            onCancelClicked()
        }
        camera.addCameraListener(object : CameraListener() {
            override fun onCameraClosed() {
                super.onCameraClosed()
                hideCamera()
            }

            override fun onCameraOpened(options: CameraOptions) {
                super.onCameraOpened(options)
                showCamera()
            }
        })

    }

    private fun takePhoto() {
        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                // FIXME: 23/9/20 call AppUtils function here
//                result.toFile(getFileForImage(requireContext())) {
//                    viewModel.imageLiveData.value = it?.absolutePath
//                }
                camera.close()
            }
        })
        camera.takePicture()
    }


    private fun onConfirmClicked() {
        if (!viewModel.imageLiveData.value.isNullOrEmpty()) {
            returnImage(viewModel.imageLiveData.value!!)
        } else {
            materialDialog("You don't have selected Image yet",
                "Select image first", "Select", {
                    it.dismiss()
                    onRePickClicked()
                }, "Cancel", {
                    it.dismiss()
                })
        }
    }

    abstract fun returnImage(value: String)


    private fun onRePickClicked() {
        camera.open()
    }

    private fun showCamera() {
        annotationImage.gone()
        if (!camera.isVisible)
            camera.visible()
        grpControls.gone()
        ivCapture.visible()
    }

    private fun hideCamera() {
        annotationImage.visible()
        camera.gone()
        grpControls.visible()
        ivCapture.gone()
    }

    private fun onCancelClicked() {
        requireActivity().finish()
    }

    override fun getViewModel() = viewModel
    override fun attachLiveData() {
        observe(viewModel.imageLiveData, {
            if (!it.isNullOrEmpty()) {
                annotationImage.visible()
                camera.gone()
                annotationImage.loadImage(it)
                ivConfirm.isEnabled = true
            }
        })
    }
}
