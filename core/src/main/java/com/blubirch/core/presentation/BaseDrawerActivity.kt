package com.blubirch.core.presentation

import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.ui.NavigationUI
import com.blubirch.core.R
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.utility.materialDialog
import com.github.loadingview.LoadingDialog
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_base_drawer.*
import java.lang.reflect.ParameterizedType


abstract class BaseDrawerActivity<T : BaseViewModel> : DaggerAppCompatActivity() {

    private lateinit var baseViewModel: T
    private var navController: NavController? = null
    private var mViewModelFactory: ViewModelFactory? = null

    open fun getViewModel(): T {
        if (!::baseViewModel.isInitialized) {
            baseViewModel = ViewModelProvider(this, mViewModelFactory!!)
                .get((this.javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>)
        }
        return baseViewModel
    }


    abstract fun attachLiveData()
    abstract fun getActivityNavController(): NavController?
    abstract fun getViewModelFactory(): ViewModelFactory

    private var loadingDialog: LoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }
    }


    override fun onResume() {
        super.onResume()
        initPrimaryComponents()

        setBaseUpViews()

        baseViewModel.progressLiveData.value = false

        baseViewModel.progressLiveData.observe(this, Observer {
            if (it) {
                if (loadingDialog == null)
                    loadingDialog = LoadingDialog[this].show()
                else
                    loadingDialog?.show()
            } else {
                loadingDialog?.hide()
            }

        })

        baseViewModel.errorLiveData.observe(this, Observer {
            baseViewModel.progressLiveData.value = false
            loadingDialog?.hide()
            it?.apply {
                baseViewModel.errorLiveData.value = null
            }
        })
        attachLiveData()
    }

    private fun initPrimaryComponents() {
        navController = getActivityNavController()
        mViewModelFactory = getViewModelFactory()
        baseViewModel = getViewModel()
    }

    override fun onBackPressed() {
        if (isTaskRoot) {
            if (navController != null) {
                if (navController?.graph?.startDestination == navController?.currentDestination?.id) {
                    showExitDialog()
                } else {
                    super.onBackPressed()
                }
            } else {
                showExitDialog()
            }
        } else {
            super.onBackPressed()
        }

    }

    private fun showExitDialog() {
        materialDialog("Are you sure you want to exit?", "Exiting Application", "Yes", {
            this@BaseDrawerActivity.finish()
        }, "No", {
            it.dismiss()
        })
    }

    private fun setBaseUpViews() {
        setupToolbar()
        setUpDrawerAndActionBar()
    }

    fun setDrawerSelection(id: Int) {
        navigationView?.post {
            for (i in 0 until navigationView.menu.size()) {
                navigationView.menu.getItem(i).isChecked = false
            }
            navigationView?.setCheckedItem(id)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
    }

    abstract fun setUpDrawerAndActionBar()

    fun setToolbarTitle(title: String) {
        toolbar?.title = title
    }

    fun setToolbarTitle(@StringRes title: Int) {
        toolbar?.title = getString(title)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController?.let {
            NavigationUI.navigateUp(it, drawerLayout)
        } ?: false
    }

    override fun onPause() {
        baseViewModel.errorLiveData.value = null
        baseViewModel.progressLiveData.value = false
        super.onPause()
    }

}