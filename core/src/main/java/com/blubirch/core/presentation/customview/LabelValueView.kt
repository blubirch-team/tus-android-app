package com.blubirch.core.presentation.customview

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.setPadding
import com.blubirch.core.R
import com.blubirch.core.utility.dp2Pixel
import com.blubirch.core.utility.getColor

/**
 * TODO: document your custom view class.
 */
class LabelValueView : LinearLayout {

    constructor(context: Context?) : super(context) {
        init(null, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init(attrs, 0)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    var label: String? = ""
        set(value) {
            field = value
            findViewById<TextView>(R.id.tvLabel)?.text = value ?: "-"
        }
    var value: String? = ""
        set(value) {
            field = value
            findViewById<TextView>(R.id.tvValue)?.text = value ?: "-"
        }

    var labelColor: Int = getColor(R.color.colorPrimary)
        set(value) {
            field = value
            findViewById<TextView>(R.id.tvLabel)?.setTextColor(value)
        }

    var valueColor: Int = getColor(R.color.text_color_black)
        set(value) {
            field = value
            findViewById<TextView>(R.id.tvValue)?.setTextColor(value)
        }

    private fun init(attrs: AttributeSet? = null, defStyleAttr: Int = 0) {
        orientation = VERTICAL
        setPadding(context.dp2Pixel(4).toInt())
        View.inflate(context, R.layout.lable_value_view, this)
        val typedArray: TypedArray? = context.obtainStyledAttributes(
            attrs,
            R.styleable.LabelValueView,
            defStyleAttr, 0
        )
        label = typedArray?.getString(R.styleable.LabelValueView_label) ?: ""
        value = typedArray?.getString(R.styleable.LabelValueView_value) ?: ""
        labelColor = typedArray?.getColor(
            R.styleable.LabelValueView_labelColor, getColor(R.color.colorPrimary)
        ) ?: getColor(R.color.colorPrimary)
        valueColor = typedArray?.getColor(
            R.styleable.LabelValueView_valueColor, getColor(R.color.text_color_black)
        ) ?: getColor(R.color.text_color_black)

        findViewById<TextView>(R.id.tvLabel)?.text = label
        findViewById<TextView>(R.id.tvValue)?.text = value
        typedArray?.recycle()
    }
}