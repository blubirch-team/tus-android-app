package com.blubirch.core.presentation.customview.attachdoc

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Filter
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onCancel
import com.blubirch.core.R
import com.blubirch.core.databinding.DialogAttachDocChooserBinding
import com.blubirch.core.databinding.DialogDocumentDetailsBinding
import com.blubirch.core.presentation.TakePictureActivity
import com.blubirch.core.presentation.customview.attachdoc.AttachedDocListAdapter.DocActionListener
import com.blubirch.core.utility.KotlinJavaDelegation.Companion.createDialog
import com.blubirch.core.utility.KotlinJavaDelegation.Companion.createFileChooserDialog
import com.blubirch.core.utility.KotlinJavaDelegation.Companion.makeBottomSheet
import com.blubirch.core.utility.KotlinJavaDelegation.FileChooserListener
import com.google.android.material.textfield.TextInputLayout
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.File
import java.util.*

class AttachDocView : ConstraintLayout {
    private var attachDocListener: AttachDocListener? = null
    private lateinit var tilDocumentType: TextInputLayout
    private lateinit var spDocumentType: AppCompatAutoCompleteTextView
    private lateinit var rcDocumentList: RecyclerView
    private lateinit var adapter: AttachedDocListAdapter
    private var optionChooserDialog: MaterialDialog? = null
    private var fileChooserDialog: MaterialDialog? = null
    private var fileConfirmDialog: MaterialDialog? = null
    private lateinit var attachDocViewModel: AttachDocViewModel

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init(attrs, 0)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private fun init(attrs: AttributeSet?, defStyleAttrs: Int) {
        View.inflate(context, R.layout.widget_file_attachment, this)
        tilDocumentType = findViewById(R.id.tilDocumentType)
        spDocumentType = findViewById(R.id.spDocumentType)
        rcDocumentList = findViewById(R.id.rcDocumentList)
        adapter = AttachedDocListAdapter(object : DocActionListener {
            override fun onEdit(attachedDoc: AttachedDoc, position: Int) {
                openFileConfirmationDialog(attachedDoc)
            }

            override fun onDelete(attachedDoc: AttachedDoc, position: Int) {
                if (this@AttachDocView::attachDocViewModel.isInitialized) {
                    attachDocViewModel.delete(attachedDoc, position)
                } else {
                    adapter.data.removeAt(position)
                    adapter.notifyDataSetChanged()
                }
            }
        })
        val lm: LinearLayoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        rcDocumentList.isNestedScrollingEnabled = false
        rcDocumentList.layoutManager = lm
        rcDocumentList.adapter = adapter
        spDocumentType.setOnItemClickListener { parent: AdapterView<*>?, view: View?, position: Int, id: Long ->
//            if (position != 0) {
                openOptionChooserDialog()
//            }
        }
    }

    fun setListener(attachDocListener: AttachDocListener) {
        this.attachDocListener = attachDocListener
    }

    fun setViewModel(owner: LifecycleOwner, attachDocViewModel: AttachDocViewModel) {
        this.attachDocViewModel = attachDocViewModel
        this.attachDocViewModel.attachedDocLiveData.observe(owner,
            Observer { attachedDocs: ArrayList<AttachedDoc>? ->
                if (attachedDocs != null) {
                    adapter.setDiffNewData(attachedDocs)
                    adapter.notifyDataSetChanged()
                }
            }
        )
        val docTypeAdapter = object: ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line){
            override fun getFilter(): Filter {
                return object : Filter() {
                    override fun performFiltering(constraint: CharSequence?): FilterResults? {
                        return null
                    }

                    override fun publishResults(constraint: CharSequence?,results: FilterResults?) { }
                }
            }
        }
        docTypeAdapter.addAll(attachDocViewModel.getDocList())
        spDocumentType.setAdapter(docTypeAdapter)
    }

    override fun setEnabled(enabled: Boolean) {
        spDocumentType.isEnabled = enabled
        rcDocumentList.isEnabled = enabled
        super.setEnabled(enabled)
    }

    fun setViewOnlyData(attachedDocs: ArrayList<AttachedDoc>?) {
        tilDocumentType.visibility = View.GONE
        if (attachedDocs != null) {
            adapter.setDiffNewData(attachedDocs)
            adapter.notifyDataSetChanged()
        }
    }

    private fun openOptionChooserDialog() {
        val binding: DialogAttachDocChooserBinding = DialogAttachDocChooserBinding.inflate(
            LayoutInflater.from(context)
        )
        if (optionChooserDialog != null) {
            optionChooserDialog!!.dismiss()
        }
        binding.imgSelectFile.setOnClickListener { v ->
            optionChooserDialog!!.setOnDismissListener(null)
            optionChooserDialog!!.dismiss()
            openFileChooserDialogWithPermission()
        }
        binding.imgTakePhoto.setOnClickListener { v ->
            optionChooserDialog!!.setOnDismissListener(null)
            optionChooserDialog!!.dismiss()
            openCameraFragmentWithPermission()
        }
        optionChooserDialog = makeBottomSheet(binding.root).apply {
            show()
        }
        optionChooserDialog!!.setOnDismissListener {
            spDocumentType.setText("", false)
        }
    }

    private fun openFileChooserDialogWithPermission() {
        Dexter.withContext(context)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                    if (!multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                        if (fileChooserDialog != null) {
                            fileChooserDialog!!.setOnDismissListener(null)
                            fileChooserDialog!!.dismiss()
                        }
                        fileChooserDialog =
                            createFileChooserDialog(
                                context,
                                object : FileChooserListener {
                                    override fun onFileSelected(file: File) {
                                        fileChooserDialog!!.setOnDismissListener(null)
                                        val docTypeId = attachDocViewModel.getDocTypeId(spDocumentType.text.toString())
                                        val attachedDoc = AttachedDoc(
                                            docName = spDocumentType.text.toString(),
                                            docKey = docTypeId,
                                            fileAbsolutePath = file.absolutePath
                                        )
                                        openFileConfirmationDialog(attachedDoc)
                                    }
                                }
                            )
                        fileChooserDialog!!.show()
                        fileChooserDialog!!.setOnDismissListener {
                            spDocumentType.setText("", false)
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    list: List<PermissionRequest>,
                    permissionToken: PermissionToken
                ) {
                    permissionToken.continuePermissionRequest()
                }
            })
            .check()
    }

    private fun openFileConfirmationDialog(attachedDoc: AttachedDoc) {
        val binding: DialogDocumentDetailsBinding =
            DialogDocumentDetailsBinding.inflate(LayoutInflater.from(context))
        if (fileConfirmDialog != null) {
            fileConfirmDialog!!.dismiss()
        }
        fileConfirmDialog = createDialog(binding.root)
        fileConfirmDialog!!.cancelable(false)
        binding.txtDocDetail.text = spDocumentType.text.toString()
        if (attachedDoc.referenceNo.isEmpty().not()) {
            binding.edtReferenceNo.setText(attachedDoc.referenceNo)
        }

        if (attachDocViewModel.docsToFillRef.contains(attachedDoc.docName.toLowerCase()) &&
            attachDocViewModel.getRefFillValue().isNullOrEmpty().not()) {
            binding.edtReferenceNo.setText(attachDocViewModel.getRefFillValue())
        }

        binding.btnCancel.setOnClickListener { fileConfirmDialog!!.dismiss() }
        binding.btnProceed.setOnClickListener {
            val referenceNoText: String = binding.edtReferenceNo.text.toString()
            if (referenceNoText.trim().isNotEmpty()) {
                if (attachedDoc.referenceNo.isEmpty().not()) {
                    attachedDoc.referenceNo = referenceNoText
                    adapter.notifyDataSetChanged()
                } else {
                    attachedDoc.referenceNo = referenceNoText
                    attachDocViewModel.addDoc(attachedDoc)
                }
                fileConfirmDialog!!.dismiss()
            } else {
                binding.edtReferenceNo.error = context.getString(R.string.enter_reference_no)
            }
        }
        fileConfirmDialog!!.show()
        fileConfirmDialog!!.setOnDismissListener {
            spDocumentType.setText("", false)
        }
    }

    private fun openCameraFragmentWithPermission() {
        Dexter.withContext(context)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                    if (!multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                        if (attachDocListener != null) {
                            attachDocListener!!.startImageCapture()
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    list: List<PermissionRequest>,
                    permissionToken: PermissionToken
                ) {
                    permissionToken.continuePermissionRequest()
                }
            })
            .check()
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == TakePictureActivity.INTENT_REQUEST_CODE_TAKE_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val extras = data.extras
                    if (extras != null) {
                        val imagePath =
                            extras.getString(TakePictureActivity.INTENT_RESULT_TAKE_PICTURE)

                        val docTypeId = attachDocViewModel.getDocTypeId(spDocumentType.text.toString())
                        val attachedDoc =
                            AttachedDoc(
                                docName = spDocumentType.text.toString(),
                                docKey = docTypeId,
                                fileAbsolutePath = imagePath
                            )
                        openFileConfirmationDialog(attachedDoc)
                    }
                } else {
                    spDocumentType.setText("", false)
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                spDocumentType.setText("", false)
            }
        }
    }

    interface AttachDocListener {
        fun startImageCapture()
    }
}
