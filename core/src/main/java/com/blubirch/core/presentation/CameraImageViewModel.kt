package com.blubirch.core.presentation

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class CameraImageViewModel @Inject constructor(): BaseViewModel() {

    val imageLiveData = MutableLiveData<String>()
}
