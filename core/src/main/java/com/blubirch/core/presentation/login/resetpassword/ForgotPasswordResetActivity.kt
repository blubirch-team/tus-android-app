package com.blubirch.core.presentation.login.resetpassword

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.afollestad.materialdialogs.MaterialDialog
import com.blubirch.core.BuildConfig
import com.blubirch.core.R
import com.blubirch.core.data.MyException.*
import com.blubirch.core.databinding.ActivityForgotPasswordResetBinding
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.customview.listeners.PrettyTextWatcher
import com.blubirch.core.presentation.login.LoginActivity
import com.blubirch.core.utility.AppUtils
import com.github.loadingview.LoadingDialog
import com.github.loadingview.LoadingDialog.Companion.get
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class ForgotPasswordResetActivity : DaggerAppCompatActivity() {

    companion object{
        const val EXTRA_TOKEN : String = "OTP"
        const val EXTRA_EMAIL : String = "EMAIL"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var token : String
    private lateinit var email : String

    private lateinit var mViewModel: ForgotPasswordResetViewModel
    private var snackbar: Snackbar? = null
    private var loadingDialog: LoadingDialog? = null
    private lateinit var binding: ActivityForgotPasswordResetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        token = intent?.extras?.getString(EXTRA_TOKEN) ?: ""
        email = intent?.extras?.getString(EXTRA_EMAIL) ?: ""

        if (token.isEmpty() || email.isEmpty()) {
            finish()
            return
        }

        binding = ActivityForgotPasswordResetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (BuildConfig.DEBUG) {
            binding.edtPassword.setText("blubirch123")
            binding.edtConfirmPassword.setText("blubirch123")
        }

        binding.edtPassword.addTextChangedListener(object : PrettyTextWatcher() {
            override fun afterTextChanged(text: String) {
                binding.edtPassword.error = null
            }
        })

        binding.edtConfirmPassword.addTextChangedListener(object : PrettyTextWatcher() {
            override fun afterTextChanged(text: String) {
                binding.edtConfirmPassword.error = null
            }
        })

        mViewModel = ViewModelProvider(this, viewModelFactory).get(
            ForgotPasswordResetViewModel::class.java
        )

        binding.btnChangePassword.setOnClickListener { v: View? ->
            if (validate()) {
                AppUtils.hideSoftKeyboard(this@ForgotPasswordResetActivity)
                changePassword()
            }
        }

        attachLiveData()
    }

    override fun onBackPressed() {
        Intent(this@ForgotPasswordResetActivity, LoginActivity::class.java).apply {
            startActivity(this)
        }
    }

    private fun changePassword(){
        showLoader()
        mViewModel.changePassword(email, token, binding.edtPassword.text.toString())
    }

    private fun validate(): Boolean {
        binding.edtPassword.let {
            if (TextUtils.isEmpty(it.text)) {
                it.requestFocus()
                it.post {
                    AppUtils.hideSoftKeyboard(this)
                    it.error = "Enter Password"
                }
                return false
            }
        }
        binding.edtConfirmPassword.let {
            if (TextUtils.isEmpty(it.text)) {
                it.requestFocus()
                it.post {
                    AppUtils.hideSoftKeyboard(this)
                    it.error = "Confirm Password"
                }
                return false
            }
        }
        if (binding.edtPassword.text.toString() != binding.edtConfirmPassword.text.toString()) {
            binding.edtConfirmPassword.let {
                it.post {
                    AppUtils.hideSoftKeyboard(this)
                    it.error = "Password does not match"
                }
            }
            return false
        }
        return true
    }

    private fun showSnackBar(text: String?) {
        hideSnackBar()
        text?.let {
            snackbar = Snackbar.make(binding.consMain, it, Snackbar.LENGTH_LONG).apply {
                show()
            }
        }
    }

    private fun hideSnackBar() {
        snackbar?.dismiss()
    }

    private fun attachLiveData() {
        mViewModel.resetResponseLiveData.observe(this, Observer {
            hideLoader()
            MaterialDialog(this@ForgotPasswordResetActivity).apply {
                title(R.string.success)
                message(R.string.password_updated)
                positiveButton(R.string.zxing_button_ok) { run {
                    dismiss()
                    finish()
                    Intent(this@ForgotPasswordResetActivity, LoginActivity::class.java).apply {
                        startActivity(this)
                    }
                } }
                show()
            }
        })

        mViewModel.errorLiveData.observe(this, Observer {
            hideLoader()
            handleError(it)
        })
    }

    private fun showLoader() {
        if (loadingDialog == null) {
            loadingDialog = get(this)
        }
        loadingDialog!!.show()
    }

    private fun hideLoader() {
        if (loadingDialog != null) loadingDialog!!.hide()
    }

    private fun handleError(throwable: Throwable) {
        if (throwable is UnKnownError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is TimeoutErrorError) {
            showSnackBar("Timeout error!")
        } else if (throwable is NetworkErrorError) {
            showSnackBar("Connection error!")
        } else if (throwable is ApiError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is ValidationError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is UnAuthenticate) {
            showSnackBar(throwable.cause!!.message)
        } else {
            Log.d("IAgreeException", throwable.message)
            showSnackBar(throwable.message)
        }
    }
}