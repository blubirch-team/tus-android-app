package com.blubirch.core.presentation.customview.barcode

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.InsetDrawable
import android.hardware.Camera
import android.os.Parcel
import android.os.Parcelable
import android.text.*
import android.text.InputFilter.LengthFilter
import android.text.method.DigitsKeyListener
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.util.Log
import android.util.SparseArray
import android.util.TypedValue
import android.view.KeyEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.blubirch.core.R
import com.blubirch.core.utility.camera.CustomIntentIntegrator
import com.blubirch.core.utility.DimensionsUtils
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.lang.ref.WeakReference


class ScanInputLayoutEditText : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init(attrs, 0)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private lateinit var scanActivity: WeakReference<Activity>
    private lateinit var scanFragment: WeakReference<Fragment>
    var onScan: ((String) -> Unit)? = null

    lateinit var editText: TextInputEditText
        private set
    private lateinit var textInputLayout: TextInputLayout
    private var inputType: Int = 0
    private var drawableIdStart: Int = 0
    private var defaultDrawableIdEnd: Int = R.drawable.ic_barcode
    private var drawableIdEnd: Int = 0
    private var hintText: String? = null
    private var maxLength: Int = 0
    private var digits: CharSequence? = null
    private var isEditable: Boolean = true
    private var displayDrawable: Boolean = true
    private var imeOptions: Int = 0
    private var requestCode: Int = 0
    private lateinit var view: View

    private fun initView() {
        editText = view.findViewById(R.id.editText)
        textInputLayout = view.findViewById(R.id.textInputLayout)
        if (hintText != null) {
            textInputLayout.hint = hintText
        }
        textInputLayout.hintTextColor = ColorStateList.valueOf(Color.BLACK)
        setInputType(inputType)
        setImeOptions(imeOptions)
        setEditable(isEditable)
        setMaxLines(1)
        setMaxLength(maxLength)
        if (digits != null) {
            if (digits.toString() == resources.getString(R.string.name_characters)) {
                editText.inputType = InputType.TYPE_CLASS_TEXT
//                editText.filters =
//                    arrayOf(label@ InputFilter { src: CharSequence, start: Int, end: Int, dst: Spanned?, dstart: Int, dend: Int ->
//                        var i = start
//                        while (i < end) {
//                            if (!src.toString().matches(Regex("[a-zA-Z ]+"))) {
//                                return@label ""
//                            }
//                            i++
//                        }
//                        null
//                    })
            } else {
                try {
                    digits.toString().toInt()
                    editText.keyListener = DigitsKeyListener.getInstance(digits.toString())
                } catch (nfe: NumberFormatException) {
//                    editText.filters =
//                        arrayOf(label@ InputFilter { src: CharSequence, start: Int, end: Int, dst: Spanned?, dstart: Int, dend: Int ->
//                            var i = start
//                            while (i < end) {
//                                if (!digits.toString().contains(src[i].toString())) {
//                                    return@label ""
//                                }
//                                i++
//                            }
//                            null
//                        })
                }
            }
        }
        showDrawableEnd()
        showDrawableStart()
    }

    fun setTypeface(font: Typeface?) {
        editText.typeface = font
        textInputLayout.typeface = font
    }

    fun setMaxLength(maxLength: Int) {
        editText.filters = arrayOf<InputFilter>(LengthFilter(maxLength))
    }

    override fun setBackground(drawable: Drawable) {
//        editText.setBackground(drawable);
    }

    fun setErrorDrawable(resId: Int) {
        textInputLayout.setErrorIconDrawable(resId)
    }

    fun setErrorDrawable(errorDrawable: Drawable?) {
        textInputLayout.errorIconDrawable = errorDrawable
    }

    fun setTextColor(color: Int) {
        editText.setTextColor(color)
    }

    fun setSelection() {
        if (editText.text.toString().isNotEmpty())
            Selection.setSelection(editText.text, editText.text!!.length)
    }

    fun setTextSize(textSize: Float) {
        editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
    }

    fun setFilters(filters: Array<InputFilter?>?) {
        editText.filters = filters
    }

    fun setInputType(inputType: Int) {
        setInputType(inputType, false)
    }

    fun setInputType(inputType: Int, isPassword: Boolean) {
        editText.inputType = inputType
        if (isPassword) editText.transformationMethod = PasswordTransformationMethod.getInstance()
    }

    fun showDrawableEnd() {
        if (displayDrawable) {
            if (drawableIdEnd == 0) {
                drawableIdEnd = defaultDrawableIdEnd
            }
            val drawable = AppCompatResources.getDrawable(context, drawableIdEnd)
            val insetDrawable = InsetDrawable(
                drawable,
                0,
                0,
                DimensionsUtils.dpToPx(context, 15f),
                DimensionsUtils.dpToPx(context, 15f)
            )
            textInputLayout.endIconDrawable = insetDrawable
        }
    }

    fun showDrawableStart() {
        if (displayDrawable && drawableIdStart != 0) {
            val drawable = AppCompatResources.getDrawable(context, drawableIdStart)
            val insetDrawable = InsetDrawable(
                drawable,
                DimensionsUtils.dpToPx(context, -5f),
                0,
                DimensionsUtils.dpToPx(context, -10f),
                DimensionsUtils.dpToPx(context, 15f)
            )
            textInputLayout.startIconDrawable = insetDrawable
        }
    }

    fun setEditable(isEditable: Boolean) {
        editText.isFocusable = isEditable
        editText.isFocusableInTouchMode = isEditable
    }

    fun setPadding(padding: Int) {
        setPadding(padding, padding, padding, padding)
    }

    override fun setPadding(left: Int, top: Int, right: Int, bottom: Int) {
        editText.setPadding(left, top, right, bottom)
    }

    var text: CharSequence?
        get() = editText.text.toString().trim { it <= ' ' }
        set(charSequence) {
            editText.setText(charSequence)
        }

    val isEmpty: Boolean
        get() = text!!.isEmpty()

    fun setErrorEnabled(isErrorEnable: Boolean) {
        textInputLayout.isErrorEnabled = isErrorEnable
    }

    fun setError(errorText: String?) {
        if (isNullOrEmpty(errorText)) {
            textInputLayout.isErrorEnabled = false
            textInputLayout.error = null
        } else {
            textInputLayout.isErrorEnabled = true
            textInputLayout.errorIconDrawable = null
            textInputLayout.error = errorText
        }
    }

    fun setCompoundDrawablesWithIntrinsicBounds(i: Int, i1: Int, i2: Int, i3: Int) {
        editText.setCompoundDrawablesWithIntrinsicBounds(i, i1, i2, i3)
    }

    fun clearText() {
        editText.text!!.clear()
    }

    /**
     * Change the editor type integer associated with the text view, which
     * is reported to an Input Method Editor (IME) with when it has focus.
     */
    fun setImeOptions(imeOptions: Int) {
        editText.imeOptions = imeOptions
    }

    fun setImeActionLabel(label: String?, imeOptions: Int) {
        editText.setImeActionLabel(label, imeOptions)
    }

    override fun setOnClickListener(listener: OnClickListener?) {
        editText.setOnClickListener(listener)
    }

    /**
     * Sets the text to be displayed when the text of the TextView is empty.
     * Null means to use the normal empty text. The hint does not currently
     * participate in determining the size of the view.
     */
    fun setHint(hint: String?) {
        editText.hint = hint
    }

    fun setHintToTextInput(hint: String?) {
        textInputLayout.hint = hint
    }

    fun setHint(hint: SpannableStringBuilder?) {
        editText.hint = hint
    }

    /**
     * Sets the text to be displayed when the text of the TextView is empty,
     * from a resource.
     */
    fun setHint(resid: Int) {
        setHint(context.resources.getString(resid))
    }

    fun setOnEditorActionListener(listener: OnEditorActionListener) {
        editText.setOnEditorActionListener { v: TextView?, actionId: Int, event: KeyEvent? ->
            listener.onEditorAction(v, actionId, event)
            false
        }
    }

    private fun setMaxLines(maxLines: Int) {
        editText.maxLines = maxLines
    }

    fun setGravity(gravity: Int) {
        editText.gravity = gravity
    }

    fun setTextInputFocusable(isFocus: Boolean) {
        editText.isFocusable = isFocus
        editText.isFocusableInTouchMode = isFocus
        textInputLayout.isFocusable = isFocus
        textInputLayout.isFocusableInTouchMode = isFocus
    }

    fun addTextWatcher(textWatcher: TextWatcher?) {
        textWatcher?.let { editText.addTextChangedListener(it) }
    }

    fun removeTextWatcher(textWatcher: TextWatcher?) {
        textWatcher?.let { editText.removeTextChangedListener(it) }
    }

    private fun setScanButtonListener(listener: OnClickListener?) {
        textInputLayout.setEndIconOnClickListener(listener)
    }

    companion object {
        private val TAG = ScanInputLayoutEditText::class.java.simpleName
        fun isNullOrEmpty(s: String?): Boolean {
            //return (s == null) || (s.length() == 0) || (s.equalsIgnoreCase("null"));
            if (s == null) return true
            return if (s.isEmpty()) true else s.equals("null", ignoreCase = true)
        }
    }

    private fun init(attrs: AttributeSet? = null, defStyleAttr: Int = 0) {
        elevation = 8f
        //        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.background));
        view = View.inflate(context, R.layout.widget_input_layout_edit_text, this)
        val styleable = context.obtainStyledAttributes(
            attrs, R.styleable.ScanInputLayoutEditText,
            defStyleAttr, 0
        )
        hintText = styleable.getString(R.styleable.ScanInputLayoutEditText_edt_hint)
        isEditable = styleable.getBoolean(R.styleable.ScanInputLayoutEditText_edt_editable, true)
        maxLength =
            styleable.getInteger(R.styleable.ScanInputLayoutEditText_android_maxLength, 10000)
        imeOptions = styleable.getInt(R.styleable.ScanInputLayoutEditText_android_imeOptions, 0)
        inputType = styleable.getInt(
            R.styleable.ScanInputLayoutEditText_android_inputType,
            EditorInfo.TYPE_CLASS_TEXT
        )
        drawableIdStart =
            styleable.getResourceId(R.styleable.ScanInputLayoutEditText_edt_drawable_start, 0)
        drawableIdEnd =
            styleable.getResourceId(R.styleable.ScanInputLayoutEditText_edt_drawable_end, 0)
        displayDrawable =
            styleable.getBoolean(R.styleable.ScanInputLayoutEditText_edt_show_drawable, false)
        digits = styleable.getText(R.styleable.ScanInputLayoutEditText_edt_digits)
        initView()
        styleable.recycle()
    }

    private fun setListeners() {
        setScanButtonListener(OnClickListener {
            openScanWithPermission()
        })
    }

    fun initialize(activity: Activity, requestCode: Int) {
        this.requestCode = requestCode
        this.scanActivity = WeakReference(activity)
        setListeners()
    }

    fun initialize(fragment: Fragment, requestCode: Int, scanListener: ((String) -> Unit)? = null) {
        this.requestCode = requestCode
        this.scanFragment = WeakReference(fragment)
        this.onScan = scanListener
        setListeners()
    }

    private fun openScanWithPermission() {
        Dexter.withContext(context)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                    if (!multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                        var intentIntegrator: CustomIntentIntegrator? = null
                        if (scanFragment.get() != null) {
                            (scanFragment.get() as Fragment).apply {
                                intentIntegrator = CustomIntentIntegrator.forSupportFragment(this)
                            }
                        } else if (scanActivity.get() != null) {
                            (scanActivity.get() as Activity).apply {
                                intentIntegrator =
                                    CustomIntentIntegrator(
                                        this
                                    )
                            }
                        }
                        intentIntegrator?.run {
                            setBeepEnabled(false)
                            setOrientationLocked(false)
                            setCameraId(Camera.CameraInfo.CAMERA_FACING_BACK)
                            setBarcodeImageEnabled(false)
                            setRequestCode(requestCode)
                            initiateScan()
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    list: List<PermissionRequest>,
                    permissionToken: PermissionToken
                ) {
                    permissionToken.continuePermissionRequest()
                }
            })
            .check()
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result: IntentResult = IntentIntegrator.parseActivityResult(resultCode, data)
        Log.d(TAG, "requestCode:$requestCode resultCode:$resultCode data: $result")
        if (resultCode == Activity.RESULT_OK && requestCode == this.requestCode) {
            data?.extras?.apply {
                editText.setText(result.contents)
                onScan?.let { it(editText.text.toString()) }
            }
        }
    }

    class SavedState(superState: Parcelable?) : View.BaseSavedState(superState) {
        var childrenStates: SparseArray<Any>? = null

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            childrenStates?.let {
                out.writeSparseArray(it)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    public override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val ss = SavedState(superState)
        ss.childrenStates = SparseArray()
        for (i in 0 until childCount) {
            getChildAt(i).saveHierarchyState(ss.childrenStates as SparseArray<Parcelable>)
        }
        return ss
    }

    @Suppress("UNCHECKED_CAST")
    public override fun onRestoreInstanceState(state: Parcelable) {
        if(state is SavedState) {
            super.onRestoreInstanceState(state.superState)
            for (i in 0 until childCount) {
                getChildAt(i).restoreHierarchyState(state.childrenStates as SparseArray<Parcelable>)
            }
        }
    }

    override fun dispatchSaveInstanceState(container: SparseArray<Parcelable>) {
        dispatchFreezeSelfOnly(container)
    }

    override fun dispatchRestoreInstanceState(container: SparseArray<Parcelable>) {
        dispatchThawSelfOnly(container)
    }
}