package com.blubirch.core.presentation.login.resetpassword

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.core.BuildConfig
import com.blubirch.core.data.MyException.*
import com.blubirch.core.databinding.ActivityForgotPasswordEmailBinding
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.customview.listeners.PrettyTextWatcher
import com.blubirch.core.utility.AppUtils
import com.github.loadingview.LoadingDialog
import com.github.loadingview.LoadingDialog.Companion.get
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class ForgotPasswordEmailActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var mViewModel: ForgotPasswordEmailViewModel
    private var snackbar: Snackbar? = null
    private var loadingDialog: LoadingDialog? = null
    private lateinit var binding: ActivityForgotPasswordEmailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordEmailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (BuildConfig.FLAVOR == "dev") {
            binding.edtEmail.setText("warehouse@blubirch.com")
        }

        binding.edtEmail.addTextChangedListener(object : PrettyTextWatcher() {
            override fun afterTextChanged(text: String) {
                binding.edtEmail.error = null
            }
        })

        binding.txtReturnToLogin.setOnClickListener {
            finish()
        }

        mViewModel = ViewModelProvider(this, viewModelFactory).get(
            ForgotPasswordEmailViewModel::class.java
        )

        binding.btnSendOTP.setOnClickListener { v: View? ->
            if (validate()) {
                AppUtils.hideSoftKeyboard(this@ForgotPasswordEmailActivity)
                getOTP()
            }
        }

        attachLiveData()
    }

    private fun getOTP(){
        showLoader()
        mViewModel.getOTP(binding.edtEmail.text.toString().trim { it <= ' ' })
    }

    private fun validate(): Boolean {
        binding.edtEmail.let {
            if (TextUtils.isEmpty(it.text)) {
                it.requestFocus()
                it.post {
                    AppUtils.hideSoftKeyboard(this)
                    it.error = "Enter email"
                }
                return false
            }
            return true
        }
    }

    private fun showSnackBar(text: String?) {
        hideSnackBar()
        text?.let {
            snackbar = Snackbar.make(binding.consMain, it, Snackbar.LENGTH_LONG).apply {
                show()
            }
        }
    }

    private fun hideSnackBar() {
        snackbar?.dismiss()
    }

    private fun attachLiveData() {
        mViewModel.otpLiveData.observe(this, Observer {
            hideLoader()
            Intent(this@ForgotPasswordEmailActivity, ForgotPasswordOTPActivity::class.java).apply {
                putExtra(ForgotPasswordOTPActivity.EXTRA_OTP, mViewModel.otpLiveData.value)
                putExtra(ForgotPasswordOTPActivity.EXTRA_EMAIL, mViewModel.email)
                startActivity(this)
            }
        })

        mViewModel.errorLiveData.observe(this, Observer {
            hideLoader()
            handleError(it)
        })
    }

    private fun showLoader() {
        if (loadingDialog == null) {
            loadingDialog = get(this)
        }
        loadingDialog!!.show()
    }

    private fun hideLoader() {
        if (loadingDialog != null) loadingDialog!!.hide()
    }

    private fun handleError(throwable: Throwable) {
        if (throwable is UnKnownError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is TimeoutErrorError) {
            showSnackBar("Timeout error!")
        } else if (throwable is NetworkErrorError) {
            showSnackBar("Connection error!")
        } else if (throwable is ApiError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is ValidationError) {
            showSnackBar(throwable.cause!!.message)
        } else if (throwable is UnAuthenticate) {
            showSnackBar(throwable.cause!!.message)
        } else {
            Log.d("IAgreeException", throwable.message)
            showSnackBar(throwable.message)
        }
    }
}