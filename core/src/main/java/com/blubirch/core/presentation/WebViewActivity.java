package com.blubirch.core.presentation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blubirch.core.constants.PrefKeys;
import com.blubirch.core.databinding.ActivityWebViewBinding;
import com.blubirch.core.utility.AppPreferences;
import com.blubirch.core.utility.AppUtils;
import com.github.loadingview.LoadingDialog;

import java.util.HashMap;
import java.util.Map;

import dagger.android.support.DaggerAppCompatActivity;
import dagger.android.support.DaggerFragment;

public class WebViewActivity extends DaggerFragment {

    private static final String URL_TO_LOAD = "url_to_load";
    private static final String TITLE_OF_PAGE = "title_of_page";

    private String mUrl;
    private String mTitile;
    private boolean finalUrlCalled;
    private HashMap<String, String> webData = new HashMap<>();
    private LoadingDialog loadingDialog = null;

    private ActivityWebViewBinding binding;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = ActivityWebViewBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        Intent intent = requireActivity().getIntent();
        if (intent != null) {
            mUrl = getArguments().getString(URL_TO_LOAD);
            mTitile = getArguments().getString(TITLE_OF_PAGE);
        }
        ((DaggerAppCompatActivity)requireActivity()).getSupportActionBar().setTitle(mTitile);

        webData.put("url", mUrl);
        setupWebView();
    }

    private void setupWebView() {
        WebSettings webSettings = binding.webViewCommon.getSettings();
        if (Build.VERSION.SDK_INT >= 21) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        binding.webViewCommon.setWebViewClient(new MyWebViewClient());
        Map<String, String> headers = new HashMap<String, String>();
        String access_token = AppPreferences.getString(PrefKeys.HEADER_ACCESS_TOKEN, "0");
        headers.put("accesstoken", access_token);
        long appVersionCode = AppUtils.getAppVersionCode(requireContext());
        headers.put("appversion", appVersionCode + "");
        binding.webViewCommon.loadUrl(mUrl, headers);
        hideOverlay();
    }

    private void hideOverlay() {
        binding.webOverlayView.setVisibility(View.GONE);
    }

    private void showOverlay() {
        binding.webOverlayView.setVisibility(View.VISIBLE);
    }

    private Intent newEmailIntent(String address, String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        return intent;
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("mailto:")) {
                MailTo mt = MailTo.parse(url);
                Intent i = newEmailIntent(mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                startActivity(i);
                view.reload();
                return true;
            }
            if (url.startsWith("tel:")) {
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(tel);
                return true;
            } else {
                finalUrlCalled = false;
            }
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            loading();
            super.onPageStarted(view, url, favicon);
        }


        @Override
        public void onPageFinished(WebView view, final String url) {
            super.onPageFinished(view, url);
            if (!finalUrlCalled) {
                complete();
            }
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

            return super.shouldInterceptRequest(view, url);
        }
    }

    private void loading(){
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog.Companion.get(requireActivity()).show();
        }
    }

    private void complete(){
        if (loadingDialog != null) {
            loadingDialog.hide();
        }
    }
}
