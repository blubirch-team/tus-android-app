package com.blubirch.core.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.blubirch.core.BuildConfig
import com.blubirch.core.data.Either
import com.blubirch.core.data.MyException
import com.blubirch.core.data.api.dto.BaseDTO
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import kotlin.coroutines.CoroutineContext


open class BaseViewModel : ViewModel(), CoroutineScope {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val progressLiveData by lazy { MutableLiveData<Boolean>() }
    val errorLiveData by lazy { MutableLiveData<Exception>() }

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    override val coroutineContext: CoroutineContext
        get() = viewModelJob


    fun <R> postData(
        liveData: MutableLiveData<R>,
        progress: Boolean = true,
        result: suspend () -> Either<MyException, R>
    ) {
        progressLiveData.postValue(false)
        launch {
            if (progress)
                progressLiveData.postValue(true)
            result.invoke().either({
                progressLiveData.postValue(false)
                errorLiveData.postValue(it)
            }, {
                progressLiveData.postValue(false)
                liveData.postValue(it)
            })
        }
    }

    fun <R> postWithOutProgressData(
        observer: MutableLiveData<R>,
        result: suspend () -> Either<MyException, R>
    ) {
        uiScope.launch(Dispatchers.IO) {
            result.invoke().either({
                errorLiveData.postValue(it)
            }, {
                observer.postValue(it)
            })
        }

    }

    fun <R> postNullData(
        observer: MutableLiveData<R>,
        result: suspend () -> Either<MyException, R>
    ) {
        progressLiveData.postValue(false)
        launch {
            progressLiveData.postValue(true)
            result.invoke().either({
                progressLiveData.postValue(false)
                errorLiveData.postValue(it)
                observer.postValue(null)
            }, {
                progressLiveData.postValue(false)
                observer.postValue(it)
            })
        }
    }

    override fun onCleared() {
        viewModelJob.cancel()
        uiScope.coroutineContext.cancelChildren()
        compositeDisposable.clear()
        super.onCleared()
    }

    private fun getAPIError(throwable: Throwable): MyException {
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace()
        }
        when (throwable) {
            is HttpException -> {
                throwable.apply {
                    var bodyMsg = response()?.errorBody()?.string()
                    if (bodyMsg == null)
                        bodyMsg = ""
                    val error = Throwable(bodyMsg, throwable)
                    return when (code()) {
                        401 -> {
                            MyException.UnAuthenticate(error).apply { code = code() }
                        }
                        else ->
                            MyException.ApiError(error).apply {
                                code = code()
                            }
                    }
                }
            }
            is SocketTimeoutException -> {
                return MyException.TimeoutErrorError(throwable)
            }
            is IOException -> {
                return MyException.NetworkErrorError(throwable)
            }
            else -> {
                return MyException.UnKnownError(throwable)
            }
        }
    }

    fun postAPIErrorIfAny(baseDTO: BaseDTO?) : Boolean{
        baseDTO?.let {
            if(it.status != 0){
                if(it.status != 200){
                    if (it.message.isEmpty().not()) {
                        postAPIError(MyException.ApiError(Throwable(it.message)))
                        return true
                    }else if(it.error.isEmpty().not()){
                        postAPIError(MyException.ApiError(Throwable(it.error)))
                        return true
                    }
                }
            }
        }
        return false
    }

    fun postAPIError(throwable: Throwable) {
        progressLiveData.postValue(false)
        val apiError = getAPIError(throwable)
        errorLiveData.postValue(apiError)
    }
}