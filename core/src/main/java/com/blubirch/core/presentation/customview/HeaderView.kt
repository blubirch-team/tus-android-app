package com.blubirch.core.presentation.customview

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import com.blubirch.core.R
import com.blubirch.core.utility.dp2Pixel
import com.blubirch.core.utility.visible

/**
 * TODO: document your custom view class.
 */
class HeaderView : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init(attrs, 0)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    var label: String? = ""
        set(value) {
            field = value
            findViewById<TextView>(R.id.tvTitle)?.text = value ?: "-"
        }

    var isInfoIcon: Boolean = false
        set(value) {
            field = value
            findViewById<AppCompatImageView>(R.id.ivInfo)?.visible(value)

        }
    var dividerColor: Int = Color.BLACK
        set(value) {
            field = value
            findViewById<View>(R.id.dividerInWarding).setBackgroundColor(value)
        }

    private fun init(attrs: AttributeSet? = null, defStyleAttr: Int = 0) {
//        setPadding(context.dp2Pixel(4).toInt())
        View.inflate(context, R.layout.header_view, this)
        val typedArray: TypedArray? = context.obtainStyledAttributes(
            attrs,
            R.styleable.LabelValueView,
            defStyleAttr, 0
        )
        label = typedArray?.getString(R.styleable.LabelValueView_label) ?: ""
        dividerColor = typedArray?.getColor(
            R.styleable.LabelValueView_lineColor,
            ContextCompat.getColor(getContext(), R.color.sdkDividerColor)
        ) ?: ContextCompat.getColor(getContext(), R.color.sdkDividerColor)
        isInfoIcon = typedArray?.getBoolean(R.styleable.LabelValueView_infoIcon, false) ?: false
        typedArray?.getResourceId(
            R.styleable.LabelValueView_infoIconDrawable,
            R.drawable.ic_info_outline
        )?.apply {
            findViewById<ImageView>(R.id.ivInfo).setImageResource(this)
        }
        findViewById<TextView>(R.id.tvTitle)?.text = label
        typedArray?.recycle()
    }

    fun setIconClickListener(onClickListener: OnClickListener) {
        findViewById<AppCompatImageView>(R.id.ivInfo)?.setOnClickListener(onClickListener)
    }

    fun setIcon(@DrawableRes int: Int) {
        findViewById<AppCompatImageView>(R.id.ivInfo).visible()
        findViewById<AppCompatImageView>(R.id.ivInfo)?.setImageResource(int)
    }
}