package com.blubirch.core.presentation.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.ViewModelProvider;

import com.blubirch.core.BuildConfig;
import com.blubirch.core.R;
import com.blubirch.core.constants.PrefKeys;
import com.blubirch.core.constants.Roles;
import com.blubirch.core.data.MyException;
import com.blubirch.core.databinding.ActivityLoginBinding;
import com.blubirch.core.di.ViewModelFactory;
import com.blubirch.core.presentation.customview.listeners.PrettyTextWatcher;
import com.blubirch.core.presentation.login.resetpassword.ForgotPasswordEmailActivity;
import com.blubirch.core.utility.AppPreferences;
import com.blubirch.core.utility.AppUtils;
import com.github.loadingview.LoadingDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class LoginActivity extends DaggerAppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;

    @Inject
    LoginRedirection loginRedirection;

    private LoginViewModel mViewModel;
    private Snackbar snackbar = null;
    private LoadingDialog loadingDialog = null;
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        if (BuildConfig.FLAVOR.equals("tataunistoreDev")) {
            binding.edtEmail.setText(R.string.user_name);
            binding.edtPassword.setText(R.string.pwd);
        }

        binding.edtEmail.addTextChangedListener(new PrettyTextWatcher() {
            @Override
            public void afterTextChanged(String text) {
                binding.tilEmail.setError(null);
            }
        });

        binding.edtPassword.addTextChangedListener(new PrettyTextWatcher() {
            @Override
            public void afterTextChanged(String text) {
                binding.tilPassword.setError(null);
            }
        });

        binding.txtForgotPassword.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, ForgotPasswordEmailActivity.class);
            startActivity(intent);
        });

        if (!AppPreferences.getString(PrefKeys.HEADER_ACCESS_TOKEN, "").isEmpty()) {
            afterLogin();
        }

        mViewModel = new ViewModelProvider(this, viewModelFactory).get(LoginViewModel.class);

        findViewById(R.id.btnSignIn).setOnClickListener(v -> {

            if (validate()) {
                AppUtils.hideSoftKeyboard(this);
                callLogin();
            }
        });
        attachLiveData();
    }

    private void callLogin() {
        showLoader();
        mViewModel.login(binding.edtEmail.getText().toString().trim(), binding.edtPassword.getText().toString().trim());
    }

    private boolean validate() {
        if (TextUtils.isEmpty(binding.edtEmail.getText())) {
            binding.edtEmail.requestFocus();
            binding.edtEmail.post(() -> {
                binding.tilEmail.setError("Enter Email / UserName");
                binding.tilEmail.setErrorIconDrawable(null);
            });
            return false;
        } else if (TextUtils.isEmpty(binding.edtPassword.getText())) {
            binding.edtPassword.requestFocus();
            binding.edtPassword.post(() -> {
                binding.tilPassword.setError("Enter password");
                binding.tilPassword.setErrorIconDrawable(null);
            });
            return false;
        }
        return true;
    }


    private void afterLogin() {
        loginRedirection.open(this);
//        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    private void showSnackBar(String text) {
        hideSnackBar();
        snackbar = Snackbar.make(findViewById(R.id.loginView), text, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void hideSnackBar() {
        if (snackbar != null)
            snackbar.dismiss();
    }

    private void attachLiveData() {
        mViewModel.userLiveData().observe(this, user -> {
            hideLoader();
            Log.d("Login", "user: " + user);
            boolean loggedIn = false;
            if (user != null) {
                ArrayList<String> roles = user.getRoles();
                if (roles != null && !roles.isEmpty()) {
                    for (String role : roles) {
                        if (Roles.WMS_USER.equalsIgnoreCase(role)) {
                            loggedIn = true;
                            AppPreferences.putString(PrefKeys.USER_NAME_KEY, user.getFirstName() + " " + user.getLastName());
                            AppPreferences.putString(PrefKeys.USER_EMAIL_KEY, user.getEmail());
                            AppPreferences.putString(PrefKeys.USER_TITLE_KEY, user.getFirstName().substring(0,1) + user.getLastName().substring(0,1));
                            afterLogin();
                            break;
                        }
                    }
                }
                if (!loggedIn) {
                    AppPreferences.removePreference(PrefKeys.HEADER_ACCESS_TOKEN);
                    showSnackBar("UnAuthorized access. Provide correct credentials.");
                }
            }
        });
        mViewModel.getErrorLiveData().observe(this, exception -> {
            hideLoader();
            handleError(exception);
        });
    }

    private void showLoader() {
        if (loadingDialog == null)
            loadingDialog = LoadingDialog.Companion.get(this);
        loadingDialog.show();
    }

    private void hideLoader() {
        if (loadingDialog != null)
            loadingDialog.hide();
    }

    private void handleError(Throwable throwable) {
        if (throwable instanceof MyException.UnKnownError) {
            showSnackBar(throwable.getCause().getMessage());
//                    materialDialog(it.throwable.localizedMessage.orEmpty())
        } else if (throwable instanceof MyException.TimeoutErrorError) {
//                            Toast.makeText(activity, "Timeout error!", Toast.LENGTH_SHORT)
//                                .show()
            showSnackBar("Timeout error!");
        } else if (throwable instanceof MyException.NetworkErrorError) {
//                            Toast.makeText(activity, "connection error!", Toast.LENGTH_SHORT)
//                                .show()
            showSnackBar("Connection error!");
        } else if (throwable instanceof MyException.ApiError) {
//                    Log.d("ApiError", it.throwable.localizedMessage)
            showSnackBar(throwable.getCause().getMessage());
        } else if (throwable instanceof MyException.ValidationError) {
            showSnackBar(throwable.getCause().getMessage());
        } else if (throwable instanceof MyException.UnAuthenticate) {
            showSnackBar(throwable.getCause().getMessage());
        } else {
            Log.d("IAgreeException", throwable.getMessage());
            showSnackBar(throwable.getMessage());
//                            materialDialog(it.throwable.localizedMessage.orEmpty())
        }
    }

}
