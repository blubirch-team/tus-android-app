package com.blubirch.core.presentation.login;

import android.app.Activity;

public interface LoginRedirection {

    void open(Activity activity);
}
