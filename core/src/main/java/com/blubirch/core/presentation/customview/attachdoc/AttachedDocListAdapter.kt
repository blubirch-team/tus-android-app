package com.blubirch.core.presentation.customview.attachdoc

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.core.R
import com.blubirch.core.databinding.ItemAttachedDocListBinding
import com.blubirch.core.utility.inVisible
import com.blubirch.core.utility.visible
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class AttachedDocListAdapter constructor(private val docActionListener: DocActionListener) :
    BaseQuickAdapter<AttachedDoc, BaseViewHolder>(R.layout.item_attached_doc_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<AttachedDoc>() {
            override fun areItemsTheSame(oldItem: AttachedDoc, newItem: AttachedDoc): Boolean {
                return false
            }

            override fun areContentsTheSame(oldItem: AttachedDoc, newItem: AttachedDoc): Boolean {
                return oldItem.docName == newItem.docName && oldItem.referenceNo == newItem.referenceNo &&
                        oldItem.fileAbsolutePath == newItem.fileAbsolutePath
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: AttachedDoc) {
        val binding = ItemAttachedDocListBinding.bind(holder.itemView)
        binding.txtDocDetail.text = item.docName + ", " + item.referenceNo

        if (item.isSyncedWithServer.not()) {
            binding.imgEditDoc.setOnClickListener {
                if (recyclerView.isEnabled) {
                    docActionListener.onEdit(item, getItemPosition(item))
                }
            }
            binding.imgDeleteDoc.setOnClickListener {
                if (recyclerView.isEnabled) {
                    docActionListener.onDelete(item, getItemPosition(item))
                }
            }
            binding.imgDeleteDoc.visible()
            binding.imgEditDoc.visible()
        } else {
            binding.imgDeleteDoc.setOnClickListener(null)
            binding.imgEditDoc.setOnClickListener(null)
            binding.imgDeleteDoc.inVisible()
            binding.imgEditDoc.inVisible()
        }
    }

    interface DocActionListener {
        fun onEdit(attachedDoc: AttachedDoc, position: Int)

        fun onDelete(attachedDoc: AttachedDoc, position: Int)
    }
}