package com.blubirch.core.presentation

import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.blubirch.core.R
import com.blubirch.core.constants.PrefKeys
import com.blubirch.core.data.MyException
import com.blubirch.core.databinding.DialogBarcodePrintViewBinding
import com.blubirch.core.di.customview.DaggerBottomSheetDialogFragment
import com.blubirch.core.presentation.login.LoginActivity
import com.blubirch.core.utility.*
import com.github.loadingview.LoadingDialog
import com.google.android.material.snackbar.Snackbar


abstract class BaseBottomSheetDialogFragment<T : BaseViewModel> :
    DaggerBottomSheetDialogFragment() {
    private lateinit var baseViewModel: T

    abstract fun getViewModel(): T

    abstract fun attachLiveData()

    private var loadingDialog: LoadingDialog? = null
    lateinit var navController: NavController


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
    }

    override fun onResume() {
        super.onResume()
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).let {
                it.supportActionBar!!.elevation = 8f
            }
        }

        baseViewModel = getViewModel()
//        setting false will dismiss previous loaders started from onViewCreated()
//        baseViewModel.progressLiveData.value = false
        attachLiveData()

        baseViewModel.progressLiveData.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (loadingDialog == null)
                    loadingDialog = LoadingDialog[requireActivity()].show()
                else
                    loadingDialog?.show()
            } else {
                loadingDialog?.hide()
            }

        })

        baseViewModel.errorLiveData.observe(this, Observer {
            baseViewModel.progressLiveData.value = false
            loadingDialog?.hide()
            it?.apply {
                if (it is MyException) {
                    when (it) {
                        is MyException.UnKnownError -> {
                            showSnackBar(it.throwable.cause?.message.orEmpty())
                        }
                        is MyException.TimeoutErrorError -> {
                            showSnackBar("Timeout error!")
                        }
                        is MyException.NetworkErrorError -> {
                            showSnackBar("Connection error!")
                        }
                        is MyException.ApiError -> {
                            it.cause!!.message?.let { it1 -> showSnackBar(it1) }
                        }

                        is MyException.UnAuthenticate -> {
                            showToast("Token Expired.")
                            AppPreferences.removePreference(PrefKeys.HEADER_ACCESS_TOKEN)
                            requireActivity().finish()
                            startActivity(Intent(requireActivity(), LoginActivity::class.java))
                        }
                        is MyException.ValidationError -> {
                            showSnackBar(it.cause?.localizedMessage.orEmpty())
                        }
                        else -> {
                            Log.d("IAgreeException", it.cause?.localizedMessage.orEmpty())
                        }
                    }
                } else {
                    Log.e("Error", it.localizedMessage, it)
                }
                baseViewModel.errorLiveData.value = null
            }
        })
    }

    override fun onPause() {
        baseViewModel.errorLiveData.value = null
        baseViewModel.progressLiveData.value = false
        AppUtils.hideSoftKeyboard(requireActivity())
        if (!baseViewModel.compositeDisposable.isDisposed) {
            baseViewModel.compositeDisposable.clear()
        }
        super.onPause()
    }

    fun getNormalizedMessage(e: Exception?): String {
        if (e != null) {
            if (e is MyException) {
                return e.msg
            } else if (e.message != null) {
                return e.message!!
            }
        }
        return resources.getString(R.string.unknown_error)
    }

    fun showSnackBar(text: String, duration: Int = Snackbar.LENGTH_SHORT, view: View? = null) {
        (requireActivity() as BaseActivity<*>).showSnackBar(text, duration, view)
    }

    fun showSnackBar(text: String, view: View? = null) {
        (requireActivity() as BaseActivity<*>).showSnackBar(text, Snackbar.LENGTH_SHORT, view)
    }

    fun hideSnackBar() {
        (requireActivity() as BaseActivity<*>).hideSnackBar()
    }

    fun showToast(text: String) {
        (requireActivity() as BaseActivity<*>).showToast(text)
    }

    fun showToastLonger(text: String) {
        (requireActivity() as BaseActivity<*>).showToastLonger(text)
    }

    fun hideToast() {
        (requireActivity() as BaseActivity<*>).hideToast()
    }


    var printTagDialog: Dialog? = null
    protected fun showPrintTagView(barCodeText: String) {
        if (printTagDialog != null && printTagDialog!!.isShowing)
            return
        val binding: DialogBarcodePrintViewBinding =
            DialogBarcodePrintViewBinding.inflate(LayoutInflater.from(requireActivity()))
        printTagDialog = KotlinJavaDelegation.makeBottomSheet(binding.root)
        printTagDialog!!.setOnShowListener {
            binding.tvBarcode.text = barCodeText
            val bitmap = generateBarcodeImage(
                barCodeText, binding.ivBarcode.width, binding.ivBarcode.height
            )
            binding.ivBarcode.setImageBitmap(bitmap)
            binding.btnCancel.setOnClickListener {
                printTagDialog!!.dismiss()
            }
            binding.btnPrintBarcode.setOnClickListener {
                val bitmap = loadBitmapFromView(binding.bracodeLayout)
                printBarcode(bitmap, barCodeText)
            }
        }
        printTagDialog!!.show()

    }

    private fun printBarcode(bitmap: Bitmap, barcode: String) {
        doPhotoPrint(requireActivity(), bitmap, barcode)
    }

    fun navigate(navDirections: NavDirections) {
        try {
            findNavController().navigate(navDirections)
        } catch (exception: Exception) {
        }
    }

}
