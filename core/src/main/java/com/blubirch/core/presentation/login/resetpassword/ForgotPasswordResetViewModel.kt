package com.blubirch.core.presentation.login.resetpassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.core.data.api.dto.BaseDTO
import com.blubirch.core.data.repository.ForgotPasswordRepo
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ForgotPasswordResetViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var forgotPasswordRepo: ForgotPasswordRepo

    val resetResponseLiveData : LiveData<String> by lazy {
        MutableLiveData<String>()
    }

    fun changePassword(email: String, token: String, password: String) {
        compositeDisposable.add(
            forgotPasswordRepo.resetPassword(email, token, password)
                .subscribeOn(Schedulers.io())
                .subscribe { baseDTO: BaseDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }

                    baseDTO?.let {
                        if (postAPIErrorIfAny(it).not()) {
                            (resetResponseLiveData as MutableLiveData).postValue(it.message)
                        }
                    }
                }
        )
    }

}