package com.blubirch.core.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.blubirch.core.R
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_about.*

open class AboutUsFragment : DaggerFragment() {

    val URL_PRIVACY_POLICY = "file:///android_asset/pp.htm"
    val URL_TERMS_CONDITIONS = "file:///android_asset/tnc.htm"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        txtPrivacyPolicy.setOnClickListener {
            findNavController().navigate(AboutUsFragmentDirections
                .actionAboutUsFragmentToWebviewFragment(
                    URL_PRIVACY_POLICY,resources.getString(R.string.privacy_policy)))
        }

        txtEndUserAgreement.setOnClickListener {
            findNavController().navigate(AboutUsFragmentDirections
                .actionAboutUsFragmentToWebviewFragment(
                    URL_TERMS_CONDITIONS,resources.getString(R.string.end_user_agreement)))
        }
    }


}
