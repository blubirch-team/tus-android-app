package com.blubirch.tus.testing.ui.annotation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject

class ImageConfirmationViewModel @Inject constructor(): BaseViewModel() {

    val imageLiveData : LiveData<String> = MutableLiveData<String>()

    var imageUrl : String?
        get() = imageLiveData.value
        set(value) = (imageLiveData as MutableLiveData).postValue(value)
}
