package com.blubirch.tus.testing.data.api.dto

import com.blubirch.tus.testing.data.models.TestRule
import com.google.gson.annotations.SerializedName

class TestRuleResponseDTO(
    var id: Long,

    @SerializedName("test_rule")
    var testRule: TestRule
)
