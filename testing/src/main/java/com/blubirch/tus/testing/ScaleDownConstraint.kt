package com.blubirch.tus.testing

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import id.zelory.compressor.constraint.Compression
import id.zelory.compressor.constraint.Constraint
import id.zelory.compressor.loadBitmap
import id.zelory.compressor.overWrite
import java.io.File


class ScaleDownConstraint(private val maxWidth: Float, private val maxHeight : Float) : Constraint {

    override fun isSatisfied(imageFile: File): Boolean {
        return BitmapFactory.Options().run {
            inJustDecodeBounds = true
            BitmapFactory.decodeFile(imageFile.absolutePath, this)
            this.outWidth <= maxWidth && this.outHeight <= maxHeight
        }
    }

    override fun satisfy(imageFile: File): File {
        loadBitmap(imageFile).run {
            scaleDown(this, maxWidth, maxHeight).run {
                return overWrite(imageFile, this)
            }
        }
    }
}

fun scaleDown(bitmap: Bitmap, maxWidth: Float, maxHeight: Float): Bitmap {
    val ratio = Math.min(
        maxWidth / bitmap.width,
        maxHeight / bitmap.height
    )
    if (ratio >= 1) {
        return bitmap
    }
    println("ScaleDownConstraint width :  " + bitmap.width + ", height : " + bitmap.height)
    val width = Math.round(ratio * bitmap.width)
    val height = Math.round(ratio * bitmap.height)
    val scaledBitmap = Bitmap.createScaledBitmap(
        bitmap, width,
        height, false
    )
    try {
        if (scaledBitmap != bitmap && bitmap.isRecycled.not()) {
            bitmap.recycle()
        }
    }catch (ex: Exception){}
    return scaledBitmap
}

fun Compression.scaleDownConstraint(maxWidth: Int, maxHeight : Int) {
    constraint(ScaleDownConstraint(maxWidth.toFloat(), maxHeight.toFloat()))
}