package com.blubirch.tus.testing.di;

import androidx.lifecycle.ViewModel;

import com.blubirch.tus.testing.ui.TestActivity;
import com.blubirch.tus.testing.ui.TestViewModel;
import com.blubirch.tus.testing.ui.annotation.AnnotationActivity;
import com.blubirch.tus.testing.ui.annotation.AnnotationViewModel;
import com.blubirch.core.di.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class TestModule {

    @ContributesAndroidInjector(modules = TestActivityModule.class)
    abstract TestActivity bindTestActivity();

    @ContributesAndroidInjector(modules = AnnotationActivityModule.class)
    abstract AnnotationActivity bindAnnotationActivity();

    @Binds
    @IntoMap
    @ViewModelKey(TestViewModel.class)
    abstract ViewModel bindTestViewModel(TestViewModel testViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(AnnotationViewModel.class)
    abstract ViewModel bindAnnotationViewModel(AnnotationViewModel annotationViewModel);
}
