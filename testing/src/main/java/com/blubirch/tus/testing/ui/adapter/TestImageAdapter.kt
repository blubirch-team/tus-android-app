package com.blubirch.tus.testing.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.core.utility.gone
import com.blubirch.core.utility.visible
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.databinding.ItemTestImageBinding
import com.blubirch.tus.testing.data.models.ImageHolder
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import java.io.File
import java.util.*

class TestImageAdapter(
    private val clickListener: ((ImageHolder) -> Unit)

) :
    BaseQuickAdapter<ImageHolder, BaseViewHolder>(R.layout.item_test_image) {

    init {
        setDiffCallback(ImageDiffCallBack())
    }

    var removeListener: ((ImageHolder) -> Unit)? = null

    private var isViewOnly = false

    fun setViewOnly(viewOnly: Boolean) {
        isViewOnly = viewOnly
    }

    override fun convert(holder: BaseViewHolder, item: ImageHolder) {
        val binding = ItemTestImageBinding.bind(holder.itemView)
        if (item.imageSrc.startsWith("http")) {
            Glide.with(binding.image)
                .load(item.imageSrc)
                .centerInside()
                .into(binding.image)
        } else {
            Glide.with(binding.image)
                .load(File(item.imageSrc))
                .centerInside()
                .into(binding.image)
        }

        binding.tvSide.text = item.side
        if (isViewOnly) {
            binding.ivRemove.gone()
        } else {
            binding.ivRemove.visible()
            binding.ivRemove.setOnClickListener {
                removeListener?.invoke(item)
            }
        }
        holder.itemView.setOnClickListener {
            clickListener(item)
        }
    }

    fun setImages(imageHolders: ArrayList<ImageHolder>) {
        setDiffNewData(imageHolders.toMutableList())
        notifyItemRangeChanged(0, imageHolders.size)
    }
}

class ImageDiffCallBack : DiffUtil.ItemCallback<ImageHolder>() {
    override fun areItemsTheSame(oldItem: ImageHolder, newItem: ImageHolder): Boolean {
        return oldItem.imageSrc == newItem.imageSrc
    }

    override fun areContentsTheSame(oldItem: ImageHolder, newItem: ImageHolder): Boolean {
        return oldItem.imageSrc == newItem.imageSrc && oldItem.side == newItem.side
    }

}