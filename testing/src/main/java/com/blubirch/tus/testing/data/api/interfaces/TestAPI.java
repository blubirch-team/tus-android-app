package com.blubirch.tus.testing.data.api.interfaces;


import com.blubirch.tus.testing.data.api.EndPoint;
import com.blubirch.tus.testing.data.api.dto.GradeDispositionRequestDTO;
import com.blubirch.tus.testing.data.api.dto.InwardGradeDispositionRequestDTO;
import com.blubirch.tus.testing.data.api.dto.ReGradeInventoryDTO;
import com.blubirch.tus.testing.data.api.dto.ReturnReasonDTO;
import com.blubirch.tus.testing.data.api.dto.SaveTestedReturnDTO;
import com.blubirch.tus.testing.data.api.dto.TagDTO;
import com.blubirch.tus.testing.data.api.dto.TestResultResponseDTO;
import com.blubirch.tus.testing.data.api.dto.UploadRequestDTO;
import com.blubirch.tus.testing.data.api.dto.UploadResponseDTO;
import com.blubirch.tus.testing.data.models.TestRule;
import com.blubirch.core.data.api.dto.BaseDTO;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface TestAPI {

    @GET(EndPoint.URL_TEST_FETCH_TEST_RULE)
    Single<TestRule> fetchTestRule(@Query(value = "id", encoded = true) long categoryId, @Query(value = "grading_type") String gradingType);

    @GET(EndPoint.URL_TEST_FETCH_TEST_RULE)
    Single<TestRule> fetchTestRule(@Query(value = "id", encoded = true) long categoryId);

    @GET(EndPoint.URL_GET_RETURN_REASON)
    Single<ReturnReasonDTO> getReturnReason(/*@Query(value = "own_label") boolean ownLabel*/);

    @POST(EndPoint.URL_TEST_UPLOAD_IMAGE)
    Single<UploadResponseDTO> uploadImage(@Body UploadRequestDTO uploadRequestDTO);

    @POST(EndPoint.URL_CALCULATE_GRADE_DISPOSITION_INWARD)
    Single<TestResultResponseDTO> calculateGradeDispositionInward(@Body InwardGradeDispositionRequestDTO inwardGradeDispositionRequestDTO);

    @POST(EndPoint.URL_CALCULATE_GRADE_DISPOSITION)
    Single<TestResultResponseDTO> calculateGradeDisposition(@Body GradeDispositionRequestDTO gradeDispositionRequestDTO);

    @GET(EndPoint.GENERATE_TAG)
    Single<TagDTO> generateTag();

    @Multipart
    @POST(EndPoint.SAVE_TESTED_RETURN)
    Single<SaveTestedReturnDTO> saveTestedReturn(@PartMap Map<String, RequestBody> body, @Part List<MultipartBody.Part> documentsPart);

    @POST(EndPoint.SAVE_REGRADING)
    Single<BaseDTO> storeInventoryGrade(@Body ReGradeInventoryDTO reGradeInventoryDTO);
}
