package com.blubirch.tus.testing.ui.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blubirch.tus.testing.R;
import com.blubirch.core.utility.DimensionsUtils;

import java.util.ArrayList;

public class DrawOverlay extends View {

    public static final String TAG = "DrawOverlay";

    private int viewWidth = 0;
    private int viewHeight = 0;
    private int imageWidth = 0;
    private int imageHeight = 0;

    private final float boxSizePercentage = 0.30f;

    private Paint borderPaint;
    private Paint numBoxPaint;
    private Rect textBounds;
    private TextPaint textPaint;

    private Paint[] imposeBorderPaint;

    private MutableLiveData<ArrayList<Coordinate>> drawnBoxes = new MutableLiveData<>(new ArrayList<Coordinate>());

    public DrawOverlay(Context context) {
        super(context);
        init(null);
    }

    public DrawOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public DrawOverlay(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.DrawOverlay);
        int colorsId = ta.getResourceId(R.styleable.DrawOverlay_colorList, 0);
        if (colorsId != 0) {
            TypedArray annotationColors = ta.getResources().obtainTypedArray(colorsId);
            int totalColors = annotationColors.length();
            imposeBorderPaint = new Paint[totalColors];
            for (int i = 0; i < totalColors; i++) {
                imposeBorderPaint[i] = new Paint();
                imposeBorderPaint[i].setColor(annotationColors.getColor(i, 0));
                imposeBorderPaint[i].setStrokeWidth(DimensionsUtils.dpToPx(1.5f));
                imposeBorderPaint[i].setStyle(Paint.Style.STROKE);
            }
            annotationColors.recycle();
        }

        borderPaint = new Paint();
        borderPaint.setColor(Color.BLUE);
        borderPaint.setStrokeWidth(DimensionsUtils.dpToPx(1.5f));
        borderPaint.setStyle(Paint.Style.STROKE);

        numBoxPaint = new Paint();
        numBoxPaint.setColor(Color.WHITE);
        numBoxPaint.setStyle(Paint.Style.FILL);

        textBounds = new Rect();

        textPaint = new TextPaint();
        textPaint.setColor(Color.BLUE);
        textPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT);
        if (!drawnBoxes.getValue().isEmpty()) {
            float sx, sy, ex, ey, rectSize, fontCX, fontCY;
            int index = 0;
            for (Coordinate drawnBox : drawnBoxes.getValue()) {
                sx = drawnBox.getViewSX();
                sy = drawnBox.getViewSY();
                ex = drawnBox.getViewEX();
                ey = drawnBox.getViewEY();

                rectSize = (ex - sx) / 5;
                textPaint.setTextSize(rectSize * 0.8f);
                canvas.drawRect(sx, sy, ex, ey, borderPaint);
                canvas.drawRect(sx, sy, sx + rectSize, sy + rectSize, numBoxPaint);
                String text = String.valueOf(++index);
                textPaint.getTextBounds(text, 0, text.length(), textBounds);
                fontCX = (sx + (rectSize / 2)) - ((float) textBounds.width() / 2);
                fontCY = (sy + (rectSize / 2)) + ((float) textBounds.height() / 2);

                canvas.drawText(text, fontCX, fontCY, textPaint);
            }
        }
    }

//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//        viewWidth = w;
//        viewHeight = h;
//        if (drawnBoxes.getValue().isEmpty()) {
//            for (Coordinate drawnBox : drawnBoxes.getValue()) {
//                drawnBox.viewSizeChanged(viewWidth, viewHeight);
//            }
//            invalidate();
//        }
//    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (isClickable()) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_UP:
                    float touchX = event.getX();
                    float touchY = event.getY();
                    if (drawnBoxes.getValue().isEmpty() ||
                            drawnBoxes.getValue().get(drawnBoxes.getValue().size() - 1).getDefectType() != null) {
                        Coordinate coordinate = new Coordinate.Builder()
                                .setViewWidth(viewWidth)
                                .setViewHeight(viewHeight)
                                .setBoxSizePercentage(boxSizePercentage)
                                .setTouchX(touchX)
                                .setImageWidth(imageWidth)
                                .setImageHeight(imageHeight)
                                .setTouchY(touchY).build();

                        addCoordinate(coordinate);
                        break;
                    }
            }

            invalidate();
        }
        return true;
    }

    private void addCoordinate(Coordinate coordinate) {
        drawnBoxes.getValue().add(coordinate);
    }

    public void setHeightWidth(int width, int height) {
        viewWidth = width;
        viewHeight = height;
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;
        setLayoutParams(layoutParams);
    }

    public void setAnnotation(ArrayList<Coordinate> coordinateList) {
        drawnBoxes.setValue(coordinateList);
        setClickable(false);
        invalidate();
    }

    public LiveData<ArrayList<Coordinate>> getDrawnBoxes() {
        return drawnBoxes;
    }

    public void removeItem(int position) {
        ArrayList<Coordinate> list = drawnBoxes.getValue();
        if (!list.isEmpty()) {
            list.remove(position);
        }
        drawnBoxes.postValue(list);
        invalidate();
    }

    public void removeLastItem() {
        ArrayList<Coordinate> list = drawnBoxes.getValue();
        if (!list.isEmpty()) {
            list.remove(list.size() - 1);
        }
        drawnBoxes.postValue(list);
        invalidate();
    }

    public void removeItem(Coordinate coordinate) {
        ArrayList<Coordinate> list = drawnBoxes.getValue();
        list.remove(coordinate);
        drawnBoxes.postValue(list);
        invalidate();
    }

    public void setDefect(String defect) {
        drawnBoxes.getValue().get(drawnBoxes.getValue().size() - 1).setDefect(defect);
    }

    public void update() {
        drawnBoxes.postValue(drawnBoxes.getValue());
    }

    public void setImageSize(int width, int height) {
        imageWidth = width;
        imageHeight = height;
    }

    public Bitmap getColoredBitmap(String imagePath) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inMutable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath,bmOptions);

        Canvas canvas = new Canvas(bitmap);

        if (!drawnBoxes.getValue().isEmpty()) {
            float sx, sy, ex, ey, rectSize, fontCX, fontCY;
            int index = 0;
            for (Coordinate drawnBox : drawnBoxes.getValue()) {
                sx = (drawnBox.getViewSX() / viewWidth) * bitmap.getWidth();
                sy = (drawnBox.getViewSY() / viewHeight) * bitmap.getHeight();
                ex = ((drawnBox.getViewEX() - drawnBox.getViewSX()) / viewWidth) * bitmap.getWidth();
                ey = ((drawnBox.getViewEY() - drawnBox.getViewSY()) / viewHeight) * bitmap.getHeight();
                ex += sx;
                ey += sy;

                rectSize = (ex - sx) / 5;
                textPaint.setTextSize(rectSize * 0.8f);

                int borderIndex = index % imposeBorderPaint.length;
                canvas.drawRect(sx, sy, ex, ey, imposeBorderPaint[borderIndex]);
                canvas.drawRect(sx, sy, sx + rectSize, sy + rectSize, numBoxPaint);
                String text = String.valueOf(++index);
                textPaint.getTextBounds(text, 0, text.length(), textBounds);
                fontCX = (sx + (rectSize / 2)) - ((float) textBounds.width() / 2);
                fontCY = (sy + (rectSize / 2)) + ((float) textBounds.height() / 2);

                canvas.drawText(text, fontCX, fontCY, textPaint);
            }
        }

        return bitmap;
    }
}
