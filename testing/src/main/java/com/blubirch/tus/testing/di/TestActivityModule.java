package com.blubirch.tus.testing.di;

import androidx.lifecycle.ViewModel;

import com.blubirch.tus.testing.ui.GradeDispositionResultFragment;
import com.blubirch.tus.testing.ui.GradeDispositionResultViewModel;
import com.blubirch.tus.testing.ui.InventoryTestingFragment;
import com.blubirch.tus.testing.ui.InventoryTestingViewModel;
import com.blubirch.tus.testing.ui.ReGradeDispositionResultFragment;
import com.blubirch.tus.testing.ui.RegradingDispositionResultViewModel;
import com.blubirch.tus.testing.ui.ReturnReasonViewModel;
import com.blubirch.tus.testing.ui.ReturnReasonFragment;
import com.blubirch.core.di.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class TestActivityModule {


    @ContributesAndroidInjector
    abstract InventoryTestingFragment bindInventoryTestingFragment();

    @ContributesAndroidInjector
    abstract ReturnReasonFragment bindReturnReasonFragment();

    @ContributesAndroidInjector
    abstract GradeDispositionResultFragment bindGradeDispositionResultFragment();

    @ContributesAndroidInjector
    abstract ReGradeDispositionResultFragment bindReGradeDispositionResultFragment();


    @Binds
    @IntoMap
    @ViewModelKey(InventoryTestingViewModel.class)
    abstract ViewModel bindInventoryTestingViewModel(InventoryTestingViewModel inventoryTestingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ReturnReasonViewModel.class)
    abstract ViewModel bindReturnReasonViewModel(ReturnReasonViewModel returnReasonViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(GradeDispositionResultViewModel.class)
    abstract ViewModel bindGradeDispositionResultViewModel(GradeDispositionResultViewModel gradeDispositionResultViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RegradingDispositionResultViewModel.class)
    abstract ViewModel bindReGradeDispositionResultViewModel(RegradingDispositionResultViewModel reGradeDispositionResultViewModel);

}
