package com.blubirch.tus.testing.ui

import android.content.ClipData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.testing.data.models.ReturnReason
import com.blubirch.tus.testing.data.models.Test
import com.blubirch.tus.testing.data.models.TestInventory
import com.blubirch.tus.testing.data.repository.TestRepository
import com.blubirch.core.data.MyException
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class InventoryTestingViewModel @Inject constructor() : BaseViewModel() {

    companion object {
        private const val TAG = "ItemTestingViewModel"
    }

    private var testMap: LinkedHashMap<String, Test>? = null
    private val tests = arrayListOf<Test>()
    val returnReasons: ArrayList<ReturnReason> = arrayListOf()
    val docTypes: ArrayList<ArrayList<String>> = arrayListOf()

    val lastTest by lazy { MutableLiveData<Test>() }

    val itemLiveData by lazy { MutableLiveData<ClipData.Item>() }

    val testLiveData by lazy { MutableLiveData<List<Test>>() }


    //required for saveTestReturn api call
    lateinit var testInventory: TestInventory


    @Inject
    lateinit var testRepository: TestRepository


    @Synchronized
    fun fetchData() {
        if (testMap != null) {
            val completable = Completable.fromRunnable {
                reloadTest()
            }
            compositeDisposable.add(completable.subscribeOn(Schedulers.computation()).subscribe {
            })
        } else {
            loadRemoteData()
        }
    }

    private fun loadRemoteData() {
        compositeDisposable.add(
            testRepository.getTestRule(testInventory.categoryId, testInventory.gradingType)
                .subscribeOn(Schedulers.io())
                .subscribe { testRule, throwable ->
                    if (throwable == null) {
                        if (testRule.rule != null && testRule.rule.tests.isNotEmpty()) {
                            testMap = testRule.rule.tests
                            reloadTest()
                        } else {
                            errorLiveData.postValue(MyException.ApiError(Throwable("Grade Rule not defined for Selected Article.")))
                        }
                    } else {
                        testLiveData.postValue(null)
                        postAPIError(throwable)
                    }
                }
        )
        if (testInventory.returnReasonRequired) {
            compositeDisposable.add(
                testRepository.getReturnReasons(testInventory.ownLabel)
                    .subscribeOn(Schedulers.io())
                    .subscribe { result, throwable ->
                        if (throwable == null) {
                            returnReasons.clear()
                            returnReasons.addAll(result.returnReasons)
                            docTypes.clear()
                            docTypes.addAll(result.docTypes)
                        }
                    }
            )
        }
    }


    private fun reloadTest() {

        var test = testMap?.get("Ref_1")
        if (tests.isEmpty()) {
            test?.let { lastTest.postValue(it) }
        } else {
            tests.clear()
        }
        while (test != null) {
            tests.add(test)
            val selectedOption = test.selectedOption

            if (selectedOption == null || !selectedOption.isChecked)
                break
            val route = test.selectedOption?.route
            if (route.isNullOrBlank() || route == "End") break
            test = testMap?.get(route)
        }
        testLiveData.postValue(tests)
    }


    fun setSelectedInventory(testInventory: TestInventory) {
        this.testInventory = testInventory
    }
}
