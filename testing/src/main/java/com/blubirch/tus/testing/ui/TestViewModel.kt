package com.blubirch.tus.testing.ui

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.blubirch.core.data.MyException
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc
import com.blubirch.core.utility.getFileBase64String
import com.blubirch.tus.testing.BuildConfig
import com.blubirch.tus.testing.data.api.dto.Disposition
import com.blubirch.tus.testing.data.api.dto.TestResultResponseDTO
import com.blubirch.tus.testing.data.api.dto.UploadRequestDTO
import com.blubirch.tus.testing.data.models.*
import com.blubirch.tus.testing.data.repository.TestRepository
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.util.*
import java.util.concurrent.Executors
import javax.inject.Inject


class TestViewModel @Inject constructor() : BaseViewModel() {

    companion object{
        const val IS_S3_UPLAOD_ENABLED = false;
    }

    private var calculateGradeRequested: Boolean = false
    private val imageScheduler = Schedulers.from(Executors.newSingleThreadExecutor())

    @Inject
    lateinit var testRepository: TestRepository
    @Inject
    lateinit var context : Context

    lateinit var testInventory: TestInventory
    lateinit var gatePassNumber: String

    var returnReasons: ArrayList<ReturnReason> = arrayListOf()
    var docNames: ArrayList<ArrayList<String>> = arrayListOf()
        private set

    lateinit var tests: ArrayList<Test>
    lateinit var imageHolders: ArrayList<ImageHolder>
    var reason: String = ""
    var docList: ArrayList<AttachedDoc>? = null

    var gradingResult: HashMap<String, String>? = null
    var grade: String = ""
    var disposition: Disposition? = null
    var dispositionNames: ArrayList<String>? = null
    var policyTypes: HashMap<String, String>? = null

    //required for ImageUpload
    private val imageHolderWaitList: Vector<ImageHolder> = Vector()
    val imageUploadLiveData by lazy { MutableLiveData<Boolean>() }

    val responseLiveData by lazy { MutableLiveData<TestResultResponseDTO?>() }


    fun uploadImage(imageHolder: ImageHolder, exceptionListener: (Throwable) -> Unit) {
        imageHolderWaitList.add(imageHolder)
        imageDisposable.add(uploadAllImageCall(imageHolder)
            .subscribeOn(imageScheduler)
            .doOnDispose { imageHolder.isUploading = false }
            .subscribe { imageHolderRes: ImageHolder?, throwable: Throwable? ->
                imageHolder.isUploading = false
                if (throwable == null) {
                    if (imageHolderRes != null) {
                        imageHolderWaitList.remove(imageHolder)
                        imageUploadLiveData.postValue(true)
                        if (calculateGradeRequested) {
                            calculateGradeRequested = false
                            findGradeAndDisposition(exceptionListener)
                        }
                    } else {
                        exceptionListener(Throwable("Unknown Error"))
                    }
                } else {
                    exceptionListener(throwable)
                }
                Log.d("pendingImageCount", ": ${imageHolderWaitList.size}")
            }
        )
    }

    private var imageDisposable: CompositeDisposable = CompositeDisposable()

    fun clearImageDisposable() {
        imageDisposable.clear()
    }

    private fun uploadAllImageCall(imageHolder: ImageHolder): Single<ImageHolder?> {

        Log.d("pendingImageCount", ": ${imageHolderWaitList.size}")
        imageHolder.isUploading = true
        if (imageHolder.imageSrc.startsWith("http") && imageHolder.annotatedImageSrc.startsWith("http")) {
            return Single.fromCallable {
                uploadImageToS3(imageHolder)
                imageHolder
            }
        } else {
            if(imageHolder.imageSrc.startsWith("http").not()) {
                if (imageHolder.annotatedImageSrc.startsWith("http").not()) {
                    val imageSrc = imageHolder.imageSrc
                    val returnSingle = if(imageSrc.isBlank().not()) {
                        var tempSingle = getFileBase64String(imageSrc).flatMap {
                            testRepository.uploadImage(UploadRequestDTO(it, File(imageHolder.imageSrc).name))
                        }.flatMap {
                            Single.fromCallable {
                                imageHolder.imageSrc = it.pathName
                                File(imageHolder.imageSrc).delete()
                                imageHolder
                            }
                        }
                        val annotatedImageSrc = imageHolder.annotatedImageSrc
                        if(annotatedImageSrc.isNotBlank()) {
                            tempSingle = tempSingle.flatMap {
                                getFileBase64String(annotatedImageSrc)
                            }.flatMap {
                                testRepository.uploadImage(UploadRequestDTO(it, File(annotatedImageSrc).name, true))
                            }.flatMap {
                                Single.fromCallable {
                                    imageHolder.annotatedImageSrc = it.pathName
                                    uploadImageToS3(imageHolder)
                                    File(annotatedImageSrc).delete()
                                    imageHolder
                                }
                            }
                        }
                        tempSingle
                    } else { Single.fromCallable<ImageHolder?> { null } }
                    return returnSingle
                } else {
                    return getFileBase64String(imageHolder.imageSrc).flatMap {
                        testRepository.uploadImage(UploadRequestDTO(it, File(imageHolder.imageSrc).name))
                    }.flatMap {
                        Single.fromCallable {
                            File(imageHolder.imageSrc).delete()
                            imageHolder.imageSrc = it.pathName
                            uploadImageToS3(imageHolder)
                            imageHolder
                        }
                    }
                }
            } else {
                return getFileBase64String(imageHolder.annotatedImageSrc)
                    .flatMap {
                        testRepository.uploadImage(UploadRequestDTO(it, File(imageHolder.annotatedImageSrc).name, true))
                    }.flatMap {
                        Single.fromCallable {
                            imageHolder.annotatedImageSrc = it.pathName
                            uploadImageToS3(imageHolder)
                            File(imageHolder.annotatedImageSrc).delete()
                            imageHolder
                        }
                    }
            }
        }
    }

    @Throws(Exception::class)
    fun uploadImageToS3(imageHolder: ImageHolder) {
        if (IS_S3_UPLAOD_ENABLED.not()) {
            return
        }
        val clientRegion = Regions.AP_SOUTH_1
        val bucketName = if(BuildConfig.FLAVOR == "dev")  "beam-saas-dev" else "croma-saas-uat"
        val pool = "ap-south-1:897a04f1-244c-402d-bfa7-bc60f1ab0f9f"

        val filePath = imageHolder.originalImageSrc
        if (filePath.startsWith("https")) {
            return
        }
        val imageFile = File(filePath)

        val key = "public/uploads/android/test_images/${imageFile.name}"
        val credentialsProvider =
            CognitoCachingCredentialsProvider(context.applicationContext, pool, clientRegion)

        val s3Client: AmazonS3 = AmazonS3Client(credentialsProvider).apply {
            setRegion(Region.getRegion(clientRegion))
        }

        val transferUtility = TransferUtility.builder()
            .context(context.applicationContext)
            .s3Client(s3Client)
            .defaultBucket(bucketName)
            .build()

        val transferObserver = transferUtility.upload(key, imageFile,
            CannedAccessControlList.PublicRead
        )
        var exception = Exception("")
        transferObserver.setTransferListener(object : TransferListener{
            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
            }

            override fun onStateChanged(id: Int, state: TransferState?) {
                if (state == TransferState.COMPLETED) {
                    val imageUrl = "https://$bucketName.s3.ap-south-1.amazonaws.com/$key"
                    imageHolder.originalImageSrc = imageUrl
                    File(imageHolder.originalImageSrc).delete()
                }
            }

            override fun onError(id: Int, ex: Exception?) {
                if (ex == null || ex.message.isNullOrBlank()) {
                    exception = Exception("Unknown Error")
                } else {
                    exception = Exception(ex.message, ex.cause)
                }
                ex?.printStackTrace()
            }

        })
        while (true) {
            if (TransferState.COMPLETED == transferObserver.getState()) {
                if(imageHolder.originalImageSrc.startsWith("https")){
                    break
                }
            } else if (TransferState.FAILED == transferObserver.getState()) {
                if (exception.message!!.isNotBlank()) {
                    throw exception
                }
            }
        }
    }


    fun findGradeAndDisposition(exceptionListener: (Throwable) -> Unit) {
        progressLiveData.postValue(true)
        if (imageHolderWaitList.isEmpty()) {
            callGetGradeAndDispositionApi()
        }
        for (imageHolder in imageHolderWaitList) {
            if (imageHolder.isUploading) {
                calculateGradeRequested = true
                continue
            }
            imageDisposable.add(uploadAllImageCall(imageHolder)
                .subscribeOn(imageScheduler)
                .doOnDispose { imageHolder.isUploading = false }
                .subscribe { imageHolderRes: ImageHolder?, throwable: Throwable? ->
                    imageHolder.isUploading = false
                    if (throwable == null) {
                        if (imageHolderRes != null) {
                            imageHolderWaitList.remove(imageHolder)
                            if (imageHolderWaitList.isEmpty()) {
                                callGetGradeAndDispositionApi()
                            }
                        } else {
                            exceptionListener(Throwable("Unknown Error"))
                        }
                    } else {
                        exceptionListener(throwable)
                    }
                    Log.d("pendingImageCount", ": ${imageHolderWaitList.size}")
                })
        }
    }

    private fun callGetGradeAndDispositionApi() {
        if (testInventory.returnReasonRequired) {
            testInventory.returnReason = reason
            callGetGradeAndDispositionInwardApi()
        } else {
            callGetGradeAndDispositionRegradeApi()
        }
    }

    private fun callGetGradeAndDispositionRegradeApi() {
        progressLiveData.postValue(true)
        val testSelectionListMap = getTestSelectionMap(tests)

        compositeDisposable.add(
            testRepository.calculateGradeDisposition(
                testInventory, testSelectionListMap
            ).subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        disposition = result.disposition
                        grade = result.grade
                        gradingResult = result.gradingResult
                        dispositionNames = result.dispositionNames
                        val policyMap: HashMap<String, String> = hashMapOf()
                        result.policyTypes.forEach {
                            policyMap.putAll(it)
                        }
                        policyTypes = policyMap
                        responseLiveData.postValue(result)
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }

    private fun callGetGradeAndDispositionInwardApi() {
        progressLiveData.postValue(true)
        val testSelectionListMap = getTestSelectionMap(tests)

        testInventory.returnReason = reason
        compositeDisposable.add(
                testRepository.calculateGradeDispositionInward(
                        testInventory, testSelectionListMap
                ).subscribeOn(Schedulers.io())
                        .subscribe { result, throwable ->
                            progressLiveData.postValue(false)
                            if (throwable == null) {
                                disposition = result.disposition
                                grade = result.grade
                                gradingResult = result.gradingResult
                                dispositionNames = result.dispositionNames
                                val policyMap: HashMap<String, String> = hashMapOf()
                                result.policyTypes.forEach {
                                    policyMap.putAll(it)
                                }
                                policyTypes = policyMap
                                responseLiveData.postValue(result)
                            } else {
                                postAPIError(throwable)
                            }
                        }
        )
    }


    fun getPendingImageCount(): Int {
        var pendingImageCount = 0
        for (imageHolder in imageHolderWaitList) {
            if (imageHolder.isUploading)
                pendingImageCount++
        }
        return pendingImageCount
    }

    fun updateDocNames(docTypes: ArrayList<ArrayList<String>>) {
        docNames.clear()
        docTypes?.let {
            docNames.addAll(it)
        }
    }


//    fun getHash1(): LinkedHashMap<String, ArrayList<TestSelection>> {
//        val testSelectionListMap: LinkedHashMap<String, ArrayList<TestSelection>> = linkedMapOf()
//        tests.forEach { test ->
//            var testSelection = TestSelection(test)
//            testSelectionListMap.apply {
//                var testSelections = get(test.testType)
//                if (testSelections == null) {
//                    testSelections = arrayListOf()
//                }
//                testSelections.add(testSelection)
//                put(test.testType, testSelections)
//                test.selectedOption!!.apply {
//                    if (annotationLabel.isNotBlank()) {
//                        testSelectionListMap.apply {
//                            var testSelectionList = get(annotationLabel)
//                            if (testSelectionList == null) {
//                                testSelectionList = arrayListOf()
//                            }
//                            testSelection.annotations.forEach {
//                                testSelection = TestSelection()
//                                testSelection.test = annotationLabel
//                                testSelection.value = it.text!!
//                                testSelection.output = it.text
//                                testSelectionList!!.add(testSelection)
//                            }
//                            put(annotationLabel, testSelectionList!!)
//                        }
//                    }
//                }
//            }
//        }
//        return testSelectionListMap
//    }

    fun saveTestReturn(errorLiveData: MutableLiveData<Exception>) {
        progressLiveData.value = true
        if (imageHolderWaitList.isEmpty())
            callSaveTestReturnApi()
        for (imageHolder in imageHolderWaitList) {
            if (imageHolder.isUploading)
                continue
            imageDisposable.add(
                uploadAllImageCall(imageHolder)
                    .subscribeOn(Schedulers.io())
                    .doOnDispose { imageHolder.isUploading = false }
                    .subscribe { imageHolderRes: ImageHolder?, throwable: Throwable? ->
                        imageHolder.isUploading = false
                        if (throwable == null) {
                            if (imageHolderRes != null) {
                                imageHolderWaitList.remove(imageHolder)
                                if (imageHolderWaitList.isEmpty()) {
                                    callSaveTestReturnApi()
                                }
                            } else {
                                errorLiveData.postValue(MyException.UnKnownError(Throwable("Unknown Error")))
                            }
                        } else {
                            errorLiveData.postValue(MyException.UnKnownError(throwable))
                        }
                        Log.d("pendingImageCount", ": ${imageHolderWaitList.size}")
                    })
        }
    }

    private fun callSaveTestReturnApi() {
        progressLiveData.postValue(true)
        val testSelectionListMap = getTestSelectionMap(tests)

//        compositeDisposable.add(
//            testRepository.saveTestedReturn(
//                testInventory, docList, reason, gatePassNumber, testSelectionListMap
//            ).subscribeOn(Schedulers.io())
//                .subscribe { result, throwable ->
//                    progressLiveData.postValue(false)
//                    if (throwable == null) {
//                        responseLiveData.postValue(result)
//                    } else {
//                        postAPIError(throwable)
//                    }
//                }
//        )
    }

}
