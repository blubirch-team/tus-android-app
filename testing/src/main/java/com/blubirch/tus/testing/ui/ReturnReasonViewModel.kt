package com.blubirch.tus.testing.ui

import androidx.lifecycle.LiveData
import com.blubirch.tus.testing.data.models.ReturnReason
import com.blubirch.tus.testing.data.repository.TestRepository
import com.blubirch.core.data.lifecycle.SingleLiveEvent
import com.blubirch.core.presentation.customview.attachdoc.AttachDocViewModel
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReturnReasonViewModel @Inject constructor() : AttachDocViewModel(){

    @Inject
    lateinit var testRepository: TestRepository

    val updateReturnReasonLiveData : LiveData<Boolean> by lazy {
        SingleLiveEvent<Boolean>()
    }

    fun updateReturnReasons(testViewModel: TestViewModel, isReasonYes: Boolean) {
        if (returnReasons.size > 0) {
            setData(testViewModel.returnReasons, isReasonYes)
            (updateReturnReasonLiveData as SingleLiveEvent).postValue(true)
        } else {
            compositeDisposable.add(
                testRepository.getReturnReasons(testViewModel.testInventory.ownLabel)
                    .subscribeOn(Schedulers.io())
                    .subscribe { result, throwable ->
                        if (throwable == null) {
                            setData(result.returnReasons, isReasonYes)
                            (updateReturnReasonLiveData as SingleLiveEvent).postValue(true)
                        }
                    }
            )
        }
    }

    private fun setData(returnReasonList: ArrayList<ReturnReason>, isReasonYes: Boolean) {
        returnReasons.clear()
//        if (isReasonYes) {
//            returnReasons.add(returnReasonList[0])
//        } else {
            returnReasons.addAll(returnReasonList)
//            returnReasons.removeAt(0)
//        }
    }

    var returnReasons: ArrayList<ReturnReason> = arrayListOf()

}
