package com.blubirch.tus.testing.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.LinkedHashMap

@Parcelize
data class TestSelection(
    var test: String ="",
    var value: String ="",
    var output: String? = null,
    var annotations: ArrayList<Annotation> = arrayListOf()
) : Parcelable {
    fun copyFrom(option: Option) {
        value = option.value
        output = option.output
    }

    constructor(testVal: Test) : this() {
        this.test = testVal.name
        testVal.selectedOption?.let { copyFrom(it) }
        testVal.imageHolders.forEach {
            annotations.addAll(it.toAnnotations())
        }
    }
}

@Parcelize
data class Annotation(
    var src: String? = null,
    var annotationImgSrc : String? = null,
    var text: String? = null,
    var shapes: List<Shape>? = arrayListOf(),
    var orientation: String? = null,
    val imageWidth: Int,
    val imageHeight: Int
) : Parcelable

@Parcelize
data class Shape(
    var type: String? = null,
    var geometry: Geometry? = null
) : Parcelable

@Parcelize
data class Geometry(
    var x: Float? = null,
    var y: Float? = null,
    var width: Float? = null,
    var height: Float? = null
) : Parcelable


fun getTestSelectionMap(tests: ArrayList<Test>): LinkedHashMap<String, ArrayList<TestSelection>> {
    val testSelectionListMap: LinkedHashMap<String, ArrayList<TestSelection>> = linkedMapOf()
    tests.forEach { test ->
        var testSelection = TestSelection(test)
        testSelectionListMap.apply {
            var testSelections = get(test.testType)
            if (testSelections == null) {
                testSelections = arrayListOf()
            }
            testSelections.add(testSelection)
            put(test.testType, testSelections)
            test.selectedOption!!.apply {
                if (annotationLabel.isNotBlank()) {
                    testSelectionListMap.apply {
                        var testSelectionList = get(annotationLabel)
                        if (testSelectionList == null) {
                            testSelectionList = arrayListOf()
                        }
                        testSelection.annotations.forEach {
                            testSelection = TestSelection()
                            testSelection.test = annotationLabel
                            testSelection.value = it.text!!
                            testSelection.output = it.text
                            testSelectionList.add(testSelection)
                        }
                        put(annotationLabel, testSelectionList)
                    }
                }
            }
        }
    }
    return testSelectionListMap
}
