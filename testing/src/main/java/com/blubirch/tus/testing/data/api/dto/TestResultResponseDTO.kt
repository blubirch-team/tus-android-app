package com.blubirch.tus.testing.data.api.dto

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

class TestResultResponseDTO(
    var grade: String,

    @SerializedName("inventory_id")
    var inventoryId: Long,
    @SerializedName("processed_grading_result")
    var gradingResult: HashMap<String, String>,

    var disposition: Disposition,
    @SerializedName("disposition_names")
    var dispositionNames: ArrayList<String>,
    @SerializedName("policy_types")
    var policyTypes: ArrayList<HashMap<String, String>>
)

@Parcelize
class Disposition(val disposition: String? = "", val flow: String? = "") : Parcelable