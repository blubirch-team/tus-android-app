package com.blubirch.tus.testing.data.models

import android.os.Parcelable
import com.blubirch.tus.testing.ui.customviews.Coordinate
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TestRule(
    @SerializedName("test_rule")
    var rule: Rule,
    var message: String? = "",
    var status: Int? = 0
) : Parcelable

@Parcelize
data class Rule(
    var tests: LinkedHashMap<String, Test>,
    @SerializedName("Physical_precedence")
    var physicalPrecedence: LinkedHashMap<String, Int>,
    @SerializedName("functional_precedence")
    var functionalPrecedence: LinkedHashMap<String, Int>
) : Parcelable

@Parcelize
data class Test(
    @SerializedName("test_type")
    var testType: String = "",
    @SerializedName("test")
    var name: String = "",
//    @SerializedName("picture")
//    var pictureRequired: Boolean = false,
//    @SerializedName("annotation")
//    var annotationRequired: Boolean = false,
//    @SerializedName("picture_labels")
//    var pictureLabels: ArrayList<String>? = arrayListOf(),
    var options: ArrayList<Option> = arrayListOf(),
    var imageHolders: ArrayList<ImageHolder> = arrayListOf(),
    var selectedOption: Option? = null
) : Parcelable

@Parcelize
data class Option(
    var value: String = "",
    var output: String = "",
    var route: String = "",
    @SerializedName("picture")
    var pictureRequired: Boolean = false, // whether picture is mandatory or not
    @SerializedName("picture_labels")
    var pictureLabels: ArrayList<String>? = arrayListOf(), // whether side selection is mandatory or not
    @SerializedName("annotation")
    var annotationRequired: Boolean = false, // whether annotation on image is required or not
    var annotations: ArrayList<String> = arrayListOf(), // selection list for the annotation
    @SerializedName("annotation_label")
    var annotationLabel: String = "",
    var isChecked: Boolean = false
) : Parcelable

@Parcelize
data class ImageHolder(
    var imageSrc: String = "",
    var annotatedImageSrc: String = "",
    var originalImageSrc : String = "",
    var side: String = "",
    var coordinatesList: ArrayList<Coordinate> = arrayListOf(),
    var isUploading: Boolean = false,
    val imageWidth: Int,
    val imageHeight: Int
) : Parcelable {
    fun toAnnotations(): ArrayList<Annotation> {
        val annotations = arrayListOf<Annotation>()
        if (coordinatesList.isEmpty()) {
            val annotation =
                Annotation(
                    src = imageSrc,
                    annotationImgSrc = annotatedImageSrc,
                    text = side,
                    imageWidth = imageWidth,
                    imageHeight = imageHeight
                )
            annotations.add(annotation)
        } else {
            coordinatesList.forEach {
                it.toAnnotation().let { annotation ->
                    annotation.src = imageSrc
                    annotation.annotationImgSrc = annotatedImageSrc
                    annotation.orientation = side
                    annotations.add(annotation)
                }
            }
        }
        return annotations
    }
}