package com.blubirch.tus.testing.data.api;

import com.blubirch.tus.testing.data.api.interfaces.TestAPI;

public interface EndPoint {
    /**
     * {@link TestAPI}
     */
    String URL_TEST_FETCH_TEST_RULE = "gradings/category_rules";
    String URL_GET_RETURN_REASON = "warehouse/wms/gate_passes/return_reasons";

    String URL_TEST_UPLOAD_IMAGE = "store/returns/customer_returns/upload";
    String URL_CALCULATE_GRADE_DISPOSITION_FOR_RETURN = "warehouse/wms/gate_passes/calculate_grade";
    String URL_CALCULATE_GRADE_DISPOSITION_INWARD = "warehouse/wms/gate_passes/calculate_grade_new";
    String URL_CALCULATE_GRADE_DISPOSITION = "gradings/calculate_grade";

    String GENERATE_TAG = "warehouse/wms/gate_passes/generate_tag";
    String SAVE_TESTED_RETURN = "warehouse/wms/gate_passes/create_inventories";

    String SAVE_REGRADING = "gradings/store_grade";

}
