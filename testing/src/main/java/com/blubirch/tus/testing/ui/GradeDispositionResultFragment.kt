package com.blubirch.tus.testing.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.data.api.dto.InventoryResult
import com.blubirch.tus.testing.data.models.ImageHolder
import com.blubirch.tus.testing.ui.adapter.DocItemAdapter
import com.blubirch.tus.testing.ui.adapter.GradingGridAdapter
import com.blubirch.tus.testing.ui.adapter.TestImageAdapter
import com.blubirch.tus.testing.ui.adapter.TestOutput
import com.blubirch.tus.testing.ui.annotation.AnnotationActivity
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc
import com.blubirch.core.utility.AppUtils
import com.blubirch.core.utility.disableEditText
import com.blubirch.core.utility.gone
import com.blubirch.core.utility.visible
import kotlinx.android.synthetic.main.fragment_grade_disposition_result.*
import javax.inject.Inject


class GradeDispositionResultFragment : BaseFragment<GradeDispositionResultViewModel>() {
    private lateinit var mViewModel: GradeDispositionResultViewModel
    private lateinit var testViewModel: TestViewModel

    companion object {
        fun newInstance(): GradeDispositionResultFragment {
            return GradeDispositionResultFragment()
        }

        private const val REQUEST_CODE_TAG_ID = 1332
        private const val REQUEST_CODE_TOAT = 1333
        private const val REQUEST_CODE_SR_NUMBER = 1334
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_grade_disposition_result, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProvider(this, viewModelFactory).get(
            GradeDispositionResultViewModel::class.java
        )
        testViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(TestViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
        setupInput()
        disableManualDispositionDropDown()
    }

    private fun setupInput() {
        silTagId.initialize(this, REQUEST_CODE_TAG_ID)
        silToatNumber.initialize(this, REQUEST_CODE_TOAT)
        silSRNumber.initialize(this, REQUEST_CODE_SR_NUMBER)
    }

    private fun setupList() {
        setupImageList()
        setupGradingSummaryList()
        setupDocList()
    }

    private fun setupManualDispositionDropDown() {
        if (testViewModel.dispositionNames == null) {
            testViewModel.dispositionNames = arrayListOf()
        }
        spDisposition.adapter = ArrayAdapter<String>(
            requireContext(), android.R.layout.simple_list_item_1,
            testViewModel.dispositionNames!!
        )
    }

    private fun enableManualDispositionDropDown() {
        spDisposition.isEnabled = true
    }

    private fun disableManualDispositionDropDown() {
        spDisposition.isEnabled = false
    }

    private fun setupImageList() {
        rvAttachedImages.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            if (adapter == null) {
                adapter = TestImageAdapter {
                    showImageForTest(it)
                }
            }
        }
    }

    private fun setupDocList() {
        rvDocuments.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            if (adapter == null) {
                adapter = DocItemAdapter {
                    showDocuments(it)
                }
            }
        }
    }

    private fun showDocuments(attachedDoc: AttachedDoc) {
        val browserIntent = Intent(Intent.ACTION_VIEW)
        browserIntent.setDataAndType(
            Uri.parse(attachedDoc.fileAbsolutePath),
            AppUtils.getMimeType(attachedDoc.fileAbsolutePath)
        )

        val chooser = Intent.createChooser(
            browserIntent, TextUtils.concat(attachedDoc.docName, " - " + attachedDoc.referenceNo)
        )
        startActivity(chooser)
    }

    private fun setupGradingSummaryList() {
        rvGrading.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter =
                GradingGridAdapter()
        }
    }

    override fun onResume() {
        super.onResume()
        setupListeners()
    }

    override fun onPause() {
        super.onPause()
        silTagId.removeTextWatcher(tagIdTextWatcher)
        silToatNumber.removeTextWatcher(toatNumberTextWatcher)
    }

    private var tagIdTextWatcher: TextWatcher? = null
    private var toatNumberTextWatcher: TextWatcher? = null
    private fun setupListeners() {
        if (tagIdTextWatcher == null) tagIdTextWatcher =
            object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    silTagId.setError(null)
                }

            }
        if (toatNumberTextWatcher == null) toatNumberTextWatcher =
            object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    silToatNumber.setError(null)
                }

            }
        silTagId.addTextWatcher(tagIdTextWatcher)
        silToatNumber.addTextWatcher(toatNumberTextWatcher)
        btContinue.setOnClickListener {
            if (!spDisposition.isEnabled) {
                mViewModel.manualDisposition = null
            }
            mViewModel.saveAndContinue(
                silTagId.text.toString(),
                silToatNumber.text.toString(),
                silSRNumber.text.toString(),
                spPolicy.text.toString()
            )
        }
        createNewTag.setOnClickListener {
            if (createNewTag.text == getString(R.string.create_new_tag_id))
                mViewModel.generateTag()
            else
                showPrintTagView(silTagId.text.toString())
        }
        cbManualDisposition.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                enableManualDispositionDropDown()
                spDisposition.apply {
                    mViewModel.manualDisposition =
                        spDisposition.adapter.getItem(0) as String?
                    setSelection(0)
                    if (mViewModel.manualDisposition!!.contains("liquidation", true)) {
                        showPolicy()
                    } else {
                        hidePolicy()
                    }
                }
            } else {
                mViewModel.manualDisposition = null
                disableManualDispositionDropDown()
                updateGradeDisposition()
            }
        }
        spDisposition.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (spDisposition.isEnabled) {
                    mViewModel.manualDisposition =
                        spDisposition.selectedItem as String
                    if (mViewModel.manualDisposition!!.contains("liquidation", true)) {
                        showPolicy()
                    } else {
                        hidePolicy()
                    }
                } else {
                    mViewModel.manualDisposition = null
                    updateGradeDisposition()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
//                mViewModel.manualDisposition = null
            }
        }
        spPolicy.setOnItemClickListener { parent, view, position, id ->
            silPolicy.error = null
        }
    }


    override fun getViewModel(): GradeDispositionResultViewModel {
        return mViewModel
    }

    override fun attachLiveData() {
        setupManualDispositionDropDown()
        updateGradeDisposition()
        updateImages()
        updateGrading()
        updateDocList()
        mViewModel.apply {
            testInventory = testViewModel.testInventory
            tests = testViewModel.tests
            reason = testViewModel.reason
            docList = testViewModel.docList
            grade = testViewModel.grade
            gradingResult = testViewModel.gradingResult
            disposition = testViewModel.disposition
            policyTypes = testViewModel.policyTypes

            responseLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    onSaved(result)
//                    inwardViewModel.updateGatePass()
//                    showSnackBar(getString(R.string.msg_inward_success))
//                    navigate(
//                        GradeDispositionResultFragmentDirections.actionGradeDispositionResultFragmentToInwardItemFragment()
//                    )
                }
            })
            tagIdLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    silTagId.text = it
                    silTagId.setError(null)
                    disableEditText(silTagId.editText)
//                    silTagId.editText.isEnabled = false
                    createNewTag.text = getString(R.string.print_item_id)
                }
            })
            tagIdErrorLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    silTagId.setError(it.message)
                    silTagId.editText.requestFocus()
                }
            })
            toatNumberErrorLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    silToatNumber.setError(it.message)
                    silToatNumber.editText.requestFocus()
                }
            })
            policyErrorLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    silPolicy.error = it.message
                }
            })
        }

    }

    private fun onSaved(result: InventoryResult) {
        requireActivity().setResult(
            Activity.RESULT_OK,
            Intent().apply {
                putExtra(TestActivity.TEST_RESULT_SAVED_INVENTORY, result)
            }
        )
        requireActivity().finish()
    }

    private fun updateDocList() {
        (rvDocuments.adapter as DocItemAdapter).apply {
            testViewModel.docList?.let {
                setDiffNewData(it)
                notifyItemRangeChanged(0, it.size)
            }
        }
    }

    private fun updateGradeDisposition() {
        tvGrade.text = testViewModel.grade
        tvDispositionResult.text = ""
        testViewModel.disposition?.apply {
            tvDispositionResult.text = disposition
        }
        hidePolicy()
        if (tvDispositionResult.text.isNotBlank()) {
            if (tvDispositionResult.text.contains("liquidation", true)) {
                showPolicy()
            }
        }
    }

    private fun hidePolicy() {
        silPolicy.gone()
        getViewModel().policyRequired = false
    }

    private fun showPolicy() {
        silPolicy.visible()
        val list: ArrayList<String> = arrayListOf()
        list.addAll(testViewModel.policyTypes!!.keys)
        spPolicy.setAdapter(
            ArrayAdapter<String>(requireContext(), android.R.layout.simple_list_item_1, list)
        )
        getViewModel().policyRequired = true
    }

    private fun updateImages() {
        (rvAttachedImages.adapter as TestImageAdapter).apply {
            setImages(testViewModel.imageHolders)
            setViewOnly(true)
        }
    }

    private fun updateGrading() {
        val newList = mutableListOf<TestOutput>().apply {
            clear()
            testViewModel.tests.forEach { test ->
                test.apply {
                    if (selectedOption?.value != null)
                        add(
                            TestOutput(
                                testType,
                                name,
                                selectedOption!!.value
                            )
                        )
                }
            }
        }
        newList.add(0, TestOutput("Return Reason", "", testViewModel.reason))
        (rvGrading.adapter as? GradingGridAdapter)?.apply {
            setDiffNewData(newList)
            notifyItemRangeChanged(0, newList.size)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_TAG_ID -> {
                    silTagId.onActivityResult(requestCode, resultCode, data)
                }
                REQUEST_CODE_TOAT -> {
                    silToatNumber.onActivityResult(requestCode, resultCode, data)
                }
                REQUEST_CODE_SR_NUMBER -> {
                    silSRNumber.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
    }

    private fun showImageForTest(imageHolder: ImageHolder) {
        Intent(activity, AnnotationActivity::class.java).apply {
            putExtra(
                AnnotationActivity.INTENT_EXTRA_OUTPUT_IMAGE,
                imageHolder.imageSrc
            )
            putParcelableArrayListExtra(
                AnnotationActivity.INTENT_EXTRA_OUTPUT_COORDINATES_FOR_IMAGE,
                imageHolder.coordinatesList
            )
            startActivity(this)
        }
    }
}
