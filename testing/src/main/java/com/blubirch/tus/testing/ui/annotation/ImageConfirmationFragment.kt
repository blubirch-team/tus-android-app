package com.blubirch.tus.testing.ui.annotation

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.normalizeOrientation
import com.blubirch.tus.testing.scaleDownConstraint
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.*
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.CameraOptions
import com.otaliastudios.cameraview.PictureResult
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.FilePickerConst.REQUEST_CODE_PHOTO
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.*
import kotlinx.android.synthetic.main.image_confirmation_fragment.*
import kotlinx.coroutines.*
import javax.inject.Inject


class ImageConfirmationFragment : BaseFragment<ImageConfirmationViewModel>() {

    enum class OnCompressComplete {
        CONFIRM_IMAGE
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    companion object {
        fun newInstance() =
            ImageConfirmationFragment()
    }

    private lateinit var viewModel: ImageConfirmationViewModel
    private lateinit var activityViewModel: AnnotationViewModel
    private var compressionJob : Job? = null
    private var onCompressComplete : OnCompressComplete? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.image_confirmation_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        camera.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ImageConfirmationViewModel::class.java)
        activityViewModel =
            ViewModelProvider(
                requireActivity(),
                viewModelFactory
            ).get(AnnotationViewModel::class.java)

        ivConfirm.setOnClickListener {
            if (showLoaderForCompression()) {
                onCompressComplete = OnCompressComplete.CONFIRM_IMAGE
            } else {
                onConfirmClicked()
            }
        }
        ivCapture.setOnClickListener {
            takePhoto()
        }
        ivRePick.setOnClickListener {
            cancelCompressionJob()
            onRePickClicked()
        }
        ivCancel.setOnClickListener {
            cancelCompressionJob()
            onCancelClicked()
        }
        camera.addCameraListener(object : CameraListener() {
            override fun onCameraClosed() {
                super.onCameraClosed()
                hideCamera()
            }

            override fun onCameraOpened(options: CameraOptions) {
                super.onCameraOpened(options)
                showCamera()
            }

            override fun onPictureTaken(result: PictureResult) {
                val imageNamePrefix = activityViewModel.stnNumber + "_" + activityViewModel.articleId
                val originalFile = AppUtils.getFileForImage(requireContext(), "original_images", imageNamePrefix, "jpeg")
                val imageFile = AppUtils.getFileForImage(requireContext(), "annotation_images", imageNamePrefix, "png")

                // toFile is bg thread
                result.toFile(originalFile) { savedFile ->
                    savedFile?.let { savedFile ->
                        activityViewModel.originalImage = savedFile.absolutePath
                        compressionJob = GlobalScope.launch {
                            val context = getContext()
                            if (context != null /*&& savedFile.exists()*/) {
                                Compressor.compress(context, savedFile) {
                                    scaleDownConstraint(1024, 1024)
                                    format(Bitmap.CompressFormat.JPEG)
                                    quality(60)
                                    normalizeOrientation()
                                    destination(imageFile)
                                }
                            }
                        }.apply {
                            invokeOnCompletion { throwable ->
                                if (host != null && activity != null) {
                                    if (throwable == null) {
                                        viewModel.imageUrl = imageFile.absolutePath
                                        requireActivity().apply {
                                            runOnUiThread {
                                                when (onCompressComplete) {
                                                    OnCompressComplete.CONFIRM_IMAGE -> {
                                                        onConfirmClicked()
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        requireActivity().runOnUiThread {
                                            onCompressComplete = null
                                        }
                                    }
                                }
                            }
                        }
                        viewModel.imageUrl = savedFile.absolutePath
                    }
                }
                camera.close()
            }
        })

    }

    private fun takePhoto() {
        camera.takePicture()
    }

    override fun onBackPressed(): Boolean {
        cancelCompressionJob()
        return false
    }

    private fun isCompressionJobActive() : Boolean {
        return compressionJob?.isActive ?: false
    }

    private fun showLoaderForCompression() : Boolean {
        if (isCompressionJobActive()) {
            viewModel.progressLiveData.value = true
            return true
        }
        return false
    }

    private fun cancelCompressionJob() {
        if (isCompressionJobActive()) {
            viewModel.progressLiveData.value = false
            compressionJob?.cancel()
        }
        onCompressComplete = null
    }

    private fun onConfirmClicked() {
        activityViewModel.imageLiveData.value = viewModel.imageUrl
        if (!activityViewModel.imageLiveData.value.isNullOrEmpty()) {
            if (!activityViewModel.sideLabelsLiveData.value.isNullOrEmpty()) {
                navigate(ImageConfirmationFragmentDirections.actionImageConfirmationFragmentToSelectSideFragment())
            } else if (!activityViewModel.annotationLabelsLiveData.value.isNullOrEmpty()) {
                navigate(ImageConfirmationFragmentDirections.actionImageConfirmationFragmentToMarkingFragment())
            } else {
                returnImage()
            }
        }
    }

    private fun returnImage() {
        requireActivity().setResult(Activity.RESULT_OK,
            Intent().also {
                if (!activityViewModel.imageLiveData.value.isNullOrBlank()) {
                    it.putExtra(
                        AnnotationActivity.INTENT_EXTRA_OUTPUT_IMAGE,
                        activityViewModel.imageLiveData.value
                    )
                }
            }
        )
        requireActivity().finish()
    }

    private fun onRePickClicked() {
        activityViewModel.imageLiveData.postValue(null)
        camera.open()
    }

    private fun showCamera() {
        annotationImage.gone()
        if (camera.isOpened) // changed isVisible to is Opened
            camera.visible()
        grpControls.gone()
        ivCapture.visible()
    }

    private fun hideCamera() {
        annotationImage.visible()
        camera.gone()
        grpControls.visible()
        ivCapture.gone()
    }

    private fun onCancelClicked() {
        requireActivity().finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var imageUrl = ""
        when (requestCode) {
            REQUEST_CODE_PHOTO -> if (resultCode == Activity.RESULT_OK) {
                data?.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA)
                    ?.also {
                        if (it.isNotEmpty()) {
                            imageUrl = it[0]
                        }
                    }
            }
        }
        if (imageUrl.isBlank()) {
            requireActivity().finish()
            return
        }
        viewModel.imageUrl = imageUrl
    }

    override fun getViewModel() = viewModel
    override fun attachLiveData() {
        observe(viewModel.imageLiveData, {
            if (!it.isNullOrEmpty()) {
                annotationImage.visible()
                camera.gone()
                annotationImage.loadImage(it)
                ivConfirm.isEnabled = true
            }
        })
    }
}
