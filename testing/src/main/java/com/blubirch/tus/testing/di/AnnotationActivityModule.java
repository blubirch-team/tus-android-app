package com.blubirch.tus.testing.di;

import androidx.lifecycle.ViewModel;

import com.blubirch.tus.testing.ui.annotation.ImageConfirmationFragment;
import com.blubirch.tus.testing.ui.annotation.ImageConfirmationViewModel;
import com.blubirch.tus.testing.ui.annotation.MarkingFragment;
import com.blubirch.tus.testing.ui.annotation.MarkingViewModel;
import com.blubirch.tus.testing.ui.annotation.SelectSideFragment;
import com.blubirch.tus.testing.ui.annotation.SelectSideViewModel;
import com.blubirch.tus.testing.ui.annotation.ViewImageFragment;
import com.blubirch.core.di.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class AnnotationActivityModule {

    @ContributesAndroidInjector
    abstract ImageConfirmationFragment bindImageConfirmationFragment();

    @ContributesAndroidInjector
    abstract SelectSideFragment bindSelectSideFragment();

    @ContributesAndroidInjector
    abstract MarkingFragment bindMarkingFragment();

    @ContributesAndroidInjector
    abstract ViewImageFragment bindViewImageFragment();


    @Binds
    @IntoMap
    @ViewModelKey(ImageConfirmationViewModel.class)
    abstract ViewModel bindImageConfirmationFragmentViewModel(ImageConfirmationViewModel imageConfirmationViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MarkingViewModel.class)
    abstract ViewModel bindMarkingViewModel(MarkingViewModel markingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectSideViewModel.class)
    abstract ViewModel bindSelectSideFragment(SelectSideViewModel selectSideViewModel);
}
