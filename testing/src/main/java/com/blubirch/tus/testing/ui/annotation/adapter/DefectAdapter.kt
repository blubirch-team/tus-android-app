package com.blubirch.tus.testing.ui.annotation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.core.utility.gone
import com.blubirch.core.utility.visible
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.databinding.ItemDefectTypeBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class DefectAdapter : BaseQuickAdapter<Defect, BaseViewHolder>(
    R.layout.item_defect_type,
    arrayListOf()
) {
    var removeEnabled = true

    init {
        setDiffCallback(DefectCallBack())
    }

    override fun convert(holder: BaseViewHolder, item: Defect) {
        val binding = ItemDefectTypeBinding.bind(holder.itemView)
        binding.tvName.text = (item.index.toString() + ". " + item.defect)
        binding.ivDelete.apply {
            if (removeEnabled)
                visible()
            else
                gone()
        }
    }

    fun stopRemoval() {
        removeEnabled = false
        notifyItemMoved(0, data.size)
    }
}

class DefectCallBack : DiffUtil.ItemCallback<Defect>() {
    override fun areItemsTheSame(oldItem: Defect, newItem: Defect): Boolean {
        return oldItem.index == newItem.index
    }

    override fun areContentsTheSame(oldItem: Defect, newItem: Defect): Boolean {
        return oldItem.index == newItem.index && return oldItem.defect == newItem.defect
    }

}

data class Defect(val index: Int, val defect: String)