package com.blubirch.tus.testing.data.repository;


import androidx.annotation.Nullable;

import com.blubirch.tus.testing.data.api.dto.ReturnReasonDTO;
import com.blubirch.tus.testing.data.api.dto.SaveTestedReturnDTO;
import com.blubirch.tus.testing.data.api.dto.TagDTO;
import com.blubirch.tus.testing.data.api.dto.TestResultResponseDTO;
import com.blubirch.tus.testing.data.api.dto.UploadRequestDTO;
import com.blubirch.tus.testing.data.api.dto.UploadResponseDTO;
import com.blubirch.tus.testing.data.models.TestInventory;
import com.blubirch.tus.testing.data.models.TestRule;
import com.blubirch.tus.testing.data.models.TestSelection;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Single;

public interface TestRepository {

    Single<TestRule> getTestRule(final long categoryId, String gradingType);

    Single<ReturnReasonDTO> getReturnReasons(boolean ownLabel);

    Single<UploadResponseDTO> uploadImage(final UploadRequestDTO uploadRequestDTO);

    Single<TestResultResponseDTO> calculateGradeDispositionInward(TestInventory testInventory,
                                                                  HashMap<String, ArrayList<TestSelection>> testSelectionListMap);

    Single<TestResultResponseDTO> calculateGradeDisposition(TestInventory testInventory,
                                                            HashMap<String, ArrayList<TestSelection>> testSelectionListMap);

    Single<TagDTO> generateTag();

    Single<SaveTestedReturnDTO> saveTestedReturn(
            TestInventory testInventory, String returnReason, @Nullable ArrayList<AttachedDoc> documents,
            String srNumber, String tagNumber, String toatNumber, String manualDisposition,
            HashMap<String, ArrayList<TestSelection>> testSelectionListMap, String grade,
            HashMap<String, String> gradingResult, String disposition, String workFlow, String policy);

    Single<BaseDTO> saveGradeAndDispositionResult(
            Long inventoryId, String toatNumber, HashMap<String, ArrayList<TestSelection>> testSelectionListMap,
            String grade, HashMap<String, String> gradingResult, String disposition, String workFlow, String policy);
}
