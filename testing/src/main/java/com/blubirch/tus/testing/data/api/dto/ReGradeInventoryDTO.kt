package com.blubirch.tus.testing.data.api.dto


import com.blubirch.tus.testing.data.models.TestSelection
import com.google.gson.annotations.SerializedName
import java.util.*

data class ReGradeInventoryDTO(
    @SerializedName("inventory_id")
    var inventoryId: Long,
    @SerializedName("toat_number")
    var toatNumber: String,

    @SerializedName("final_grading_result")
    var finalGradingResult: HashMap<String, ArrayList<TestSelection>>?,
    @SerializedName("processed_grading_result")
    var processedGradingResult: HashMap<String, String>,
    @SerializedName("grade")
    var grade: String,

    @SerializedName("disposition")
    var disposition: String?,
    @SerializedName("work_flow_name")
    var workFlowName: String?,
    @SerializedName("policy_type")
    var policyTpe: String?
)

