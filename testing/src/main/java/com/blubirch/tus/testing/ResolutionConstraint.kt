package com.blubirch.tus.testing

import com.blubirch.core.utility.AppUtils
import id.zelory.compressor.constraint.Compression
import id.zelory.compressor.constraint.Constraint
import id.zelory.compressor.loadBitmap
import id.zelory.compressor.overWrite
import java.io.File

class OrientationConstraint : Constraint {

    override fun isSatisfied(imageFile: File): Boolean {
        return AppUtils.isImageOrientationNormal(imageFile.absolutePath)
    }

    override fun satisfy(imageFile: File): File {
        loadBitmap(imageFile).run {
            return overWrite(imageFile, this)
        }
    }
}

fun Compression.normalizeOrientation() {
    constraint(OrientationConstraint())
}