package com.blubirch.tus.testing.di;

import com.blubirch.tus.testing.data.api.interfaces.TestAPI;
import com.blubirch.tus.testing.data.repository.TestRepository;
import com.blubirch.tus.testing.data.repository.TestRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class TestDataModule {

    @Singleton
    @Provides
    public TestAPI provideTestAPI(Retrofit retrofit) {
        return retrofit.create(TestAPI.class);
    }

    @Singleton
    @Provides
    public TestRepository provideTestRepository(TestAPI testAPI) {
        return new TestRepositoryImpl(testAPI);
    }
}
