package com.blubirch.tus.testing.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class TestInventory(
    var id: Long = 123123,
    var sku: String = "",
    var desc: String = "",
    var itemCode: String? = "",
    var brand: String? = "",
    var model: String? = "",
    var serialNumber: String? = "",
    var serialNumber2: String? = "",
    var categoryId: Long = 0L,
    var category: String? = "",
    var categoryL1: String? = "",
    var categoryL2: String? = "",
    var dispatchDate: Date? = null,
    var returnReason: String? = "",
    var gatePassNumber: String? = "",

    var gradingType: String = "Warehouse",
    var returnReasonRequired: Boolean = false,
    var ownLabel: Boolean = false,
    var distributionCenterId: Long = 0L,
    var status: String = "",
    var tagNumber: String? = ""
) : Parcelable