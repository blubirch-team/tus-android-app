package com.blubirch.tus.testing.ui

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.testing.data.api.dto.Disposition
import com.blubirch.tus.testing.data.models.ImageHolder
import com.blubirch.tus.testing.data.models.Test
import com.blubirch.tus.testing.data.models.TestInventory
import com.blubirch.tus.testing.data.models.getTestSelectionMap
import com.blubirch.tus.testing.data.repository.TestRepository
import com.blubirch.core.data.api.dto.BaseDTO
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class RegradingDispositionResultViewModel @Inject constructor(private val testRepository: TestRepository) :
    BaseViewModel() {

    lateinit var testInventory: TestInventory

    //Selected Tests, Images taken, document list and Grade & Disposition
    var tests: ArrayList<Test> = arrayListOf()
    var imageHolders: ArrayList<ImageHolder> = arrayListOf()
    var grade: String = ""
    var disposition: Disposition? = null

    //Selected Tests, Images taken, document list and Grade & Disposition
    var gradingResult: HashMap<String, String>? = null
    var dispositionNames: ArrayList<String>? = null
    var policyTypes: HashMap<String, String>? = null
    var policyType: String? = null
    var policyRequired: Boolean = false

    val toatNumberErrorLiveData by lazy { MutableLiveData<Exception>() }
    val policyErrorLiveData by lazy { MutableLiveData<Exception>() }

    fun saveAndContinue(toatNumber: String, policy: String) {
        var validate = true
        if (toatNumber == null || toatNumber.isBlank()) {
            validate = false
            toatNumberErrorLiveData.value = Exception("Invalid Toat Number")
        }
        if (policyRequired && (policy == null || policy.isBlank())) {
            validate = false
            policyErrorLiveData.value = Exception("Select Policy")
        }
        updatePolicy(policy)
        if (validate) {
            progressLiveData.value = true
            saveApiCall(toatNumber)
        }
    }

    val responseLiveData by lazy { MutableLiveData<BaseDTO>() }
    private fun saveApiCall(toatNumber: String) {
        if (disposition == null && testInventory!!.status.contains("liquidation", true)) {
            disposition = Disposition("Liquidation")
//        } else if (disposition == null && testInventory.status.equals("Pending Repair Grade", true)) {
//            disposition = Disposition("Repair")
        }
        compositeDisposable.add(
            testRepository.saveGradeAndDispositionResult(
                testInventory!!.id,
                toatNumber,
                getTestSelectionMap(tests),
                grade,
                gradingResult,
                disposition!!.disposition,
                disposition!!.flow,
                policyType
            )
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    if (throwable == null) {
                        responseLiveData.postValue(result)
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }

    private fun updatePolicy(policy: String) {
        policyType = policyTypes?.get(policy)
    }
}
