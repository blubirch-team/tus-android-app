package com.blubirch.tus.testing.data.api;

import com.blubirch.tus.testing.data.api.interfaces.TestAPI;

public interface MockEndPoint {
    /**
     * {@link TestAPI}
     */
    String URL_TEST_FETCH_TEST_RULE = "category_rules";
    String URL_TEST_UPLOAD_IMAGE = "store/returns/customer_returns/upload";
    String URL_SAVE_TESTED_RETURN = "store/returns/customer_returns/generate_rr";
    String URL_SAVE_RETURN_SERIAL_NO = "store/returns/customer_returns/finalize_grading";
    String URL_SAVE_RETURNS = "store/invoices/save_inventories";
}
