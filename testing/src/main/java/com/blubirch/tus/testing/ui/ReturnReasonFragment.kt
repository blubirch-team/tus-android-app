package com.blubirch.tus.testing.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.ui.adapter.GradingGridAdapter
import com.blubirch.tus.testing.ui.adapter.TestOutput
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.presentation.TakePictureActivity
import com.blubirch.core.presentation.customview.attachdoc.AttachDocView.AttachDocListener
import com.blubirch.core.utility.visible
import kotlinx.android.synthetic.main.fragment_return_reason.*
import kotlinx.android.synthetic.main.layout_grading_summary.*
import kotlinx.android.synthetic.main.layout_inwarding_attribute.*
import kotlinx.android.synthetic.main.layout_return_reason.*
import javax.inject.Inject


class ReturnReasonFragment : BaseFragment<ReturnReasonViewModel>() {

    companion object {
        fun newInstance() = ReturnReasonFragment()
    }

    private lateinit var testViewModel: TestViewModel
    private lateinit var viewModel: ReturnReasonViewModel
    private var isBackPressed = false

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_return_reason, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ReturnReasonViewModel::class.java)
        testViewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(TestViewModel::class.java)

        docViewReason.setListener(object : AttachDocListener {
            override fun startImageCapture() {
                startActivityForResult(
                    Intent(activity, TakePictureActivity::class.java),
                    TakePictureActivity.INTENT_REQUEST_CODE_TAKE_PICTURE
                )
            }
        })
        viewModel.setDocTypes(testViewModel.docNames)
        viewModel.setRefFillValue(testViewModel.testInventory.gatePassNumber)
        docViewReason.setViewModel(viewLifecycleOwner, viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupGradingSummaryList()
    }

    private fun setupGradingSummaryList() {
        rvGrading.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter =
                GradingGridAdapter()
        }
    }

    override fun onResume() {
        super.onResume()
        setupListeners()
    }

    private fun setupListeners() {
        btNext.setOnClickListener {
            val reason = spReturnReason.text.toString()
            if (reason.isBlank()) {
                spReturnReason.error = "Please Select"
                spReturnReason.requestFocus()
                return@setOnClickListener
            }
            testViewModel.reason = reason
            saveAndCalculateGradeDisposition()
        }
        btPrev.setOnClickListener {
            requireActivity().onBackPressed()
            showBackButton()
        }
        spReturnReason.setOnClickListener {
            spReturnReason.error = null
        }
        spReturnReason.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->
            btNext.isEnabled = true
        }
    }

    override fun onBackPressed(): Boolean {
        isBackPressed = true
        navController.navigateUp()
        showBackButton()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        docViewReason.onActivityResult(requestCode, resultCode, data)
    }

    override fun getViewModel(): ReturnReasonViewModel {
        return viewModel
    }

    override fun attachLiveData() {
        updateGrading()
        updateInwarding()
        testViewModel.apply {

            imageUploadLiveData.observe(viewLifecycleOwner, Observer {
                if (it != null && it) {
                    showSnackBar("Image Uploaded.")
                    testViewModel.imageUploadLiveData.postValue(null)
                }
            })

            responseLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    testViewModel.responseLiveData.value = null
                    navigate(ReturnReasonFragmentDirections.actionReturnReasonFragmentToDispositionResultFragment())
                }
            })
            errorLiveData.observe(viewLifecycleOwner, Observer {
                viewModel.errorLiveData.postValue(it)
            })
        }
        viewModel.apply {
            updateReturnReasonLiveData.observe(viewLifecycleOwner, Observer {
                if (it) {
                    updateReturnReasons()
                }
            })
            attachedDocLiveData.observe(viewLifecycleOwner, Observer {
                testViewModel.docList = it
//                btNext.isEnabled = it.isNotEmpty()
            })
        }
    }

    private fun updateReturnReasons() {
        val returnReasonAdapter =
            ArrayAdapter<String>(
                requireContext(),
                android.R.layout.simple_dropdown_item_1line,
                viewModel.returnReasons.map { returnReason -> returnReason.name })
        spReturnReason.setAdapter(returnReasonAdapter)

        if (viewModel.returnReasons.size == 1) {
            spReturnReason.post {
                spReturnReason.setText(spReturnReason.adapter.getItem(0).toString())
                btNext.isEnabled = true
            }
        } else {
            if (testViewModel.reason.isNotBlank())
                spReturnReason.post {
                    spReturnReason.setText(testViewModel.reason)
                    btNext.isEnabled = true
                }
        }
    }

    private fun updateGrading() {
        cvGrading.visible()
        var isReasonYes = false
        val newList = mutableListOf<TestOutput>().apply {
            clear()
            testViewModel.tests.forEach { test ->
                test.apply {
                    if (selectedOption?.value != null) {
                        isReasonYes = test.testType.equals("reason", true) &&
                                test.selectedOption!!.output.equals("yes", true)
                        add(TestOutput(testType, name, selectedOption!!.value))
                    }
                }
            }
        }
        (rvGrading.adapter as? GradingGridAdapter)?.apply {
            setDiffNewData(newList)
            notifyItemRangeChanged(0, newList.size)
        }

        // specific to croma for first question of Non-grading
            viewModel.updateReturnReasons(testViewModel,isReasonYes)

    }

    private fun updateInwarding() {
        cvInwarding.visible()
        testViewModel.testInventory.apply {
            setToolbarTitle("Reason")
            lvSku.value = sku
            lvDescription.value = desc
            lvSerialNo.value = serialNumber
            lvBrand.value = brand
        }
    }

    private fun saveAndCalculateGradeDisposition() {
//        if (testViewModel.getPendingImageCount() > 0) {
//            showSnackBar("Uploading Images....")
//        } else {
        testViewModel.findGradeAndDisposition { viewModel.postAPIError(it) }
//        }
    }

    override fun onDestroy() {
        if(isBackPressed.not()) {
            testViewModel.clearImageDisposable()
        }
        super.onDestroy()
    }

}
