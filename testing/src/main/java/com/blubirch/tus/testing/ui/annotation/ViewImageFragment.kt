package com.blubirch.tus.testing.ui.annotation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.testing.R
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.loadImage
import com.blubirch.core.utility.observe
import kotlinx.android.synthetic.main.fragment_view_image.*
import javax.inject.Inject


class ViewImageFragment : BaseFragment<MarkingViewModel>() {

    companion object {
        fun newInstance() =
            ViewImageFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var listener: ViewTreeObserver.OnGlobalLayoutListener
    private lateinit var viewModel: MarkingViewModel
    private lateinit var activityViewModel: AnnotationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_view_image, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MarkingViewModel::class.java)
        activityViewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        ).get(AnnotationViewModel::class.java)
    }

    private fun setupViews(url: String) {
        ivAnnotationImage.loadImage(url)

    }

    override fun getViewModel() = viewModel

    override fun attachLiveData() {
        observe(activityViewModel.imageLiveData) {
            if (it != null) {
                setupViews(it)
            }
        }
        observe(activityViewModel.sideLiveData) {
            if (it != null) {
                setToolbarTitle(it)
            }
        }
    }
}
