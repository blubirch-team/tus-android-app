package com.blubirch.tus.testing.ui.customviews;

import android.os.Parcel;
import android.os.Parcelable;


import com.blubirch.tus.testing.data.models.Annotation;
import com.blubirch.tus.testing.data.models.Geometry;
import com.blubirch.tus.testing.data.models.Shape;

import java.util.ArrayList;
import java.util.List;

public class Coordinate implements Parcelable {

    private static final String SHAPE_RECT = "rect";

    private float viewWidth;
    private float viewHeight;

    private int imageWidth;
    private int imageHeight;

    private float boxSizePercentage;

    private float touchX;
    private float touchY;

    private float viewSX;
    private float viewSY;
    private float viewEX;
    private float viewEY;
    private String defectType;


    private Coordinate(float viewWidth, float viewHeight,
                       int imageWidth, int imageHeight,
                       float boxSizePercentage, float touchX, float touchY) {
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;

        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;

        this.boxSizePercentage = boxSizePercentage;

        this.touchX = touchX;
        this.touchY = touchY;

        setCoordinates();
    }

    /**
     * Convert @Annotation model to @link Coordinates. It require width and height for attached ImageView.
     *
     * @param annotation annotation Model to be  converted.
     * @param viewWidth  width  of attached ImageView.
     * @param viewHeight height of attached ImageView.
     */
    public void fromAnnotation(Annotation annotation, float viewWidth, float viewHeight) {
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
        defectType = annotation.getText();
        Geometry cords = annotation.getShapes().get(0).getGeometry();
        viewSX = cords.getX() * viewWidth;
        viewSY = cords.getY() * viewHeight;
        viewEX = (cords.getWidth() * viewWidth) + viewSX;
        viewEY = (cords.getHeight() * viewHeight) + viewSY;
    }

    public Annotation toAnnotation() {
        Annotation annotation = new Annotation(null, null, null, new ArrayList<Shape>(), null, imageWidth, imageHeight);
        annotation.setText(defectType);
        Shape shape = new Shape();
        shape.setType(SHAPE_RECT);
        Geometry geometry = new Geometry(viewSX / viewWidth, viewSY / viewHeight, (viewEX - viewSX) / viewWidth, (viewEY - viewSY) / viewHeight);
        shape.setGeometry(geometry);
        List<Shape> shapes = new ArrayList<>();
        shapes.add(shape);
        annotation.setShapes(shapes);
        return annotation;
    }

    public Coordinate(float viewWidth, float viewHeight) {
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
    }

    protected Coordinate(Parcel in) {
        viewWidth = in.readFloat();
        viewHeight = in.readFloat();
        imageWidth = in.readInt();
        imageHeight = in.readInt();
        boxSizePercentage = in.readFloat();
        touchX = in.readFloat();
        touchY = in.readFloat();
        viewSX = in.readFloat();
        viewSY = in.readFloat();
        viewEX = in.readFloat();
        viewEY = in.readFloat();
        defectType = in.readString();
    }

    public static final Creator<Coordinate> CREATOR = new Creator<Coordinate>() {
        @Override
        public Coordinate createFromParcel(Parcel in) {
            return new Coordinate(in);
        }

        @Override
        public Coordinate[] newArray(int size) {
            return new Coordinate[size];
        }
    };

    public void viewSizeChanged(float viewWidth, float viewHeight) {
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
        setCoordinates();
    }

    public float getViewSX() {
        return viewSX;
    }

    public float getViewSY() {
        return viewSY;
    }

    public float getViewEX() {
        return viewEX;
    }

    public float getViewEY() {
        return viewEY;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    private void setCoordinates() {
        float dev = viewWidth * boxSizePercentage / 2f;

        viewSX = touchX - dev;
        viewSY = touchY - dev;

        viewEX = touchX + dev;
        viewEY = touchY + dev;

        if (viewSX < 0) {
            viewEX = (viewWidth * boxSizePercentage);
            viewSX = 0;
        }

        if (viewSY < 0) {
            viewEY = (viewWidth * boxSizePercentage);
            viewSY = 0;
        }

        if (viewEX > viewWidth) {
            viewEX = viewWidth;
            viewSX = viewWidth - (viewWidth * boxSizePercentage);
        }

        if (viewEY > viewHeight) {
            viewEY = viewHeight;
            viewSY = viewHeight - (viewWidth * boxSizePercentage);
        }
    }

    public void setDefect(String defect) {
//        this.itemType = itemType;
        this.defectType = defect;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(viewWidth);
        dest.writeFloat(viewHeight);
        dest.writeInt(imageWidth);
        dest.writeInt(imageHeight);
        dest.writeFloat(boxSizePercentage);
        dest.writeFloat(touchX);
        dest.writeFloat(touchY);
        dest.writeFloat(viewSX);
        dest.writeFloat(viewSY);
        dest.writeFloat(viewEX);
        dest.writeFloat(viewEY);
        dest.writeString(defectType);
    }

    public static class Builder {

        private float viewWidth;
        private float viewHeight;

        private int imageWidth;
        private int imageHeight;

        private float boxSizePercentage;
        private float touchX;
        private float touchY;

        public Builder setViewWidth(float viewWidth) {
            this.viewWidth = viewWidth;
            return this;
        }

        public Builder setViewHeight(float viewHeight) {
            this.viewHeight = viewHeight;
            return this;
        }

        public Builder setImageWidth(int imageWidth) {
            this.imageWidth = imageWidth;
            return this;
        }

        public Builder setImageHeight(int imageHeight) {
            this.imageHeight = imageHeight;
            return this;
        }

        public Builder setBoxSizePercentage(float boxSizePercentage) {
            this.boxSizePercentage = boxSizePercentage;
            return this;
        }

        public Builder setTouchX(float touchX) {
            this.touchX = touchX;
            return this;
        }

        public Builder setTouchY(float touchY) {
            this.touchY = touchY;
            return this;
        }

        public Coordinate build() {
            if (viewWidth == 0 || viewHeight == 0) {
                throw new IllegalStateException("View's width cannot be zero");
            }

            if (imageWidth == 0 || imageHeight == 0) {
                throw new IllegalStateException("Width or Height of an image can not be zero");
            }

            if (boxSizePercentage == 0) {
                throw new IllegalStateException("Box percentage size width cannot be zero");
            }

            if (touchX == 0 || touchY == 0) {
                throw new IllegalStateException("Touch coordinates cannot be zero");
            }

            return new Coordinate(viewWidth, viewHeight, imageWidth, imageHeight, boxSizePercentage, touchX, touchY);
        }
    }

    public String getDefectType() {
        return defectType;
    }

}