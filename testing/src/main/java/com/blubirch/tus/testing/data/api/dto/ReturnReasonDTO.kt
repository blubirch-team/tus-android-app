package com.blubirch.tus.testing.data.api.dto

import com.blubirch.tus.testing.data.models.ReturnReason
import com.google.gson.annotations.SerializedName

class ReturnReasonDTO(

    @SerializedName("return_reasons")
    var returnReasons: ArrayList<ReturnReason>,
    @SerializedName("document_types")
    var docTypes: ArrayList<ArrayList<String>>
)