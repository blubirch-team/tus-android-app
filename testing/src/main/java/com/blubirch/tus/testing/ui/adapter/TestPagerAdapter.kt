package com.blubirch.tus.testing.ui.adapter

import android.text.Html
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.core.utility.gone
import com.blubirch.core.utility.inVisible
import com.blubirch.core.utility.visible
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.databinding.ItemTestInventoryBinding
import com.blubirch.tus.testing.data.models.ImageHolder
import com.blubirch.tus.testing.data.models.Option
import com.blubirch.tus.testing.data.models.Test
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class TestPagerAdapter(
    private val testChangeListener: (Test) -> Unit,
    private val addImageClickListener: (Int, Test) -> Unit,
    private val imageClickListener: (ImageHolder) -> Unit,
    private val imageRemoveListener: (ImageHolder) -> Unit
) :
    BaseQuickAdapter<Test, BaseViewHolder>(R.layout.item_test_inventory) {

    init {
        setDiffCallback(TestDiffCallBack())
    }

    override fun convert(holder: BaseViewHolder, item: Test) {

        val binding = ItemTestInventoryBinding.bind(holder.itemView)
        binding.headerView.label = item.testType
        when (item.name.isBlank()) {
            true -> {
                binding.tvSubTitle.text = ""
                binding.tvSubTitle.gone()
            }
            false -> {
                binding.tvSubTitle.text = item.name
                binding.tvSubTitle.visible()
            }
        }

        binding.tvAttachImage.text = Html.fromHtml(context.resources.getString(R.string.attach_images));

        binding.rvTests.layoutManager = GridLayoutManager(binding.rvTests.context, 2)
        binding.rvTests.setHasFixedSize(true)
        binding.rvTests.adapter = TestGridAdapter(item.options) { option: Option, isSelected: Boolean ->
            setItemAndListener(binding, holder, item, option, isSelected, true)
            testChangeListener(item)
        }
        item.selectedOption?.let { selectedOption ->
            setItemAndListener(binding, holder, item, selectedOption, selectedOption.isChecked)
        }
        (binding.rvTests.layoutParams as ConstraintLayout.LayoutParams).height = ConstraintLayout.LayoutParams.WRAP_CONTENT
        binding.rvImage.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            if (adapter == null) {
                adapter = TestImageAdapter(imageClickListener)
            }
            (adapter as TestImageAdapter).apply {
                setImages(item.imageHolders)
                removeListener = {
                    item.imageHolders.remove(it)
                    (adapter as TestImageAdapter).setImages(item.imageHolders)
                    imageRemoveListener(it)
                }
            }
        }
        val selectedOption = item.selectedOption
        if(selectedOption != null) {
            if(selectedOption.pictureRequired) {
                showImageLayouts(binding)
            } else {
                hideImageLayouts(binding)
            }
        } else {
            hideImageLayouts(binding)
        }
    }

    private fun setItemAndListener(binding: ItemTestInventoryBinding,holder: BaseViewHolder,
                                   item: Test, option: Option, isSelected: Boolean, openImageCapture : Boolean = false) {
        if (isSelected) {
            item.selectedOption = option
            if (option.pictureRequired) {
                showImageLayouts(binding)
            } else {
                hideImageLayouts(binding)
            }
            if (option.pictureRequired) {
                if(openImageCapture) {
                    addImageClickListener(holder.adapterPosition, item)
                }
                if (item.selectedOption != null) {
                    setAddImageListener(binding, holder.adapterPosition, item)
                }
            }
        } else {
            item.selectedOption = null
            unsetAddImageListener(binding)
        }
    }

    private fun setAddImageListener(binding: ItemTestInventoryBinding, position: Int, item: Test) {
        binding.clAddImage.setOnClickListener {
            addImageClickListener(position, item)
        }
    }

    private fun unsetAddImageListener(binding: ItemTestInventoryBinding) {
        binding.clAddImage.setOnClickListener(null)
    }

    private fun showImageLayouts(binding: ItemTestInventoryBinding) {
        binding.rvImage.visible()
        binding.tvAttachImage.visible()
        binding.clAddImage.visible()
    }

    private fun hideImageLayouts(binding: ItemTestInventoryBinding) {
        binding.rvImage.inVisible()
        binding.tvAttachImage.inVisible()
        binding.clAddImage.inVisible()
    }
}

class TestDiffCallBack : DiffUtil.ItemCallback<Test>() {
    override fun areItemsTheSame(oldItem: Test, newItem: Test): Boolean {
        return oldItem.testType == newItem.testType && oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: Test, newItem: Test): Boolean {
        return oldItem.testType == newItem.testType && oldItem.name == newItem.name && oldItem.options == newItem.options
    }

}