package com.blubirch.tus.testing.data.api.dto

import com.google.gson.annotations.SerializedName

class UploadRequestDTO(@field:SerializedName("image_url") val imageUrl: String,
                       @field:SerializedName("image_name") val imageName: String,
                       @field:SerializedName("is_screenshot") val isScreenshot: Boolean = false)