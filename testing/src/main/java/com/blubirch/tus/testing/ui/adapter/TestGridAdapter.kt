package com.blubirch.tus.testing.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.databinding.ItemTestCheckListBinding
import com.blubirch.tus.testing.data.models.Option
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class TestGridAdapter(
    val list: List<Option>,
    val testChangeListener: (Option, Boolean) -> Unit
) : BaseQuickAdapter<Option, BaseViewHolder>(
    R.layout.item_test_check_list,
    list.toMutableList()
) {

    init {
        setDiffCallback(OptionDiffCallBack())
    }


    override fun convert(holder: BaseViewHolder, item: Option) {
        val binding = ItemTestCheckListBinding.bind(holder.itemView)
        binding.checkbox.setOnCheckedChangeListener(null)
        binding.checkbox.text = item.value
        binding.checkbox.isChecked = item.isChecked
        binding.checkbox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked != item.isChecked) {
                if (isChecked)
                    uncheckOthers()
                item.isChecked = isChecked
                testChangeListener(item, isChecked)
            }
        }
    }

    private fun uncheckOthers() {
        data.filter { it.isChecked }.forEach {
            it.isChecked = false
            notifyItemChanged(getItemPosition(it))
        }
    }
}

class OptionDiffCallBack : DiffUtil.ItemCallback<Option>() {
    override fun areItemsTheSame(oldItem: Option, newItem: Option): Boolean {
        return oldItem.value == newItem.value
    }

    override fun areContentsTheSame(oldItem: Option, newItem: Option): Boolean {
        return oldItem.value == newItem.value && oldItem.isChecked == newItem.isChecked
    }

}