package com.blubirch.tus.testing.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.databinding.ItemDocAttachmentBinding
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class DocItemAdapter(
    private val clickListener: ((AttachedDoc) -> Unit)
) :
    BaseQuickAdapter<AttachedDoc, BaseViewHolder>(R.layout.item_doc_attachment) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<AttachedDoc>() {
            override fun areItemsTheSame(
                oldItem: AttachedDoc,
                newItem: AttachedDoc
            ): Boolean {
                return false
            }

            override fun areContentsTheSame(
                oldItem: AttachedDoc,
                newItem: AttachedDoc
            ): Boolean {
                return false
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: AttachedDoc) {
        val binding = ItemDocAttachmentBinding.bind(holder.itemView)
        item.apply {
            binding.tvDocName.text = item.docName
            binding.tvRefrenceNo.text = item.referenceNo
        }
        holder.itemView.setOnClickListener {
            clickListener(item)
        }
    }
}