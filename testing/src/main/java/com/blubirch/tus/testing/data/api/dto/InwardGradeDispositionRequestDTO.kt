package com.blubirch.tus.testing.data.api.dto

import com.blubirch.tus.testing.data.models.TestInventory
import com.blubirch.tus.testing.data.models.TestSelection
import com.google.gson.annotations.SerializedName
import java.util.*

class InwardGradeDispositionRequestDTO {

    @SerializedName("client_gate_pass_number")
    var clientGatePassNumber: String = ""

    @SerializedName("sku_code")
    var skuCode: String = ""

    @SerializedName("return_reason")
    var returnReason: String? = ""

    @SerializedName("final_grading_result")
    var selection: HashMap<String, ArrayList<TestSelection>>? = null

//    @SerializedName("grading_type")
//    var gradingType: String = ""

    constructor(
        testInventory: TestInventory,
        testSelectionListMap: HashMap<String, ArrayList<TestSelection>>
    ) {
        testInventory.let {
            clientGatePassNumber = it.gatePassNumber!!
            skuCode = it.sku
            returnReason = it.returnReason
//            gradingType = it.gradingType
        }
        selection = testSelectionListMap

    }

}