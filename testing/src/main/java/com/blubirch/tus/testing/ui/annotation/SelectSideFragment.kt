package com.blubirch.tus.testing.ui.annotation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.blubirch.tus.testing.R
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.loadImage
import com.blubirch.core.utility.materialDialog
import com.blubirch.core.utility.observe
import kotlinx.android.synthetic.main.fragment_select_side.*
import javax.inject.Inject

class SelectSideFragment : BaseFragment<SelectSideViewModel>(),
    CompoundButton.OnCheckedChangeListener {

    companion object {
        fun newInstance() =
            SelectSideFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: SelectSideViewModel
    private lateinit var activityViewModel: AnnotationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_select_side, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(SelectSideViewModel::class.java)
        activityViewModel =
            ViewModelProvider(
                requireActivity(),
                viewModelFactory
            ).get(AnnotationViewModel::class.java)
        setupRadioButtons()
        btCancel.setOnClickListener {
            findNavController().popBackStack()
        }
        btConfirm.setOnClickListener {
            if (activityViewModel.sideLiveData.value?.isEmpty() == false) {
                navigate(SelectSideFragmentDirections.actionSelectSideFragmentToMarkingFragment())
            } else {
                materialDialog(
                    "Please select side of product in image to continue marking!",
                    "Select side"
                )
            }
        }
    }

    private fun setupRadioButtons() {
        rbRight.setOnCheckedChangeListener(this)
        rbBottom.setOnCheckedChangeListener(this)
        rbTop.setOnCheckedChangeListener(this)
        rbFont.setOnCheckedChangeListener(this)
        rbBack.setOnCheckedChangeListener(this)
        rbLeft.setOnCheckedChangeListener(this)
    }

    fun resetOther(radio: CompoundButton) {
        arrayListOf(
            rbRight,
            rbBottom,
            rbTop,
            rbFont,
            rbBack,
            rbLeft
        ).forEach {
            if (it.id != radio.id) {
                it.isChecked = false
            }
        }
    }

    override fun getViewModel(): SelectSideViewModel {
        return viewModel
    }

    override fun attachLiveData() {
        observe(activityViewModel.imageLiveData) {
            it?.let { annotationImage.loadImage(it) }
        }
    }


    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            activityViewModel.sideLiveData.postValue(buttonView.text.toString())
            resetOther(buttonView)
        }
    }

}
