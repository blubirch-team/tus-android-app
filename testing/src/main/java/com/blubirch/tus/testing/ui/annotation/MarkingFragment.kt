package com.blubirch.tus.testing.ui.annotation

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.PopupWindow
import android.widget.Spinner
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.normalizeOrientation
import com.blubirch.tus.testing.ui.annotation.adapter.Defect
import com.blubirch.tus.testing.ui.annotation.adapter.DefectAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.*
import com.daasuu.bl.BubbleLayout
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.format
import id.zelory.compressor.constraint.quality
import kotlinx.android.synthetic.main.fragment_marking.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.FileOutputStream
import javax.inject.Inject


class MarkingFragment : BaseFragment<MarkingViewModel>() {

    companion object {
        fun newInstance() =
            MarkingFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var listener: ViewTreeObserver.OnGlobalLayoutListener
    private lateinit var viewModel: MarkingViewModel
    private lateinit var activityViewModel: AnnotationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_marking, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MarkingViewModel::class.java)
        activityViewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        ).get(AnnotationViewModel::class.java)
    }

    private fun setupViews(url: String) {
        rvMakings?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = DefectAdapter()
                .also { defectAdapter ->
                defectAdapter.addChildClickViewIds(R.id.ivDelete)
                defectAdapter.setOnItemChildClickListener { _, view, position ->
                    if (doAnnotations.isClickable && view.id == R.id.ivDelete) {
                        doAnnotations.removeItem(position)
                    }
                }
            }
        }
        btCancel.setOnClickListener {
            findNavController().navigateUp()
        }
        btConfirm.setOnClickListener {
            if ((rvMakings.adapter as? DefectAdapter)?.data?.isEmpty() == false) {
                viewModel.progressLiveData.postValue(true)

                val finishIntent = Intent()

                if (!doAnnotations.drawnBoxes.value.isNullOrEmpty())
                    finishIntent.putExtra(
                        AnnotationActivity.INTENT_EXTRA_OUTPUT_COORDINATES_FOR_IMAGE,
                        doAnnotations.drawnBoxes.value
                    )
                if (!activityViewModel.sideLiveData.value.isNullOrBlank()) {
                    finishIntent.putExtra(
                        AnnotationActivity.INTENT_EXTRA_OUTPUT_SIDE_FOR_IMAGE,
                        activityViewModel.sideLiveData.value
                    )
                }

                val originalImage = activityViewModel.imageLiveData.value
                if (!originalImage.isNullOrBlank()) {
                    finishIntent.putExtra(
                        AnnotationActivity.INTENT_EXTRA_ORIGINAL_IMAGE,
                        activityViewModel.originalImage
                    )
                    finishIntent.putExtra(
                        AnnotationActivity.INTENT_EXTRA_OUTPUT_IMAGE,
                        originalImage
                    )
                    val imageNamePrefix = activityViewModel.stnNumber + "_" + activityViewModel.articleId + "_annotated"
                    val annotatedImageFile = AppUtils.getFileForImage(
                        requireContext(),
                        "annotation_images",
                        imageNamePrefix,
                        "png"
                    )
                    GlobalScope.launch {
                        withContext(Dispatchers.IO) {
                            val annotatedBitmap = doAnnotations.getColoredBitmap(originalImage)
                            val fOut = FileOutputStream(annotatedImageFile)
                            annotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                            fOut.flush()
                            fOut.close()
                            annotatedBitmap.recycle()
                            Compressor.compress(requireContext(), annotatedImageFile) {
                                normalizeOrientation()
                                quality(60)
                                format(Bitmap.CompressFormat.JPEG)
                            }
                        }
                    }.invokeOnCompletion { throwable ->
                        if (host != null && activity != null) {
                            if (throwable == null) {
                                requireActivity().apply {
                                    viewModel.progressLiveData.postValue(false)
                                    finishIntent.putExtra(
                                        AnnotationActivity.INTENT_EXTRA_ANNOTATED_IMAGE,
                                        annotatedImageFile.absolutePath
                                    )
                                    setResult(Activity.RESULT_OK, finishIntent)
                                    finish()
                                }
                            }
                        }
                    }

                } else {
                    requireActivity().setResult(Activity.RESULT_OK, finishIntent)
                    requireActivity().finish()
                }

            } else {
                showSnackBar(getString(R.string.plz_mark_annotation), btConfirm)
            }
        }
        listener = ViewTreeObserver.OnGlobalLayoutListener {
            try {
                onGlobalLayout(url)
            } catch (e: Exception) {
                ivAnnotationImage.viewTreeObserver.removeOnGlobalLayoutListener(listener)
            }
        }

        ivAnnotationImage.viewTreeObserver.addOnGlobalLayoutListener(listener)
    }

    private fun onGlobalLayout(url: String) {

        var imageWidth = 0f
        var imageHeight = 0f

        if (url.startsWith("http")) {
            doAnnotations.drawnBoxes?.value?.let {
                if (it.size > 0) {
                    imageWidth = it[0].imageWidth.toFloat()
                    imageHeight = it[0].imageHeight.toFloat()
                }
            }
        } else {
            val optionsSize = BitmapFactory.Options()
            optionsSize.inJustDecodeBounds = true
            BitmapFactory.decodeFile(url, optionsSize)
            imageWidth = optionsSize.outWidth.toFloat()
            imageHeight = optionsSize.outHeight.toFloat()
        }

        var guidePercentage = 0.0f

        when (val params = guideH70.layoutParams) {
            is ConstraintLayout.LayoutParams -> {
                guidePercentage = params.guidePercent
            }
        }

        val parent = ivAnnotationImage.parent
        var parentWidth = 0f
        var parentHeight = 0f
        when (parent) {
            is ViewGroup -> {
                parentWidth = parent.width.toFloat()
                parentHeight = (parent.height.toFloat() * guidePercentage)
            }
        }

        val viewWidth :Int
        val viewHeight :Int

        var imgRatio = imageWidth / imageHeight
        val maxRatio = parentWidth / parentHeight
        when {
            imgRatio < maxRatio -> {
                imgRatio = parentHeight / imageHeight
                viewWidth = (imgRatio * imageWidth).toInt()
                viewHeight = parentHeight.toInt()
            }
            imgRatio > maxRatio -> {
                imgRatio = parentWidth / imageWidth
                viewHeight = (imgRatio * imageHeight).toInt()
                viewWidth = parentWidth.toInt()
            }
            else -> {
                viewHeight = parentHeight.toInt()
                viewWidth = parentWidth.toInt()
            }
        }

        ivAnnotationImage.loadImage(url, parentWidth.toInt(), parentHeight.toInt())

        doAnnotations.setImageSize(imageWidth.toInt(), imageHeight.toInt())

        doAnnotations.post {
            doAnnotations?.setHeightWidth(
                viewWidth, viewHeight
            )
        }

        if (doAnnotations.drawnBoxes.value.isNullOrEmpty()) {
            doAnnotations.isClickable = true
            doAnnotations.setOnTouchListener { v, event ->
                val toReturn: Boolean = doAnnotations.onTouchEvent(event)
                if (doAnnotations.isClickable) {
                    when (event.action) {
                        MotionEvent.ACTION_UP -> {
                            doAnnotations.isClickable = false
                            showDefectDialogAndAdd()
                        }
                    }
                }
                toReturn
            }
        }

    }

    private fun showDefectDialogAndAdd() {
        if (doAnnotations.drawnBoxes.value?.isNotEmpty() == true) {
            doAnnotations?.drawnBoxes?.value.orEmpty().last()?.apply {
                val bubbleLayout: BubbleLayout = LayoutInflater.from(requireActivity())
                    .inflate(R.layout.dialog_annotation, null) as BubbleLayout

                val metrics: DisplayMetrics = Resources.getSystem().displayMetrics
                val displayWidth = metrics.widthPixels

                val innerBubble = bubbleLayout.findViewById<BubbleLayout>(R.id.bubbleLayout)

                if (this.viewEX > displayWidth / 2) {
                    val offset = bubbleLayout.context.dp2Pixel(40)

                    innerBubble.arrowPosition = viewEX - offset
                } else {
                    innerBubble.arrowPosition = viewSX
                }

                val popupWindow = PopupWindow(requireContext())


                popupWindow.contentView = bubbleLayout
                popupWindow.isOutsideTouchable = false
                popupWindow.width = ViewGroup.LayoutParams.MATCH_PARENT
                popupWindow.height = ViewGroup.LayoutParams.WRAP_CONTENT
                popupWindow.animationStyle = android.R.style.Animation_Dialog
                // change background color to transparent
                // change background color to transparent
                popupWindow.setBackgroundDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.popup_window_transparent
                    )
                )
                val locationArray = IntArray(2)
                doAnnotations.getLocationInWindow(locationArray)

                popupWindow.showAtLocation(
                    doAnnotations,
                    Gravity.TOP,
                    locationArray[0] + viewSX.toInt(),
                    locationArray[1] + viewEY.toInt()
                )
                popupWindow.setOnDismissListener {
                    doAnnotations?.isClickable = true
                }


                innerBubble.findViewById<Button>(R.id.btCancel)
                    .setOnClickListener {
                        if (doAnnotations != null)
                            doAnnotations.removeLastItem()
                        popupWindow.dismiss()
                    }

                val spinnerReason = innerBubble.findViewById<Spinner>(R.id.spinnerReason)

                innerBubble.findViewById<Button>(R.id.btAddDefect)
                    .setOnClickListener {
                        if (spinnerReason.selectedItemPosition > 0) {
                            val defect = spinnerReason.selectedItem as String
                            this.setDefect(defect)
                            doAnnotations.update()
                            popupWindow.dismiss()
                        }
                    }

                spinnerReason.adapter = ArrayAdapter<String>(
                    spinnerReason.context,
                    android.R.layout.simple_dropdown_item_1line,
                    activityViewModel.annotationLabelsLiveData.value?.apply {
                        if (this[0] != "Select Defect")
                            this.add(0, "Select Defect")
                    }.orEmpty()
                )
            }
        }
    }


    override fun getViewModel() = viewModel

    override fun attachLiveData() {
        observe(activityViewModel.imageLiveData) {
            if (it != null) {
                setupViews(it)
            }
        }
        observe(doAnnotations.drawnBoxes) { it1 ->
            var count = 0
            val newData = mutableListOf<Defect>().apply {
                it1?.map {
                    Defect(
                        ++count, when (it.defectType == null) {
                            true -> ""
                            else -> it.defectType
                        }
                    )
                }?.let { addAll(it) }
            }
            (rvMakings.adapter as DefectAdapter).apply {
                setDiffNewData(newData)
                notifyItemRangeChanged(0, newData.size)
            }
        }
        observe(activityViewModel.coordinatesModelLiveData) {
            if (it != null) {
                if (it.isNotEmpty()) {
                    doAnnotations.setAnnotation(it)
                }
                grpActions.gone()
                (rvMakings.adapter as DefectAdapter).removeEnabled = false
            }
        }
        observe(activityViewModel.sideLiveData) {
            it?.also {
                setToolbarTitle(it)
            }
        }
    }

    override fun onPause() {
        ivAnnotationImage.viewTreeObserver.removeOnGlobalLayoutListener(listener)
        super.onPause()
    }
}
