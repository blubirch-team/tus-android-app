package com.blubirch.tus.testing.ui.annotation

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.testing.ui.customviews.Coordinate
import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject


class AnnotationViewModel @Inject constructor() : BaseViewModel() {

    var stnNumber: String = "stn"
    var articleId: String = "article"
    lateinit var originalImage: String
    val annotationLabelsLiveData = MutableLiveData<ArrayList<String>>()
    val sideLabelsLiveData = MutableLiveData<ArrayList<String>>()

    val coordinatesModelLiveData = MutableLiveData<ArrayList<Coordinate>>()
    val imageLiveData = MutableLiveData<String>()
    val sideLiveData = MutableLiveData<String>()
}