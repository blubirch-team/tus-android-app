package com.blubirch.tus.testing.ui.annotation

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.ui.customviews.Coordinate
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseActivity
import javax.inject.Inject


class AnnotationActivity : BaseActivity<AnnotationViewModel>() {
    companion object {
        const val INTENT_EXTRA_STN_NUMBER= "item_stn_number"
        const val INTENT_EXTRA_ARTICLE_ID= "item_article_id"
        const val INTENT_EXTRA_SIDES_FOR_IMAGE = "sides_for_image"
        const val INTENT_EXTRA_ANNOTATIONS_FOR_IMAGE = "annotations_for_image"
        const val INTENT_EXTRA_ORIGINAL_IMAGE = "original_image"
        const val INTENT_EXTRA_OUTPUT_IMAGE = "output_image"
        const val INTENT_EXTRA_ANNOTATED_IMAGE = "annotated_image"
        const val INTENT_EXTRA_OUTPUT_SIDE_FOR_IMAGE = "output_side_for_image"
        const val INTENT_EXTRA_OUTPUT_COORDINATES_FOR_IMAGE = "output_coordinates_for_image"
    }

    @Inject
    lateinit var mViewModelFactory: ViewModelFactory



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_annotation)

        val annotationViewModel: AnnotationViewModel =
            ViewModelProvider(this, mViewModelFactory).get(
                AnnotationViewModel::class.java
            )

        intent.extras?.getString(INTENT_EXTRA_STN_NUMBER)?.let {
            annotationViewModel.stnNumber = it
        }

        intent.extras?.getString(INTENT_EXTRA_ARTICLE_ID)?.let {
            annotationViewModel.articleId = it
        }

        intent.extras?.getParcelableArrayList<Coordinate>(INTENT_EXTRA_OUTPUT_COORDINATES_FOR_IMAGE)
            ?.also {
                annotationViewModel.coordinatesModelLiveData.value = it
            }
        annotationViewModel.imageLiveData.value = null
        intent.extras?.getString(INTENT_EXTRA_OUTPUT_IMAGE)?.also {
            annotationViewModel.imageLiveData.value = it
        }
        intent.extras?.getString(INTENT_EXTRA_OUTPUT_SIDE_FOR_IMAGE)?.also {
            annotationViewModel.sideLiveData.value = it
        }

        intent.extras?.getStringArrayList(INTENT_EXTRA_SIDES_FOR_IMAGE).also {
            annotationViewModel.sideLabelsLiveData.value = it
        }
        intent.extras?.getStringArrayList(INTENT_EXTRA_ANNOTATIONS_FOR_IMAGE).also {
            annotationViewModel.annotationLabelsLiveData.value = it
        }

    }

    override fun attachLiveData() {

    }

    override fun getViewModelFactory(): ViewModelFactory {
        return mViewModelFactory
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onResume() {
        super.onResume()
//        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)//your icon here
        supportActionBar?.setHomeButtonEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun setupNavGraph(navController: NavController) {
        var imageData = ""
        intent.extras?.getString(INTENT_EXTRA_OUTPUT_IMAGE)?.also {
            imageData = it
        }
        val graph = navController.navInflater.inflate(R.navigation.annotation_navigation)

        if (imageData.isNotBlank()) {
            val coordinates: ArrayList<Coordinate> = arrayListOf()
            intent.extras?.getParcelableArrayList<Coordinate>(
                INTENT_EXTRA_OUTPUT_COORDINATES_FOR_IMAGE
            )
                ?.also {
                    coordinates.addAll(it)
                }
            coordinates.let {
                if (it == null || it.isEmpty()) {
                    graph.startDestination = R.id.viewImageFragment
                } else {
                    graph.startDestination = R.id.markingFragment
                }
            }
        } else {
            graph.startDestination = R.id.imageConfirmationFragment
        }
        navController.graph = graph
    }
}
