package com.blubirch.tus.testing.data.api.dto

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class TagDTO(
    @SerializedName("tag_number")
    var tagNumber: String
) : Parcelable
