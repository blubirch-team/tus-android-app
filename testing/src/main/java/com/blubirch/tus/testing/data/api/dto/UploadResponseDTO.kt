package com.blubirch.tus.testing.data.api.dto

import com.google.gson.annotations.SerializedName

class UploadResponseDTO(@SerializedName("path_name") var pathName: String)