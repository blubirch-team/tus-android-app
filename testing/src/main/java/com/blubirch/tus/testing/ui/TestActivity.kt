package com.blubirch.tus.testing.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.data.models.TestInventory
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseActivity
import javax.inject.Inject

class TestActivity : BaseActivity<TestViewModel>() {


    @Inject
    lateinit var mViewModelFactory: ViewModelFactory

    companion object {
        private const val EXTRA_KEY_INVENTORY: String = "inventory"

        const val TEST_RESULT_INVENTORY_ID = "test_result_inventory_id"
        const val TEST_RESULT_GRADE = "test_result_grade"
        const val TEST_RESULT_DISPOSITION = "test_result_disposition"
        const val TEST_RESULT_TESTS = "test_result_tests"
        const val TEST_RESULT_REASON = "test_result_reason"
        const val TEST_RESULT_DOC_LIST = "test_result_doc_list"
        const val TEST_RESULT_IMAGES = "test_result_images"
        const val TEST_RESULT_PROCESSED_TEST_MAP = "test_result_processed_test_map"
        const val TEST_RESULT_DISPOSITION_NAMES = "test_result_disposition_names"
        const val TEST_RESULT_POLICY_TYPES = "test_result_policy_types"

        const val TEST_RESULT_SAVED_INVENTORY = "test_result_saved_inventory"
        const val TEST_RESULT_INVENTORY_UPDATED = "test_result_inventory_updated"


        fun launch(
            activity: Activity,
            testInventory: TestInventory,
            requestCode: Int
        ) {
            val intent = Intent(activity, TestActivity::class.java)
            intent.putExtra(EXTRA_KEY_INVENTORY, testInventory)
            activity.startActivityForResult(intent, requestCode)
        }

        fun launch(
            fragment: Fragment,
            testInventory: TestInventory,
            requestCode: Int
        ) {
            val intent = Intent(fragment.requireActivity(), TestActivity::class.java)
            intent.putExtra(EXTRA_KEY_INVENTORY, testInventory)
            fragment.startActivityForResult(intent, requestCode)
        }
    }

    @Throws(RuntimeException::class)
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.hasExtra(EXTRA_KEY_INVENTORY)) {
            getViewModel().testInventory = intent.getParcelableExtra(EXTRA_KEY_INVENTORY)!!
        } else {
            throw RuntimeException("Inventory Not Provided")
        }
    }

    override fun onResume() {
        super.onResume()
        //       supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)//your icon here
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun setupNavGraph(navController: androidx.navigation.NavController) {
        navController.graph = navController.navInflater.inflate(R.navigation.test_nvaigation)
    }

    override fun getViewModelFactory(): ViewModelFactory {
        return mViewModelFactory
    }

    override fun attachLiveData() {

    }
}
