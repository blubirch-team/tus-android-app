package com.blubirch.tus.testing.ui

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.data.models.ImageHolder
import com.blubirch.tus.testing.data.models.Test
import com.blubirch.tus.testing.databinding.DialogInventoryDescriptionBinding
import com.blubirch.tus.testing.ui.adapter.GradingGridAdapter
import com.blubirch.tus.testing.ui.adapter.TestOutput
import com.blubirch.tus.testing.ui.adapter.TestPagerAdapter
import com.blubirch.tus.testing.ui.annotation.AnnotationActivity
import com.blubirch.tus.testing.ui.customviews.Coordinate
import com.blubirch.core.data.MyException
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.gone
import com.blubirch.core.utility.materialDialog
import com.blubirch.core.utility.visible
import kotlinx.android.synthetic.main.fragment_inventory_testing.*
import kotlinx.android.synthetic.main.layout_grading_summary.*
import kotlinx.android.synthetic.main.layout_inventory_test.*
import kotlinx.android.synthetic.main.layout_inwarding_attribute.*
import javax.inject.Inject


class InventoryTestingFragment : BaseFragment<InventoryTestingViewModel>() {
    companion object {
        fun newInstance() =
            InventoryTestingFragment()

        private val TAG = InventoryTestingFragment::class.java.simpleName
        private const val ANNOTATION_RESULT = 1000
    }

    private var descriptionDialog: Dialog? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: InventoryTestingViewModel
    private lateinit var testViewModel: TestViewModel
    private var dataChangeObserver: RecyclerView.AdapterDataObserver? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_inventory_testing, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(InventoryTestingViewModel::class.java)
        testViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(TestViewModel::class.java)
        viewModel.progressLiveData.postValue(true)
        setViews()
        viewModel.setSelectedInventory(testViewModel.testInventory)
        viewModel.fetchData()
    }

    private fun setViews() {
        cvInWarding.gone()
        setupAdapter()
        setUpClickListener()
    }

    private fun setupAdapter() {
        scrollView.isNestedScrollingEnabled = false
        if (dataChangeObserver == null) {
            dataChangeObserver = object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    super.onItemRangeInserted(positionStart, itemCount)
                    onNewTestAdded()
                }
            }
        }
        val testChangeListener: (Test) -> Unit = {
            getViewModel().lastTest.postValue(it)
            getViewModel().fetchData()
            setNextButtonState()
        }
        val addImageClickListener: (Int, Test) -> Unit = { _, test ->
            addImageForTest(test)
        }
        val imageClickListener: (ImageHolder) -> Unit = {
            showImageForTest(it)
        }
        val imageRemoveListener: (ImageHolder) -> Unit = {
            setNextButtonState()
        }

        testPager.apply {
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            isUserInputEnabled = false //to disable swipe
            offscreenPageLimit = 3
            if (adapter == null) {
                adapter =
                    TestPagerAdapter(
                        testChangeListener, addImageClickListener,
                        imageClickListener, imageRemoveListener
                    )
            }
            if (dataChangeObserver != null) {
                adapter?.registerAdapterDataObserver(dataChangeObserver!!)
            }
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    btPrev.isEnabled = (position != 0)
                    setNextButtonState()
                }
            })
        }

        //setup grading summary
        rvGrading.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter =
                GradingGridAdapter()
        }

    }

    private fun onNewTestAdded() {
        getViewModel().lastTest.let {
            val item = it.value
            item?.selectedOption?.let { selectedOption ->
                if(selectedOption.pictureRequired.not()) {
                    moveToNext()
                }
                it.postValue(null)
            }
            setNextButtonState()
        }
    }

    override fun onResume() {
        super.onResume()
        showBackButton()
    }

    override fun onBackPressed(): Boolean {
        materialDialog(message = R.string.test_back_confirm,
            title = R.string.confirm,
            positiveText = R.string.yes,
            negativeText = R.string.no,
            positiveClickListener = {
                requireActivity().finish()
                it.dismiss()
            })
        return true
    }

    private fun addImageForTest(test: Test) {
        Intent(activity, AnnotationActivity::class.java).apply {
            putExtra(AnnotationActivity.INTENT_EXTRA_STN_NUMBER, testViewModel.testInventory.gatePassNumber)
            putExtra(AnnotationActivity.INTENT_EXTRA_ARTICLE_ID, testViewModel.testInventory.sku)

            val selectedOption = test.selectedOption
            val annotations = selectedOption?.annotations
            if(selectedOption != null) {
                if (selectedOption.annotationRequired && annotations != null) {
                    putStringArrayListExtra(AnnotationActivity.INTENT_EXTRA_ANNOTATIONS_FOR_IMAGE, annotations)
                }
                putStringArrayListExtra(AnnotationActivity.INTENT_EXTRA_SIDES_FOR_IMAGE, selectedOption.pictureLabels)
            }
            startActivityForResult(
                this,
                ANNOTATION_RESULT
            )
        }

    }

    private fun showImageForTest(imageHolder: ImageHolder) {
        Intent(activity, AnnotationActivity::class.java).apply {
            putExtra(
                AnnotationActivity.INTENT_EXTRA_OUTPUT_IMAGE,
                imageHolder.imageSrc
            )
            putParcelableArrayListExtra(
                AnnotationActivity.INTENT_EXTRA_OUTPUT_COORDINATES_FOR_IMAGE,
                imageHolder.coordinatesList
            )
            startActivity(this)
        }
    }


    private fun setNextButtonState() {
        testPager.apply {
            (adapter as TestPagerAdapter?)?.run {
                btNext.isEnabled = getItem(currentItem).let {
                    val selectedOption = it.selectedOption
                    selectedOption != null && (selectedOption.pictureRequired.not() || it.imageHolders.isNotEmpty())
                }
            }
        }
    }

    private fun setUpClickListener() {
        btNext.setOnClickListener {
            moveToNext()
        }
        btPrev.setOnClickListener {
            if (testPager.currentItem > 0)
                testPager.setCurrentItem(testPager.currentItem - 1, true)
        }

        headerInwarding.setIconClickListener(View.OnClickListener {
            openDescriptionDialog()
        })

    }

    private fun moveToNext() {
        if (testPager.currentItem < (((testPager.adapter?.itemCount) ?: 0) - 1)) {
            testPager.setCurrentItem(testPager.currentItem + 1, true)
        } else if (testPager.currentItem == (((testPager.adapter?.itemCount) ?: 0) - 1)) {
            updateSelectedTestAndImages()
            if (testViewModel.testInventory.returnReasonRequired) {
                testViewModel.let {
                    it.returnReasons.clear()
                    it.returnReasons.addAll(viewModel.returnReasons)
                    it.updateDocNames(viewModel.docTypes)
                }
                navigate(InventoryTestingFragmentDirections.actionItemTestingFragmentToReturnReasonFragment())
            } else
                calculateGradeAndDisposition()
        }
    }

    private fun calculateGradeAndDisposition() {
        testViewModel.findGradeAndDisposition {
            viewModel.postAPIError(it)
        }
    }

    private fun updateSelectedTestAndImages() {
        val tests = arrayListOf<Test>()
        val imageHolders = ArrayList<ImageHolder>()

        val itemCount = testPager.adapter?.itemCount ?: 0
        (testPager.adapter as? TestPagerAdapter)?.apply {
            tests.addAll(data)
            for (position in 0 until itemCount) {
                getItem(position).imageHolders.let {
                    it.let { it1 -> imageHolders.addAll(it1) }
                }
            }
        }
        testViewModel.tests = tests
        testViewModel.imageHolders = imageHolders

    }

    private fun openDescriptionDialog() {
        descriptionDialog = MaterialDialog(requireContext(), BottomSheet(LayoutMode.WRAP_CONTENT))
            .show {
                val view =
                    LayoutInflater.from(context)
                        .inflate(R.layout.dialog_inventory_description, null)
                customView(view = view, noVerticalPadding = false, horizontalPadding = true)
                cornerRadius(16f)
                val binding = DialogInventoryDescriptionBinding.bind(view)
                setUpDialogData(binding)
            }
    }

    private fun setUpDialogData(binding: DialogInventoryDescriptionBinding) {
        getViewModel().itemLiveData.value?.apply {

            binding.apply {
                headerView.setIcon(R.drawable.ic_attach_doc_delete)
                headerView.setOnClickListener {
                    if (descriptionDialog?.isShowing == true)
                        descriptionDialog?.dismiss()
                }
//                hlvCategory.value = categoryL3
//                hlvBrand.value = brand
//                hlvModel.value = model
//                hlvSubCategory.value = subModel
            }
        }
    }

    override fun getViewModel() = viewModel

    override fun attachLiveData() {
        viewModel.apply {

            testLiveData.observe(viewLifecycleOwner, Observer { tests ->
                viewModel.progressLiveData.postValue(false)
                if (tests == null) {
                    navController.navigateUp()
                } else {
                    (testPager?.adapter as TestPagerAdapter?)?.apply {
                        if (data.isNullOrEmpty()) {
                            setNewInstance(tests.toMutableList())
                            testPager.setCurrentItem(tests.size - 1, false)
                        } else {
                            setDiffNewData(tests.toMutableList())
                            notifyItemRangeChanged(0, tests.size)
                        }
                        updateGrading(tests)
                    }
                }
            })
            testViewModel.responseLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    testViewModel.responseLiveData.value = null
                    navigate(InventoryTestingFragmentDirections.actionItemTestingFragmentToDispositionResultFragment())
                }
            })
            errorLiveData.observe(viewLifecycleOwner, Observer { exception ->
                exception?.printStackTrace()
                if (exception is MyException.ApiError) {
                    navController.navigateUp()
                }
            })
        }
        updateInwarding()
        testViewModel.errorLiveData.observe(viewLifecycleOwner, Observer {
            viewModel.errorLiveData.postValue(it)
        })

    }


    private fun updateGrading(tests: List<Test>?) {
        cvGrading.visible()
        val newList = mutableListOf<TestOutput>().apply {
            clear()
            tests?.forEach { test ->
                test.apply {
                    if (selectedOption?.value != null)
                        add(
                            TestOutput(
                                testType,
                                name,
                                selectedOption!!.value
                            )
                        )
                }
            }
        }
        (rvGrading.adapter as? GradingGridAdapter)?.apply {
            setDiffNewData(newList)
            notifyItemRangeChanged(0, newList.size)
        }

    }

    private fun updateInwarding() {
        cvInWarding.visible()
        testViewModel.testInventory.apply {
            setToolbarTitle(getString(R.string.grading_s))
            lvSku.value = sku
            lvDescription.value = desc
            lvSerialNo.value = serialNumber
            lvBrand.value = brand
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ANNOTATION_RESULT) {
                data?.extras?.also { bundle ->
                    (testPager.adapter as? TestPagerAdapter)?.apply {
                        val position = testPager.currentItem

                        val coordinateList =
                            bundle.getParcelableArrayList<Coordinate>(AnnotationActivity.INTENT_EXTRA_OUTPUT_COORDINATES_FOR_IMAGE)
                        var imageWidth = 0
                        var imageHeight = 0
                        coordinateList?.let {
                            if (it.size > 0) {
                                imageWidth = it[0].imageWidth
                                imageHeight = it[0].imageHeight
                            }
                        }
                        val imageHolder = ImageHolder(imageWidth = imageWidth, imageHeight = imageHeight)
                        imageHolder.originalImageSrc =
                            bundle.getString(AnnotationActivity.INTENT_EXTRA_ORIGINAL_IMAGE, "")
                        imageHolder.imageSrc =
                            bundle.getString(AnnotationActivity.INTENT_EXTRA_OUTPUT_IMAGE, "")
                        imageHolder.annotatedImageSrc =
                            bundle.getString(AnnotationActivity.INTENT_EXTRA_ANNOTATED_IMAGE, "")
                        imageHolder.side = bundle.getString(
                            AnnotationActivity.INTENT_EXTRA_OUTPUT_SIDE_FOR_IMAGE,
                            ""
                        )

                        getItem(position).apply {
                            if (imageHolder.side.isBlank()) {
                                imageHolder.side = testType.trim().substring(0, 4)
                            }
                            imageHolders.add(imageHolder)
                        }
                        notifyItemChanged(position)
                        testViewModel.uploadImage(imageHolder) {
                            viewModel.postAPIError(it)
                        }
                        coordinateList?.forEach { coordinates ->
                                imageHolder.coordinatesList.add(coordinates)
                            }
                    }

                }
            }
            setNextButtonState()
        }
    }
}
