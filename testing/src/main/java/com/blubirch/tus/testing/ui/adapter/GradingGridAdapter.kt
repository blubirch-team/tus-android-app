package com.blubirch.tus.testing.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.databinding.ItemGradingSummaryBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class GradingGridAdapter : BaseQuickAdapter<TestOutput, BaseViewHolder>(
    R.layout.item_grading_summary,
    arrayListOf()
) {
    init {
        setDiffCallback(TestSelectionDiffCallBack())
    }

    override fun convert(holder: BaseViewHolder, item: TestOutput) {
        val binding = ItemGradingSummaryBinding.bind(holder.itemView)
        if (item.name.isBlank())
            binding.tvName.text = item.value
        else
            binding.tvName.text = (item.name + ": " + item.value)
    }
}

class TestSelectionDiffCallBack : DiffUtil.ItemCallback<TestOutput>() {
    override fun areItemsTheSame(oldItem: TestOutput, newItem: TestOutput): Boolean {
        return oldItem.testType == newItem.testType && oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: TestOutput, newItem: TestOutput): Boolean {
        return oldItem.testType == newItem.testType && oldItem.name == newItem.name && oldItem.value == newItem.value
    }
}

data class TestOutput(var testType: String, var name: String, var value: String)