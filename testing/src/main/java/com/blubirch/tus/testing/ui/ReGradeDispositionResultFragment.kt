package com.blubirch.tus.testing.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.tus.testing.R
import com.blubirch.tus.testing.data.api.dto.Disposition
import com.blubirch.tus.testing.data.models.ImageHolder
import com.blubirch.tus.testing.ui.adapter.GradingGridAdapter
import com.blubirch.tus.testing.ui.adapter.TestImageAdapter
import com.blubirch.tus.testing.ui.adapter.TestOutput
import com.blubirch.tus.testing.ui.annotation.AnnotationActivity
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.gone
import com.blubirch.core.utility.visible
import kotlinx.android.synthetic.main.fragment_re_grade_disposition_result.*
import javax.inject.Inject


class ReGradeDispositionResultFragment : BaseFragment<RegradingDispositionResultViewModel>() {
    private lateinit var viewModel: RegradingDispositionResultViewModel
    private lateinit var testViewModel: TestViewModel

    companion object {
        fun newInstance(): ReGradeDispositionResultFragment {
            return ReGradeDispositionResultFragment()
        }

        private const val REQUEST_CODE_TOAT = 1333
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_re_grade_disposition_result, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProvider(
                this,
                viewModelFactory
            ).get(RegradingDispositionResultViewModel::class.java)

        testViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(TestViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
        setupInput()
    }

    private fun setupInput() {
        silToatNumber.initialize(this, REQUEST_CODE_TOAT)
    }

    private fun setupList() {
        setupImageList()
        setupGradingSummaryList()
    }

    private fun setupManualDispositionList() {
        spDisposition.visible()
        tvDisposition.gone()
        spDisposition.adapter = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_list_item_1,
            testViewModel.dispositionNames!!
        )
        if (viewModel.testInventory.status.equals("Pending Repair Grade", true)) {

            if (viewModel.grade.equals("A1", true)) {
                testViewModel.dispositionNames?.let {
                    if (it.contains(getString(R.string.disposition_redeploy))) {
                        spDisposition.post {
                            spDisposition.setSelection(it.indexOf(getString(R.string.disposition_redeploy)))
                        }
                    }
                }
            } else {
                testViewModel.dispositionNames?.let {
                    if (it.contains(getString(R.string.disposition_liquidation))) {
                        spDisposition.post {
                            spDisposition.setSelection(it.indexOf(getString(R.string.disposition_liquidation)))
                        }
                    }
                }
            }
        }
    }

    private fun setupImageList() {
        rvAttachedImages.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            if (adapter == null) {
                adapter = TestImageAdapter {
                    showImageForTest(it)
                }
            }
        }
    }

    private fun setupGradingSummaryList() {
        rvGrading.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = GradingGridAdapter()
        }
    }

    override fun onResume() {
        super.onResume()
        setupListeners()
    }

    private fun setupListeners() {
        printTag.setOnClickListener {
            viewModel.testInventory.tagNumber?.let { it1 -> showPrintTagView(it1) }
        }
        btContinue.setOnClickListener {
            getViewModel().saveAndContinue(silToatNumber.text.toString(), spPolicy.text.toString())
        }
        if (spDisposition.isEnabled) {
            spDisposition.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val disposition = spDisposition.selectedItem as String
                    viewModel.disposition = Disposition(
                        disposition
                    )
                    if (disposition!!.contains("liquidation", true)) {
                        showPolicy()
                    } else {
                        hidePolicy()
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
//                    regradingListViewModel.disposition = null
                }
            }
        }
        spPolicy.setOnItemClickListener { parent, view, position, id ->
            silPolicy.error = null
        }
    }

    override fun getViewModel(): RegradingDispositionResultViewModel {
        return viewModel
    }

    override fun attachLiveData() {

        viewModel.apply {
            testInventory = testViewModel.testInventory
            tests = testViewModel.tests
            imageHolders = testViewModel.imageHolders
            grade = testViewModel.grade
            gradingResult = testViewModel.gradingResult
            disposition = testViewModel.disposition
            policyTypes = testViewModel.policyTypes
            updateUI()
            responseLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    onSaved()
//                        showSnackBar(getString(R.string.msg_regraded_successfully))
//                        navigate(
//                            ReGradeDispositionResultFragmentDirections.actionReGradingDispositionFragmentToReGradingFragment()
//                        )
                }
            })
            toatNumberErrorLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    silToatNumber.setError(it.message)
                    silToatNumber.editText.requestFocus()
                }
            })
            policyErrorLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    silPolicy.error = it.message
                }
            })
        }

    }

    private fun updateUI() {
        updateGradeDisposition()
        updateManualDispositionPolicy()
        updateImages()
        updateGradingSummary()
    }

    private fun updateManualDispositionPolicy() {
        if (!viewModel.testInventory!!.status.contains("liquidation", true)) {
            setupManualDispositionList()
        } else {
            showPolicy()
        }
    }

//
//    private fun updateManualDispositionPolicy() {
//        if (viewModel.testInventory.status.contains("liquidation", true)) {
//            showPolicy()
//            tvDisposition.setText(R.string.disposition_liquidation)
//        } else if (viewModel.testInventory.status.equals("Pending Repair Grade", true)) {
//            tvDisposition.setText(R.string.disposition_repair)
//        } else {
//            setupManualDispositionList()
//        }
//    }

    private fun onSaved() {
        requireActivity().setResult(
            Activity.RESULT_OK,
            Intent().apply {
                putExtra(TestActivity.TEST_RESULT_INVENTORY_UPDATED, true)
            }
        )
        requireActivity().finish()
    }

    private fun updateGradeDisposition() {
        tvItemId.text = viewModel.testInventory!!.tagNumber
        tvGrade.text = viewModel.grade
    }

    private fun hidePolicy() {
        silPolicy.gone()
        getViewModel().policyRequired = false
    }

    private fun showPolicy() {
        silPolicy.visible()
        val list: ArrayList<String> = arrayListOf()
        list.addAll(viewModel.policyTypes!!.keys)
        spPolicy.setAdapter(
            ArrayAdapter<String>(requireContext(), android.R.layout.simple_list_item_1, list)
        )
        getViewModel().policyRequired = true
    }

    private fun updateImages() {
        (rvAttachedImages.adapter as TestImageAdapter).apply {
            setImages(viewModel.imageHolders)
            setViewOnly(true)
        }
    }

    private fun updateGradingSummary() {
        val newList = mutableListOf<TestOutput>().apply {
            clear()
            viewModel.tests.forEach { test ->
                test.apply {
                    if (selectedOption?.value != null)
                        add(
                            TestOutput(
                                testType,
                                name,
                                selectedOption!!.value
                            )
                        )
                }
            }
        }
        (rvGrading.adapter as? GradingGridAdapter)?.apply {
            setDiffNewData(newList)
            notifyItemRangeChanged(0, newList.size)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_TOAT -> {
                    silToatNumber.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
    }

    private fun showImageForTest(imageHolder: ImageHolder) {
        Intent(activity, AnnotationActivity::class.java).apply {
            putExtra(
                AnnotationActivity.INTENT_EXTRA_OUTPUT_IMAGE,
                imageHolder.imageSrc
            )
            putParcelableArrayListExtra(
                AnnotationActivity.INTENT_EXTRA_OUTPUT_COORDINATES_FOR_IMAGE,
                imageHolder.coordinatesList
            )
            startActivity(this)
        }
    }
}
