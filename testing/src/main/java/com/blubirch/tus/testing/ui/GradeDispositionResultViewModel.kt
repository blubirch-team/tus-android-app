package com.blubirch.tus.testing.ui

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.testing.data.api.dto.Disposition
import com.blubirch.tus.testing.data.api.dto.SaveTestedReturnDTO
import com.blubirch.tus.testing.data.models.Test
import com.blubirch.tus.testing.data.models.TestInventory
import com.blubirch.tus.testing.data.models.getTestSelectionMap
import com.blubirch.tus.testing.data.repository.TestRepository
import com.blubirch.core.data.MyException
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class GradeDispositionResultViewModel @Inject constructor() : BaseViewModel() {

    var inventoryId: Long = 0L

    @Inject
    lateinit var testRepository: TestRepository

    //    lateinit var gatePass: GatePass
    lateinit var testInventory: TestInventory
    var serialNumber: String? = null
    var serialNumber2: String? = null

    //Selected Tests, Images taken, document list and Grade & Disposition
    var tests: ArrayList<Test> = arrayListOf()
    var reason: String? = ""
    var docList: ArrayList<AttachedDoc>? = arrayListOf()
    var gradingResult: HashMap<String, String>? = null
    var grade: String = ""
    var disposition: Disposition? = null
    var manualDisposition: String? = null
    var policyTypes: HashMap<String, String>? = null
    var policyType: String? = null
    var policyRequired: Boolean = false

    val tagIdLiveData by lazy { MutableLiveData<String>() }

    fun generateTag() {
        progressLiveData.postValue(true)
        compositeDisposable.add(
            testRepository.generateTag()
                .subscribeOn(Schedulers.io())
                .subscribe { tagDto, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        tagIdLiveData.postValue(tagDto.tagNumber)
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }

    val tagIdErrorLiveData by lazy { MutableLiveData<Exception>() }
    val toatNumberErrorLiveData by lazy { MutableLiveData<Exception>() }
    val policyErrorLiveData by lazy { MutableLiveData<Exception>() }

    fun saveAndContinue(tagId: String, toatNumber: String, srNumber: String, policy: String) {
        var validate = true
        if (tagId == null || tagId.isBlank()) {
            validate = false
            tagIdErrorLiveData.value = Exception("Invalid TagId")
        }
        if (toatNumber == null || toatNumber.isBlank()) {
            validate = false
            toatNumberErrorLiveData.value = Exception("Invalid Toat Number")
        }
        if (policyRequired && (policy == null || policy.isBlank())) {
            validate = false
            policyErrorLiveData.value = Exception("Select Policy")
        }
        updatePolicy(policy)
        if (validate) {
            progressLiveData.value = true
            saveApiCall(tagId, toatNumber, srNumber, policyType)
        }
    }

    val responseLiveData by lazy { MutableLiveData<SaveTestedReturnDTO>() }
    private fun saveApiCall(tagId: String, toatNumber: String, srNumber: String, policy: String?) {
        if (disposition == null) {
            disposition = Disposition()
        }
        compositeDisposable.add(
            testRepository.saveTestedReturn(
                testInventory,
                reason,
                docList,
                srNumber,
                tagId,
                toatNumber,
                manualDisposition,
                getTestSelectionMap(tests),
                grade,
                gradingResult,
                disposition!!.disposition,
                disposition!!.flow, policy
            )
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    if (throwable == null && result.status == 200) {
                        if (testInventory.id == 0L) {
                            testInventory.id = result.result.id
                            testInventory.status = result.result.status
                            testInventory.distributionCenterId =
                                result.result.distributionCenterId
                            testInventory.category = result.result.clientCategoryName
                        }
                        responseLiveData.postValue(result)
                    } else if (throwable != null) {
                        postAPIError(throwable)
                    } else {
                        errorLiveData.postValue(MyException.ApiError(Throwable(result.message)))
                    }
                }
        )
    }

    private fun updatePolicy(policy: String) {
        policyType = policyTypes?.get(policy)
    }
}
