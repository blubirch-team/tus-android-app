package com.blubirch.tus.testing.data.repository;


import androidx.annotation.Nullable;

import com.blubirch.tus.testing.data.api.dto.GradeDispositionRequestDTO;
import com.blubirch.tus.testing.data.api.dto.InventoryDataDTO;
import com.blubirch.tus.testing.data.api.dto.InwardGradeDispositionRequestDTO;
import com.blubirch.tus.testing.data.api.dto.ReGradeInventoryDTO;
import com.blubirch.tus.testing.data.api.dto.ReturnReasonDTO;
import com.blubirch.tus.testing.data.api.dto.SaveTestedReturnDTO;
import com.blubirch.tus.testing.data.api.dto.TagDTO;
import com.blubirch.tus.testing.data.api.dto.TestResultResponseDTO;
import com.blubirch.tus.testing.data.api.dto.UploadRequestDTO;
import com.blubirch.tus.testing.data.api.dto.UploadResponseDTO;
import com.blubirch.tus.testing.data.api.interfaces.TestAPI;
import com.blubirch.tus.testing.data.models.TestInventory;
import com.blubirch.tus.testing.data.models.TestRule;
import com.blubirch.tus.testing.data.models.TestSelection;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TestRepositoryImpl implements TestRepository {

    private TestAPI testAPI;

    @Inject
    public TestRepositoryImpl(final TestAPI testAPI) {
        this.testAPI = testAPI;
    }

    @Override
    public Single<TestRule> getTestRule(final long categoryId, String gradingType) {
//        return testAPI.fetchTestRule(categoryId, gradingType); // Rajeev C
        return testAPI.fetchTestRule(categoryId);
    }

    @Override
    public Single<ReturnReasonDTO> getReturnReasons(boolean ownLabel) {
        return testAPI.getReturnReason(/*ownLabel*/);
    }

    @Override
    public Single<UploadResponseDTO> uploadImage(UploadRequestDTO uploadRequestDTO) {
        return testAPI.uploadImage(uploadRequestDTO);
    }

    @Override
    public Single<TestResultResponseDTO> calculateGradeDispositionInward(TestInventory testInventory,
                                                                         final HashMap<String, ArrayList<TestSelection>> testSelectionListMap) {
        return testAPI.calculateGradeDispositionInward(new InwardGradeDispositionRequestDTO(testInventory, testSelectionListMap));
    }

    @Override
    public Single<TestResultResponseDTO> calculateGradeDisposition(TestInventory testInventory,
                                                                   final HashMap<String, ArrayList<TestSelection>> testSelectionListMap) {
        return testAPI.calculateGradeDisposition(new GradeDispositionRequestDTO(testInventory, testSelectionListMap));
    }

    @Override
    public Single<TagDTO> generateTag() {
        return testAPI.generateTag();
    }

    @Override
    public Single<SaveTestedReturnDTO> saveTestedReturn(
            TestInventory gatePassInventory, String returnReason, @Nullable ArrayList<AttachedDoc> documents,
            String srNumber, String tagNumber, String toatNumber, String manualDisposition,
            HashMap<String, ArrayList<TestSelection>> testSelectionListMap, String grade,
            HashMap<String, String> gradingResult, String disposition, String workFlow, String policy) {

        InventoryDataDTO inventoryDataDTO = new InventoryDataDTO(
                gatePassInventory.getGatePassNumber(), gatePassInventory.getId(),
                gatePassInventory.getSku(), gatePassInventory.getSerialNumber(),
                gatePassInventory.getSerialNumber2(), srNumber, tagNumber, toatNumber, manualDisposition,
                testSelectionListMap, gradingResult, grade, returnReason, disposition, workFlow, policy);

        Map<String, RequestBody> bodyHashMap = new HashMap<>();
        bodyHashMap.put("inventory_data", RequestBody.create(MultipartBody.FORM, new Gson().toJson(inventoryDataDTO)));
        List<MultipartBody.Part> documentsPart = new ArrayList<>();
        int index = 0;
        if(documents != null) {
            for (AttachedDoc attachedDoc : documents) {
                if(attachedDoc != null) {
                    bodyHashMap.put("documents[" + index + "][code]", RequestBody.create(MultipartBody.FORM, attachedDoc.getDocKey()));
                    bodyHashMap.put("documents[" + index + "][reference_number]", RequestBody.create(MultipartBody.FORM, attachedDoc.getReferenceNo()));

                    File file = new File(attachedDoc.getFileAbsolutePath());
                    // Parsing any Media type file
                    RequestBody fileRequestBody = RequestBody.create(MediaType.parse("*/*"), file);
                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("documents[" + index + "][document]", file.getName(), fileRequestBody);

                    documentsPart.add(fileToUpload);
                    index++;
                }
            }
        }
        return testAPI.saveTestedReturn(bodyHashMap, documentsPart);
    }


    @Override
    public Single<BaseDTO> saveGradeAndDispositionResult(
            Long inventoryId, String toatNumber, HashMap<String, ArrayList<TestSelection>> testSelectionListMap,
            String grade, HashMap<String, String> gradingResult, String disposition, String workFlow, String policy) {

        ReGradeInventoryDTO reGradeInventoryDTO = new ReGradeInventoryDTO(
                inventoryId, toatNumber, testSelectionListMap, gradingResult, grade, disposition, workFlow, policy);

        return testAPI.storeInventoryGrade(reGradeInventoryDTO);
    }

}
