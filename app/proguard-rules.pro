# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


-keepattributes LineNumberTable,Signature,*Annotation*

-keep class sun.misc.Unsafe { *; }

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keepclassmembernames interface * {
    @retrofit2.http.* <methods>;
}

-keepclassmembernames interface * {
    @com.google.gson.annotations.* <methods>;
}

-keepclassmembernames interface * {
    @com.google.gson.annotations.* <fields>;
}

-keep class com.blubirch.tus.data.api.dto.** { *; }
-keep class com.blubirch.tus.data.model.** { *; }
-keep class com.blubirch.tus.testing.data.api.dto.** { *; }
-keep class com.blubirch.tus.testing.data.models.** { *; }
-keep class com.blubirch.core.data.api.dto.** { *; }
-keep class com.blubirch.core.data.model.** { *; }

-keepclassmembers enum * { *; }