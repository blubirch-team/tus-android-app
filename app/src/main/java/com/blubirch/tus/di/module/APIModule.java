    package com.blubirch.tus.di.module;

import com.blubirch.tus.data.api.interfaces.DispatchAPI;
import com.blubirch.tus.data.api.interfaces.GatePassAPI;
import com.blubirch.tus.data.api.interfaces.ItemInfoAPI;
import com.blubirch.tus.data.api.interfaces.PackAPI;
import com.blubirch.tus.data.api.interfaces.PickAPI;
import com.blubirch.tus.data.api.interfaces.PickPackAPI;
import com.blubirch.tus.data.api.interfaces.ReGradingAPI;
import com.blubirch.tus.data.api.interfaces.StowingAPI;
import com.blubirch.core.di.modules.CoreAPIModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {CoreAPIModule.class})
public class APIModule {

    @Singleton
    @Provides
    public GatePassAPI providesGatePassAPI(Retrofit retrofit) {
        return retrofit.create(GatePassAPI.class);
    }

    @Singleton
    @Provides
    public StowingAPI providesStowingAPI(Retrofit retrofit) {
        return retrofit.create(StowingAPI.class);
    }

    @Singleton
    @Provides
    public PickPackAPI providesPickPackAPI(Retrofit retrofit) {
        return retrofit.create(PickPackAPI.class);
    }

    @Singleton
    @Provides
    public PickAPI providesPickAPI(Retrofit retrofit) {
        return retrofit.create(PickAPI.class);
    }

    @Singleton
    @Provides
    public PackAPI providesPackAPI(Retrofit retrofit) {
        return retrofit.create(PackAPI.class);
    }

    @Singleton
    @Provides
    public DispatchAPI providesDispatchAPI(Retrofit retrofit) {
        return retrofit.create(DispatchAPI.class);
    }

    @Singleton
    @Provides
    public ReGradingAPI providesReGradingAPI(Retrofit retrofit) {
        return retrofit.create(ReGradingAPI.class);
    }

    @Singleton
    @Provides
    public ItemInfoAPI providesItemInfoAPI(Retrofit retrofit) {
        return retrofit.create(ItemInfoAPI.class);
    }

}
