package com.blubirch.tus.di.module;

import android.app.Application;
import android.content.Context;

import com.blubirch.tus.WMSApp;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ApplicationModule {

    @Binds
    abstract Context bindContext(WMSApp posApp);

    @Singleton
    @Binds
    abstract Application bindApplication(WMSApp posApp);
}