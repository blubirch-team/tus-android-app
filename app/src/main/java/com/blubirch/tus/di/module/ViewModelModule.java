package com.blubirch.tus.di.module;

import androidx.lifecycle.ViewModel;

import com.blubirch.tus.testing.di.TestModule;
import com.blubirch.tus.MainViewModel;
import com.blubirch.tus.ui.MenuViewModel;
import com.blubirch.tus.ui.PendingReceiptViewModel;
import com.blubirch.tus.ui.dispatch.DispatchDetailsViewModel;
import com.blubirch.tus.ui.dispatch.DispatchListViewModel;
import com.blubirch.tus.ui.dispatch.DispatchViewModel;
import com.blubirch.tus.ui.dispatch.adapters.DispatchGatepassConfirmViewModel;
import com.blubirch.tus.ui.inward.GatePassDetailsViewModel;
import com.blubirch.tus.ui.inward.GatePassSearchViewModel;
import com.blubirch.tus.ui.inward.GrnSummaryViewModel;
import com.blubirch.tus.ui.inward.InwardItemViewModel;
import com.blubirch.tus.ui.inward.InwardViewModel;
import com.blubirch.tus.ui.inward.UpdateGrnViewModel;
import com.blubirch.tus.ui.iteminfo.ItemInfoViewModel;
import com.blubirch.tus.ui.pack.PackItemListViewModel;
import com.blubirch.tus.ui.pack.PackListViewModel;
import com.blubirch.tus.ui.pack.PackViewModel;
import com.blubirch.tus.ui.pick.PickItemListViewModel;
import com.blubirch.tus.ui.pick.PickListViewModel;
import com.blubirch.tus.ui.pick.PickMainViewModel;
import com.blubirch.tus.ui.pick.PickViewModel;
import com.blubirch.tus.ui.pickpack.PickPackOrderListViewModel;
import com.blubirch.tus.ui.pickpack.PickPackOrderViewModel;
import com.blubirch.tus.ui.pickpack.PickPackViewModel;
import com.blubirch.tus.ui.regrading.RegradingListViewModel;
import com.blubirch.tus.ui.stowing.SearchToatViewModel;
import com.blubirch.tus.ui.stowing.StowingViewModel;
import com.blubirch.tus.ui.stowing.ToatTagListViewModel;
import com.blubirch.core.di.ViewModelKey;
import com.blubirch.core.di.modules.AttachDocModule;
import com.blubirch.core.di.modules.ForgotPasswordDataModule;
import com.blubirch.core.di.modules.ForgotPasswordModule;
import com.blubirch.core.di.modules.LoginModule;
import com.blubirch.core.presentation.customview.attachdoc.AttachDocViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module(includes = {LoginModule.class, TestModule.class, AttachDocModule.class,
        ForgotPasswordModule.class, ForgotPasswordDataModule.class})
abstract public class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MenuViewModel.class)
    abstract ViewModel bindMenuViewModel(MenuViewModel menuViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(InwardViewModel.class)
    abstract ViewModel bindInwardViewModel(InwardViewModel inwardViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(GatePassSearchViewModel.class)
    abstract ViewModel bindGatePassSearchViewModel(GatePassSearchViewModel gatePassSearchViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(GatePassDetailsViewModel.class)
    abstract ViewModel bindGatePassDetailsViewModel(GatePassDetailsViewModel gatePassDetailsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(InwardItemViewModel.class)
    abstract ViewModel bindInwardItemViewModel(InwardItemViewModel inwardItemViewModel);

       @Binds
    @IntoMap
    @ViewModelKey(GrnSummaryViewModel.class)
    abstract ViewModel bindGrnSummaryViewModel(GrnSummaryViewModel grnSummaryViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(UpdateGrnViewModel.class)
    abstract ViewModel bindGradeUpdateGrnViewModel(UpdateGrnViewModel updateGrnViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StowingViewModel.class)
    abstract ViewModel bindStowingViewModel(StowingViewModel stowingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SearchToatViewModel.class)
    abstract ViewModel bindSearchToatViewModel(SearchToatViewModel searchToatViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ToatTagListViewModel.class)
    abstract ViewModel bindToatTagListViewModel(ToatTagListViewModel toatTagListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PickViewModel.class)
    abstract ViewModel bindPickViewModel(PickViewModel pickViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PickPackViewModel.class)
    abstract ViewModel bindPickPackViewModel(PickPackViewModel pickPackViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PickPackOrderListViewModel.class)
    abstract ViewModel bindPickPackOrderListViewModel(PickPackOrderListViewModel pickPackOrderListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PickPackOrderViewModel.class)
    abstract ViewModel bindPickPackOrderViewModel(PickPackOrderViewModel pickPackOrderViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PickMainViewModel.class)
    abstract ViewModel bindPickMainViewModel(PickMainViewModel pickMainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PickListViewModel.class)
    abstract ViewModel bindPickListViewModel(PickListViewModel pickListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PickItemListViewModel.class)
    abstract ViewModel bindPickItemListViewModel(PickItemListViewModel pickItemListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PackViewModel.class)
    abstract ViewModel bindPackViewModel(PackViewModel packViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DispatchViewModel.class)
    abstract ViewModel bindDispatchViewModel(DispatchViewModel dispatchViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AttachDocViewModel.class)
    abstract ViewModel bindAttachDocViewModel(AttachDocViewModel attachDocViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PendingReceiptViewModel.class)
    abstract ViewModel bindPendingReceiptViewModel(PendingReceiptViewModel pendingReceiptViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PackItemListViewModel.class)
    abstract ViewModel bindPackItemListViewModel(PackItemListViewModel packItemListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PackListViewModel.class)
    abstract ViewModel bindPackListViewModel(PackListViewModel packListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DispatchListViewModel.class)
    abstract ViewModel bindDispatchListViewModel(DispatchListViewModel dispatchListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DispatchDetailsViewModel.class)
    abstract ViewModel bindDispatchDetailsViewModel(DispatchDetailsViewModel dispatchDetailsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DispatchGatepassConfirmViewModel.class)
    abstract ViewModel bindDispatchGatepassConfirmViewModel(DispatchGatepassConfirmViewModel dispatchGatepassConfirmViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RegradingListViewModel.class)
    abstract ViewModel bindDispatchRegradingListViewModel(RegradingListViewModel regradingListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ItemInfoViewModel.class)
    abstract ViewModel bindItemInfoViewModel(ItemInfoViewModel itemInfoViewModel);

}

