package com.blubirch.tus.di.component;

import com.blubirch.tus.WMSApp;
import com.blubirch.tus.di.AndroidInjectBuilder;
import com.blubirch.tus.di.module.APIModule;
import com.blubirch.tus.di.module.ApplicationModule;
import com.blubirch.tus.di.module.AssistedViewModelModule;
import com.blubirch.tus.di.module.RedirectionModule;
import com.blubirch.tus.di.module.RepositoryModule;
import com.blubirch.tus.di.module.ViewModelModule;
import com.blubirch.core.di.ViewModelFactoryModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidInjectBuilder.class, AndroidSupportInjectionModule.class,
        ViewModelFactoryModule.class, ViewModelModule.class, RedirectionModule.class,
        APIModule.class, RepositoryModule.class, ApplicationModule.class, AssistedViewModelModule.class}
)

public interface ApplicationComponent extends AndroidInjector<WMSApp> {

    @Override
    void inject(WMSApp instance);

    @Component.Factory
    interface Factory extends AndroidInjector.Factory<WMSApp> {

    }


}
