package com.blubirch.tus.di.module;

import android.content.Intent;

import com.blubirch.tus.MainActivity;
import com.blubirch.core.presentation.login.LoginRedirection;

import dagger.Module;
import dagger.Provides;

@Module
public class RedirectionModule {
    @Provides
    LoginRedirection provideLoginRedirection() {
        return activity -> activity.startActivity(new Intent(activity, MainActivity.class));
    }
}
