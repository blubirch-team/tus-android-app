package com.blubirch.tus.di;


import com.blubirch.tus.MainActivity;
import com.blubirch.tus.di.module.MainActivityModule;
import com.blubirch.tus.ui.MenuFragment;
import com.blubirch.tus.ui.PendingReceiptFragment;
import com.blubirch.tus.ui.dispatch.DispatchDetailConfirmDialog;
import com.blubirch.tus.ui.dispatch.DispatchDetailsFragment;
import com.blubirch.tus.ui.dispatch.DispatchGatepassConfirmDialog;
import com.blubirch.tus.ui.dispatch.DispatchListFragment;
import com.blubirch.tus.ui.inward.GRNSummaryFragment;
import com.blubirch.tus.ui.inward.GatePassDetailsFragment;
import com.blubirch.tus.ui.inward.GatePassSearchFragment;
import com.blubirch.tus.ui.inward.InwardItemFragment;
import com.blubirch.tus.ui.inward.InwardSelectionFragment;
import com.blubirch.tus.ui.inward.UpdateGrnFragment;
import com.blubirch.tus.ui.iteminfo.ItemInfoDispatchDetailsFragment;
import com.blubirch.tus.ui.iteminfo.ItemInfoDispositionOptionsFragment;
import com.blubirch.tus.ui.iteminfo.ItemInfoInwardDetailsFragment;
import com.blubirch.tus.ui.iteminfo.ItemInfoOptionsFragment;
import com.blubirch.tus.ui.iteminfo.ItemInfoRTVDetailsFragment;
import com.blubirch.tus.ui.iteminfo.ItemInfoRepairDetailsFragment;
import com.blubirch.tus.ui.iteminfo.ItemInfoSearchFragment;
import com.blubirch.tus.ui.pack.PackConfirmDialog;
import com.blubirch.tus.ui.pack.PackItemListFragment;
import com.blubirch.tus.ui.pack.PackItemStatusListFragment;
import com.blubirch.tus.ui.pack.PackListFragment;
import com.blubirch.tus.ui.pack.PackMainFragment;
import com.blubirch.tus.ui.pick.PickItemListFragment;
import com.blubirch.tus.ui.pick.PickListFragment;
import com.blubirch.tus.ui.pick.PickMainFragment;
import com.blubirch.tus.ui.pickpack.PickPackOrderFragment;
import com.blubirch.tus.ui.pickpack.PickPackOrderListFragment;
import com.blubirch.tus.ui.regrading.RegradingListFragment;
import com.blubirch.tus.ui.stowing.SearchToatFragment;
import com.blubirch.tus.ui.stowing.ToatTagListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AndroidInjectBuilder {


    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindPosActivity();

    @ContributesAndroidInjector
    abstract MenuFragment bindMenuFragment();

    @ContributesAndroidInjector
    abstract GatePassSearchFragment bindGatePassSearchFragment();

    @ContributesAndroidInjector
    abstract GatePassDetailsFragment bindGatePassDetailsFragment();

    @ContributesAndroidInjector
    abstract InwardItemFragment bindInwardItemFragment();

    @ContributesAndroidInjector
    abstract PickListFragment bindPickListFragment();

    @ContributesAndroidInjector
    abstract SearchToatFragment bindSearchToatFragment();

    @ContributesAndroidInjector
    abstract ToatTagListFragment bindToatTagListFragment();

    @ContributesAndroidInjector
    abstract PickPackOrderListFragment bindPickPackOrderListFragment();

    @ContributesAndroidInjector
    abstract PickPackOrderFragment bindPickPackOrderFragment();

    @ContributesAndroidInjector
    abstract PickMainFragment bindPickMainFragment();

    @ContributesAndroidInjector
    abstract PackMainFragment bindPackMainFragment();

    @ContributesAndroidInjector
    abstract PackListFragment bindPackListFragment();

    @ContributesAndroidInjector
    abstract PackConfirmDialog bindPackConfirmDialog();

    @ContributesAndroidInjector
    abstract PackItemStatusListFragment bindPackItemStatusListFragment();

    @ContributesAndroidInjector
    abstract DispatchListFragment bindDispatchListFragment();

    @ContributesAndroidInjector
    abstract DispatchDetailsFragment bindDispatchDetailsFragment();

    @ContributesAndroidInjector
    abstract DispatchDetailConfirmDialog bindDispatchDetailConfirmDialog();

    @ContributesAndroidInjector
    abstract DispatchGatepassConfirmDialog bindDispatchGatepassConfirmDialog();

    @ContributesAndroidInjector
    abstract PendingReceiptFragment bindPendingReceiptFragment();

    @ContributesAndroidInjector
    abstract UpdateGrnFragment bindUpdateGrnMainFragment();

    @ContributesAndroidInjector
    abstract GRNSummaryFragment bindFragmentGRNSummary();

    @ContributesAndroidInjector
    abstract InwardSelectionFragment bindInwardSelectionFragment();

    @ContributesAndroidInjector
    abstract PickItemListFragment bindPickItemListFragment();

    @ContributesAndroidInjector
    abstract PackItemListFragment bindPackItemListFragment();

    @ContributesAndroidInjector
    abstract RegradingListFragment bindRegradingListFragment();

    @ContributesAndroidInjector
    abstract ItemInfoSearchFragment bindItemInfoSearchFragment();

    @ContributesAndroidInjector
    abstract ItemInfoOptionsFragment bindItemInfoOptionsFragment();

    @ContributesAndroidInjector
    abstract ItemInfoInwardDetailsFragment bindItemInfoInwardDetailsFragment();

    @ContributesAndroidInjector
    abstract ItemInfoDispositionOptionsFragment bindDispositionOptionsFragment();

    @ContributesAndroidInjector
    abstract ItemInfoRTVDetailsFragment bindItemInfoRTVDetailsFragment();

    @ContributesAndroidInjector
    abstract ItemInfoRepairDetailsFragment bindItemInfoRepairDetailsFragment();

    @ContributesAndroidInjector
    abstract ItemInfoDispatchDetailsFragment bindItemInfoDispatchDetailsFragment();


}
