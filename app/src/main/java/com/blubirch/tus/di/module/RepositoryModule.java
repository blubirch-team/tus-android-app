package com.blubirch.tus.di.module;

import com.blubirch.tus.testing.di.TestDataModule;
import com.blubirch.tus.data.api.interfaces.DispatchAPI;
import com.blubirch.tus.data.api.interfaces.GatePassAPI;
import com.blubirch.tus.data.api.interfaces.ItemInfoAPI;
import com.blubirch.tus.data.api.interfaces.PackAPI;
import com.blubirch.tus.data.api.interfaces.PickAPI;
import com.blubirch.tus.data.api.interfaces.PickPackAPI;
import com.blubirch.tus.data.api.interfaces.ReGradingAPI;
import com.blubirch.tus.data.api.interfaces.StowingAPI;
import com.blubirch.tus.data.repository.DispatchRepo;
import com.blubirch.tus.data.repository.DispatchRepoImpl;
import com.blubirch.tus.data.repository.GatePassRepo;
import com.blubirch.tus.data.repository.GatePassRepoImpl;
import com.blubirch.tus.data.repository.ItemInfoRepo;
import com.blubirch.tus.data.repository.ItemInfoRepoImpl;
import com.blubirch.tus.data.repository.PackRepo;
import com.blubirch.tus.data.repository.PackRepoImpl;
import com.blubirch.tus.data.repository.PickPackRepo;
import com.blubirch.tus.data.repository.PickPackRepoImpl;
import com.blubirch.tus.data.repository.PickRepo;
import com.blubirch.tus.data.repository.PickRepoImpl;
import com.blubirch.tus.data.repository.ReGradingRepo;
import com.blubirch.tus.data.repository.ReGradingRepoImpl;
import com.blubirch.tus.data.repository.StowingRepo;
import com.blubirch.tus.data.repository.StowingRepoImpl;
import com.blubirch.core.di.modules.LoginDataModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {LoginDataModule.class, TestDataModule.class})
public class RepositoryModule {

    @Singleton
    @Provides
    public GatePassRepo providesGatePassRepo(GatePassAPI gatePassAPI) {
        return new GatePassRepoImpl(gatePassAPI);
    }

    @Singleton
    @Provides
    public StowingRepo providesStowingRepo(StowingAPI stowingAPI) {
        return new StowingRepoImpl(stowingAPI);
    }

    @Singleton
    @Provides
    public PickPackRepo providesPickPackRepo(PickPackAPI pickPackAPI) {
        return new PickPackRepoImpl(pickPackAPI);
    }

    @Singleton
    @Provides
    public PickRepo providesPickRepo(PickAPI pickAPI) {
        return new PickRepoImpl(pickAPI);
    }

    @Singleton
    @Provides
    public PackRepo providesPackRepo(PackAPI packAPI) {
        return new PackRepoImpl(packAPI);
    }

    @Singleton
    @Provides
    public DispatchRepo providesDispatchRepo(DispatchAPI dispatchAPI) {
        return new DispatchRepoImpl(dispatchAPI);
    }

    @Singleton
    @Provides
    public ReGradingRepo providesReGradingRepo(ReGradingAPI reGradingAPI) {
        return new ReGradingRepoImpl(reGradingAPI);
    }

    @Singleton
    @Provides
    public ItemInfoRepo providesItemInfoRepo(ItemInfoAPI itemInfoAPI) {
        return new ItemInfoRepoImpl(itemInfoAPI);
    }
}
