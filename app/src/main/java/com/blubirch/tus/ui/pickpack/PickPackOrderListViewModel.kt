package com.blubirch.tus.ui.pickpack

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.repository.PickPackRepo
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.utility.CustomPredicate
import com.blubirch.core.utility.filterWith
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PickPackOrderListViewModel @Inject constructor(private val pickPackRepo: PickPackRepo) : BaseViewModel() {

    private lateinit var pickPackViewModel : PickPackViewModel

    val warehouseOrdersLiveData: LiveData<List<WarehouseOrders>> by lazy {
        MutableLiveData<List<WarehouseOrders>>()
    }

    private val warehouseOrders: ArrayList<WarehouseOrders> by lazy {
        ArrayList<WarehouseOrders>()
    }

    private var orderSearchTerm: String = ""


    fun setMainModel(pickPackViewModel: PickPackViewModel) {
        PickPackOrderListViewModel@this.pickPackViewModel = pickPackViewModel
    }

    fun setSelectedOrder(warehouseOrder: WarehouseOrders) {
        pickPackViewModel.selectedWarehouseOrder = warehouseOrder
    }

    fun getOrderList() {
        compositeDisposable.add(
            pickPackRepo.getList()
                .subscribeOn(Schedulers.io())
                .subscribe { warehouseOrdersResDTO: WarehouseOrdersResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    if (postAPIErrorIfAny(warehouseOrdersResDTO).not()) {
                        warehouseOrdersResDTO?.let {
                            warehouseOrders.clear()
                            warehouseOrders.addAll(warehouseOrdersResDTO.warehouseOrders)
                            removeDispatchedItemAndFixQuantity();
                            postFilteredList()
                        }
                    }
                }
        )
    }

    private fun removeDispatchedItemAndFixQuantity() {
        for (warehouseOrder in warehouseOrders) {
            warehouseOrder.warehouseOrderItems.filterWith(CustomPredicate {
                it.status != "Pending Pick"
            })
            warehouseOrder.totalQuantity = warehouseOrder.warehouseOrderItems.size.toLong()
        }
    }

    fun searchOrderByVendor(vendor : String) {
        orderSearchTerm = vendor
        postFilteredList()
    }

    private fun postFilteredList() {
        if (orderSearchTerm.isBlank()) {
            postListData(ArrayList(warehouseOrders))
        } else {
            compositeDisposable.add(
                Observable.fromIterable(warehouseOrders)
                    .filter {
                        it.vendorCode.orEmpty().toLowerCase().contains(orderSearchTerm.toLowerCase())
                    }.toList()
                    .subscribeOn(Schedulers.computation())
                    .subscribe { filteredOrders, throwable ->
                        throwable?.let {
                            it.printStackTrace()
                            postAPIError(it)
                        }
                        filteredOrders?.let {
                            postListData(it)
                        }
                    }
            )
        }
    }

    private fun postListData(warehouseOrders: List<WarehouseOrders>) {
        (warehouseOrdersLiveData as MutableLiveData).postValue(warehouseOrders)
    }

}