package com.blubirch.tus.ui.inward

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.constants.GatePassStatus
import com.blubirch.tus.databinding.DialogConfirmProceedGrnDialogBinding
import com.blubirch.tus.databinding.FragmentGrnSummaryBinding
import com.blubirch.tus.ui.inward.adapters.GRNSummaryAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.KotlinJavaDelegation
import kotlinx.android.synthetic.main.fragment_grn_summary.*
import javax.inject.Inject

class GRNSummaryFragment : BaseFragment<GrnSummaryViewModel>() {

    companion object {
        fun newInstance() = GRNSummaryFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: GrnSummaryViewModel
    private lateinit var inwardViewModel: InwardViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel =
            ViewModelProvider(this, viewModelFactory).get(GrnSummaryViewModel::class.java)

        val backStackEntry = navController.getBackStackEntry(R.id.navInward)
        inwardViewModel =
            ViewModelProvider(backStackEntry, viewModelFactory).get(InwardViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentGrnSummaryBinding.inflate(inflater).root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
    }

    private fun setupList() {
        rvGrnInventories.apply {
            adapter = GRNSummaryAdapter()
            addItemDecoration(com.blubirch.tus.utils.DividerItemDecoration(requireContext()))
        }

    }

    override fun onResume() {
        super.onResume()
        setupListeners()
    }

    private fun setupListeners() {
        btnProceedToGrn.setOnClickListener {
            doGrn()
        }
    }

    private fun doGrn() {
        inwardViewModel.apply {
            if (com.blubirch.tus.constants.GatePassStatus.COMPLETED.equals(viewModel.gatePass.status, true) ||
                com.blubirch.tus.constants.GatePassStatus.CLOSED.equals(viewModel.gatePass.status, true)
            ) {
                messageGrnAlreadySubmitted()
            } else {
                showConfirmDialog()
            }
//            when (checkGrn()) {
//                GatePassStatus.GRN_NOT_SUBMITTED -> {
//                        showConfirmDialog()
//                }
//                else -> {
//                    grnAlreadySubmitted()
//                }
//            }
        }
    }

    private fun messageGrnAlreadySubmitted() {
        showSnackBar(getString(R.string.grn_already_submitted), btnProceedToGrn)
    }


    private fun showConfirmDialog() {
        val binding =
            DialogConfirmProceedGrnDialogBinding.inflate(LayoutInflater.from(requireContext()))
        val dialog = KotlinJavaDelegation.createDialog(binding.root)
        dialog.show()
        binding.btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        binding.btnConfirm.setOnClickListener {
            dialog.dismiss()
            viewModel.proceedToGrn()

        }
    }

    override fun getViewModel(): GrnSummaryViewModel {
        return viewModel
    }

    override fun attachLiveData() {

        inwardViewModel.apply {
            viewModel.updateGatePass(gatePass!!)
            txtSentItem.text = getString(R.string.item_d, sentInventoryCount)
            txtReceivedItem.text = getString(R.string.item_d, receivedInventoryCount)
            txtInTransitItem.text = getString(R.string.item_d, inTransitInventoryCount)
            txtExcessItem.text = getString(R.string.item_d, excessInventoryCount)
        }
        checkGrn()
        viewModel.apply {

            (rvGrnInventories.adapter as GRNSummaryAdapter).apply {
                if (data.size == 0) {
                    setNewInstance(gatePassInventories.toMutableList())
                } else {
                    setDiffNewData(gatePassInventories.toMutableList())
                }
            }

            proceedToGrnLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    showSnackBar(it)
                    navigate(
                        GRNSummaryFragmentDirections.actionGrnSummaryFragmentToGatePassSearchFragment()
                    )
                }
            })
        }

    }

    private fun checkGrn() {
        inwardViewModel.apply {
            if (com.blubirch.tus.constants.GatePassStatus.COMPLETED.equals(viewModel.gatePass.status, true) ||
                com.blubirch.tus.constants.GatePassStatus.CLOSED.equals(viewModel.gatePass.status, true)
            ) {
                grnSubmitted()
            } else {
                grnNotSubmitted()
            }
//            when (checkGrn()) {
//                GatePassStatus.GRN_NOT_SUBMITTED -> {
//                    grnNotSubmitted()
//                }
//                GatePassStatus.GRN_SUBMITTED -> {
//                    grnSubmitted()
//                }
//                GatePassStatus.GRN_NUMBER_UPDATED -> {
//                    grnSubmitted()
//                }
//            }
        }
    }

    private fun grnNotSubmitted() {
        btnProceedToGrn.text = getString(R.string.proceed_to_grn)
    }

    private fun grnSubmitted() {
        btnProceedToGrn.text = getString(R.string.grn_submitted)
    }


}