package com.blubirch.tus.ui

import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import kotlinx.android.synthetic.main.menu_fragment.*
import javax.inject.Inject

class MenuFragment : BaseFragment<MenuViewModel>() {

    companion object {
        fun newInstance() = MenuFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: MenuViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MenuViewModel::class.java)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.menu_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()
        setListeners()
    }

    private fun setListeners() {
        cvInward.setOnClickListener{
            navigate(MenuFragmentDirections.actionMenuFragmentToNavInward())
        }
        cvStowing.setOnClickListener{
            navigate(MenuFragmentDirections.actionMenuFragmentToNavStowing())
        }

//        cvPickPack.setOnClickListener {
//            navigate(MenuFragmentDirections.actionMenuFragmentToNavPickPack())
//        }

//        cvPick.setOnClickListener{
//            navigate(MenuFragmentDirections.actionMainFragmentToNavPick())
//        }
//
//        cvPack.setOnClickListener {
//            navigate(MenuFragmentDirections.actionMenuFragmentToNavPack())
//        }

//        cvDispatch.setOnClickListener {
//            navigate(MenuFragmentDirections.actionMenuFragmentToNavDispatch())
//        }
//
//        cvRegrading.setOnClickListener {
//            navigate(MenuFragmentDirections.actionMenuFragmentToNavReGrading())
//        }

        cvItemInfo.setOnClickListener {
            navigate(MenuFragmentDirections.actionMenuFragmentToNavItemInfo())
        }
//
//        cvUpdateGRN.setOnClickListener {
//            navigate(MenuFragmentDirections.actionMenuFragmentToUpdateGrnFragment())
//        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.profile_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_profile -> {
                navigate(MenuFragmentDirections.actionMenuFragmentToNavUserInfo())
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun getViewModel(): MenuViewModel {
        return viewModel
    }

    override fun attachLiveData() {

    }


}
