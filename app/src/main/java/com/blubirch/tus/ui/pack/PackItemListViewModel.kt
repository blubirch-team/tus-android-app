package com.blubirch.tus.ui.pack

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.repository.PackRepo
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PackItemListViewModel @Inject constructor(private val packRepo: PackRepo) : BaseViewModel() {


    val warehouseOrdersLiveData: LiveData<List<WarehouseOrders>> by lazy {
        MutableLiveData<List<WarehouseOrders>>()
    }

    private val warehouseOrdersList: ArrayList<WarehouseOrders> by lazy {
        ArrayList<WarehouseOrders>()
    }

    private var warehouseSearchTerm: String = ""

    fun getList() {
        compositeDisposable.add(
            packRepo.list
                .subscribeOn(Schedulers.io())
                .subscribe { warehouseOrdersResDTO: WarehouseOrdersResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    if (postAPIErrorIfAny(warehouseOrdersResDTO).not()) {
                        warehouseOrdersResDTO?.warehouseOrders?.let {
                            warehouseOrdersList.clear()
                            warehouseOrdersList.addAll(it)
                            postFilteredList()
                        }
                    }
                }
        )
    }

    fun searchWarehouseByVendor(vendor : String) {
        warehouseSearchTerm = vendor
        postFilteredList()
    }


    private fun postFilteredList(){
        compositeDisposable.add(
            Observable.fromIterable(warehouseOrdersList)
                .filter{it.vendorCode.toLowerCase().contains(warehouseSearchTerm.toLowerCase())}
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { filteredWarehouseOrders: MutableList<WarehouseOrders>?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    filteredWarehouseOrders?.let {
                        postOrdersList(it)
                    }
                }
        )
    }

    fun postOrdersList(warehouseOrders: List<WarehouseOrders>) {
        (warehouseOrdersLiveData as MutableLiveData).postValue(warehouseOrders)
    }
}
