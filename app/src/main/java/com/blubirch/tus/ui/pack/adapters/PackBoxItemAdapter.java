package com.blubirch.tus.ui.pack.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.blubirch.tus.R;
import com.blubirch.tus.data.model.WarehouseOrderItems;
import com.blubirch.tus.databinding.ItemBoxItemBinding;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PackBoxItemAdapter extends BaseQuickAdapter<WarehouseOrderItems, BaseViewHolder> {

    private OnDeleteListener onDeleteListener;
    public PackBoxItemAdapter(List<WarehouseOrderItems> items, OnDeleteListener onDeleteListener) {
        super(R.layout.item_box_item, items);
        this.onDeleteListener = onDeleteListener;

        setDiffCallback(new DiffUtil.ItemCallback<WarehouseOrderItems>() {
            @Override
            public boolean areItemsTheSame(@NonNull WarehouseOrderItems oldItem, @NonNull WarehouseOrderItems newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(@NonNull WarehouseOrderItems oldItem, @NonNull WarehouseOrderItems newItem) {
                return oldItem.getTagNumber().equals(newItem.getTagNumber()) &&
                        oldItem.getItemDescription().equals(newItem.getItemDescription());
            }
        });

    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, WarehouseOrderItems warehouseOrderItem) {
        ItemBoxItemBinding binding = ItemBoxItemBinding.bind(baseViewHolder.itemView);
        binding.txtTagNumber.setText(warehouseOrderItem.getTagNumber());
        binding.txtItemDescription.setText(warehouseOrderItem.getItemDescription());
        binding.imgDelete.setOnClickListener(v -> {
            if (onDeleteListener != null) {
                onDeleteListener.onItemDeleted(getItemPosition(warehouseOrderItem), warehouseOrderItem);
            }
        });
    }

    public interface OnDeleteListener{
        void onItemDeleted(int position, @NonNull WarehouseOrderItems warehouseOrderItem);
    }

}
