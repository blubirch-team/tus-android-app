package com.blubirch.tus.ui.stowing

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.model.Inventory
import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject

class StowingViewModel @Inject constructor() : BaseViewModel() {

    lateinit var selectedToatNumber : String
    val selectedToatInventories: MutableList<Inventory> = arrayListOf()
    val stowedCountLiveData : LiveData<Int> = MutableLiveData<Int>()

}
