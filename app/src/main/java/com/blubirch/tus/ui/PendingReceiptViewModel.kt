package com.blubirch.tus.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.model.PendingReceipt
import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject

class PendingReceiptViewModel @Inject constructor() : BaseViewModel() {


    val pendingReceiptLiveData: LiveData<List<PendingReceipt>> by lazy {
        MutableLiveData<List<PendingReceipt>>(ArrayList<PendingReceipt>())
    }

    private val pendingReceiptList: ArrayList<PendingReceipt> by lazy {
        ArrayList<PendingReceipt>()
    }

    fun getList() {
        for (i in 1..10) {
            pendingReceiptList.add(PendingReceipt())
        }
        postPendingReceiptList()
    }

    fun postPendingReceiptList() {
        (pendingReceiptLiveData as MutableLiveData).postValue(pendingReceiptList)
    }

}