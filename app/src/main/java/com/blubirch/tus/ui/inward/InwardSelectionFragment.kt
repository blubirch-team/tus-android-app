package com.blubirch.tus.ui.inward

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blubirch.tus.databinding.FragmentInwardSelectionBinding
import dagger.android.support.DaggerFragment

class InwardSelectionFragment : DaggerFragment() {

    companion object{
        fun newInstance() = InwardSelectionFragment()
    }
    private lateinit var binding: FragmentInwardSelectionBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.cvInwardSTN.setOnClickListener {
//            findNavController().navigate(InwardSelectionFragmentDirections.actionInwardSelectionFragmentToNavInward())
        }

        binding.cvUpdateGRN.setOnClickListener {
//            findNavController().navigate(InwardSelectionFragmentDirections.actionInwardSelectionFragmentToUpdateGrnFragment())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentInwardSelectionBinding.inflate(inflater)
        return binding.root
    }

}