package com.blubirch.tus.ui.dispatch.adapters

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.databinding.ItemDialogGatePassBoxBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class DispatchDialogGatePassAdapter constructor(warehouseOrders: MutableList<WarehouseOrders>):
    BaseQuickAdapter<WarehouseOrders, BaseViewHolder>(R.layout.item_dialog_gate_pass_box, warehouseOrders) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrders>() {
            override fun areItemsTheSame(oldItem: WarehouseOrders, newItem: WarehouseOrders): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: WarehouseOrders, newItem: WarehouseOrders): Boolean {
                return oldItem.gatepassNumber == newItem.gatepassNumber &&
                        areBoxesSame(oldItem.warehouseOrderItems, newItem.warehouseOrderItems)
            }
        })
    }

    private fun areBoxesSame(oldItems: List<WarehouseOrderItems>, newItems: List<WarehouseOrderItems>) : Boolean {
        if (oldItems.size != newItems.size) {
            return false
        }
        for (index in oldItems.indices) {
            if (oldItems[index].packagingBoxNumber != newItems[index].packagingBoxNumber) {
                return false
            }
        }
        return true
    }

    override fun convert(holder: BaseViewHolder, item: WarehouseOrders) {
        val binding = ItemDialogGatePassBoxBinding.bind(holder.itemView)
        binding.txtGatePass.text = item.gatepassNumber

        val boxNumbers = ArrayList<String>();
        for (warehouseOrderItem in item.warehouseOrderItems) {
            warehouseOrderItem.packagingBoxNumber?.let {
                if (boxNumbers.contains(it).not()) {
                    boxNumbers.add(it)
                }
            }
        }

        binding.txtBoxCount.text = String.format(
            context.resources.getString(R.string._d_boxes), boxNumbers.size
        )

        binding.txtOrderNumber.text = item.orderNumber //todo label is "invoice number"

        val dispatchDialogGatePassItemAdapter = DispatchDialogGatePassItemAdapter(boxNumbers)
        val lm: LinearLayoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        binding.rvBoxItems.isNestedScrollingEnabled = false;
        binding.rvBoxItems.layoutManager = lm
        binding.rvBoxItems.adapter = dispatchDialogGatePassItemAdapter
    }
}
















