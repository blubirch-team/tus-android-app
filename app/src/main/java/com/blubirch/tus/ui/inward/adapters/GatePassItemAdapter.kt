package com.blubirch.tus.ui.inward.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.inward.GatePassInventory
import com.blubirch.tus.databinding.ItemGatePassInventoryListBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class GatePassItemAdapter :
    BaseQuickAdapter<GatePassInventory, BaseViewHolder>(R.layout.item_gate_pass_inventory_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<GatePassInventory>() {
            override fun areItemsTheSame(
                    oldItem: GatePassInventory,
                    newItem: GatePassInventory
            ): Boolean {
                return false
            }

            override fun areContentsTheSame(
                    oldItem: GatePassInventory,
                    newItem: GatePassInventory
            ): Boolean {
                return false
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: GatePassInventory) {
        val binding = ItemGatePassInventoryListBinding.bind(holder.itemView)
        item.apply {
            binding.tvSKU.text = skuCode
            binding.tvItemDesc.text = itemDescription
            binding.tvQty.text = quantity.toString()
//            binding.tvMAP.text = holder.itemView.resources.getString(R.string.amount, map)
        }

    }
}