package com.blubirch.tus.ui.pickpack

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.databinding.FragmentPickPackOrderListBinding
import com.blubirch.tus.ui.pickpack.adapters.PickPackOrderListAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import javax.inject.Inject

class PickPackOrderListFragment : BaseFragment<PickPackOrderListViewModel>(){

    companion object{
        fun newInstance() = PickPackOrderListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var pickPackOrderListViewModel: PickPackOrderListViewModel

    private lateinit var binding: FragmentPickPackOrderListBinding

    private lateinit var pickPackOrderListAdapter: PickPackOrderListAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPickPack)
        val pickPackViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(
            PickPackViewModel::class.java)

        pickPackOrderListViewModel = ViewModelProvider(this, viewModelFactory).get(PickPackOrderListViewModel::class.java)
        pickPackOrderListViewModel.setMainModel(pickPackViewModel)

        pickPackOrderListAdapter = PickPackOrderListAdapter(object : PickPackOrderListAdapter.ItemClickListener{
            override fun onItemClick(position: Int, warehouseOrder: WarehouseOrders) {
                getViewModel().setSelectedOrder(warehouseOrder)
                navigate(PickPackOrderListFragmentDirections.actionPickPackOrderListFragmentToPickPackOrderFragment())
            }
        })

        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvOrderList.addItemDecoration(dividerItemDecoration)
        binding.rvOrderList.adapter = pickPackOrderListAdapter

        binding.edtSearchVendor.addTextChangedListener(afterTextChanged = {
            it?.let {
                pickPackOrderListViewModel.searchOrderByVendor(it.toString())
            }
        })

        pickPackOrderListViewModel.progressLiveData.value = true
        pickPackOrderListViewModel.getOrderList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPickPackOrderListBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): PickPackOrderListViewModel {
        return pickPackOrderListViewModel
    }

    override fun attachLiveData() {
        getViewModel().warehouseOrdersLiveData.observe(viewLifecycleOwner, Observer {
            getViewModel().progressLiveData.postValue(false)
            it?.let {
                if (it.isEmpty()) {
                    pickPackOrderListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                    pickPackOrderListAdapter.setNewInstance(null)
                } else {
                    if (pickPackOrderListAdapter.data.size == 0) {
                        pickPackOrderListAdapter.setNewInstance(it as ArrayList)
                    } else {
                        pickPackOrderListAdapter.setDiffNewData(it as ArrayList)
                    }
                }
            }
        })
    }

}