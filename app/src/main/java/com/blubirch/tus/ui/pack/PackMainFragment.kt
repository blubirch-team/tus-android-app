package com.blubirch.tus.ui.pack

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.databinding.FragmentPackMainBinding
import com.blubirch.tus.ui.pack.adapters.PackMainCategoryAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import java.lang.NumberFormatException
import javax.inject.Inject


class PackMainFragment : BaseFragment<PackViewModel>() {

    companion object {
        fun newInstance() = PackMainFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: PackViewModel

    private lateinit var binding: FragmentPackMainBinding

    private lateinit var categoryAdapter: PackMainCategoryAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPack)
        viewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(PackViewModel::class.java)



        val warehouseOrder = viewModel.selectedWarehouseOrder
        binding.txtOrderNumber.setText(warehouseOrder.orderNumber)

        val toatsOfSelectedOrder = viewModel.getToatsOfSelectedOrder()
        var strToats = ""
        for (index in toatsOfSelectedOrder.indices) {
            strToats += toatsOfSelectedOrder[index]
            if (index != toatsOfSelectedOrder.size - 1) {
                strToats += ", "
            }
        }
        binding.txtToats.text = strToats

        for (index in warehouseOrder.warehouseOrderItems.indices) {
            warehouseOrder.warehouseOrderItems[index].toatNumber
        }

        var totalItems : Long = 0
        for (item in warehouseOrder.itemsByCategory) {
            try {
                totalItems += item[2].toLong()
            } catch (nfe: NumberFormatException) {
                nfe.printStackTrace()
            }
        }
        binding.txtQuantity.text = String.format(resources.getString(R.string.qty_d), totalItems)
        categoryAdapter = PackMainCategoryAdapter()

        categoryAdapter.setNewInstance(warehouseOrder.itemsByCategory)
        binding.rvCategoryList.adapter = categoryAdapter

        binding.btnProceed.setOnClickListener {
            navigate(PackMainFragmentDirections.actionPackMainFragmentToPackListFragment())
        }

        setToolbarTitle(String.format(resources.getString(R.string.pack_s), viewModel.selectedWarehouseOrder.orderNumber))


    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPackMainBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): PackViewModel {
        return viewModel
    }

    override fun attachLiveData() {

    }


}
