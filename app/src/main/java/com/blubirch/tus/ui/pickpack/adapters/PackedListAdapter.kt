package com.blubirch.tus.ui.pickpack.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.tus.databinding.ItemPickPackPackedBoxListBinding
import com.blubirch.tus.utils.DividerItemDecoration
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PackedListAdapter constructor(
        private val onItemDelete: (itemPosition: Int, WarehouseOrderItems, parentPosition: Int, BoxListItem) -> Unit,
        private val onBoxDelete: (position: Int, BoxListItem) -> Unit,
        private val onPrint : (strToPrint: String) -> Unit)
    : BaseQuickAdapter<BoxListItem, BaseViewHolder>(R.layout.item_pick_pack_packed_box_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<BoxListItem>() {
            override fun areItemsTheSame(oldItem: BoxListItem, newItem: BoxListItem): Boolean {
                return oldItem.box.id == newItem.box.id
            }

            override fun areContentsTheSame(oldItem: BoxListItem, newItem: BoxListItem): Boolean {
                return oldItem.box.boxNumber == newItem.box.boxNumber &&
                        oldItem.warehouseOrderItems.size == newItem.warehouseOrderItems.size &&
                        areListItemSame(oldItem.warehouseOrderItems, newItem.warehouseOrderItems)
            }
        })
    }

    private fun areListItemSame(oldItems: ArrayList<WarehouseOrderItems>,
                                newItems: ArrayList<WarehouseOrderItems>) : Boolean{
        if (oldItems.size != newItems.size) {
            return false
        }
        for (index in oldItems.indices) {
            if(oldItems[index].tagNumber != newItems[index].tagNumber ||
                    oldItems[index].itemDescription != newItems[index].itemDescription){
                return false
            }
        }
        return true
    }

    override fun convert(holder: BaseViewHolder, boxListItem: BoxListItem) {
        val binding = ItemPickPackPackedBoxListBinding.bind(holder.itemView)
        binding.txtBoxId.text = boxListItem.box.boxNumber
        binding.txtBoxCount.text = String.format(context.resources.getString(R.string._d_items),boxListItem.warehouseOrderItems.size)
        binding.imgDelete.setOnClickListener {
            onBoxDelete(getItemPosition(boxListItem), boxListItem)
        }
        binding.imgPrint.setOnClickListener {
            onPrint(boxListItem.box.boxNumber)
        }
        val packedBoxItemListAdapter = PackedBoxItemListAdapter(boxListItem.warehouseOrderItems,
            onItemDeleted = { position: Int, warehouseOrderItems: WarehouseOrderItems ->
                onItemDelete(position, warehouseOrderItems, getItemPosition(boxListItem), boxListItem)
            }
        )

        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(binding.rvPackedBoxItems.context)
        binding.rvPackedBoxItems.addItemDecoration(dividerItemDecoration)
        binding.rvPackedBoxItems.adapter = packedBoxItemListAdapter
    }

}