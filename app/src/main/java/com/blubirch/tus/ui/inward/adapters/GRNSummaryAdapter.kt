package com.blubirch.tus.ui.inward.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.constants.GatePassInventoryStatus
import com.blubirch.tus.data.model.inward.GatePassInventory
import com.blubirch.tus.databinding.ItemGrnSummaryBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import kotlin.math.abs

class GRNSummaryAdapter :
    BaseQuickAdapter<GatePassInventory, BaseViewHolder>(R.layout.item_grn_summary) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<GatePassInventory>() {
            override fun areItemsTheSame(
                    oldItem: GatePassInventory,
                    newItem: GatePassInventory
            ): Boolean {
                return oldItem.skuCode == newItem.skuCode
            }

            override fun areContentsTheSame(
                    oldItem: GatePassInventory,
                    newItem: GatePassInventory
            ): Boolean {
                return oldItem.skuCode == newItem.skuCode && oldItem.inwardedQuantity == newItem.inwardedQuantity
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: GatePassInventory) {
        val binding = ItemGrnSummaryBinding.bind(holder.itemView)
        binding.txtArticleId.text = item.skuCode
        var statusIconResId: Int = R.drawable.ic_close
        var quantity: Int = item.quantity
        var status: String = com.blubirch.tus.constants.GatePassInventoryStatus.UIStatus.PENDING_RECEIPT
        when (item.status) {
            com.blubirch.tus.constants.GatePassInventoryStatus.PENDING_RECEIPT -> {
                statusIconResId = R.drawable.ic_close
                quantity = item.quantity
                status = com.blubirch.tus.constants.GatePassInventoryStatus.UIStatus.PENDING_RECEIPT
            }
            com.blubirch.tus.constants.GatePassInventoryStatus.PART_RECEIVED -> {
                quantity = item.quantity - item.inwardedQuantity
                when {
                    quantity < 0 -> {
                        statusIconResId = R.drawable.ic_remove
                        quantity = abs(item.quantity - item.inwardedQuantity)
                        status = com.blubirch.tus.constants.GatePassInventoryStatus.UIStatus.EXCESS_RECEIVED
                    }
                    quantity == 0 -> {
                        statusIconResId = R.drawable.ic_done
                        quantity = item.quantity
                        status = com.blubirch.tus.constants.GatePassInventoryStatus.UIStatus.FULLY_RECEIVED
                    }
                    quantity > 0 -> {
                        statusIconResId = R.drawable.ic_close
                        status = com.blubirch.tus.constants.GatePassInventoryStatus.UIStatus.PART_RECEIVED
                    }
                }
            }
                com.blubirch.tus.constants.GatePassInventoryStatus.FULLY_RECEIVED -> {
                    statusIconResId = R.drawable.ic_done
                    quantity = item.quantity
                    status = com.blubirch.tus.constants.GatePassInventoryStatus.UIStatus.FULLY_RECEIVED

                }
                com.blubirch.tus.constants.GatePassInventoryStatus.EXCESS_RECEIVED -> {
                    statusIconResId = R.drawable.ic_remove
                    quantity = abs(item.quantity - item.inwardedQuantity)
                    status = com.blubirch.tus.constants.GatePassInventoryStatus.UIStatus.EXCESS_RECEIVED

                }
            }
        binding.imgStatus.setImageResource(statusIconResId)
        binding.txtStatus.text = status
        binding.txtQuantity.text = quantity.toString()
    }
}