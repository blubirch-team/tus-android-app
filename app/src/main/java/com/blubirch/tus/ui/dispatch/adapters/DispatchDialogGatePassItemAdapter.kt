package com.blubirch.tus.ui.dispatch.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.databinding.ItemDispatchGatepassItemBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder


class DispatchDialogGatePassItemAdapter constructor(private val boxNumbers: ArrayList<String>) : BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_dispatch_gatepass_item, boxNumbers) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<String>() {
            override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
                return oldItem == newItem
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: String) {
        val binding = ItemDispatchGatepassItemBinding.bind(holder.itemView)
        binding.txtGatePass.text = item
    }
}