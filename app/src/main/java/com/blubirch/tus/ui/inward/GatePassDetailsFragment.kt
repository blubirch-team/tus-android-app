package com.blubirch.tus.ui.inward

import android.app.Dialog
import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import com.blubirch.core.utility.KotlinJavaDelegation
import com.blubirch.core.utility.visible
import com.blubirch.tus.R
import com.blubirch.tus.data.model.inward.GatePass
import com.blubirch.tus.databinding.DialogUpdateGrnBinding
import com.blubirch.tus.databinding.FragmentGatepassDetailsBinding
import com.blubirch.tus.ui.inward.adapters.GatePassItemAdapter
import kotlinx.android.synthetic.main.fragment_gatepass_details.*
import kotlinx.android.synthetic.main.layout_gate_pass_summary.*
import javax.inject.Inject


class GatePassDetailsFragment : BaseFragment<GatePassDetailsViewModel>() {

    companion object {
        fun newInstance() = GatePassDetailsFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var inwardViewModel: InwardViewModel
    private lateinit var viewModel: GatePassDetailsViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(
            GatePassDetailsViewModel::class.java
        )
        val backStackEntry =
            navController.getBackStackEntry(R.id.navInward)
        inwardViewModel =
            ViewModelProvider(backStackEntry, viewModelFactory).get(InwardViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentGatepassDetailsBinding.inflate(inflater, container, false).root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
    }

    private fun setupList() {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvGatePassItemList.apply {
            layoutManager = linearLayoutManager
            adapter = GatePassItemAdapter()
        }
    }

    override fun getViewModel(): GatePassDetailsViewModel {
        return viewModel
    }

    override fun onResume() {
        super.onResume()
        setupListeners()
        checkGRN()
    }

    private fun checkGRN() {
        /*when (viewModel.checkGrn()) {
            com.blubirch.tus.constants.GatePassStatus.GRN_NOT_SUBMITTED -> {
                hideUpdateGrn()
                showProceedToGrade()
                hideProceedToGrnSummary()
            }
            com.blubirch.tus.constants.GatePassStatus.GRN_SUBMITTED -> {
                if (viewModel.isAllInwarded()) {
                    hideProceedToGrade()
                    showProceedToGrnSummary()
                } else {
                    hideProceedToGrnSummary()
                }
                showUpdateGrn()
            }
            else -> hideProceedToGrnSummary()
//            GatePassStatus.GRN_NUMBER_UPDATED -> {
//                if (viewModel.isAllInwarded()) {
//                    hideProceedToGrade()
//                    showProceedToGrnSummary()
//                }
//                hideUpdateGrn()
//            }
        }*/
//        hideUpdateGrn()
        showProceedToGrade()
//        hideProceedToGrnSummary()
//        if (com.blubirch.tus.constants.GatePassStatus.COMPLETED.equals(viewModel.gatePass.status, true) ||
//            com.blubirch.tus.constants.GatePassStatus.CLOSED.equals(viewModel.gatePass.status, true)
//        ) {
//            hideProceedToGrade()
//            hideUpdateGrn()
//            showProceedToGrnSummary()
//        }
    }

    private fun setupListeners() {
        btnProceedToGrade.setOnClickListener {
            navigate(GatePassDetailsFragmentDirections.actionGatePassDetailsFragmentToInwardItemFragment())
        }
//        btnUpdateGrn.setOnClickListener {
//            openUpdateGrnDialog()
//        }
//        btnProceedToGRNSummary.setOnClickListener {
//            navigate(GatePassDetailsFragmentDirections.actionGatePassDetailsFragmentToGrnSummaryFragment())
//        }
    }

    override fun attachLiveData() {
        inwardViewModel.gatePass?.also {
            (rvGatePassItemList.adapter as GatePassItemAdapter).setNewInstance(it.gatePassInventories as ArrayList)
            updateGatePassSummary(it)
            viewModel.gatePass = it
        }
        updateItemSummary()
        viewModel.grnResponseLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                showSnackBar(it)
                navigate(GatePassDetailsFragmentDirections.actionGatePassDetailsFragmentToGatePassSearchFragment())
            }
        })
    }

    private fun updateItemSummary() {
        inwardViewModel.apply {
            txtSentItems.text = sentInventoryCount.toString()
            txtReceivedItems.text = receivedInventoryCount.toString()
            txtInTransitItems.text = inTransitInventoryCount.toString()
            tvItemCount.text = resources.getQuantityString(
                R.plurals.numberOfItems,
                sentInventoryCount,
                sentInventoryCount
            )
        }
    }

    private fun updateGatePassSummary(gatePass: GatePass) {
        gatePass.apply {
            setToolbarTitle(clientGatePassNumber)
            txtSource.text = resources.getString(
                R.string.source_destination,
                sourceCode,
                sourceState
            )
            txtDestination.text = resources.getString(
                R.string.source_destination,
                destinationCode,
                destinationState
            )
            txtSRNumber.text = srNumber
            txtDispatchDate.text = DateFormat.format("dd-MM-yyyy", dispatchDate)
        }
    }

    var updateGrnDialog: Dialog? = null
    private fun openUpdateGrnDialog() {
        if (updateGrnDialog != null && updateGrnDialog!!.isShowing)
            return
        val dialogBinding: DialogUpdateGrnBinding =
            DialogUpdateGrnBinding.inflate(LayoutInflater.from(requireActivity()))
        updateGrnDialog = KotlinJavaDelegation.makeBottomSheet(dialogBinding.root)
        val filters: Array<InputFilter> =
            dialogBinding.btNumber.editText!!.filters
        val inputFilters = arrayOfNulls<InputFilter>(filters.size + 1)
        var index = 0
        while (index < filters.size) {
            inputFilters[index] = filters[index]
            index++
        }
        inputFilters[index] = LengthFilter(30)
        dialogBinding.btNumber.editText!!.filters = inputFilters
        dialogBinding.btnUpdate.setOnClickListener { _ ->
            dialogBinding.btNumber.editText?.let {
                it.text.toString()
                if (viewModel.updateGRN(it.text.toString())) {
                    it.error = null
                    updateGrnDialog!!.dismiss()
                } else {
                    dialogBinding.btNumber.apply {
                        errorIconDrawable = null
                        error = "Invalid GRN Number"
                        AppUtils.showSoftKeyboard(requireContext(), it)
                        requestFocus()
                    }
                    dialogBinding.btNumber
                }
            }
        }
        dialogBinding.btNumber.editText?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                dialogBinding.btnUpdate.callOnClick()
            }
            true
        }
        updateGrnDialog!!.setOnShowListener {
            dialogBinding.btNumber.requestFocus()
            AppUtils.showSoftKeyboard(
                requireActivity(),
                dialogBinding.btNumber.editText
            )
        }
        updateGrnDialog!!.show()
    }


//    private fun showUpdateGrn() {
//        btnUpdateGrn.visible()
//    }
//
//    private fun hideUpdateGrn() {
//        btnUpdateGrn.gone()
//    }

    private fun showProceedToGrade() {
        btnProceedToGrade.visible()
    }

//    private fun hideProceedToGrade() {
//        btnProceedToGrade.gone()
//
//    }
//
//    private fun showProceedToGrnSummary() {
//        btnProceedToGRNSummary.visible()
//    }
//
//    private fun hideProceedToGrnSummary() {
//        btnProceedToGRNSummary.gone()
//    }
}