package com.blubirch.tus.ui.stowing

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.tus.R
import com.blubirch.tus.constants.APIResponseStatus
import com.blubirch.tus.constants.SearchStatus
import com.blubirch.tus.databinding.DialogStowingCompletionBinding
import com.blubirch.tus.ui.stowing.adapter.ToatInventoryListAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import com.blubirch.core.utility.KotlinJavaDelegation
import com.blubirch.core.utility.inVisible
import kotlinx.android.synthetic.main.fragment_toat_tag_list.*
import kotlinx.android.synthetic.main.layout_toat_summary.*
import javax.inject.Inject


class ToatTagListFragment : BaseFragment<ToatTagListViewModel>() {

    companion object {
        const val TAG_SCAN_REQUEST_CODE = 7788
        const val LOCATION_SCAN_REQUEST_CODE = 8877
        fun newInstance() = ToatTagListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var toatTagListViewModel: ToatTagListViewModel

    private lateinit var toatInventoryListAdapter: ToatInventoryListAdapter

    private var textWatcher: TextWatcher? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val backStackEntry = navController.getBackStackEntry(R.id.navStowing)
        val stowingViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(StowingViewModel::class.java)
        toatTagListViewModel = ViewModelProvider(this, viewModelFactory).get(ToatTagListViewModel::class.java)
        toatTagListViewModel.stowingViewModel = stowingViewModel

        setHasOptionsMenu(true)
//        toatTagListViewModel.progressLiveData.value = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_toat_tag_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScanInput()
        setupList()
    }

    private fun setupScanInput() {
        tilTagNumber.initialize(this, TAG_SCAN_REQUEST_CODE) {
            toatTagListViewModel.findInventory(it)
        }

        tilLocation.initialize(this, LOCATION_SCAN_REQUEST_CODE) {
            updateLocation(it)
        }
        tilLocation.editText.addTextChangedListener(afterTextChanged = {
            tilLocation.setError(null)
        })
    }

    private fun updateLocation(it: String) {
        AppUtils.hideSoftKeyboard(requireActivity())
        toatTagListViewModel.updateLocation(it)
    }

    private fun setupList() {
        rvInventoryList.apply {
            val linearLayoutManager = LinearLayoutManager(requireContext())
            linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
            layoutManager = linearLayoutManager
            toatInventoryListAdapter = ToatInventoryListAdapter {
                tilTagNumber.text = it.tagNumber
            }
//            toatInventoryListAdapter.loadMoreModule.apply {
//                setOnLoadMoreListener { toatTagListViewModel.loadMore() }
//                isAutoLoadMore = true
//                isEnableLoadMoreIfNotFullPage = true
//            }
            adapter = toatInventoryListAdapter
        }

    }

    override fun onResume() {
        super.onResume()
        setupListeners()
        setToolbarTitle(getString(R.string.stowing_d, toatTagListViewModel.selectedToatNumber))
    }

    private fun setupListeners() {
        tilLocation.editText.setOnEditorActionListener { view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                tilLocation.text?.let {
                    updateLocation(it.toString())
                }
                return@setOnEditorActionListener true
            }
            false
        }

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                tilTagNumber.text?.let {
                    if (it.isNotBlank()) toatTagListViewModel.findInventory(it.toString())
                }
            }
        }
        tilTagNumber.addTextWatcher(textWatcher)
    }

    override fun onStop() {
        super.onStop()
        textWatcher?.let {
            tilTagNumber.removeTextWatcher(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.check_only, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_ok -> onCompleteClick()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onCompleteClick() {
        toatTagListViewModel.stowedCountLiveData.value?.let {
            if (it > 0)
                showCompleteDialog()
            else {
                showSnackBar(getString(R.string.msg_error_stowing))
            }
        }
    }

    private fun showCompleteDialog() {
        val binding = DialogStowingCompletionBinding.inflate(LayoutInflater.from(requireContext()))
        val dialog = KotlinJavaDelegation.createDialog(binding.root)
        binding.txtToatNumber.text = toatTagListViewModel.selectedToatNumber
        binding.txtTotalInventory.text = txtTotalInventory.text
        binding.txtItemFound.text = txtStowed.text
        binding.txtItemNotFound.text = txtNotStowed.text
        binding.txtExcessInv.text = 0.toString()
        binding.txtTimeTaken.text = getString(R.string.time_taken_d_mins, toatTagListViewModel.timeTaken)
        binding.txtTimeTaken.inVisible()
        binding.btnGoBack.setOnClickListener {
            dialog.dismiss()
        }
        binding.btnComplete.setOnClickListener {
            toatTagListViewModel.completeStow()
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun getViewModel(): ToatTagListViewModel {
        return toatTagListViewModel
    }

    override fun attachLiveData() {

        toatTagListViewModel.apply {
            rvInventoryList.apply {
                post {
                    selectedToatInventoriesLiveData.observe(viewLifecycleOwner, Observer {
                        rvInventoryList.apply {
                            post {
                                toatInventoryListAdapter.apply {
                                    if (data.isEmpty() || it.isEmpty()) {
                                        setNewInstance(it)
                                    } else {
                                        setDiffNewData(it)
                                        notifyItemRangeChanged(0, it.size)
                                    }
                                    if (loadMoreModule.isLoading) {
                                        loadMoreModule.loadMoreComplete()
                                    }
                                    rvInventoryList.post {
                                        progressLiveData.postValue(false)
                                    }
                                }
                            }
                        }
                    })
                }
            }

            loadEndLiveData.observe(viewLifecycleOwner, Observer {
                if (it) {
                    toatInventoryListAdapter.loadMoreModule.loadMoreEnd()
                }
            })

            stowedCountLiveData.observe(viewLifecycleOwner, Observer {
                val stowedCount = it
                val totalInventories = toatTagListViewModel.totalInventories
                txtTotalInventory.text = totalInventories.toString()
                txtStowed.text = stowedCount.toString()
                txtNotStowed.text = (totalInventories - stowedCount).toString()
            })

            tagNumberSearchLiveData.observe(viewLifecycleOwner, Observer {
                when (it) {
                    com.blubirch.tus.constants.SearchStatus.NOT_FOUND -> {
                        tilTagNumber.setError(getString(R.string.incorrect_item_id))
                    }
                    com.blubirch.tus.constants.SearchStatus.FOUND -> {
                        tilTagNumber.setError(null)
                    }
                    com.blubirch.tus.constants.SearchStatus.ERROR -> {
                        tilTagNumber.setError(getString(R.string.incorrect_item_id))
                        showSnackBar(getString(R.string.provide_item_id))
                        tilTagNumber.requestFocus()
                    }
                }
                if (it != com.blubirch.tus.constants.SearchStatus.NOT_SEARCHED)
                    tagNumberSearchLiveData.value = com.blubirch.tus.constants.SearchStatus.NOT_SEARCHED
            })

            locationErrorLiveData.observe(viewLifecycleOwner, Observer {
                it?.let {
                    tilLocation.setError(it.cause!!.message)
                    locationErrorLiveData.value = null
                }
            })

            updateLocationResponseLiveData.observe(viewLifecycleOwner, Observer {
                when (it) {
                    com.blubirch.tus.constants.APIResponseStatus.ERROR -> {
                        showSnackBar(
                            getString(
                                R.string.msg_location_update_failed,
                                tilTagNumber.text.toString()
                            )
                        )
                    }
                    com.blubirch.tus.constants.APIResponseStatus.FOUND -> {
                        showSnackBar(
                            getString(
                                R.string.msg_location_update_success,
                                tilTagNumber.text.toString(),
                                tilLocation.text.toString()
                            )
                        )
                        tilTagNumber.text = ""
                        tilLocation.text = ""
                        tilTagNumber.requestFocus()
                        toatInventoryListAdapter.notifyItemRangeChanged(0, toatTagListViewModel.selectedToatInventoriesLiveData.value!!.size)
                    }
                }
                if (it != com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED)
                    updateLocationResponseLiveData.value = com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED
            })

            completeStowingResponseLiveData.observe(viewLifecycleOwner, Observer {
                when (it) {
                    com.blubirch.tus.constants.APIResponseStatus.ERROR -> {
                        showSnackBar(
                            getString(
                                R.string.msg_stowing_complete_failed,
                                toatTagListViewModel.selectedToatNumber
                            )
                        )
                    }
                    com.blubirch.tus.constants.APIResponseStatus.SUCCESS -> {
                        navigate(ToatTagListFragmentDirections.actionToatTagListFragmentToSearchToatFragment())
                        showSnackBar(
                            getString(
                                R.string.msg_stowing_complete_success,
                                toatTagListViewModel.selectedToatNumber
                            )
                        )
                    }
                }
                if (it != com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED)
                    completeStowingResponseLiveData.value = com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED
            })
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            TAG_SCAN_REQUEST_CODE -> {
                tilTagNumber.onActivityResult(requestCode, resultCode, data)
            }
            LOCATION_SCAN_REQUEST_CODE -> {
                tilLocation.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

}