package com.blubirch.tus.ui.inward.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.inward.GatePassInventory
import com.blubirch.tus.databinding.ItemInwardListBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class InwardItemAdapter(val onItemClick: (GatePassInventory) -> Unit) :
    BaseQuickAdapter<GatePassInventory, BaseViewHolder>(R.layout.item_inward_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<GatePassInventory>() {
            override fun areItemsTheSame(
                    oldItem: GatePassInventory,
                    newItem: GatePassInventory
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                    oldItem: GatePassInventory,
                    newItem: GatePassInventory
            ): Boolean {
                return oldItem.skuCode == newItem.skuCode &&
                        oldItem.itemCode == newItem.itemDescription &&
                        oldItem.inwardedQuantity == newItem.inwardedQuantity &&
                        oldItem.quantity == newItem.quantity
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: GatePassInventory) {
        val binding = ItemInwardListBinding.bind(holder.itemView)
        item.apply {
            binding.tvSKU.text = skuCode
            binding.tvItemDesc.text = itemDescription
            binding.tvQty.text = holder.itemView.resources.getString(
                R.string.inwarded_quantity,
                inwardedQuantity,
                quantity
            )
        }
        holder.itemView.setOnClickListener {
            onItemClick(item)
        }
    }
}