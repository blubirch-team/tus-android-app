package com.blubirch.tus.ui.dispatch

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.repository.DispatchRepo
import com.blubirch.core.presentation.customview.attachdoc.AttachDocViewModel
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DispatchDetailsViewModel @Inject constructor() : AttachDocViewModel() {


    @Inject
    lateinit var dispatchRepo: DispatchRepo

    private lateinit var dispatchViewModel: DispatchViewModel

    var deliveryRef: String
        get() { return dispatchViewModel.deliveryRef }
        set(value) { dispatchViewModel.deliveryRef = value }
    var invoiceNo: String
        get() { return dispatchViewModel.invoiceNo }
        set(value) { dispatchViewModel.invoiceNo = value }
    var transporter: String
        get() { return dispatchViewModel.transporter }
        set(value) { dispatchViewModel.transporter = value }
    var lorryRecNo: String
        get() { return dispatchViewModel.lorryRecNo }
        set(value) { dispatchViewModel.lorryRecNo = value }
    var vehicleNo: String
        get() { return dispatchViewModel.vehicleNo }
        set(value) { dispatchViewModel.vehicleNo = value }
    var drivePhNo: String
        get() { return dispatchViewModel.drivePhNo }
        set(value) { dispatchViewModel.drivePhNo = value }


    val dispatchGatePassResponseLiveData by lazy {
        MutableLiveData<Long>()
    }


    fun setMainModel(dispatchViewModel: DispatchViewModel) {
        DispatchDetailsViewModel@this.dispatchViewModel = dispatchViewModel
    }

    fun dispatchGatePass() {
        progressLiveData.value = true
        compositeDisposable.add(
            dispatchRepo.dispatchGatePass(
                dispatchViewModel.currentlySelectedWarehouseOrders,
                deliveryRef,
                invoiceNo,
                dispatchViewModel.consignmentId,
                transporter,
                lorryRecNo,
                vehicleNo,
                drivePhNo,
                "",
                attachedDocLiveData.value
            )
                .subscribeOn(Schedulers.io())
                .subscribe { dispatchGatePassResponseDTO, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        val id: Long? =
                            if (dispatchGatePassResponseDTO.id > 0) {
                                dispatchGatePassResponseDTO.id
                            } else {
                                dispatchGatePassResponseDTO.consignmentId
                            }

                        id?.let {
                            if (it > 0) {
                                for (warehouseOrder in dispatchViewModel.currentlySelectedWarehouseOrders) {
                                    warehouseOrder.isDispatched = true
                                }
                                dispatchViewModel.consignmentId = it
                                dispatchViewModel.vendorCode = dispatchViewModel.currentlySelectedWarehouseOrders.last().vendorCode
                                val attachedDocs = attachedDocLiveData.value
                                attachedDocs?.let {
                                    for (attachedDoc in it) {
                                        attachedDoc.isSyncedWithServer = true
                                    }
                                    dispatchViewModel.setAttachedDocs(it)
                                }
                                dispatchGatePassResponseLiveData.postValue(it)
                            }
                        }
                    } else {
                        postAPIError(throwable)
                    }

                }
        )
    }

    fun setAttachedDocs(attachedDocs: ArrayList<AttachedDoc>) {
        addDoc(attachedDocs)
        (attachedDocLiveData as MutableLiveData<ArrayList<AttachedDoc>>).value = attachedDocs
    }

    fun getVendorCode(): String {
        return dispatchViewModel.vendorCode
    }

    fun getAllSelectedOrders(): MutableList<WarehouseOrders> {
        return dispatchViewModel.getAllSelectedOrders();
    }

    fun getWarehouseOrderFileTypes(): ArrayList<ArrayList<String?>?>? {
        return dispatchViewModel.getWarehouseOrderFileTypes()
    }

}