package com.blubirch.tus.ui.inward

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.constants.GatePassInventoryStatus
import com.blubirch.tus.data.model.inward.GatePass
import com.blubirch.tus.data.model.inward.GatePassInventory
import com.blubirch.tus.data.repository.GatePassRepo
import com.blubirch.core.data.MyException
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import kotlin.collections.ArrayList

class GrnSummaryViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var gatePassRepo: GatePassRepo

    //Selected GatePass
    lateinit var gatePass: GatePass
        private set

    val gatePassInventories: ArrayList<GatePassInventory> = arrayListOf()

    val proceedToGrnLiveData by lazy {
        MutableLiveData<String?>(null)
    }

    fun proceedToGrn() {
        progressLiveData.value = true
        compositeDisposable.add(
            gatePassRepo.proceedGRN(gatePass.clientGatePassNumber)
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        if (result.status == 200) {
                            proceedToGrnLiveData.postValue(gatePass.clientGatePassNumber + ": " + result.message)
                        } else {
                            errorLiveData.postValue(MyException.ApiError(Throwable(result.message)))
                        }
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }

    fun updateGatePass(gatePass: GatePass) {
        this.gatePass = gatePass
        gatePassInventories.clear()
        gatePass.gatePassInventories.forEach {

            if (it.quantity > 0 && it.inwardedQuantity > 0 && it.inwardedQuantity != it.quantity) {
                val gatePassInventory = GatePassInventory()
                gatePassInventory.skuCode = it.skuCode
                gatePassInventory.clientCategoryId = it.clientCategoryId
                gatePassInventory.clientCategory = it.clientCategory
                gatePassInventory.brand = it.brand
                gatePassInventory.status = it.status
                gatePassInventory.distributionCenterId = it.distributionCenterId
                gatePassInventory.id = it.id
                gatePassInventory.quantity = it.quantity
//                if (it.inwardedQuantity > it.quantity) {
//                    gatePassInventory.status = GatePassInventoryStatus.FULLY_RECEIVED
//                    gatePassInventory.inwardedQuantity = it.quantity
//                } else if (it.inwardedQuantity < it.quantity) {
                gatePassInventory.status = com.blubirch.tus.constants.GatePassInventoryStatus.FULLY_RECEIVED
                gatePassInventory.quantity = it.inwardedQuantity
//                }
                gatePassInventories.add(gatePassInventory)
            }
            gatePassInventories.add(it)
        }
    }

}