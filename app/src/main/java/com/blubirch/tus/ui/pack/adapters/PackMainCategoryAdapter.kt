package com.blubirch.tus.ui.pack.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.databinding.ItemPackCategoryListBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PackMainCategoryAdapter : BaseQuickAdapter<ArrayList<String>, BaseViewHolder>(R.layout.item_pack_category_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<ArrayList<String>>() {
            override fun areItemsTheSame( oldItem: ArrayList<String>, newItem: ArrayList<String>): Boolean {
                return false
            }

            override fun areContentsTheSame( oldItem: ArrayList<String>, newItem: ArrayList<String> ): Boolean {
                return false
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: ArrayList<String>) {
        val binding = ItemPackCategoryListBinding.bind(holder.itemView)
        binding.txtArticleId.text = item[0]
        binding.txtArticleDesc.text = item[1]
        binding.txtQuantity.text = item[2]
    }

}