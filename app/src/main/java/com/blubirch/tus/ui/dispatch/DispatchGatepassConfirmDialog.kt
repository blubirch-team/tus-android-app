package com.blubirch.tus.ui.dispatch

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.databinding.DialogDispatchCompletionDialogBinding
import com.blubirch.tus.ui.dispatch.adapters.DispatchGatepassConfirmViewModel
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseBottomSheetDialogFragment
import com.blubirch.core.presentation.TakePictureActivity
import com.blubirch.core.presentation.customview.attachdoc.AttachDocView
import com.blubirch.core.presentation.customview.attachdoc.AttachDocViewModel
import com.blubirch.core.utility.KotlinJavaDelegation
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.dialog_dispatch_gatepass_confirm.*
import javax.inject.Inject


class DispatchGatepassConfirmDialog : BaseBottomSheetDialogFragment<DispatchGatepassConfirmViewModel>() {

    companion object {
        fun newInstance() =
            DispatchGatepassConfirmDialog()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var dispatchGatepassConfirmViewModel: DispatchGatepassConfirmViewModel
    private lateinit var attachDocViewModel: AttachDocViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.dialog_dispatch_gatepass_confirm, container, false)
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val dialog = it as BottomSheetDialog

            dialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
            val containerLayout: FrameLayout =
                dialog.findViewById<FrameLayout>(com.google.android.material.R.id.container) as FrameLayout
            val parent = frameButton.parent as ViewGroup
            parent.removeView(frameButton)
            containerLayout.addView(frameButton, containerLayout.childCount)
            (frameButton.layoutParams as FrameLayout.LayoutParams).gravity = Gravity.BOTTOM
        }
        return bottomSheetDialog
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        isCancelable = false

        val backStackEntry = navController.getBackStackEntry(R.id.navDispatch)
        val dispatchViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(DispatchViewModel::class.java)
        dispatchGatepassConfirmViewModel = ViewModelProvider(this, viewModelFactory).get(DispatchGatepassConfirmViewModel::class.java)
        dispatchGatepassConfirmViewModel.setMainModel(dispatchViewModel)

        attachDocViewModel =
            ViewModelProvider(this, viewModelFactory).get(AttachDocViewModel::class.java)

        attachedDocs.setListener(object : AttachDocView.AttachDocListener {
            override fun startImageCapture() {
                Intent(activity, TakePictureActivity::class.java).let {
                    startActivityForResult(it, TakePictureActivity.INTENT_REQUEST_CODE_TAKE_PICTURE)
                }
            }

        })

        attachDocViewModel.setDocTypes(dispatchViewModel.getWarehouseOrderFileTypes())
        attachDocViewModel.setRefFillValue(dispatchViewModel.getDispatchedOrSelectedOrder()?.gatepassNumber ?: "")


        txtTotalGatePass.text = getViewModel().getGatePassCount()
        txtTotalQuantity.text = getViewModel().getTotalQuantity()

        attachedDocs.setViewModel(viewLifecycleOwner, attachDocViewModel)

        btnCancel.setOnClickListener {
            dismiss()
        }
        btnConfirmAndProceed.setOnClickListener {
            if (attachDocViewModel.attachedDocLiveData.value.isNullOrEmpty()) {
                showSnackBar("Please Provide attachment.")
            } else {
                val binding =
                    DialogDispatchCompletionDialogBinding.inflate(LayoutInflater.from(requireContext()))
                val dialog = KotlinJavaDelegation.createDialog(binding.root)
                binding.btnCancel.setOnClickListener {
                    dialog.dismiss()
                }
                binding.btnConfirm.setOnClickListener {
                    getViewModel().confirmDispatch(attachDocViewModel.attachedDocLiveData.value!!)
                    dialog.dismiss()
                }
                dialog.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        attachedDocs.onActivityResult(requestCode, resultCode, data)
    }

    override fun getViewModel(): DispatchGatepassConfirmViewModel {
        return dispatchGatepassConfirmViewModel
    }

    override fun attachLiveData() {
        getViewModel().dispatchConsignmentResponseLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    showSnackBar("Successfully Dispatched.")
                    completeDispatch()
                }
            }
        })
    }

    private fun completeDispatch() {
        try {
            dismiss()
            navigate(DispatchGatepassConfirmDialogDirections.actionDispatchGatepassConfirmDialogToMenuFragment())
        } catch (ex: Exception) {
        }
    }

}
