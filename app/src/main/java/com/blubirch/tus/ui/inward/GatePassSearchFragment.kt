package com.blubirch.tus.ui.inward

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.data.model.inward.GatePass
import com.blubirch.tus.databinding.DialogConfirmOtherDestinationGatePassBinding
import com.blubirch.tus.ui.adapters.PendingReceiptListAdapter
import com.blubirch.tus.utils.CustomTextWatcher
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.data.MyException
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.KotlinJavaDelegation
import kotlinx.android.synthetic.main.fragment_gatepass_search.*
import javax.inject.Inject


class GatePassSearchFragment : BaseFragment<GatePassSearchViewModel>() {

    companion object {
        fun newInstance() = GatePassSearchFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: GatePassSearchViewModel
    private lateinit var inwardViewModel: InwardViewModel

    private var customTextWatcher: CustomTextWatcher? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(GatePassSearchViewModel::class.java)
        val backStackEntry =
            navController.getBackStackEntry(R.id.navInward)
        inwardViewModel =
            ViewModelProvider(backStackEntry, viewModelFactory).get(InwardViewModel::class.java)
        viewModel.loadData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_gatepass_search, container, false)
    }

    override fun getViewModel(): GatePassSearchViewModel {
        return viewModel
    }

    override fun onResume() {
        super.onResume()
        setListeners()
    }

    private fun setListeners() {
        edtSearchSTN.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.searchGatePass(edtSearchSTN.text.toString())
                return@OnEditorActionListener true
            }
            false
        })

        customTextWatcher = object : CustomTextWatcher(edtSearchSTN) {
            override fun textChanged(s: Editable?) {
                removeError()
                viewModel.filterGatePass(edtSearchSTN.text.toString())
            }
        }
        edtSearchSTN.addTextChangedListener(customTextWatcher)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
    }


    override fun onPause() {
        super.onPause()
        if (customTextWatcher != null)
            edtSearchSTN.removeTextChangedListener(customTextWatcher)
    }

    private fun setupList() {
        val dividerItemDecoration = DividerItemDecoration(requireContext())

        rvItemList.apply {
            addItemDecoration(dividerItemDecoration)
            rvItemList.adapter =
                PendingReceiptListAdapter(object : PendingReceiptListAdapter.OnItemClickListener {
                    override fun onItemClicked(gatePass: GatePass, position: Int) {
                        inwardViewModel.updateGatePass(gatePass)
                        openDetails()
                    }
                })
        }
    }


    private fun openDetails() {
        navigate(GatePassSearchFragmentDirections.actionGatePassSearchFragmentToGatePassDetailsFragment())
    }

    override fun attachLiveData() {

        viewModel.apply {
            gatePassesLiveData.observe(viewLifecycleOwner, Observer {
                (rvItemList.adapter as PendingReceiptListAdapter).apply {
                    setEmptyView(R.layout.layout_empty_list_msg_view)
                    if (data.size == 0) {
                        setNewInstance(it)
                    } else {
                        setDiffNewData(it)
                        notifyItemRangeChanged(0, it.size)
                    }
                }
            })

            gatePassLiveData.observe(viewLifecycleOwner, Observer {
                it?.also {
                    inwardViewModel.updateGatePass(it)
                    removeError()
                    openDetails()
                    viewModel.gatePassLiveData.value = null
                }

            })

            otherDestinationGatePassLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    if (it) {
                        openOtherDestinationConfirmation()
                        otherDestinationGatePassLiveData.postValue(false)
                    }
                }
            })
            errorLiveData.observe(viewLifecycleOwner, Observer {
                if (it != null && it is MyException.ValidationError) {
                    setError()
                }
            })
        }

    }

    private fun openOtherDestinationConfirmation() {
        val binding =
            DialogConfirmOtherDestinationGatePassBinding.inflate(LayoutInflater.from(requireContext()))
        val dialog = KotlinJavaDelegation.createDialog(binding.root)
        dialog.show()
        binding.btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        binding.btnConfirm.setOnClickListener {
            dialog.dismiss()
//            viewModel.loadOtherDestinationGatePass()
        }
    }

    private fun setError() {
        tilSearchItem.errorIconDrawable = null
        tilSearchItem.error = getString(R.string.incorrect_stn_message)
        tilSearchItem.requestFocus()
    }

    private fun removeError() {
        tilSearchItem.error = null
    }
}