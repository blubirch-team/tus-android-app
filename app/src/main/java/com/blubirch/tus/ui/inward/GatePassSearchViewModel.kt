package com.blubirch.tus.ui.inward

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.model.inward.GatePass
import com.blubirch.tus.data.repository.GatePassRepo
import com.blubirch.core.data.MyException
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GatePassSearchViewModel @Inject constructor(private val gatePassRepo: GatePassRepo) :
    BaseViewModel() {

    private val gatePasses: ArrayList<GatePass> = arrayListOf()
    private lateinit var otherGatePass: GatePass

    val gatePassesLiveData by lazy { MutableLiveData<ArrayList<GatePass>>() }
    val gatePassLiveData by lazy { MutableLiveData<GatePass?>() }
    val otherDestinationGatePassLiveData by lazy { MutableLiveData<Boolean?>(null) }

    fun loadData() {
        progressLiveData.value = true
        compositeDisposable.add(
            gatePassRepo.loadData()
                .subscribeOn(Schedulers.io())
                .subscribe { gatePassDto, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        gatePasses.clear()
                        gatePassDto.gatePasses?.let {
                            gatePasses.addAll(it.sortedByDescending { it.dispatchDate })
                        }
                        gatePassesLiveData.postValue(ArrayList(gatePasses))
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }

    fun searchGatePass(gatePassNumber: String) {
        progressLiveData.value = true
        compositeDisposable.add(
            Observable.fromIterable(gatePasses)
                .filter { gatePass ->
                    gatePass.clientGatePassNumber == gatePassNumber
                }
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { gatePassList, throwable ->
                    if (throwable == null) {
                        if (gatePassList.size == 1) {
                            gatePassList[0].let {
                                gatePassLiveData.postValue(it)
                                progressLiveData.postValue(false)
                            }
                        } else {
                            searchGatePassAPI(gatePassNumber)
                        }
                    } else {
                        errorLiveData.postValue(MyException.ValidationError(Throwable("Incorrect STN Number")))
                        progressLiveData.postValue(false)
                    }

                })
    }

    private fun searchGatePassAPI(gatePassNumber: String) {
        progressLiveData.postValue(true)
        compositeDisposable.add(
            gatePassRepo.searchGatePass(gatePassNumber)
                .subscribeOn(Schedulers.computation())
                .subscribe { gatePassDTO, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null && gatePassDTO.gatePasses != null && gatePassDTO.gatePasses.isNotEmpty()) {
                        progressLiveData.postValue(false)
                        if (gatePassDTO.meta != null && gatePassDTO.meta.message != null && gatePassDTO.meta.message == "Destination Mismatch") {
                            otherGatePass = gatePassDTO.gatePasses[0]
                            otherDestinationGatePassLiveData.postValue(true)
                        } else {
                            gatePassDTO.gatePasses[0].let {
                                gatePassLiveData.postValue(it)
                            }
                        }
                    } else {
                        errorLiveData.postValue(Exception("Incorrect STN Number"))
                    }
                })
    }

    fun filterGatePass(gatePassNumber: String) {
        if(gatePassNumber.isBlank()){
            gatePassesLiveData.postValue(ArrayList(gatePasses))
            return
        }
        compositeDisposable.add(
            Observable.fromIterable(gatePasses)
                .filter { gatePass ->
                    gatePass.clientGatePassNumber.contains(gatePassNumber, true)
                }
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { gatePassList, throwable ->
                    if (throwable == null && gatePassList.isNotEmpty()) {
                        gatePassesLiveData.postValue(gatePassList as ArrayList<GatePass>)
                    } else {
                        gatePassesLiveData.postValue(arrayListOf())
//                        errorLiveData.value = Exception("Incorrect STN Number")
                    }
                })
    }


    fun loadOtherDestinationGatePass() {
        gatePassLiveData.postValue(otherGatePass)
    }
}
