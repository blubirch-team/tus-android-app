package com.blubirch.tus.ui.stowing.adapter

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.Inventory
import com.blubirch.tus.databinding.ItemToatInventoryListBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class ToatInventoryListAdapter(val onItemClick: (Inventory) -> Unit) :
    BaseQuickAdapter<Inventory, BaseViewHolder>(R.layout.item_toat_inventory_list), LoadMoreModule  {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<Inventory>() {
            override fun areItemsTheSame(oldItem: Inventory, newItem: Inventory): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                    oldItem: Inventory,
                    newItem: Inventory
            ): Boolean {
                return oldItem.itemDesc == newItem.itemDesc &&
                        oldItem.tagNumber == newItem.tagNumber &&
                        oldItem.location == newItem.location
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: Inventory) {
        val binding = ItemToatInventoryListBinding.bind(holder.itemView)
        binding.txtItemTitle.text = item.itemDesc
        binding.txtTagNumber.text = item.tagNumber
        when {
            item.location == null || item.location.isBlank() -> {
                binding.txtLocation.text = "-"
                binding.txtStatus.setText(R.string.not_stowed)
                binding.txtStatus.setTextColor(holder.itemView.resources.getColor(R.color.text_color_E62626))
            }
            else -> {
                binding.txtLocation.text = item.location
                binding.txtStatus.setText(R.string.stowed)
                binding.txtStatus.setTextColor(holder.itemView.resources.getColor(R.color.text_color_219653))
            }
        }
        holder.itemView.setOnClickListener {
            onItemClick(item)
        }
    }
}