package com.blubirch.tus.ui.iteminfo

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import kotlinx.android.synthetic.main.fragment_item_info_search.*
import javax.inject.Inject

class ItemInfoSearchFragment : BaseFragment<ItemInfoViewModel>() {

    companion object {
        const val ITEM_SEARCH_REQUEST_CODE = 4455
        fun newInstance() = ItemInfoSearchFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: ItemInfoViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val backStackEntry = navController.getBackStackEntry(R.id.navItemInfo)
        viewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(ItemInfoViewModel::class.java)

        tilSearchItem.initialize(this, ITEM_SEARCH_REQUEST_CODE) {
            btnConfirm.performClick()
            AppUtils.hideSoftKeyboard(requireActivity())
        }

        tilSearchItem.editText.addTextChangedListener(afterTextChanged = {
            tilSearchItem.setError(null)
        })

        btnConfirm.setOnClickListener {
            viewModel.progressLiveData.value = true
            viewModel.searchItem(tilSearchItem.text.toString())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_item_info_search, container, false)
    }

    override fun getViewModel(): ItemInfoViewModel {
        return viewModel
    }

    override fun attachLiveData() {
        viewModel.itemInfoLiveData.observe(viewLifecycleOwner, Observer {
            viewModel.progressLiveData.value = false
            it?.let {
                navigate(ItemInfoSearchFragmentDirections.actionItemSearchFragmentToItemOptionsFragment())
            }
        })
        viewModel.itemInfoSearchErrorLive.observe(viewLifecycleOwner, Observer {
            viewModel.progressLiveData.value = false
            it?.let {
                tilSearchItem.setError(it)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ITEM_SEARCH_REQUEST_CODE -> {
                tilSearchItem.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

}
