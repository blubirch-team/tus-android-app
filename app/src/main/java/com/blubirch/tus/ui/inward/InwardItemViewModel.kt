package com.blubirch.tus.ui.inward

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.model.inward.Detail
import com.blubirch.tus.data.model.inward.GatePassInventory
import com.blubirch.tus.data.repository.GatePassRepo
import com.blubirch.core.data.MyException
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class InwardItemViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var gatePassRepo: GatePassRepo
    val gatePassInventories: List<GatePassInventory> = arrayListOf()
    var itemCode = ""
    var serialNumber: String? = null
    var serialNumber2: String? = null

    val itemCodeErrorLiveData by lazy { MutableLiveData<Exception>() }
    val serialNumberErrorLiveData by lazy { MutableLiveData<Exception>() }

    val gatePassInventory by lazy { MutableLiveData<GatePassInventory?>() }
    val filteredInventoryLiveData: LiveData<List<GatePassInventory>> =
        MutableLiveData<List<GatePassInventory>>()

    fun updateGatePassInventories(gatePassInventories: List<GatePassInventory>) {
        (this.gatePassInventories as ArrayList).apply {
            clear()
            addAll(gatePassInventories)
        }
        (this.filteredInventoryLiveData as MutableLiveData).value = gatePassInventories
    }

    fun filterGatePass(searchTerm: String) {
        gatePassInventories?.apply {
            compositeDisposable.add(
                Observable.fromIterable(this)
                    .filter {
                        val skuCode : String? = it.skuCode?.toLowerCase()
                        val ean : String? = it.ean?.toLowerCase()
                        val searchTerm = searchTerm.toLowerCase()

                        (skuCode != null && skuCode.contains(searchTerm)) ||
                                (ean != null && ean.contains(searchTerm))
                    }.toList()
                    .subscribeOn(Schedulers.computation())
                    .subscribe { filteredList: MutableList<GatePassInventory>?, throwable: Throwable? ->
                        throwable?.let {
                            it.printStackTrace()
                            postAPIError(it)
                        }
                        filteredList?.let {
                            (filteredInventoryLiveData as MutableLiveData).postValue(it)
                        }
                    }
            )
        }
    }

    fun isItemInCurrentDoc(itemCode: String): Boolean {
        return try {
            getItemPresentObservable(itemCode).blockingGet() != null
        } catch (ex : Exception) {
            false
        }
    }

    fun areAllItemsGraded(itemCode: String): Boolean? {
        for (gatePassInventory in gatePassInventories) {
            if (gatePassInventory.skuCode == itemCode) {
                return gatePassInventory.inwardedQuantity >= gatePassInventory.quantity
            }
        }
        return null
    }

    private fun checkItemPresentInList(itemCode: String, onlineSearchRequired: Boolean = false) {
        compositeDisposable.add(
            getItemPresentObservable(itemCode)
                .subscribeOn(Schedulers.computation())
                .subscribe { searchedGatePassInventory: GatePassInventory?, throwable: Throwable? ->
                    progressLiveData.postValue(false)
                    if (throwable == null && searchedGatePassInventory != null) {
                        gatePassInventory.postValue(searchedGatePassInventory)
                    } else if (onlineSearchRequired) {
                        searchItemCodeOnline()
                    } else {
                        itemCodeErrorLiveData.postValue(Exception("Incorrect Article Id"))
                    }
                }
        )
    }

    private fun getItemPresentObservable(itemCode: String): Single<GatePassInventory?> {
        return Observable.fromIterable(gatePassInventories)
            .filter { gatePassInventory ->
                gatePassInventory.skuCode.equals(itemCode, true)
                    .or(gatePassInventory.ean.equals(itemCode, true))
            }.firstOrError()
    }

    fun searchItemCode(itemCode: String, serialNumber: String, serialNumber2: String) {
        this.itemCode = itemCode
        this.serialNumber = serialNumber
        this.serialNumber2 = serialNumber2
        var validated = true
        if (itemCode == null || itemCode.isBlank()) {
            itemCodeErrorLiveData.postValue(Exception("Incorrect Article Id"))
            validated = false
        }
        if (serialNumber != null && serialNumber.isNotBlank()) {
            verifySerialNumber()
            validated = false
        } else if (serialNumber2 != null && serialNumber2.isNotBlank()) {
            verifySerialNumber()
            validated = false
        }
        if (validated) {
            checkItemPresentInList(itemCode, true)
        }
    }

    private fun verifySerialNumber() {
        progressLiveData.postValue(true)
        compositeDisposable.add(
            gatePassRepo.verifySerialNumber(serialNumber, serialNumber2)
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    if (throwable == null) {
                        if (result.status == 204) {
                            checkItemPresentInList(itemCode, true)
                        } else {
                            progressLiveData.postValue(false)
                            errorLiveData.postValue(MyException.ApiError(Throwable(result.message)))
                        }
                    } else {
                        progressLiveData.postValue(false)
                        postAPIError(throwable)
                    }
                })
    }

    private fun searchItemCodeOnline() {
        progressLiveData.postValue(true)
        compositeDisposable.add(
            gatePassRepo.searchSKU(itemCode)
                .subscribeOn(Schedulers.io())
                .subscribe { item, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null && item.SKUS != null && item.SKUS.isNotEmpty()) {
                        val gatePassInventory1 = GatePassInventory()
                        gatePassInventory1.apply {
                            quantity = 0
                            inwardedQuantity = 0
                            skuCode = item.SKUS[0].code
                            brand = item.SKUS[0].brand
                            clientCategoryId = item.SKUS[0].clientCategoryId
                            item.SKUS[0].desc?.let { itemDescription = it }
                            details = Detail(item.SKUS[0].ownLabel)
                        }
                        gatePassInventory.postValue(gatePassInventory1)
                    } else {
                        itemCodeErrorLiveData.postValue(Exception("Incorrect Article Id"))
                    }
                })
    }
}
