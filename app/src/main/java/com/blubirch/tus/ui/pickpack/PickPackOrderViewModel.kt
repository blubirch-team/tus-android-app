package com.blubirch.tus.ui.pickpack

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.api.dto.pack.AssignBoxResDTO
import com.blubirch.tus.data.api.dto.pack.CreateBoxResDTO
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.data.model.pack.Box
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.tus.data.repository.PickPackRepo
import com.blubirch.core.data.MyException
import com.blubirch.core.data.api.dto.BaseDTO
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import kotlin.collections.ArrayList

class PickPackOrderViewModel @Inject constructor(private val pickPackRepo: PickPackRepo) : BaseViewModel() {

    private lateinit var pickPackViewModel: PickPackViewModel

    private var isPackedVisible : Boolean = false

    val notPickedLiveData : LiveData<MutableList<WarehouseOrderItems>> by lazy {
        MutableLiveData<MutableList<WarehouseOrderItems>>(ArrayList())
    }

    val pickNotPickedCountLiveData : LiveData<Pair<Int, Int>> by lazy {
        MutableLiveData<Pair<Int, Int>>()
    }

    val boxListItemLiveData: LiveData<ArrayList<BoxListItem>> by lazy {
        MutableLiveData<ArrayList<BoxListItem>>(ArrayList<BoxListItem>())
    }

    val dispatchResponseLiveData : LiveData<String> by lazy {
        MutableLiveData<String>()
    }

    fun setMainModel(pickPackViewModel: PickPackViewModel) {
        PickPackOrderViewModel@this.pickPackViewModel = pickPackViewModel
    }

    fun getOrderNumber(): String {
        return pickPackViewModel.selectedWarehouseOrder.orderNumber
    }

    fun getTotalInventories(): String {
        return pickPackViewModel.selectedWarehouseOrder.warehouseOrderItems.size.toString()
    }

    fun getPickedCount(): Int {
        pickNotPickedCountLiveData.value?.let {
            return it.second
        }
        return 0
    }

    fun getNotPickedCount(): Int {
        pickNotPickedCountLiveData.value?.let {
            return it.first
        }
        return 0
    }

    fun getTotalBoxes(): Int {
        return boxListItemLiveData.value?.size ?: 0
    }

    fun confirmDispatch() {
        compositeDisposable.add(
            pickPackRepo.confirmDispatch(pickPackViewModel.selectedWarehouseOrder.id)
                .subscribeOn(Schedulers.io())
                .subscribe { baseDTO: BaseDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    baseDTO?.let {
                        if (it.status == 200) {
                            (dispatchResponseLiveData as MutableLiveData).postValue(it.message)
                        } else {
                            if (it.error.trim().isNotEmpty()) {
                                postAPIError(Throwable(it.error))
                            } else if (it.message.trim().isNotEmpty()){
                                postAPIError(Throwable(it.message))
                            }
                        }
                    }
                }
        )
    }

    fun fetchNotPickedItems() {
        isPackedVisible = false
        compositeDisposable.add(
            Observable.fromIterable(pickPackViewModel.selectedWarehouseOrder.warehouseOrderItems)
                .filter{it.packagingBoxNumber == null && it.status == "Pending Pick"}
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { warehouseOrderItems: MutableList<WarehouseOrderItems>?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    warehouseOrderItems?.let {
                        postNotPickedList(it)
                    }
                }
        )
    }


    fun fetchPackedItems() {
        isPackedVisible = true

        val selectedWarehouseOrder = pickPackViewModel.selectedWarehouseOrder
        val boxListItems = boxListItemLiveData.value?: ArrayList()

        for (warehouseOrderItem in selectedWarehouseOrder.warehouseOrderItems) {
            val packagingBoxNumber: String = warehouseOrderItem.packagingBoxNumber ?: ""
            val packagingBoxId = warehouseOrderItem.packagingBoxId
            if(packagingBoxNumber.isNotEmpty() && packagingBoxId != null && packagingBoxId != 0L) {
                var boxListItem: BoxListItem? = null
                for (it in boxListItems) {
                    if (it.box.id == packagingBoxId && it.box.boxNumber == packagingBoxNumber) {
                        boxListItem = it
                        break
                    }
                }
                if (boxListItem == null) {
                    val box = Box(
                        id = packagingBoxId,
                        boxNumber = packagingBoxNumber, distributionCenterId = 0, userId = 0,
                        createdAt = "", updatedAt = "", deletedAt = ""
                    )
                    boxListItem = BoxListItem(box)
                    boxListItem.warehouseOrderItems.add(warehouseOrderItem)
                    boxListItems.add(boxListItem)
                } else {
                    var containsWarehouseItem = false
                    for (it in boxListItem.warehouseOrderItems) {
                        if (it.id == warehouseOrderItem.id) {
                            containsWarehouseItem = true
                            break
                        }
                    }
                    if (containsWarehouseItem.not()) {
                        boxListItem.warehouseOrderItems.add(warehouseOrderItem)
                    }
                }
            }
        }

        postPackedList()
    }

    private fun postNotPickedList(warehouseOrderItems: MutableList<WarehouseOrderItems>) {
        (notPickedLiveData as MutableLiveData).postValue(warehouseOrderItems)
        val notPicked = warehouseOrderItems.size
        val picked = pickPackViewModel.selectedWarehouseOrder.warehouseOrderItems.size - notPicked
        (pickNotPickedCountLiveData as MutableLiveData).postValue(Pair(notPicked, picked))
    }

    private fun postPackedList() {
        (boxListItemLiveData as MutableLiveData).postValue(boxListItemLiveData.value)
    }

    fun setBoxNumber(itemId: String, boxNumber: String) {

        if (itemId.isNotEmpty() && boxNumber.isNotEmpty()) {
            progressLiveData.postValue(true)

            compositeDisposable.add(
                Observable.fromIterable(boxListItemLiveData.value)
                    .concatMapIterable { it.warehouseOrderItems }
                    .any { it.packagingBoxNumber.isNullOrBlank().not() && it.tagNumber.toLowerCase() == itemId.toLowerCase() }
                    .subscribeOn(Schedulers.computation())
                    .subscribe { isAlreadyAdded: Boolean?, throwable: Throwable? ->
                        throwable?.let {
                            errorLiveData.postValue(MyException.UnKnownError(throwable))
                        }
                        isAlreadyAdded?.let {
                            if (it) {
                                errorLiveData.postValue(MyException.ValidationError(Throwable("Item already packed!")))
                            } else {
                                compositeDisposable.add(
                                    Observable.fromIterable(boxListItemLiveData.value)
                                        .filter{ it.box.boxNumber.toLowerCase() == boxNumber.toLowerCase() }
                                        .firstOrError()
                                        .subscribeOn(Schedulers.computation())
                                        .subscribe { boxListItem: BoxListItem?, throwable: Throwable? ->
                                            throwable?.let {
                                                val box = Box(
                                                    id = 0, boxNumber = boxNumber, distributionCenterId = 0,
                                                    userId = 0, createdAt = "", updatedAt = "", deletedAt = ""
                                                )
                                                val boxListItem = BoxListItem(box)
                                                boxListItemLiveData.value?.add(boxListItem)
                                                addItemToTheBox(itemId, boxListItem)
                                            }
                                            boxListItem?.let {
                                                addItemToTheBox(itemId, it)
                                            }
                                        }
                                )
                            }
                        }
                    }

            )
        }
    }

    private fun addItemToTheBox(tagNumber: String, boxListItem: BoxListItem) {
        compositeDisposable.add(
            Observable.fromIterable(pickPackViewModel.selectedWarehouseOrder.warehouseOrderItems)
                .filter{it.tagNumber.toLowerCase() == tagNumber.toLowerCase()}
                .firstOrError()
                .subscribeOn(Schedulers.computation())
                .subscribe { warehouseOrderItem: WarehouseOrderItems?, throwable: Throwable? ->
                    throwable?.let {
                        errorLiveData.postValue(MyException.ValidationError(Throwable("Could not find item")))
                    }
                    warehouseOrderItem?.let { outer ->
                        pickPackRepo.assignBox(outer.id, boxListItem.box.boxNumber)
                            .subscribeOn(Schedulers.io())
                            .subscribe { assignBoxResDTO: AssignBoxResDTO?, throwable: Throwable? ->
                                throwable?.let {
                                    it.printStackTrace()
                                    postAPIError(it)
                                }
                                assignBoxResDTO?.let { assignBoxRes ->
                                    if (!postAPIErrorIfAny(assignBoxRes)) {
                                        assignBoxRes.gatePass?.let {
                                            pickPackViewModel.selectedWarehouseOrder
                                                .gatepassNumber = it.gatepassNumber
                                        }
                                        assignBoxRes.warehouseOrderItem.let {
                                            outer.copy(it)
                                            assignBoxRes.boxId?.let { packagingBoxId ->
                                                boxListItem.box.id = packagingBoxId
                                                outer.packagingBoxId = packagingBoxId
                                            }
                                            boxListItem.warehouseOrderItems.add(outer)
                                            fetchNotPickedItems()
                                            postPackedList()
                                        }
                                    }
                                }
                            }
                }
            }
        )
    }


    fun removeFromBox(warehouseOrderItem: WarehouseOrderItems) {
        compositeDisposable.add(
            pickPackRepo.removeFromBox(warehouseOrderItem.id)
                .subscribeOn(Schedulers.io())
                .subscribe { baseDTO: BaseDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    baseDTO?.let {
                        if (baseDTO.status == 200) {
                            (boxListItemLiveData as MutableLiveData).value?.let {
                                warehouseOrderItem.packagingBoxNumber = null
                                for (boxListItem in it) {
                                    var removed = false
                                    for (index in boxListItem.warehouseOrderItems.indices) {
                                        if (boxListItem.warehouseOrderItems[index].id == warehouseOrderItem.id) {
                                            boxListItem.warehouseOrderItems.removeAt(index)
                                            removed = true
                                            break
                                        }
                                    }
                                    if(removed){
                                        break
                                    }
                                }
                            }
                            postPackedList()
                        } else {
                            if (it.error.trim().isNotEmpty()) {
                                postAPIError(Throwable(it.error))
                            } else if (it.message.trim().isNotEmpty()){
                                postAPIError(Throwable(it.message))
                            }
                        }
                    }
                }
        )

    }

    fun deleteBox(boxListItem: BoxListItem) {
        compositeDisposable.add(
            pickPackRepo.deleteBox(boxListItem.box.id)
                .subscribeOn(Schedulers.io())
                .subscribe { baseDTO: BaseDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    baseDTO?.let {
                        if (baseDTO.status == 200) {
                            (boxListItemLiveData as MutableLiveData).value?.let {
                                for (index in boxListItem.warehouseOrderItems.indices) {
                                    boxListItem.warehouseOrderItems[index].packagingBoxNumber = null;
                                }
                                it.remove(boxListItem)
                            }
                            postPackedList()
                        } else {
                            if (it.error.trim().isNotEmpty()) {
                                postAPIError(Throwable(it.error))
                            } else if (it.message.trim().isNotEmpty()){
                                postAPIError(Throwable(it.message))
                            }
                        }
                    }
                }
        )
    }

    fun createBox() {
        compositeDisposable.add(
            pickPackRepo.createBox(pickPackViewModel.selectedWarehouseOrder.id)
                .subscribeOn(Schedulers.io())
                .subscribe { createBoxResDTO: CreateBoxResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    createBoxResDTO?.let {
                        (boxListItemLiveData as MutableLiveData).value?.add(0, BoxListItem(it.box))
                        postPackedList()
                    }
                }
        )
    }

}