package com.blubirch.tus.ui.pickpack

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.tus.databinding.DialogPickPackCompleteConfirmBinding
import com.blubirch.tus.databinding.FragmentPickPackOrderBinding
import com.blubirch.tus.ui.pick.PickListFragment
import com.blubirch.tus.ui.pickpack.adapters.NotPickedListAdapter
import com.blubirch.tus.ui.pickpack.adapters.PackedListAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import com.blubirch.core.utility.KotlinJavaDelegation
import com.blubirch.core.utility.gone
import com.blubirch.core.utility.visible
import javax.inject.Inject

class PickPackOrderFragment : BaseFragment<PickPackOrderViewModel>() {

    companion object {
        const val SCAN_ITEM_ID_REQUEST_CODE = 7788
        const val SCAN_BOX_NO_REQUEST_CODE = 8877
        fun newInstance() = PickPackOrderFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var pickPackOrderViewModel: PickPackOrderViewModel

    private lateinit var binding : FragmentPickPackOrderBinding

    private lateinit var notPickedListAdapter : NotPickedListAdapter
    private lateinit var packedListAdapter : PackedListAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPickPack)
        val pickPackViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(
            PickPackViewModel::class.java)

        pickPackOrderViewModel = ViewModelProvider(this, viewModelFactory).get(PickPackOrderViewModel::class.java)
        pickPackOrderViewModel.setMainModel(pickPackViewModel)

        setNotPickedAdapter()
        setPackedAdapter()

        setNotPickedItems()

        binding.tilScanBoxNumber.initialize(this, PickListFragment.TOAT_SCAN_REQUEST_CODE){
            addItemToTheBox()
            AppUtils.hideSoftKeyboard(requireActivity())
        }

        binding.tilScanBoxNumber.editText.imeOptions = EditorInfo.IME_ACTION_DONE
        binding.tilScanBoxNumber.editText.setOnEditorActionListener { view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addItemToTheBox()
                AppUtils.hideSoftKeyboard(requireActivity())
                return@setOnEditorActionListener true
            }
            false
        }


        binding.txtCreateNewBoxId.setOnClickListener {
            getViewModel().progressLiveData.postValue(true)
            getViewModel().createBox()
        }

        binding.btnSaveAndContinue.setOnClickListener {
            saveAndContinue()
        }

        binding.btnNotPicked.setOnClickListener {
            setNotPickedItems()
        }

        binding.btnPacked.setOnClickListener {
            setPackedItems()
        }

        setToolbarTitle(
            String.format(
                resources.getString(R.string.pick_and_pack_s), getViewModel().getOrderNumber()
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        binding = FragmentPickPackOrderBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): PickPackOrderViewModel {
        return pickPackOrderViewModel
    }

    private fun setNotPickedAdapter() {
        notPickedListAdapter = NotPickedListAdapter{
            binding.tilScanItemId.text = it
        }
        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvNotPickedList.addItemDecoration(dividerItemDecoration)
        binding.rvNotPickedList.adapter = notPickedListAdapter
    }

    private fun setPackedAdapter() {
        packedListAdapter = PackedListAdapter(
            onItemDelete = { itemPosition: Int, warehouseOrderItem: WarehouseOrderItems, parentPosition: Int, BoxListItem ->
                getViewModel().progressLiveData.postValue(true)
                getViewModel().removeFromBox(warehouseOrderItem)
            },
            onBoxDelete = { position: Int, boxListItem : BoxListItem ->
                getViewModel().progressLiveData.postValue(true)
                getViewModel().deleteBox(boxListItem)
            },
            onPrint = { strToPrint ->
                showPrintTagView(strToPrint)
            }
        )
        binding.rvPackedList.adapter = packedListAdapter
    }


    private fun addItemToTheBox() {
        val itemId = binding.tilScanItemId.text?.trim()?.toString() ?: ""
        val boxNumber = binding.tilScanBoxNumber.text?.trim()?.toString() ?: ""

        getViewModel().setBoxNumber(itemId, boxNumber)
    }

    private fun setNotPickedItems(){
        binding.btnNotPicked.setBackgroundResource(R.drawable.ic_pick_selected_bg)
        binding.btnPacked.setBackgroundResource(R.drawable.ic_pick_normal_bg)
        binding.btnNotPicked.setTextColor(resources.getColor(R.color.button_selected_text_color))
        binding.btnPacked.setTextColor(resources.getColor(R.color.button_normal_text_color))
        binding.consNotPicked.visible()
        binding.consPacked.gone()
        getViewModel().fetchNotPickedItems()
    }

    private fun setPackedItems(){
        binding.btnPacked.setBackgroundResource(R.drawable.ic_pick_selected_bg)
        binding.btnNotPicked.setBackgroundResource(R.drawable.ic_pick_normal_bg)
        binding.btnPacked.setTextColor(resources.getColor(R.color.button_selected_text_color))
        binding.btnNotPicked.setTextColor(resources.getColor(R.color.button_normal_text_color))
        binding.consPacked.visible()
        binding.consNotPicked.gone()
        getViewModel().fetchPackedItems()
    }

    private fun saveAndContinue() {
        if (getViewModel().getPickedCount() < 1) {
            showSnackBar("Add any item first", binding.btnSaveAndContinue)
        } else {
            val binding : DialogPickPackCompleteConfirmBinding = DialogPickPackCompleteConfirmBinding.inflate(
                LayoutInflater.from(requireContext()))
            binding.txtOrderNumber.text = getViewModel().getOrderNumber()

            binding.txtTotalInventory.text = getViewModel().getTotalInventories()
            binding.txtTotalBoxes.text = getViewModel().getTotalBoxes().toString()
            binding.txtItemFound.text = getViewModel().getPickedCount().toString()
            binding.txtItemNotFound.text = getViewModel().getNotPickedCount().toString()

            val dialog = KotlinJavaDelegation.createDialog(binding.root)

            binding.btnGoBack.setOnClickListener {
                dialog.dismiss()
            }

            binding.btnConfirm.setOnClickListener {
                dialog.dismiss()
                getViewModel().progressLiveData.postValue(true)
                getViewModel().confirmDispatch()
            }

            dialog.show()
        }
    }


    override fun attachLiveData() {

        getViewModel().notPickedLiveData.observe(viewLifecycleOwner, Observer {
            getViewModel().progressLiveData.postValue(false)
            it?.let {
                if (it.isEmpty()) {
                    notPickedListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                    notPickedListAdapter.setNewInstance(null)
                } else {
                    if (notPickedListAdapter.data.size == 0) {
                        notPickedListAdapter.setNewInstance(it as ArrayList)
                    } else {
                        notPickedListAdapter.setDiffNewData(it as ArrayList)
                    }
                }
            }
        })

        getViewModel().pickNotPickedCountLiveData.observe(viewLifecycleOwner, Observer {
            binding.txtTotalItems.text =
                String.format(resources.getString(R.string.total_items_d), it.first + it.second)
            binding.txtNotPicked.text =
                String.format(resources.getString(R.string.not_picked_d), it.first)
            binding.txtPicked.text =
                String.format(resources.getString(R.string.picked_d), it.second)
        })

        getViewModel().boxListItemLiveData.observe(viewLifecycleOwner, Observer {

            getViewModel().progressLiveData.postValue(false)

            val adapterListData = packedListAdapter.data
            val adapterAndLiveDataSizeSame = it.size == adapterListData.size
            var dataChanged = adapterAndLiveDataSizeSame.not()

            if (it.isEmpty()) {
                packedListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                packedListAdapter.setNewInstance(null)
            } else {
                if(adapterAndLiveDataSizeSame) {
                    for (index in it.indices) {
                        if (it[index].warehouseOrderItems.size != adapterListData[index].warehouseOrderItems.size) {
                            dataChanged = true
                            break
                        }
                    }
                }

                val list: ArrayList<BoxListItem> = ArrayList(it)
                packedListAdapter.setDiffNewData(list)
                packedListAdapter.notifyItemRangeChanged(0, list.size)
                binding.rvPackedList.post { binding.rvPackedList.smoothScrollToPosition(0) }
            }

            if(dataChanged){
//                binding.tilScanItemId.text = ""
//                binding.tilScanBoxNumber.text = ""
                if (it.size > 0) {
                    binding.tilScanBoxNumber.text = (it[0].box.boxNumber)
                }
            }
        })

        getViewModel().dispatchResponseLiveData.observe(viewLifecycleOwner, Observer {
            getViewModel().progressLiveData.postValue(false)
            showSnackBar(it)
            navigate(PickPackOrderFragmentDirections.actionPickPackOrderFragmentToMenuFragment())
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SCAN_ITEM_ID_REQUEST_CODE -> {
                binding.tilScanItemId.onActivityResult(requestCode, resultCode, data)
            }
            SCAN_BOX_NO_REQUEST_CODE -> {
                binding.tilScanBoxNumber.onActivityResult(requestCode, resultCode, data)
            }
        }
    }
}