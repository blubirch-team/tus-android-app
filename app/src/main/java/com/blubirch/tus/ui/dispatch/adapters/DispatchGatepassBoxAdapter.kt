package com.blubirch.tus.ui.dispatch.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.databinding.ItemDispatchGatepassBoxesBinding
import com.blubirch.tus.utils.DividerItemDecoration
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class DispatchGatepassBoxAdapter: BaseQuickAdapter<WarehouseOrders, BaseViewHolder>(R.layout.item_dispatch_gatepass_boxes) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrders>() {
            override fun areItemsTheSame(oldItem: WarehouseOrders, newItem: WarehouseOrders): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: WarehouseOrders, newItem: WarehouseOrders): Boolean {
                return oldItem.gatepassNumber == newItem.gatepassNumber &&
                        areBoxesSame(oldItem.warehouseOrderItems, newItem.warehouseOrderItems)
            }
        })
    }

    private fun areBoxesSame(oldItems: List<WarehouseOrderItems>, newItems: List<WarehouseOrderItems>) : Boolean {
        if (oldItems.size != newItems.size) {
            return false
        }

        for (index in oldItems.indices) {
            if (oldItems[index].packagingBoxNumber != newItems[index].packagingBoxNumber) {
                return false
            }
        }

        return true
    }

    override fun convert(holder: BaseViewHolder, warehouseOrder: WarehouseOrders) {
        val binding = ItemDispatchGatepassBoxesBinding.bind(holder.itemView)
        binding.txtGatepass.text = warehouseOrder.gatepassNumber

        val boxNumbers = ArrayList<String>();
        for (warehouseOrderItem in warehouseOrder.warehouseOrderItems) {
            warehouseOrderItem.packagingBoxNumber?.let {
                if (boxNumbers.contains(it).not()) {
                    boxNumbers.add(it)
                }
            }
        }

        if (binding.expansionHeader.isExpanded.not()) {
            binding.expansionHeader.apply {
                post {
                    performClick()
                }
            }
        }

        binding.txtBoxCount.text = String.format(
            binding.root.context.resources.getString(R.string._d_boxes), boxNumbers.size)
        val dispatchBoxItemAdapter = DispatchBoxItemAdapter()
        dispatchBoxItemAdapter.setNewInstance(boxNumbers)
        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(context)
        binding.rvBoxItems.addItemDecoration(dividerItemDecoration)
        binding.rvBoxItems.adapter = dispatchBoxItemAdapter
    }

}