package com.blubirch.tus.ui.pick

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.databinding.FragmentPickItemListBinding
import com.blubirch.tus.ui.pick.adapters.PickItemListAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import javax.inject.Inject


class PickItemListFragment : BaseFragment<PickItemListViewModel>() {

    companion object {
        fun newInstance() = PickItemListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var pickItemListViewModel: PickItemListViewModel

    private lateinit var binding: FragmentPickItemListBinding

    private lateinit var pickItemListAdapter: PickItemListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPick)
        val pickViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(PickViewModel::class.java)
        pickItemListViewModel = ViewModelProvider(this, viewModelFactory).get(PickItemListViewModel::class.java)

        pickItemListAdapter = PickItemListAdapter(object : PickItemListAdapter.ItemClickListener{
            override fun onItemClick(position: Int, warehouseOrder: WarehouseOrders) {
                pickViewModel.selectedWarehouseOrder = warehouseOrder
                navigate(PickItemListFragmentDirections.actionPickItemListFragmentToPickMainFragment())
            }
        })

        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvItemList.addItemDecoration(dividerItemDecoration)
        binding.rvItemList.adapter = pickItemListAdapter

        binding.edtSearchVendor.addTextChangedListener(afterTextChanged = {
            it?.let {
                pickItemListViewModel.searchWarehouseByVendor(it.toString())
            }
        })

        pickItemListViewModel.progressLiveData.value = true
        pickItemListViewModel.getWareHouseList()

    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPickItemListBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): PickItemListViewModel {
        return pickItemListViewModel;
    }

    override fun attachLiveData() {
        getViewModel().warehouseOrdersLiveData.observe(viewLifecycleOwner, Observer {
            pickItemListViewModel.progressLiveData.postValue(false)
            it?.let {
                if (it.isEmpty()) {
                    pickItemListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                    pickItemListAdapter.setNewInstance(null)
                } else {
//                    if (BuildConfig.DEBUG && BuildConfig.FLAVOR == "dev") {
//                        while (it.size < 300) {
//                            (it as MutableList).apply {
//                                val copy = get(0).copy(
//                                    id = (1000 + size).toLong(),
//                                    vendorCode = AppUtils.generateSafeToken(),
//                                    vendorName = AppUtils.generateSafeToken())
//                                add(copy)
//                            }
//                        }
//                    }
                    if (pickItemListAdapter.data.size == 0) {
                        pickItemListAdapter.setNewInstance(it as ArrayList)
                    } else {
                        pickItemListAdapter.setDiffNewData(it as ArrayList)
                    }
                }
            }
        })
    }

}
