package com.blubirch.tus.ui.dispatch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.repository.DispatchRepo
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.utility.CustomPredicate
import com.blubirch.core.utility.filterWith
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DispatchListViewModel @Inject constructor(private val dispatchRepo: DispatchRepo) : BaseViewModel() {

    private lateinit var dispatchViewModel: DispatchViewModel

    val dispatchListLiveData: LiveData<List<WarehouseOrders>> by lazy {
        MutableLiveData<List<WarehouseOrders>>()
    }

    private var dispatchListSearchTerm: String = ""

    fun setMainModel(dispatchViewModel: DispatchViewModel) {
        DispatchListViewModel@this.dispatchViewModel = dispatchViewModel
    }

    fun getList() {
        if (dispatchViewModel.dispatchWarehouseOrders.isNotEmpty()) {
            postFilteredList()
            return
        }
        compositeDisposable.add(
            dispatchRepo.list
                .subscribeOn(Schedulers.io())
                .subscribe { warehouseOrdersResDTO: WarehouseOrdersResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }

                    if (postAPIErrorIfAny(warehouseOrdersResDTO).not()) {
                        warehouseOrdersResDTO?.warehouseOrders?.let {
                            dispatchViewModel.dispatchWarehouseOrders.clear()
                            dispatchViewModel.dispatchWarehouseOrders.addAll(it)
                            fixQuantityAndRemoveItems()
                            postFilteredList()
                        }
                    }
                }
        )
    }

    private fun fixQuantityAndRemoveItems() {
        dispatchViewModel.dispatchWarehouseOrders.filterWith(CustomPredicate {
            it.partiallyDispatched.not()
        })
        for (dispatchWarehouseOrder in dispatchViewModel.dispatchWarehouseOrders) {
            dispatchWarehouseOrder.warehouseOrderItems.filterWith(CustomPredicate {
                !(it.status == "Pending Dispatch" || it.status == "In Dispatch")
            })
            dispatchWarehouseOrder.totalQuantity = dispatchWarehouseOrder.warehouseOrderItems.size.toLong()
        }
    }

    fun setSelectedWarehouseOrders(selectedOrders: ArrayList<WarehouseOrders>) {
        dispatchViewModel.setSelectedWarehouseOrders(selectedOrders)
    }

    fun searchByVendorCode(vendorCode : String) {
        dispatchListSearchTerm = vendorCode
        postFilteredList()
    }

    private fun postFilteredList() {
        compositeDisposable.add(
            Observable.fromIterable(dispatchViewModel.dispatchWarehouseOrders)
                .filter {
                    (dispatchListSearchTerm.isEmpty() || it.vendorCode.toLowerCase().contains(dispatchListSearchTerm.toLowerCase()))
                            && it.isDispatched.not() && (dispatchViewModel.vendorCode.isEmpty() || dispatchViewModel.vendorCode == it.vendorCode) }
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { filteredWarehouseOrders: MutableList<WarehouseOrders>?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    filteredWarehouseOrders?.let {
                        postDispatchList(it)
                    }
                }
        )
    }

    private fun postDispatchList(warehouseOrders: List<WarehouseOrders>) {
        (dispatchListLiveData as MutableLiveData).postValue(warehouseOrders)
    }

}
