package com.blubirch.tus.ui.iteminfo.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.stowing.DispositionItem
import com.blubirch.tus.databinding.ItemDispositionItemBinding
import com.blubirch.core.utility.AppUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class DispositionItemAdapter : BaseQuickAdapter<DispositionItem, BaseViewHolder>(R.layout.item_disposition_item) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<DispositionItem>() {
            override fun areItemsTheSame(oldItem: DispositionItem, newItem: DispositionItem): Boolean {
                return true
            }

            override fun areContentsTheSame(oldItem: DispositionItem, newItem: DispositionItem): Boolean {
                return oldItem.dispositionLabel == newItem.dispositionLabel &&
                        oldItem.dispositionDate == newItem.dispositionDate
            }
        })
    }

    override fun convert(holder: BaseViewHolder, dispositionItem: DispositionItem) {
        val binding = ItemDispositionItemBinding.bind(holder.itemView)
        binding.txtDispositionLabel.text = dispositionItem.dispositionLabel
        binding.txtDispositionDate.text = AppUtils.dateStringToShortDateTime(dispositionItem.dispositionDate)
    }

}