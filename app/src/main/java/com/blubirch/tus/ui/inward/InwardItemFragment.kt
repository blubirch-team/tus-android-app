package com.blubirch.tus.ui.inward

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.blubirch.tus.testing.data.api.dto.InventoryResult
import com.blubirch.tus.testing.ui.TestActivity
import com.blubirch.tus.R
import com.blubirch.tus.data.model.inward.GatePassInventory
import com.blubirch.tus.databinding.FragmentInwardItemBinding
import com.blubirch.tus.ui.inward.adapters.InwardItemAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.materialDialog
import kotlinx.android.synthetic.main.fragment_inward_item.*
import java.util.*
import javax.inject.Inject

class InwardItemFragment : BaseFragment<InwardItemViewModel>() {

    companion object {
        private const val TEST_REQUEST_CODE: Int = 1122
        private const val ITEM_CODE_REQUEST_CODE: Int = 1123
        private const val SERIAL_NUMBER_REQUEST_CODE: Int = 1124
        private const val SERIAL_NUMBER2_REQUEST_CODE: Int = 1125
        fun newInstance() = InwardItemFragment()
    }

    private var itemCodeTextWatcher: TextWatcher? = null
    private var serialNoTextWatcher: TextWatcher? = null
    private var serialNoTextWatcher2: TextWatcher? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: InwardItemViewModel
    private lateinit var inwardViewModel: InwardViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(InwardItemViewModel::class.java)
        val backStackEntry =
            navController.getBackStackEntry(R.id.navInward)
        inwardViewModel =
            ViewModelProvider(backStackEntry, viewModelFactory).get(InwardViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        return FragmentInwardItemBinding.inflate(inflater, container, false).root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScanInput()
        setupList()
        setupListeners()
        hideSerial2IfNecessary()
    }

    private fun setupScanInput() {
        tilItemCode.initialize(this, ITEM_CODE_REQUEST_CODE)

        tilItemCode.editText.addTextChangedListener(afterTextChanged = {
            it?.let {
                viewModel.filterGatePass(it.toString())
            }
        })
        tilSerialNumber.initialize(this, SERIAL_NUMBER_REQUEST_CODE)
        tilSerialNumber2.initialize(this, SERIAL_NUMBER2_REQUEST_CODE)
        tilSerialNumber.editText.setFilters(arrayOf(InputFilter.AllCaps()));
        tilSerialNumber2.editText.setFilters(arrayOf(InputFilter.AllCaps()));

    }

    private fun setupList() {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvInwardItemList.apply {
            layoutManager = linearLayoutManager
            adapter = InwardItemAdapter { gatePassInventory ->
                tilItemCode.requestFocus()
                tilItemCode.text = ""
                tilItemCode.editText.append(gatePassInventory.skuCode)
                tilSerialNumber.requestFocus()
            }
        }
    }

    override fun getViewModel(): InwardItemViewModel {
        return viewModel
    }

    override fun onResume() {
        super.onResume()
        validate()
    }

    private fun searchItemCode() {
        viewModel.searchItemCode(
            tilItemCode.text.toString(),
            tilSerialNumber.text.toString(),
            tilSerialNumber2.text.toString()
        )
    }

    private fun setupListeners() {
        btnProceedToGrade.setOnClickListener {
            if (viewModel.isItemInCurrentDoc(tilItemCode.text.toString()).not()) {
                MaterialDialog(requireContext())
                    .show {
                        lifecycleOwner(this@InwardItemFragment)
                        title(R.string.alert)
                        message(R.string.article_not_in_document_message)
                        positiveButton(text = resources.getString(R.string.ok), click = {
                            it.dismiss()
                        })
                    }
//                materialDialog(message = R.string.article_not_in_document_message
//                positiveText = R.string.yes, positiveClickListener = {
//                        it.dismiss()
//                        searchItemCode()
//                    },
//                negativeText = R.string.no
//                )
            } else if(viewModel.areAllItemsGraded(tilItemCode.text.toString()) == true) {
                materialDialog(message = R.string.all_items_in_article_graded_message,
                    positiveText = R.string.yes, positiveClickListener = {
                        it.dismiss()
                        searchItemCode()
                    },
                    negativeText = R.string.no)
            } else {
                searchItemCode()
            }
        }
//        btnProceedToGrn.setOnClickListener {
//            navigate(InwardItemFragmentDirections.actionInwardItemFragmentToGrnSummaryFragment())
//        }
        itemCodeTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                validate()
            }

        }
        this.serialNoTextWatcher = itemCodeTextWatcher
        this.serialNoTextWatcher2 = itemCodeTextWatcher

        tilItemCode.addTextWatcher(itemCodeTextWatcher)
        tilSerialNumber.addTextWatcher(serialNoTextWatcher)
        tilSerialNumber2.addTextWatcher(serialNoTextWatcher2)
    }

    override fun attachLiveData() {
//        tilItemCode.text = viewModel.itemCode
//        tilSerialNumber.text = viewModel.serialNumber
        viewModel.apply {
            inwardViewModel.apply {
                tvItemCount.text =
                    resources.getQuantityString(
                        R.plurals.numberOfItems,
                        sentInventoryCount,
                        sentInventoryCount
                    )
                (rvInwardItemList.adapter as InwardItemAdapter).setNewInstance(gatePass!!.gatePassInventories.toMutableList())
                updateGatePassInventories(gatePass!!.gatePassInventories)
            }
            filteredInventoryLiveData.observe(viewLifecycleOwner, Observer {
                it?.let { filteredList ->
                    (rvInwardItemList.adapter as InwardItemAdapter).apply {
                        if (data.size == 0) {
                            setNewInstance(filteredList.toMutableList())
                        } else {
                            setDiffNewData(filteredList.toMutableList())
                        }
                    }
                }
            })
            gatePassInventory.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    tilItemCode.setError(null)
                    inwardViewModel.gatePassInventory = gatePassInventory.value!!
                    launchTesting(gatePassInventory.value!!)
                    gatePassInventory.value = null
                }
            })
            itemCodeErrorLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    tilItemCode.setError(it.message)
                    tilItemCode.editText.requestFocus()
                }
            })
            serialNumberErrorLiveData.observe(viewLifecycleOwner, Observer {
                it?.apply {
                    tilSerialNumber.setError(it.message)
                    tilSerialNumber.editText.requestFocus()
                }
            })
        }
    }

    private fun launchTesting(gatePassInventory: GatePassInventory) {
        val testInventory = gatePassInventory.toTestInventory()
        testInventory.serialNumber = viewModel.serialNumber
        testInventory.serialNumber2 = viewModel.serialNumber2
        testInventory.gatePassNumber = inwardViewModel.gatePass!!.clientGatePassNumber
        TestActivity.launch(
            this,
            testInventory,
            TEST_REQUEST_CODE
        )
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (Activity.RESULT_OK == resultCode) {
            when (requestCode) {
                TEST_REQUEST_CODE -> {
                    data?.extras?.let {
                        updateTestResult(it)
                    }
                    navigate(InwardItemFragmentDirections.actionInwardItemFragmentSelf())
//                    navController.popBackStack()
//                    navigate(InwardItemFragmentDirections.actionInwardItemFragmentToGradeDispositionResultFragment())
                }
                ITEM_CODE_REQUEST_CODE -> {
                    tilItemCode.onActivityResult(requestCode, resultCode, data)
                }
                SERIAL_NUMBER_REQUEST_CODE -> {
                    tilSerialNumber.onActivityResult(requestCode, resultCode, data)
                }
                SERIAL_NUMBER2_REQUEST_CODE -> {
                    tilSerialNumber2.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
    }

    private fun updateTestResult(bundle: Bundle) {

        inwardViewModel.apply {
//            serialNumber = viewModel.serialNumber
//            serialNumber2 = viewModel.serialNumber2

            val result: InventoryResult =
                bundle.getParcelable<InventoryResult>(TestActivity.TEST_RESULT_SAVED_INVENTORY) as InventoryResult

            if (gatePassInventory.id == 0L) {
                gatePassInventory.id = result.id
                gatePassInventory.status = result.status
                gatePassInventory.distributionCenterId =
                    result.distributionCenterId
                gatePassInventory.clientCategory = result.clientCategoryName
                (gatePass!!.gatePassInventories as ArrayList).add(gatePassInventory)
            }
            updateGatePassInventory()
            updateGatePass()
            showSnackBar(getString(R.string.msg_inward_success))
        }
    }

    private fun hideSerial2IfNecessary() {
//        if (tilSerialNumber.text.isNullOrEmpty()) {
//            tilSerialNumber2.visible(false)
//            tilSerialNumber2.removeTextWatcher(serialNoTextWatcher2)
//            tilSerialNumber2.editText.setText("")
//            tilSerialNumber2.setError(null)
//            tilSerialNumber2.addTextWatcher(serialNoTextWatcher2)
//        } else {
//            tilSerialNumber2.visible(true)
//        }
    }

    private fun validate() {
        var validated = true
        if (tilItemCode.text!!.isEmpty()) {
            validated = false
        }
        tilSerialNumber.setError(null)
        tilSerialNumber2.setError(null)

        hideSerial2IfNecessary()

        if (tilSerialNumber.text.toString().isBlank()) {
            if (tilSerialNumber2.text.toString().isNotBlank()) {
                tilSerialNumber.setError(getString(R.string.msg_error_missing_serial_number1))
                validated = false
            } else {
                tilSerialNumber.setError(null)
            }
        } else {
            if (tilSerialNumber2.text.toString().isNotBlank() && tilSerialNumber.text.toString()
                    .equals(tilSerialNumber2.text.toString(), false)
            ) {
                tilSerialNumber2.setError(getString(R.string.msg_error_same_serial))
                validated = false
            } else {
                tilSerialNumber2.setError(null)
            }
        }
        if (validated) {
            enableGrading()
        } else {
            disableGrading()
        }
    }

    private fun enableGrading() {
        btnProceed.isEnabled = true
    }

    private fun disableGrading() {
        btnProceed.isEnabled = false
    }
}