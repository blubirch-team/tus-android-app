package com.blubirch.tus.ui.pick

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.databinding.FragmentPickMainBinding
import com.blubirch.tus.ui.pick.adapters.PickMainCategoryAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import javax.inject.Inject


class PickMainFragment : BaseFragment<PickMainViewModel>() {

    companion object {
        fun newInstance() = PickMainFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var pickMainViewModel: PickMainViewModel

    private lateinit var binding: FragmentPickMainBinding

    private lateinit var categoryAdapter: PickMainCategoryAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPick)
        val pickViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(PickViewModel::class.java)
        pickMainViewModel = ViewModelProvider(this, viewModelFactory).get(PickMainViewModel::class.java)
        pickMainViewModel.setMainModel(pickViewModel)

        binding.btnProceed.setOnClickListener {
            navigate(PickMainFragmentDirections.actionPickMainFragmentToPickList())
        }

        val orderNumber = pickMainViewModel.getOrderNumber()
        binding.edtOrderNumber.setText(orderNumber)
        setToolbarTitle(String.format(resources.getString(R.string.pick_s), orderNumber))

        binding.txtQuantity.text = String.format(resources.getString(R.string.qty_d), pickMainViewModel.getTotalItems())
        categoryAdapter = PickMainCategoryAdapter()

        pickMainViewModel.progressLiveData.value = true
        pickMainViewModel.getCategoryItems()

    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPickMainBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): PickMainViewModel {
        return pickMainViewModel
    }

    override fun attachLiveData() {
        pickMainViewModel.categoryItemsLiveData.observe(viewLifecycleOwner, Observer {
            binding.rvCategoryList.apply {
                post {
                    if (adapter == null) {
                        categoryAdapter.setNewInstance(it)
                        adapter = categoryAdapter
                    } else {
                        categoryAdapter.apply {
                            if (data.isEmpty() || it.isEmpty()) {
                                setNewInstance(it)
                            } else {
                                setDiffNewData(it)
                                notifyItemRangeChanged(0, it.size)
                            }
                        }
                    }
                    pickMainViewModel.progressLiveData.postValue(false)
                }
            }
        })

    }


}
