package com.blubirch.tus.ui.stowing

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.constants.APIResponseStatus
import com.blubirch.tus.constants.SearchStatus
import com.blubirch.tus.data.model.Inventory
import com.blubirch.tus.data.repository.StowingRepo
import com.blubirch.core.data.MyException
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import kotlin.math.min

class ToatTagListViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var stowingRepo: StowingRepo

    lateinit var stowingViewModel: StowingViewModel

    val selectedToatNumber: String
        get() = stowingViewModel.selectedToatNumber

    private val selectedToatInventories: MutableList<Inventory>
        get() = stowingViewModel.selectedToatInventories

    val totalInventories : Int by lazy {
        selectedToatInventories.size
    }

    val stowedCountLiveData : LiveData<Int>
        get() = stowingViewModel.stowedCountLiveData

    val loadEndLiveData : LiveData<Boolean> = MutableLiveData<Boolean>()

    private val pageSize : Int by lazy {
        selectedToatInventories.size
    }

    private var currentPage = 1
    private var noOfInventoriesDisplayed = 0

    val selectedToatInventoriesLiveData : LiveData<MutableList<Inventory>> by lazy {
        noOfInventoriesDisplayed = pageSize * currentPage
        noOfInventoriesDisplayed = min(noOfInventoriesDisplayed, selectedToatInventories.size)
        val subList = selectedToatInventories.subList(0, noOfInventoriesDisplayed)
        MutableLiveData<MutableList<Inventory>>(subList)
    }

    private var inventoryForLocationUpdate: Inventory? = null
    val tagNumberSearchLiveData by lazy { MutableLiveData<Int>() }
    val locationErrorLiveData by lazy { MutableLiveData<MyException?>(null) }
    val updateLocationResponseLiveData by lazy {
        MutableLiveData<Int>(com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED)
    }


    fun loadMore() {
        currentPage++
        noOfInventoriesDisplayed = min((pageSize * currentPage), selectedToatInventories.size)
        val subList = selectedToatInventories.subList(0, noOfInventoriesDisplayed)
        if(noOfInventoriesDisplayed >= selectedToatInventories.size){
            (loadEndLiveData as MutableLiveData).postValue(true)
        }
        (selectedToatInventoriesLiveData as MutableLiveData).postValue(subList)
    }

    private fun updateStowCount() {
        stowingViewModel.selectedToatInventories.apply {
            var count = 0
            forEach {
                it.location?.apply {
                    if (isNotEmpty())
                        count++
                }
            }
            (stowingViewModel.stowedCountLiveData as MutableLiveData).postValue(count)
        }
    }

    fun findInventory(tagNumber: String) {
        compositeDisposable.add(
            Observable.fromIterable(stowingViewModel.selectedToatInventories)
                .subscribeOn(Schedulers.computation())
                .filter { it.tagNumber.toLowerCase() == tagNumber.toLowerCase() }
                .toList()
                .subscribe { list, throwable ->
                    if (throwable == null && list.isNotEmpty()) {
                        inventoryForLocationUpdate = list[0]
                        tagNumberSearchLiveData.postValue(com.blubirch.tus.constants.SearchStatus.FOUND)
                    } else {
                        tagNumberSearchLiveData.postValue(com.blubirch.tus.constants.SearchStatus.NOT_FOUND)
                    }
                })
    }


    fun updateLocation(location: String) {
        var validated = true
        if (inventoryForLocationUpdate == null) {
            validated = false
            tagNumberSearchLiveData.postValue(com.blubirch.tus.constants.SearchStatus.ERROR)
        }
        if (location.isNullOrBlank()) {
            validated = false
            locationErrorLiveData.postValue(
                MyException.ValidationError(Throwable("Please enter location"))
            )
        }
        if (validated)
            updateLocationCall(location)

    }

    private fun updateLocationCall(location: String) {
        progressLiveData.value = true
        compositeDisposable.add(
            stowingRepo.updateLocationDTO(inventoryForLocationUpdate!!.id, location)
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        if (result.status == 200) {
                            inventoryForLocationUpdate!!.location = location
                            updateStowCount()
                            updateLocationResponseLiveData.postValue(com.blubirch.tus.constants.APIResponseStatus.FOUND)
                        } else {
                            errorLiveData.postValue(MyException.ApiError(Throwable(result.message)))
                        }
                    } else {
                        updateLocationResponseLiveData.postValue(com.blubirch.tus.constants.APIResponseStatus.ERROR)
                        postAPIError(throwable)
                    }
                }
        )
    }

    val timeTaken = 0L

    val completeStowingResponseLiveData by lazy {
        MutableLiveData<Int>()
    }

    fun completeStow() {
        progressLiveData.value = true
        compositeDisposable.add(
            stowingRepo.completeStowing(stowingViewModel.selectedToatNumber)
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        if (result.status == 200) {
                            completeStowingResponseLiveData.postValue(com.blubirch.tus.constants.APIResponseStatus.SUCCESS)
                        } else {
                            errorLiveData.postValue(MyException.ApiError(Throwable(result.message)))
                        }
                    } else {
                        completeStowingResponseLiveData.postValue(com.blubirch.tus.constants.APIResponseStatus.ERROR)
                        postAPIError(throwable)
                    }
                }
        )
    }


}
