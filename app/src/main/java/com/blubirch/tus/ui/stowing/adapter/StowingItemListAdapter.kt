package com.blubirch.tus.ui.stowing.adapter

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.stowing.StowingItem
import com.blubirch.tus.databinding.ItemStowingListItemBinding
import com.blubirch.core.utility.AppUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class StowingItemListAdapter (private val clickListener: (Int, StowingItem) -> Unit) :
    BaseQuickAdapter<StowingItem, BaseViewHolder>(R.layout.item_stowing_list_item) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<StowingItem>() {
            override fun areItemsTheSame(oldItem: StowingItem, newItem: StowingItem): Boolean {
                return oldItem.toatNumber == newItem.toatNumber
            }

            override fun areContentsTheSame(oldItem: StowingItem, newItem: StowingItem): Boolean {
                return oldItem.quantity == newItem.quantity &&
                        oldItem.lastUpdatedAt == newItem.lastUpdatedAt
            }
        })
    }

    override fun convert(holder: BaseViewHolder, stowingItem: StowingItem) {
        val binding = ItemStowingListItemBinding.bind(holder.itemView)
        holder.itemView.setOnClickListener {
            clickListener(getItemPosition(stowingItem), stowingItem)
        }
        binding.txtToatNumber.text = stowingItem.toatNumber
        binding.txtDate.text = AppUtils.dateStringToShortDate(stowingItem.lastUpdatedAt)
        binding.txtQuantity.text = stowingItem.quantity.toString()
    }
}