package com.blubirch.tus.ui.dispatch

import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.repository.DispatchRepo
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc
import javax.inject.Inject

class DispatchViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var dispatchRepo: DispatchRepo

    var deliveryRef: String = ""
    var invoiceNo: String = ""
    var transporter: String = ""
    var lorryRecNo: String = ""
    var vehicleNo: String = ""
    var drivePhNo: String = ""

    var consignmentId: Long = 0
    var vendorCode: String = ""

    var dispatchWarehouseOrders = ArrayList<WarehouseOrders>()

    var currentlySelectedWarehouseOrders = ArrayList<WarehouseOrders>()

    private var attachedDocs = ArrayList<AttachedDoc>()

    fun setSelectedWarehouseOrders(selectedOrders: ArrayList<WarehouseOrders>) {
        currentlySelectedWarehouseOrders = selectedOrders
    }

    fun getDispatchedOrSelectedOrder() : WarehouseOrders?{
        return dispatchWarehouseOrders.find {
            it.isChecked || it.isDispatched
        }
    }

    fun getWarehouseOrderFileTypes(): ArrayList<ArrayList<String?>?>? {
        return getDispatchedOrSelectedOrder()?.warehouseOrderFileTypes
    }

    fun getAllSelectedOrders(): MutableList<WarehouseOrders> {
        val allSelectedOrders = mutableListOf<WarehouseOrders>()
        for (dispatchWarehouseOrder in dispatchWarehouseOrders) {
            if (dispatchWarehouseOrder.isChecked) {
                allSelectedOrders.add(dispatchWarehouseOrder)
            }
        }
        return allSelectedOrders
    }


    fun clearVehicleDetails() {
        deliveryRef = ""
        invoiceNo = ""
        transporter = ""
        lorryRecNo = ""
        vehicleNo = ""
        drivePhNo = ""
    }

    fun getAttachedDocs(): ArrayList<AttachedDoc> {
        return attachedDocs
    }

    fun setAttachedDocs(attachedDocs: ArrayList<AttachedDoc>) {
        DispatchViewModel@this.attachedDocs = attachedDocs
    }

}
