package com.blubirch.tus.ui.pickpack.adapters

import android.view.View
import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.databinding.ItemPickPackPackedBoxItemsListBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PackedBoxItemListAdapter constructor(
        items: MutableList<WarehouseOrderItems>,
        private val onItemDeleted: (position: Int, warehouseOrderItem: WarehouseOrderItems) -> Unit
) : BaseQuickAdapter<WarehouseOrderItems, BaseViewHolder>(R.layout.item_pick_pack_packed_box_items_list, items) {


    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrderItems>() {
            override fun areItemsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return oldItem.tagNumber == newItem.tagNumber
                        && oldItem.itemDescription == newItem.itemDescription
            }
        })
    }

    override fun convert( baseViewHolder: BaseViewHolder, warehouseOrderItem: WarehouseOrderItems) {
        val binding = ItemPickPackPackedBoxItemsListBinding.bind(baseViewHolder.itemView)
        binding.txtTagNumber.text = warehouseOrderItem.tagNumber
        binding.txtItemDescription.text = warehouseOrderItem.itemDescription
        binding.imgDelete.setOnClickListener { v: View? ->
            onItemDeleted(
                getItemPosition(warehouseOrderItem),
                warehouseOrderItem
            )
        }
    }

}