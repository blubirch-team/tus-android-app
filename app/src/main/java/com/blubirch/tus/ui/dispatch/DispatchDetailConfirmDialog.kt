package com.blubirch.tus.ui.dispatch

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.tus.R
import com.blubirch.tus.databinding.DialogDispatchConfirmBinding
import com.blubirch.tus.ui.dispatch.adapters.DispatchDialogGatePassAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseBottomSheetDialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import javax.inject.Inject

class DispatchDetailConfirmDialog : BaseBottomSheetDialogFragment<DispatchViewModel>() {

    companion object {
        fun newInstance() = DispatchDetailsFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var dispatchViewModel : DispatchViewModel

    private lateinit var binding: DialogDispatchConfirmBinding

    private lateinit var dispatchDialogGatePassAdapter: DispatchDialogGatePassAdapter

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val dialog = it as BottomSheetDialog

            dialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
            val containerLayout: FrameLayout =
                dialog.findViewById<FrameLayout>(com.google.android.material.R.id.container) as FrameLayout
            val frameButton = binding.frameButton
            val parent = frameButton.parent as ViewGroup
            parent.removeView(frameButton)
            containerLayout.addView(frameButton, containerLayout.childCount)
            (frameButton.layoutParams as FrameLayout.LayoutParams).gravity = Gravity.BOTTOM
        }
        return bottomSheetDialog
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        isCancelable = false

        val backStackEntry = navController.getBackStackEntry(R.id.navDispatch)
        dispatchViewModel = ViewModelProvider(backStackEntry,viewModelFactory).get(DispatchViewModel::class.java)

        dispatchDialogGatePassAdapter = DispatchDialogGatePassAdapter(dispatchViewModel.getAllSelectedOrders())
        val lm: LinearLayoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        binding.rvInvoiceItems.isNestedScrollingEnabled = false
        binding.rvInvoiceItems.layoutManager = lm
        binding.rvInvoiceItems.adapter = dispatchDialogGatePassAdapter

        binding.txtDeliveryRef.text = dispatchViewModel.deliveryRef
        binding.txtInvoiceNo.text = dispatchViewModel.invoiceNo
        binding.txtTransporter.text = dispatchViewModel.transporter
        binding.txtLorryReceiptNumber.text = dispatchViewModel.lorryRecNo
        binding.txtVehicleNumber.text = dispatchViewModel.vehicleNo
        binding.txtDriverPhNo.text = dispatchViewModel.drivePhNo

        dispatchViewModel.getAttachedDocs().let {
            if (it.size == 0) {
                binding.txtAttachDocTitle.visibility = View.GONE
                binding.attachedDocs.visibility = View.GONE
            } else {
                binding.txtAttachDocTitle.visibility = View.VISIBLE
                binding.attachedDocs.visibility = View.VISIBLE
                binding.attachedDocs.setViewOnlyData(it)
            }
        }

        binding.btnConfirmAndProceed.setOnClickListener {
            navigate(DispatchDetailConfirmDialogDirections.actionDispatchDetailConfirmDialogToDispatchGatepassConfirmDialog())
        }
        binding.btnAddGatePass.setOnClickListener {
            dismiss()
            navigate(DispatchDetailConfirmDialogDirections.actionDispatchDetailConfirmDialogToDispatchListFragment())
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogDispatchConfirmBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): DispatchViewModel {
        return dispatchViewModel
    }

    override fun attachLiveData() {

    }
}