package com.blubirch.tus.ui.inward

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.databinding.FragmentUpdateGrnMainBinding
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import kotlinx.android.synthetic.main.fragment_update_grn_main.*
import javax.inject.Inject

class UpdateGrnFragment : BaseFragment<UpdateGrnViewModel>() {

    companion object {
        fun newInstance() = UpdateGrnFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: UpdateGrnViewModel
    private lateinit var inwardViewModel: InwardViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(
            UpdateGrnViewModel::class.java
        )

        btnUpdate.setOnClickListener {
            AppUtils.hideSoftKeyboard(requireActivity())
            viewModel.updateGRN(edtStnNumber.text.toString(), edtGrnNumber.text.toString())
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentUpdateGrnMainBinding.inflate(inflater).root
    }

    override fun getViewModel(): UpdateGrnViewModel {
        return viewModel
    }

    override fun attachLiveData() {
        viewModel.apply {
            grnResponseLiveData.observe(viewLifecycleOwner, Observer {
                it?.let {
                    if (it) {
                        showSnackBar("GRN Updated Successfully")
                        tilStnNumber.editText?.text?.clear()
                        tilGrnNumber.editText?.text?.clear()
                    }
                    grnResponseLiveData.value = null
                }
            })
            stnNumberErrorLiveData.observe(viewLifecycleOwner, Observer {
                tilStnNumber.errorIconDrawable = null
                tilStnNumber.error = it.message
                AppUtils.showSoftKeyboard(requireContext(), tilStnNumber.editText)
                tilStnNumber.requestFocus()
            })
            grnNumberErrorLiveData.observe(viewLifecycleOwner, Observer {
                tilGrnNumber.errorIconDrawable = null
                tilGrnNumber.error = it.message
                AppUtils.showSoftKeyboard(requireContext(), tilGrnNumber.editText)
                tilGrnNumber.requestFocus()
            })
        }
    }


}