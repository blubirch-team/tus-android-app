package com.blubirch.tus.ui.pack

import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject

class PackViewModel @Inject constructor() : BaseViewModel() {

    var boxListItem: ArrayList<BoxListItem>? = null
    lateinit var selectedWarehouseOrder : WarehouseOrders

    fun getToatsOfSelectedOrder(): ArrayList<String> {
        val listOfToats = ArrayList<String>()
        for (index in selectedWarehouseOrder.warehouseOrderItems.indices) {
            val warehouseOrderItem = selectedWarehouseOrder.warehouseOrderItems[index]
            val toat = warehouseOrderItem.toatNumber
            toat?.let {
                if(!listOfToats.contains(toat)){
                    listOfToats.add(toat)
                }
            }
        }
        return listOfToats
    }

}
