package com.blubirch.tus.ui.iteminfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import javax.inject.Inject

class ItemInfoInwardDetailsFragment : BaseFragment<ItemInfoViewModel>() {

    companion object {
        fun newInstance() = ItemInfoInwardDetailsFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: ItemInfoViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val backStackEntry = navController.getBackStackEntry(R.id.navItemInfo)
        viewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(ItemInfoViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_item_inward_details, container, false)
    }

    override fun getViewModel(): ItemInfoViewModel {
        return viewModel
    }

    override fun attachLiveData() {

    }

}
