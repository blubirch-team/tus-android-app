package com.blubirch.tus.ui.pickpack.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.databinding.ItemPickPackOrderNotPickedListBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class NotPickedListAdapter constructor(private val onItemClick: (String) -> Unit)
    : BaseQuickAdapter<WarehouseOrderItems, BaseViewHolder>(R.layout.item_pick_pack_order_not_picked_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrderItems>() {
            override fun areItemsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return oldItem.tagNumber == newItem.tagNumber &&
                        oldItem.aisleLocation == newItem.skuMasterCode
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: WarehouseOrderItems) {
        val binding = ItemPickPackOrderNotPickedListBinding.bind(holder.itemView)
        binding.txtItemId.text = item.tagNumber
        binding.txtLocation.text = item.aisleLocation
        binding.root.setOnClickListener {
            onItemClick(item.tagNumber)
        }
    }
}