package com.blubirch.tus.ui.dispatch.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.databinding.ItemDispatchBoxItemBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class DispatchBoxItemAdapter: BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_dispatch_box_item) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<String>() {
            override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
                return oldItem == newItem
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: String) {
        val binding = ItemDispatchBoxItemBinding.bind(holder.itemView)
        val formattedBoxNumber = String.format(
            binding.root.context.getString(R.string.s_s),
            (getItemPosition(item) + 1).toString(),
            item
        )
        binding.txtBoxNumber.text = formattedBoxNumber
    }

}