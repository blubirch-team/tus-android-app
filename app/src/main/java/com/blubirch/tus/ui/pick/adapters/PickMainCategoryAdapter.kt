package com.blubirch.tus.ui.pick.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.databinding.ItemPickCategoryListBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PickMainCategoryAdapter : BaseQuickAdapter<ArrayList<String>, BaseViewHolder>(R.layout.item_pick_category_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<ArrayList<String>>() {
            override fun areItemsTheSame(oldItem: ArrayList<String>, newItem: ArrayList<String>): Boolean {
                return oldItem[0] == newItem[1]
            }

            override fun areContentsTheSame(oldItem: ArrayList<String>, newItem: ArrayList<String>): Boolean {
                return oldItem[0] == newItem[0] && oldItem[1] == newItem[1] && oldItem[2] == newItem[2]
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: ArrayList<String>) {
        val binding = ItemPickCategoryListBinding.bind(holder.itemView)
        binding.txtArticleId.text = item[0]
        binding.txtArticleDesc.text = item[1]
        binding.txtQuantity.text = item[2]
    }
}