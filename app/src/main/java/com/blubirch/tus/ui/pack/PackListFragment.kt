package com.blubirch.tus.ui.pack

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.tus.databinding.FragmentPackListBinding
import com.blubirch.tus.ui.pack.adapters.PackListAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import javax.inject.Inject


class PackListFragment : BaseFragment<PackListViewModel>() {

    companion object {
        const val TAG_SCAN_REQUEST_CODE = 7788
        const val BOX_SCAN_REQUEST_CODE = 8877
        fun newInstance() = PackListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var packListViewModel: PackListViewModel

    private lateinit var binding: FragmentPackListBinding

    private lateinit var packListAdapter: PackListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPack)
        val packViewModel =
            ViewModelProvider(backStackEntry, viewModelFactory).get(PackViewModel::class.java)
        packListViewModel =
            ViewModelProvider(this, viewModelFactory).get(PackListViewModel::class.java)
        packListViewModel.selectedWarehouseOrder = packViewModel.selectedWarehouseOrder

        packListViewModel.getBoxList()

        binding.tilScanItemId.initialize(this, TAG_SCAN_REQUEST_CODE)
        binding.tilScanBoxNumber.initialize(this, BOX_SCAN_REQUEST_CODE) {
            addItemToTheBox()
            AppUtils.hideSoftKeyboard(requireActivity())
        }

        binding.tilScanBoxNumber.editText.imeOptions = EditorInfo.IME_ACTION_DONE
        binding.tilScanBoxNumber.editText.setOnEditorActionListener { view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addItemToTheBox()
                AppUtils.hideSoftKeyboard(requireActivity())
                return@setOnEditorActionListener true
            }
            false
        }

        binding.txtCreateNewBoxId.setOnClickListener {
            packListViewModel.progressLiveData.postValue(true)
            packListViewModel.createBox()
        }

        packListAdapter = PackListAdapter(object : PackListAdapter.BoxItemListener {
            override fun onItemDelete(
                itemPosition: Int, warehouseOrderItem: WarehouseOrderItems,
                parentPosition: Int, boxListItem: BoxListItem
            ) {
                packListViewModel.progressLiveData.postValue(true)
                packListViewModel.removeFromBox(warehouseOrderItem)
            }

            override fun onBoxDelete(position: Int, boxListItem: BoxListItem) {
                packListViewModel.progressLiveData.postValue(true)
                packListViewModel.deleteBox(boxListItem)
            }

            override fun onPrint(strToPrint: String) {
                showPrintTagView(strToPrint)
            }
        })
        binding.rvItemList.adapter = packListAdapter

        binding.btnComplete.setOnClickListener {
            if (packListViewModel.getNoOfItemsPacked() > 0) {
                packViewModel.boxListItem = packListViewModel.boxListItemLiveData.value
                navigate(PackListFragmentDirections.actionPackListFragmentToPackConfirmDialog())
            } else {
                showSnackBar("Please add item first", binding.btnComplete)
            }
        }

        setHasOptionsMenu(true)

        setToolbarTitle(String.format(resources.getString(R.string.pack_s), packViewModel.selectedWarehouseOrder.orderNumber))

    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPackListBinding.inflate(inflater)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.list_only, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_list -> navigate(PackListFragmentDirections.actionPackListFragmentToPackItemStatusListFragment())
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addItemToTheBox() {
        val itemId = binding.tilScanItemId.text?.trim()?.toString() ?: ""
        val boxNumber = binding.tilScanBoxNumber.text?.trim()?.toString() ?: ""

        if (itemId.isNotEmpty() && boxNumber.isNotEmpty()) {
            packListViewModel.progressLiveData.postValue(true)
            packListViewModel.setBoxNumber(itemId, boxNumber)
        }
    }

    override fun getViewModel(): PackListViewModel {
        return packListViewModel
    }

    override fun attachLiveData() {
        packListViewModel.boxListItemLiveData.observe(viewLifecycleOwner, Observer {
            packListViewModel.progressLiveData.postValue(false)

            val adapterListData = packListAdapter.data
            val adapterAndLiveDataSizeSame = it.size == adapterListData.size
            var dataChanged = adapterAndLiveDataSizeSame.not()

            if (it.isEmpty()) {
                packListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                packListAdapter.setNewInstance(null)
            } else {
                if(adapterAndLiveDataSizeSame) {
                    for (index in it.indices) {
                        if (it[index].warehouseOrderItems.size != adapterListData[index].warehouseOrderItems.size) {
                            dataChanged = true
                            break
                        }
                    }
                }

                val list: ArrayList<BoxListItem> = ArrayList(it)
                packListAdapter.setDiffNewData(list)
                packListAdapter.notifyItemRangeChanged(0, list.size)
                binding.rvItemList.post { binding.rvItemList.smoothScrollToPosition(0) }
            }

            if(dataChanged){
//                binding.tilScanItemId.text = ""
//                binding.tilScanBoxNumber.text = ""
                if (it.size > 0) {
                    binding.tilScanBoxNumber.text = (it[0].box.boxNumber)
                }
            }

            binding.txtTotalBoxes.text = it.size.toString()

            binding.txtItemPacked.text =
                "${packListViewModel.getNoOfItemsPacked()}/${packListViewModel.getTotalItems()}"
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            TAG_SCAN_REQUEST_CODE -> {
                binding.tilScanItemId.onActivityResult(requestCode, resultCode, data)
            }
            BOX_SCAN_REQUEST_CODE -> {
                binding.tilScanBoxNumber.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

}
