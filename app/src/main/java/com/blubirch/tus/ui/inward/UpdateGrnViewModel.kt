package com.blubirch.tus.ui.inward

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.repository.GatePassRepo
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UpdateGrnViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var gatePassRepo: GatePassRepo

    val grnResponseLiveData by lazy {
        MutableLiveData<Boolean?>(null)
    }

    val stnNumberErrorLiveData by lazy { MutableLiveData<Exception>() }
    val grnNumberErrorLiveData by lazy { MutableLiveData<Exception>() }


    fun updateGRN(stnNumber: String, grnNumber: String) {
        var validated = true
        if (stnNumber.isBlank()) {
            validated = false
            stnNumberErrorLiveData.postValue(Exception("Invalid STN Number"))
        }
        if (grnNumber.isBlank()) {
            validated = false
            grnNumberErrorLiveData.postValue(Exception("Invalid GRN Number"))
        }
        if (validated) updateGrnCall(stnNumber, grnNumber)
    }

    private fun updateGrnCall(stnNumber: String, grnNumber: String) {
        progressLiveData.postValue(true)
        compositeDisposable.add(
            gatePassRepo.updateGRN(stnNumber, grnNumber)
                .subscribeOn(Schedulers.io())
                .subscribe { t1, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        grnResponseLiveData.postValue(true)
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }
}