package com.blubirch.tus.ui.pick

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.databinding.DialogPickCompletionBinding
import com.blubirch.tus.databinding.FragmentPickListBinding
import com.blubirch.tus.ui.pick.adapters.PickListAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import com.blubirch.core.utility.KotlinJavaDelegation
import javax.inject.Inject


class PickListFragment : BaseFragment<PickListViewModel>() {

    companion object {
        const val TAG_SCAN_REQUEST_CODE = 7788
        const val TOAT_SCAN_REQUEST_CODE = 8877
        fun newInstance() = PickListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: FragmentPickListBinding

    private lateinit var pickListViewModel: PickListViewModel
    private lateinit var pickListAdapter: PickListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPick)
        val pickViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(PickViewModel::class.java)
        pickListViewModel = ViewModelProvider(this, viewModelFactory).get(PickListViewModel::class.java)
        pickListViewModel.setMainModel(pickViewModel)

        pickListAdapter = PickListAdapter()
        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvItemList.addItemDecoration(dividerItemDecoration)
        binding.rvItemList.adapter = pickListAdapter

        binding.btnComplete.setOnClickListener {

            if (pickListViewModel.getPickedCount() < 1) {
                showSnackBar("Add any item first", binding.btnComplete)
            } else {
                val binding : DialogPickCompletionBinding  = DialogPickCompletionBinding.inflate(
                    LayoutInflater.from(requireContext()))
                binding.txtOrderNumber.text = pickListViewModel.getOrderNumber()

                binding.txtTotalInventory.text = pickListViewModel.getTotalInventories()
                binding.txtTotalToat.text = pickListViewModel.getTotalToats().toString()
                binding.txtItemFound.text = pickListViewModel.getPickedCount().toString()
                binding.txtItemNotFound.text = pickListViewModel.getNotPickedCount().toString()

                binding.txtTimeTaken.text = String.format(
                    resources.getString(R.string.time_taken_d_mins), pickListViewModel.getTimeTaken())


                val dialog = KotlinJavaDelegation.createDialog(binding.root)

                binding.btnGoBack.setOnClickListener {
                    dialog.dismiss()
                }

                binding.btnConfirm.setOnClickListener {
                    dialog.dismiss()
                    pickListViewModel.progressLiveData.postValue(true)
                    pickListViewModel.confirmPick()
                }

                dialog.show()
            }
        }

        setNotPickedItems()

        binding.btnNotPicked.setOnClickListener {
            setNotPickedItems()
        }

        binding.btnPicked.setOnClickListener {
            setPickedItems()
        }

        binding.tilScanItemId.initialize(this, TAG_SCAN_REQUEST_CODE){
            //todo after scan operation
        }
        binding.tilScanToatNumber.initialize(this, TOAT_SCAN_REQUEST_CODE){
            setToatNumber()
            AppUtils.hideSoftKeyboard(requireActivity())
        }

        binding.tilScanToatNumber.editText.imeOptions = EditorInfo.IME_ACTION_DONE
        binding.tilScanToatNumber.editText.setOnEditorActionListener { view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                setToatNumber()
                AppUtils.hideSoftKeyboard(requireActivity())
                return@setOnEditorActionListener true
            }
            false
        }

        setToolbarTitle(
            String.format(
                resources.getString(R.string.pick_s), pickListViewModel.getOrderNumber()
            )
        )

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPickListBinding.inflate(inflater)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        removeActionBarElevation()
    }

    private fun setToatNumber() {
        val itemId = binding.tilScanItemId.text?.trim()?.toString() ?: ""
        val toatNumber = binding.tilScanToatNumber.text?.trim()?.toString() ?: ""

        if (itemId.isNotEmpty() && toatNumber.isNotEmpty()) {
            if (pickListViewModel.setToatNumber(itemId, toatNumber)) {
                pickListViewModel.progressLiveData.postValue(true)
                binding.tilScanItemId.text = ""
                binding.tilScanToatNumber.text = ""
                binding.tilScanItemId.setError("")
            } else {
                binding.tilScanItemId.setError(resources.getString(R.string.not_found))
            }
        }
    }

    private fun setNotPickedItems(){
        binding.btnNotPicked.setBackgroundResource(R.drawable.ic_pick_selected_bg)
        binding.btnPicked.setBackgroundResource(R.drawable.ic_pick_normal_bg)
        binding.btnNotPicked.setTextColor(resources.getColor(R.color.button_selected_text_color))
        binding.btnPicked.setTextColor(resources.getColor(R.color.button_normal_text_color))
        pickListViewModel.fetchNotPickedItems()
    }

    private fun setPickedItems(){
        binding.btnPicked.setBackgroundResource(R.drawable.ic_pick_selected_bg)
        binding.btnNotPicked.setBackgroundResource(R.drawable.ic_pick_normal_bg)
        binding.btnPicked.setTextColor(resources.getColor(R.color.button_selected_text_color))
        binding.btnNotPicked.setTextColor(resources.getColor(R.color.button_normal_text_color))
        pickListViewModel.fetchPickedItems()
    }

    override fun getViewModel(): PickListViewModel {
        return pickListViewModel
    }

    override fun attachLiveData() {
        pickListViewModel.warehouseOrderItemsLiveData.observe(viewLifecycleOwner, Observer {
            pickListViewModel.progressLiveData.postValue(false)
            it?.let {
                if (it.isEmpty()) {
                    pickListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                    pickListAdapter.setNewInstance(null)
                } else {
                    if (pickListAdapter.data.size == 0) {
                        pickListAdapter.setNewInstance(it as ArrayList)
                    } else {
                        pickListAdapter.setDiffNewData(it as ArrayList)
                    }
                }
            }
        })
        pickListViewModel.pickNotPickedCountLiveData.observe(viewLifecycleOwner, Observer {
            binding.txtTotalItems.text =
                String.format(resources.getString(R.string.total_items_d), it.first + it.second)
            binding.txtNotPicked.text =
                String.format(resources.getString(R.string.not_picked_d), it.first)
            binding.txtPicked.text =
                String.format(resources.getString(R.string.picked_d), it.second)
        })
        pickListViewModel.pickResponseLiveData.observe(viewLifecycleOwner, Observer {
            pickListViewModel.progressLiveData.postValue(false)
            showSnackBar("Pick Successful")
            navigate(PickListFragmentDirections.actionPickListFragmentToMenuFragment())
        })
        pickListViewModel.updateToatLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                showSnackBar(it, binding.btnComplete)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            TAG_SCAN_REQUEST_CODE -> {
                binding.tilScanItemId.onActivityResult(requestCode, resultCode, data)
            }
            TOAT_SCAN_REQUEST_CODE -> {
                binding.tilScanToatNumber.onActivityResult(requestCode, resultCode, data)
            }
        }
    }
}
