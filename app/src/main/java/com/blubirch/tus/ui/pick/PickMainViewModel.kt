package com.blubirch.tus.ui.pick

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.repository.PickRepo
import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject

class PickMainViewModel @Inject constructor(private val pickRepo: PickRepo) : BaseViewModel() {

    private lateinit var pickViewModel: PickViewModel

    val categoryItemsLiveData : LiveData<MutableList<ArrayList<String>>> = MutableLiveData()

    fun setMainModel(pickViewModel: PickViewModel) {
        PickMainViewModel@this.pickViewModel = pickViewModel
    }

    fun getOrderNumber(): String {
        return pickViewModel.selectedWarehouseOrder.orderNumber
    }

    fun getTotalItems(): Long {
//        if (BuildConfig.DEBUG && BuildConfig.FLAVOR == "dev") {
//            pickViewModel.selectedWarehouseOrder.itemsByCategory.apply {
//                while (size < 300) {
//                    val arrayList = ArrayList<String>()
//                    arrayList.add(AppUtils.generateSafeToken())
//                    arrayList.add(AppUtils.generateSafeToken())
//                    arrayList.add(Random.nextInt(5).toString())
//                    add(arrayList)
//                }
//            }
//        }
        var totalItems : Long = 0
        for (item in pickViewModel.selectedWarehouseOrder.itemsByCategory) {
            try {
                totalItems += item[2].toLong()
            } catch (nfe: NumberFormatException) {
                nfe.printStackTrace()
            }
        }
        return totalItems
    }

    fun getCategoryItems() {
        (categoryItemsLiveData as MutableLiveData).apply {
            postValue(pickViewModel.selectedWarehouseOrder.itemsByCategory)
        }
    }

}
