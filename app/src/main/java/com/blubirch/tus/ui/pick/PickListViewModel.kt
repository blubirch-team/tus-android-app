package com.blubirch.tus.ui.pick

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.api.dto.pick.UpdateToatResDTO
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.data.repository.PickRepo
import com.blubirch.core.data.api.dto.BaseDTO
import com.blubirch.core.data.lifecycle.SingleLiveEvent
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.utility.AppUtils
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class PickListViewModel @Inject constructor(private val pickRepo: PickRepo) : BaseViewModel() {

    private lateinit var pickViewModel: PickViewModel

    val warehouseOrderItemsLiveData: LiveData<MutableList<WarehouseOrderItems>> by lazy {
        MutableLiveData<MutableList<WarehouseOrderItems>>(ArrayList())
    }

    val pickNotPickedCountLiveData : LiveData<Pair<Int, Int>> by lazy {
        MutableLiveData<Pair<Int, Int>>()
    }

    val pickResponseLiveData : LiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val updateToatLiveData : LiveData<String?> by lazy {
        SingleLiveEvent<String?>()
    }

    private var isPickedVisible : Boolean = false


    fun setMainModel(pickViewModel: PickViewModel) {
        PickListViewModel@this.pickViewModel = pickViewModel
    }

    fun fetchNotPickedItems() {
        isPickedVisible = false
        compositeDisposable.add(
            Observable.fromIterable(pickViewModel.selectedWarehouseOrder.warehouseOrderItems)
                .filter{it.toatNumber == null}
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { warehouseOrderItems: MutableList<WarehouseOrderItems>?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    warehouseOrderItems?.let {
                        postItemListData(it)
                    }
                }
        )
    }

    fun fetchPickedItems() {
        isPickedVisible = true
        compositeDisposable.add(
            Observable.fromIterable(pickViewModel.selectedWarehouseOrder.warehouseOrderItems)
                .filter{it.toatNumber != null}
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { warehouseOrderItems: MutableList<WarehouseOrderItems>?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    warehouseOrderItems?.let {
                        postItemListData(it)
                    }
                }
        )
    }

    fun setToatNumber(itemId: String, toatNumber: String) : Boolean {
        val warehouseOrderItems = pickViewModel.selectedWarehouseOrder.warehouseOrderItems
        warehouseOrderItems.iterator().let {
            while (it.hasNext()) {
                val warehouseOrderItem = it.next()
                if (warehouseOrderItem.tagNumber == itemId) {
                    compositeDisposable.add(
                        pickRepo.updateToat(warehouseOrderItem.id.toString(), toatNumber)
                            .subscribeOn(Schedulers.io())
                            .subscribe { updateToatResDTO: UpdateToatResDTO?, throwable: Throwable? ->
                                throwable?.let {
                                    it.printStackTrace()
                                    postAPIError(it)
                                }
                                updateToatResDTO?.warehouseOrderItem?.let {
                                    (updateToatLiveData as SingleLiveEvent).apply {
                                        postValue("Item successfully picked")
                                    }
                                    updateAndPostItemListData(it)
                                }
                            }
                    )
                    return true
                }
            }
        }
        return false
    }

    private fun updateAndPostItemListData(updatedWarehouseItem: WarehouseOrderItems) {
        val warehouseOrderItems = pickViewModel.selectedWarehouseOrder.warehouseOrderItems
        warehouseOrderItems.let {
            for(i in it.indices){
                if (it[i].tagNumber == updatedWarehouseItem.tagNumber) {
                    it[i] = updatedWarehouseItem
                    break
                }
            }
        }
        if (isPickedVisible) {
            fetchPickedItems()
        } else {
            fetchNotPickedItems()
        }
    }

    fun getTotalToats(): Int {
        val listOfToats = ArrayList<String>()
        for (index in pickViewModel.selectedWarehouseOrder.warehouseOrderItems.indices) {
            val warehouseOrderItem = pickViewModel.selectedWarehouseOrder.warehouseOrderItems[index]
            val toat = warehouseOrderItem.toatNumber
            toat?.let {
                if(!listOfToats.contains(toat)){
                    listOfToats.add(toat)
                }
            }
        }
        return listOfToats.size
    }

    fun getTimeTaken() : Long {
        var earlyDate : Date? = null
        var laterDate : Date? = null
        for (index in pickViewModel.selectedWarehouseOrder.warehouseOrderItems.indices) {
            val warehouseOrderItem = pickViewModel.selectedWarehouseOrder.warehouseOrderItems[index]
            val updatedAt = AppUtils.getDateFromString(warehouseOrderItem.updatedAt)
            if (earlyDate == null) {
                earlyDate = updatedAt
            } else {
                earlyDate = AppUtils.getEarlyDate(earlyDate, updatedAt)
            }
            if (laterDate == null) {
                laterDate = updatedAt
            } else {
                laterDate = AppUtils.getLaterDate(earlyDate, updatedAt)
            }
        }

        return AppUtils.calculateMinutes(earlyDate, laterDate)
    }

    fun getPickedCount(): Int {
        pickNotPickedCountLiveData.value?.let {
            return it.second
        }
        return 0
    }

    fun getNotPickedCount(): Int {
        pickNotPickedCountLiveData.value?.let {
            return it.first
        }
        return 0
    }

    fun confirmPick() {
        compositeDisposable.add(
            pickRepo.confirmPick(pickViewModel.selectedWarehouseOrder.id.toString())
                .subscribeOn(Schedulers.io())
                .subscribe { baseDTO: BaseDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    baseDTO?.let {
                        if (it.status == 200) {
                            (pickResponseLiveData as MutableLiveData).postValue(it.message)
                        } else {
                            if (it.error.trim().isNotEmpty()) {
                                postAPIError(Throwable(it.error))
                            } else if (it.message.trim().isNotEmpty()){
                                postAPIError(Throwable(it.message))
                            }
                        }
                    }
                }
        )
    }

    fun postItemListData(warehouseOrderItems: MutableList<WarehouseOrderItems>) {
        (warehouseOrderItemsLiveData as MutableLiveData).postValue(warehouseOrderItems)
        var picked: Int = 0
        var notPicked: Int = 0
        if (isPickedVisible) {
            picked = warehouseOrderItems.size
            notPicked = pickViewModel.selectedWarehouseOrder.warehouseOrderItems.size - warehouseOrderItems.size
        } else {
            notPicked = warehouseOrderItems.size
            picked = pickViewModel.selectedWarehouseOrder.warehouseOrderItems.size - warehouseOrderItems.size
        }
        (pickNotPickedCountLiveData as MutableLiveData).postValue(Pair(notPicked, picked))
    }

    fun getOrderNumber(): String {
        return pickViewModel.selectedWarehouseOrder.orderNumber
    }

    fun getTotalInventories(): String {
        return pickViewModel.selectedWarehouseOrder.warehouseOrderItems.size.toString()
    }

}
