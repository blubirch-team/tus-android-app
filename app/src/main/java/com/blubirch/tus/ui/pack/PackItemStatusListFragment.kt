package com.blubirch.tus.ui.pack

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.databinding.FragmentPackItemStatusListBinding
import com.blubirch.tus.ui.pack.adapters.PackItemStatusListAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import javax.inject.Inject


class PackItemStatusListFragment : BaseFragment<PackViewModel>() {

    companion object {
        fun newInstance() = PackItemStatusListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: PackViewModel

    private lateinit var binding: FragmentPackItemStatusListBinding

    private lateinit var packItemStatusListAdapter: PackItemStatusListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPack)
        viewModel = ViewModelProvider(backStackEntry,viewModelFactory).get(PackViewModel::class.java)

        packItemStatusListAdapter = PackItemStatusListAdapter()

        val warehouseOrderItems = viewModel.selectedWarehouseOrder.warehouseOrderItems
        packItemStatusListAdapter.setNewInstance(warehouseOrderItems)

        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvItemList.addItemDecoration(dividerItemDecoration)
        binding.rvItemList.adapter = packItemStatusListAdapter

        var totalPacked = 0
        var totalItems = 0;
        for (warehouseOrderItem in warehouseOrderItems) {
            if (warehouseOrderItem.packagingBoxNumber.isNullOrEmpty().not()) {
                totalPacked++
            }
            totalItems++
        }

        setToolbarTitle(String.format(resources.getString(R.string.item_status_packed_d_d), totalPacked, totalItems))

    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPackItemStatusListBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): PackViewModel {
        return viewModel;
    }

    override fun attachLiveData() {
    }


}
