package com.blubirch.tus.ui.iteminfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.data.model.iteminfo.ItemInfo
import com.blubirch.tus.data.model.stowing.DispositionItem
import com.blubirch.tus.ui.iteminfo.adapters.DispositionItemAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import kotlinx.android.synthetic.main.fragment_item_options.*
import javax.inject.Inject

class ItemInfoOptionsFragment : BaseFragment<ItemInfoViewModel>() {

    companion object {
        fun newInstance() = ItemInfoOptionsFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: ItemInfoViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val backStackEntry = navController.getBackStackEntry(R.id.navItemInfo)
        viewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(ItemInfoViewModel::class.java)
        setItemData(viewModel.itemInfoLiveData.value)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_item_options, container, false)
    }

    override fun getViewModel(): ItemInfoViewModel {
        return viewModel
    }

    private fun setItemData(itemInfo : ItemInfo?) {
        itemInfo?.let {
            txtCurrentStatus.text = it.status
            txtUpdatedOn.text = String.format(
                getString(R.string.updated_on_s),
                AppUtils.dateStringToShortDateTime(it.updatedAt)
            )
            if (it.itemInwardDate != null) {
                txtInwardedOn.text = String.format(
                    getString(
                        R.string.inwarded_on_s,
                        AppUtils.dateStringToShortDateTime(it.itemInwardDate)
                    )
                )
            } else {
                txtInwardedOn.text = "---"
            }
            if (it.disptachDate != null) {
                txtDispatchedOn.text = String.format(
                    getString(
                        R.string.dispatched_on_s,
                        AppUtils.dateStringToShortDateTime(it.disptachDate)
                    )
                )
            } else {
                txtDispatchedOn.text = "---"
            }
            val dispositionItems: ArrayList<DispositionItem> = viewModel.getDispositions()
            dispositionItems.sortByDescending { it.dispositionDate }
            val adapter = DispositionItemAdapter()
            adapter.setNewInstance(dispositionItems)
            rvDispositionItems.adapter = adapter

            val dispositionDate = viewModel.getLastDispositionDate()
            if (dispositionDate == null) {
                txtLastDisposition.text =
                    String.format(getString(R.string.last_disposition_on_s), "---")
            } else {
                txtLastDisposition.text =
                    String.format(
                        getString(R.string.last_disposition_on_s),
                        AppUtils.dateStringToShortDateTime(dispositionDate)
                    )
                expansionHeader.setHeaderIndicatorId(R.id.imgDropDown)
                imgDropDown.visibility = View.VISIBLE
                expansionLayout.addListener { expansionLayout, expanded ->
                    var color: Int = if(expanded) resources.getColor(R.color.colorPrimary) else
                        resources.getColor(R.color.black)
                    txtDisposition.setTextColor(color)
                    imgDisposition.background.setTint(color)
                }
            }
        }
    }

    override fun attachLiveData() {
        viewModel.itemInfoLiveData.observe(viewLifecycleOwner, Observer {
            setItemData(it)
        })
    }

}
