package com.blubirch.tus.ui.pack.adapters

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.tus.R
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.tus.databinding.ItemPackGatePassBoxBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder


class PackGatePassAdapter: BaseQuickAdapter<BoxListItem, BaseViewHolder>(R.layout.item_pack_gate_pass_box) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<BoxListItem>() {
            override fun areItemsTheSame(oldItem: BoxListItem, newItem: BoxListItem): Boolean {
                return false
            }

            override fun areContentsTheSame(oldItem: BoxListItem, newItem: BoxListItem): Boolean {
                return false
            }
        })
    }

    override fun convert(holder: BaseViewHolder, boxListItem: BoxListItem) {
        val binding = ItemPackGatePassBoxBinding.bind(holder.itemView)
        binding.txtBoxNumber.text = boxListItem.box.boxNumber
        binding.txtItemCount.text = String.format(binding.root.context.resources.getString(R.string.d_items), boxListItem.warehouseOrderItems.size)
        val packGatePassItemAdapter = PackGatePassItemAdapter(boxListItem.warehouseOrderItems)

        val lm: LinearLayoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        binding.rvBotItem.isNestedScrollingEnabled = false;
        binding.rvBotItem.layoutManager = lm
        binding.rvBotItem.adapter = packGatePassItemAdapter
    }
}