package com.blubirch.tus.ui.pack.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.databinding.ItemPackGatePassBoxItemBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PackGatePassItemAdapter(items: MutableList<WarehouseOrderItems>) : BaseQuickAdapter<WarehouseOrderItems, BaseViewHolder>(R.layout.item_pack_gate_pass_box_item, items) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrderItems>() {
            override fun areItemsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return false
            }

            override fun areContentsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return false
            }
        })
    }

    override fun convert( holder: BaseViewHolder, item: WarehouseOrderItems) {
        val binding = ItemPackGatePassBoxItemBinding.bind(holder.itemView)
        binding.txtItemName.text = item.itemDescription
    }

}