package com.blubirch.tus.ui.pick

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.repository.PickRepo
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PickItemListViewModel @Inject constructor(private val pickRepo: PickRepo) : BaseViewModel() {

    val warehouseOrdersLiveData: LiveData<List<WarehouseOrders>> by lazy {
        MutableLiveData<List<WarehouseOrders>>()
    }

    private val warehouseOrders: ArrayList<WarehouseOrders> by lazy {
        ArrayList<WarehouseOrders>()
    }

    private var warehouseSearchTerm: String = ""

    fun getWareHouseList() {
        compositeDisposable.add(
            pickRepo.list
                .subscribeOn(Schedulers.io())
                .subscribe { warehouseOrdersResDTO: WarehouseOrdersResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    if (postAPIErrorIfAny(warehouseOrdersResDTO).not()) {
                        warehouseOrdersResDTO?.let {
                            warehouseOrders.clear()
                            warehouseOrders.addAll(warehouseOrdersResDTO.warehouseOrders)
                            postFilteredList()
                        }
                    }
                }
        )
    }

    fun searchWarehouseByVendor(vendor : String) {
        warehouseSearchTerm = vendor
        postFilteredList()
    }

    private fun postFilteredList(){
        compositeDisposable.add(
            Observable.fromIterable(warehouseOrders)
                .filter {
                    when {
                        warehouseSearchTerm.isBlank() -> true
                        it.vendorCode == null -> false
                        else -> {
                            it.vendorCode.toLowerCase().contains(warehouseSearchTerm.toLowerCase())
                        }
                    }
                }
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { filteredWarehouseOrders, throwable ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    filteredWarehouseOrders?.let {
                        postListData(it)
                    }
                }
        )
    }

    fun postListData(warehouseOrders: List<WarehouseOrders>) {
        (warehouseOrdersLiveData as MutableLiveData).postValue(warehouseOrders)
    }

}
