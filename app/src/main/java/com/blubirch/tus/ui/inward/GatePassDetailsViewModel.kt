package com.blubirch.tus.ui.inward


import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.constants.GatePassStatus
import com.blubirch.tus.data.model.inward.GatePass
import com.blubirch.tus.data.repository.GatePassRepo
import com.blubirch.core.data.MyException
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GatePassDetailsViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var gatePassRepo: GatePassRepo

    lateinit var gatePass: GatePass

    val grnResponseLiveData by lazy {
        MutableLiveData<String?>(null)
    }


    fun updateGRN(grnNumber: String): Boolean {
        if (grnNumber.isBlank()) {
            return false
        }
        updateGrnCall(grnNumber)
        return true
    }

    fun checkGrn(): Int {
        return when {
            gatePass.details?.grnSubmittedDate == null -> {
                com.blubirch.tus.constants.GatePassStatus.GRN_NOT_SUBMITTED
            }
            gatePass.details!!.grnNumber.isBlank() -> {
                com.blubirch.tus.constants.GatePassStatus.GRN_SUBMITTED
            }
            else -> com.blubirch.tus.constants.GatePassStatus.GRN_NUMBER_UPDATED
        }
    }

    fun isAllInwarded(): Boolean {
        return gatePass.gatePassInventories.none {
            it.quantity > it.inwardedQuantity
        }
    }

    private fun updateGrnCall(grnNumber: String) {
        progressLiveData.postValue(true)
        compositeDisposable.add(
            gatePassRepo.updateGRN(gatePass.clientGatePassNumber, grnNumber)
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        if (result.status == 200) {
                            gatePass.details!!.grnNumber = grnNumber
                            grnResponseLiveData.postValue(gatePass.clientGatePassNumber + ": " + result.message)
                        } else {
                            errorLiveData.postValue(MyException.ApiError(Throwable(result.message)))
                        }
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }
}