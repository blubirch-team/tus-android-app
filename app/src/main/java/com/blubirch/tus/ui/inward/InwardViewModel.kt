package com.blubirch.tus.ui.inward

import com.blubirch.tus.testing.data.api.dto.Disposition
import com.blubirch.tus.testing.data.models.ImageHolder
import com.blubirch.tus.testing.data.models.Test
import com.blubirch.tus.constants.GatePassInventoryStatus
import com.blubirch.tus.constants.GatePassStatus
import com.blubirch.tus.data.model.inward.GatePass
import com.blubirch.tus.data.model.inward.GatePassInventory
import com.blubirch.tus.data.repository.GatePassRepo
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc
import java.util.*
import javax.inject.Inject

class InwardViewModel @Inject constructor() : BaseViewModel() {


    @Inject
    lateinit var gatePassRepo: GatePassRepo

    //Selected GatePass
    var gatePass: GatePass? = null
        private set

    var sentInventoryCount: Int = 0
        private set
    var receivedInventoryCount: Int = 0
        private set
    var excessInventoryCount: Int = 0
        private set
    var inTransitInventoryCount: Int = 0
        private set

    //Selected Inventory
    lateinit var gatePassInventory: GatePassInventory
    var serialNumber: String? = null
    var serialNumber2: String? = null

    //Selected Tests, Images taken, document list and Grade & Disposition
    var tests: ArrayList<Test> = arrayListOf()
    var imageHolders: ArrayList<ImageHolder> = arrayListOf()
    var reason: String = ""
    var docList: ArrayList<AttachedDoc> = arrayListOf()

    //    var inventoryId: Long = 0
    var grade: String = ""
    var disposition: Disposition? = null
    var gradingResult: HashMap<String, String>? = null
    var dispositionNames: ArrayList<String>? = null
    var policyTypes: HashMap<String, String>? = null


    fun updateGatePass(gatePass: GatePass? = null) {
        if (gatePass != null) {
            this.gatePass = gatePass
        }

        sentInventoryCount = 0
        inTransitInventoryCount = 0
        excessInventoryCount = 0
        receivedInventoryCount = 0
        this.gatePass?.let {
            for (gatePassInventory: GatePassInventory in this.gatePass!!.gatePassInventories) {
                sentInventoryCount += gatePassInventory.quantity
                when (gatePassInventory.status) {
                    GatePassInventoryStatus.PENDING_RECEIPT -> {
                        if (gatePassInventory.quantity >= gatePassInventory.inwardedQuantity)
                            inTransitInventoryCount += gatePassInventory.quantity - gatePassInventory.inwardedQuantity
                        else
                            excessInventoryCount += (gatePassInventory.inwardedQuantity - gatePassInventory.quantity)
                        receivedInventoryCount += gatePassInventory.inwardedQuantity
                    }
                    GatePassInventoryStatus.PART_RECEIVED -> {
                        if (gatePassInventory.quantity >= gatePassInventory.inwardedQuantity)
                            inTransitInventoryCount += gatePassInventory.quantity - gatePassInventory.inwardedQuantity
                        else
                            excessInventoryCount += (gatePassInventory.inwardedQuantity - gatePassInventory.quantity)
                        receivedInventoryCount += gatePassInventory.inwardedQuantity
                    }
                    GatePassInventoryStatus.EXCESS_RECEIVED -> {
                        excessInventoryCount += (gatePassInventory.inwardedQuantity - gatePassInventory.quantity)
                        receivedInventoryCount += gatePassInventory.inwardedQuantity
                    }
                    GatePassInventoryStatus.FULLY_RECEIVED -> {
                        receivedInventoryCount += gatePassInventory.inwardedQuantity
                    }
                }
            }
        }
    }

    fun checkGrn(): Int {
        return when {
            gatePass!!.details?.grnSubmittedDate == null -> {
                GatePassStatus.GRN_NOT_SUBMITTED
            }
            gatePass!!.details!!.grnNumber.isBlank() -> {
                GatePassStatus.GRN_SUBMITTED
            }
            else -> GatePassStatus.GRN_NUMBER_UPDATED
        }
    }

    fun updateGatePassInventory() {
        gatePassInventory.let {
            if (it.inwardedQuantity == null) {
                it.inwardedQuantity = 0
            }
            it.inwardedQuantity++
            when {
                (it.inwardedQuantity > it.quantity) -> {
                    it.status = GatePassInventoryStatus.EXCESS_RECEIVED
                }
                it.inwardedQuantity < it.quantity -> {
                    it.status = GatePassInventoryStatus.PART_RECEIVED
                }
                it.inwardedQuantity == it.quantity -> {
                    it.status = GatePassInventoryStatus.FULLY_RECEIVED
                }
            }
        }
    }

}