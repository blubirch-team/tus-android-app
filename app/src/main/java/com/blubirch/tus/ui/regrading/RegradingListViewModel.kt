package com.blubirch.tus.ui.regrading

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.testing.data.api.dto.Disposition
import com.blubirch.tus.testing.data.models.ImageHolder
import com.blubirch.tus.testing.data.models.Test
import com.blubirch.tus.testing.data.models.getTestSelectionMap
import com.blubirch.tus.data.api.dto.regrading.ReGradeInventory
import com.blubirch.tus.data.repository.ReGradingRepo
import com.blubirch.core.data.api.dto.BaseDTO
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.arrayListOf
import kotlin.collections.isNotEmpty
import kotlin.collections.sortedByDescending

class RegradingListViewModel @Inject constructor(private val reGradingRepo: ReGradingRepo) :
    BaseViewModel() {

    private val reGradingList: ArrayList<ReGradeInventory> = arrayListOf()

    val reGradingListLiveData by lazy { MutableLiveData<ArrayList<ReGradeInventory>>() }
    var reGradeInventory: ReGradeInventory? = null

    //Selected Tests, Images taken, document list and Grade & Disposition
    var tests: ArrayList<Test> = arrayListOf()
    var imageHolders: ArrayList<ImageHolder> = arrayListOf()
    var grade: String = ""
    var disposition: Disposition? = null

    //Selected Tests, Images taken, document list and Grade & Disposition
    var gradingResult: HashMap<String, String>? = null
    var dispositionNames: ArrayList<String>? = null
    var policyTypes: HashMap<String, String>? = null
    var policyType: String? = null
    var policyRequired: Boolean = false

    fun loadData() {
        progressLiveData.value = true
        compositeDisposable.add(
            reGradingRepo.loadRegradingItems()
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    progressLiveData.postValue(false)
                    if (throwable == null) {
                        reGradingList.clear()
                        val list = arrayListOf<ReGradeInventory>()
                        list.addAll(result.liquidations)
                        list.addAll(result.repairs)
                        reGradingList.addAll(list.sortedByDescending { it.createdAt })
                        reGradingListLiveData.postValue(ArrayList(reGradingList))
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }


    fun filterGatePass(searchString: String) {
        compositeDisposable.add(
            Observable.fromIterable(reGradingList)
                .filter { item ->
                    if (searchString.isBlank())
                        true
                    else {
                        item.itemDescription.contains(searchString, true)
                                || item.skuCode.contains(searchString, true)
                                || item.tagNumber.contains(searchString, true)
                    }
                }
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { list, throwable ->
                    if (throwable == null && list.isNotEmpty()) {
                        reGradingListLiveData.postValue(list as ArrayList<ReGradeInventory>)
                    } else {
                        reGradingListLiveData.postValue(arrayListOf())
//                        errorLiveData.value = Exception("Incorrect STN Number")
                    }
                })
    }

    val toatNumberErrorLiveData by lazy { MutableLiveData<Exception>() }
    val policyErrorLiveData by lazy { MutableLiveData<Exception>() }

    fun saveAndContinue(toatNumber: String,policy: String) {
        var validate = true
        if (toatNumber == null || toatNumber.isBlank()) {
            validate = false
            toatNumberErrorLiveData.value = Exception("Invalid Toat Number")
        }
        if (policyRequired && (policy == null || policy.isBlank())) {
            validate = false
            policyErrorLiveData.value = Exception("Select Policy")
        }
        updatePolicy(policy)
        if (validate) {
            progressLiveData.value = true
            saveApiCall(toatNumber)
        }
    }

    val responseLiveData by lazy { MutableLiveData<BaseDTO>() }
    private fun saveApiCall(toatNumber: String) {
        if (disposition == null && reGradeInventory!!.status.contains("liquidation", true)) {
            disposition = Disposition("Liquidation")
        }
        compositeDisposable.add(
            reGradingRepo.saveGradeAndDispositionResult(
                reGradeInventory!!.inventoryId,
                toatNumber,
                getTestSelectionMap(tests),
                grade,
                gradingResult,
                disposition!!.disposition,
                disposition!!.flow,
                policyType
            )
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    if (throwable == null) {
                        responseLiveData.postValue(result)
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }

    private fun updatePolicy(policy: String) {
        policyType = policyTypes?.get(policy)
    }
}
