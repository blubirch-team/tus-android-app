package com.blubirch.tus.ui.iteminfo

import androidx.lifecycle.LiveData
import com.blubirch.tus.data.api.dto.ItemInfoResDTO
import com.blubirch.tus.data.model.iteminfo.ItemInfo
import com.blubirch.tus.data.model.stowing.DispositionItem
import com.blubirch.tus.data.repository.ItemInfoRepo
import com.blubirch.core.data.lifecycle.SingleLiveEvent
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.utility.AppUtils
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ItemInfoViewModel @Inject constructor(private val itemInfoRepo: ItemInfoRepo) : BaseViewModel() {

    val itemInfoSearchErrorLive: LiveData<String> by lazy {
        SingleLiveEvent<String>()
    }

    val itemInfoLiveData : LiveData<ItemInfo> by lazy {
        SingleLiveEvent<ItemInfo>()
    }

    fun searchItem(text: String?) {
        if (text.isNullOrEmpty()) {
            (itemInfoSearchErrorLive as SingleLiveEvent).postValue("Please enter item id")
        } else {
            compositeDisposable.add(
                itemInfoRepo.getInfo(text).subscribeOn(Schedulers.io())
                    .subscribe { itemInfoResDTO: ItemInfoResDTO?, throwable: Throwable? ->
                        throwable?.let {
                            postAPIError(it)
                            it.printStackTrace()
                        }
                        itemInfoResDTO?.let {
                            (itemInfoLiveData as SingleLiveEvent).postValue(itemInfoResDTO[0])
                        }
                    }
            )
        }
    }

    fun getDispositions(): ArrayList<DispositionItem> {
        val dispositions = ArrayList<DispositionItem>()
        dispositions.apply {
            itemInfoLiveData.value?.let {
                if (it.redeployId.isNullOrEmpty().not() && it.redeployUpdatedAt != null) {
                    add(DispositionItem("Redeploy", it.redeployUpdatedAt))
                }
                if (it.vendorReturnId.isNullOrEmpty().not() && it.vendorReturnUpdatedAt != null) {
                    add(DispositionItem("Brand Call-Log", it.vendorReturnUpdatedAt))
                }
                if (it.replacementId.isNullOrEmpty().not() && it.replacementUpdatedAt != null) {
                    add(DispositionItem("Replacement", it.replacementUpdatedAt))
                }
                if (it.insuranceId.isNullOrEmpty().not() && it.insuranceUpdatedAt != null) {
                    add(DispositionItem("Insurance", it.insuranceUpdatedAt))
                }
                if (it.markdownId.isNullOrEmpty().not() && it.markdownUpdatedAt != null) {
                    add(DispositionItem("Pending Transfer Out", it.markdownUpdatedAt))
                }
                if (it.repairId.isNullOrEmpty().not() && it.repairUpdatedAt != null) {
                    add(DispositionItem("Repair", it.repairUpdatedAt))
                }
                if (it.liquidationId.isNullOrEmpty().not() && it.liquidationUpdatedAt != null) {
                    add(DispositionItem("Liquidation", it.liquidationUpdatedAt))
                }
            }
        }
        return dispositions
    }

    fun getLastDispositionDate(): Date? {
        val dates = Observable.fromIterable(getDispositions())
            .map { it.dispositionDate }
            .toList()
            .blockingGet()
        dates?.let {
            return AppUtils.getLaterDateFromDate(dates)
        }
        return null
    }
}


