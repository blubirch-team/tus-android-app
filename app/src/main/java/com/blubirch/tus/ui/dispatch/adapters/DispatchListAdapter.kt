package com.blubirch.tus.ui.dispatch.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.databinding.ItemDispatchListItemBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class DispatchListAdapter constructor(private val onChecked: (selectedOrders : ArrayList<WarehouseOrders>) -> Unit) :
    BaseQuickAdapter<WarehouseOrders, BaseViewHolder>(R.layout.item_dispatch_list_item) {

    private var vendorCode = "";

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrders>() {
            override fun areItemsTheSame(oldItem: WarehouseOrders, newItem: WarehouseOrders): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: WarehouseOrders, newItem: WarehouseOrders): Boolean {
                return oldItem.orderNumber == newItem.orderNumber &&
                        oldItem.vendorCode == newItem.vendorCode &&
                        oldItem.totalQuantity == newItem.totalQuantity
            }
        })
    }

    override fun convert(holder: BaseViewHolder, warehouseOrder: WarehouseOrders) {
        val binding = ItemDispatchListItemBinding.bind(holder.itemView)

        if (vendorCode.isEmpty() || (vendorCode.trim().isNotEmpty() && warehouseOrder.vendorCode == vendorCode)) {
            binding.chkSelectOrder.isEnabled = true
            binding.chkSelectOrder.setOnCheckedChangeListener(null)
            binding.chkSelectOrder.isChecked = warehouseOrder.isChecked
            binding.chkSelectOrder.setOnCheckedChangeListener { buttonView, isChecked ->
                if (vendorCode.isEmpty() || (vendorCode.trim().isNotEmpty() && warehouseOrder.vendorCode == vendorCode)) {
                    warehouseOrder.isChecked = isChecked
                } else {
                    binding.chkSelectOrder.setOnCheckedChangeListener(null)
                    binding.chkSelectOrder.isChecked = false
                    binding.chkSelectOrder.isEnabled = false
                }
                val selectedOrders = ArrayList<WarehouseOrders>()
                for (order in data) {
                    if (order.isChecked) {
                        selectedOrders.add(order)
                    }
                }
                onChecked(selectedOrders)
                if (selectedOrders.size > 0) {
                    if (selectedOrders.size == 1 && vendorCode.isEmpty()) {
                        vendorCode = selectedOrders[0].vendorCode
                        notifyItemRangeChanged(0, data.size)
                    }
                } else {
                    vendorCode = ""
                    notifyItemRangeChanged(0, data.size)
                }
            }
        } else {
            binding.chkSelectOrder.isEnabled = false
            binding.chkSelectOrder.setOnCheckedChangeListener(null)
            binding.chkSelectOrder.isChecked = false
            warehouseOrder.isChecked = false
        }

        binding.txtVendorName.text = warehouseOrder.vendorName.orEmpty()
        binding.txtVendorCode.text = warehouseOrder.vendorCode
        binding.txtQuantity.text = warehouseOrder.totalQuantity.toString()
    }


}