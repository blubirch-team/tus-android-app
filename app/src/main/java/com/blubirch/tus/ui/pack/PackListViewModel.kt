package com.blubirch.tus.ui.pack

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.api.dto.pack.AssignBoxResDTO
import com.blubirch.tus.data.api.dto.pack.CreateBoxResDTO
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.model.pack.Box
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.tus.data.repository.PackRepo
import com.blubirch.core.data.MyException
import com.blubirch.core.data.api.dto.BaseDTO
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PackListViewModel @Inject constructor(private val packRepo: PackRepo) : BaseViewModel() {

    lateinit var selectedWarehouseOrder : WarehouseOrders

    val boxListItemLiveData: LiveData<ArrayList<BoxListItem>> by lazy {
        MutableLiveData<ArrayList<BoxListItem>>(ArrayList<BoxListItem>())
    }

    fun getBoxList() {
        val boxListItems = boxListItemLiveData.value?: ArrayList<BoxListItem>()

        for (warehouseOrderItem in selectedWarehouseOrder.warehouseOrderItems) {
            val packagingBoxNumber: String = warehouseOrderItem.packagingBoxNumber ?: ""
            val packagingBoxId = warehouseOrderItem.packagingBoxId
            if(packagingBoxNumber.isNotEmpty() && packagingBoxId != null && packagingBoxId != 0L) {
                var boxListItem: BoxListItem? = null
                for (it in boxListItems) {
                    if (it.box.id == packagingBoxId && it.box.boxNumber == packagingBoxNumber) {
                        boxListItem = it
                        break
                    }
                }
                if (boxListItem == null) {
                    val box = Box(
                        id = packagingBoxId,
                        boxNumber = packagingBoxNumber, distributionCenterId = 0, userId = 0,
                        createdAt = "", updatedAt = "", deletedAt = ""
                    )
                    boxListItem = BoxListItem(box)
                    boxListItem.warehouseOrderItems.add(warehouseOrderItem)
                    boxListItems.add(boxListItem)
                } else {
                    var containsWarehouseItem = false
                    for (it in boxListItem.warehouseOrderItems) {
                        if (it.id == warehouseOrderItem.id) {
                            containsWarehouseItem = true
                            break
                        }
                    }
                    if (containsWarehouseItem.not()) {
                        boxListItem.warehouseOrderItems.add(warehouseOrderItem)
                    }
                }
            }
        }

        postBoxListItemLiveDate()
    }

    fun createBox() {
        compositeDisposable.add(
            packRepo.createBox(selectedWarehouseOrder.id)
                .subscribeOn(Schedulers.io())
                .subscribe { createBoxResDTO: CreateBoxResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    createBoxResDTO?.let {
                        (boxListItemLiveData as MutableLiveData).value?.add(0, BoxListItem(it.box))
                        postBoxListItemLiveDate()
                    }
                }
        )
    }

    fun setBoxNumber(tagNumber: String, boxNumber: String) {
        compositeDisposable.add(
            Observable.fromIterable(boxListItemLiveData.value)
                .concatMapIterable { it.warehouseOrderItems }
                .any { it.packagingBoxNumber.isNullOrBlank().not() && it.tagNumber.toLowerCase() == tagNumber.toLowerCase() }
                .subscribeOn(Schedulers.computation())
                .subscribe { isAlreadyAdded: Boolean?, throwable: Throwable? ->
                    throwable?.let {
                        errorLiveData.postValue(MyException.UnKnownError(throwable))
                    }
                    isAlreadyAdded?.let {
                        if (it) {
                            errorLiveData.postValue(MyException.ValidationError(Throwable("Item already added!")))
                        } else {
                            compositeDisposable.add(
                                Observable.fromIterable(boxListItemLiveData.value)
                                    .filter{ it.box.boxNumber.toLowerCase() == boxNumber.toLowerCase() }
                                    .firstOrError()
                                    .subscribeOn(Schedulers.computation())
                                    .subscribe { boxListItem: BoxListItem?, throwable: Throwable? ->
                                        throwable?.let {
                                            val box = Box(
                                                id = 0, boxNumber = boxNumber, distributionCenterId = 0,
                                                userId = 0, createdAt = "", updatedAt = "", deletedAt = ""
                                            )
                                            val boxListItem = BoxListItem(box)
                                            boxListItemLiveData.value?.add(boxListItem)
                                            addItemToTheBox(tagNumber, boxListItem)
//                                            errorLiveData.postValue(MyException.ValidationError(Throwable("Could not find box")))
                                        }
                                        boxListItem?.let {
                                            addItemToTheBox(tagNumber, it)
                                        }
                                    }
                            )
                        }
                    }
                }

        )
    }

    private fun addItemToTheBox(tagNumber: String, boxListItem: BoxListItem) {
        compositeDisposable.add(
            Observable.fromIterable(selectedWarehouseOrder.warehouseOrderItems)
                .filter{it.tagNumber.toLowerCase() == tagNumber.toLowerCase()}
                .firstOrError()
                .subscribeOn(Schedulers.computation())
                .subscribe { warehouseOrderItem: WarehouseOrderItems?, throwable: Throwable? ->
                    throwable?.let {
                        errorLiveData.postValue(MyException.ValidationError(Throwable("Could not find item")))
                    }
                    warehouseOrderItem?.let { outer ->
                        if (outer.toatNumber.isNullOrBlank()) {
                            postAPIError(Throwable("Item not picked"))
                        }else {
                            packRepo.assignBox(outer.id, boxListItem.box.boxNumber)
                                .subscribeOn(Schedulers.io())
                                .subscribe { assignBoxResDTO: AssignBoxResDTO?, throwable: Throwable? ->
                                    throwable?.let {
                                        it.printStackTrace()
                                        postAPIError(it)
                                    }
                                    assignBoxResDTO?.let { assignBoxRes ->
                                        if (!postAPIErrorIfAny(assignBoxRes)) {
                                            assignBoxRes.gatePass?.let {
                                                selectedWarehouseOrder.gatepassNumber =
                                                    it.gatepassNumber
                                            }
                                            assignBoxRes.warehouseOrderItem.let {
                                                outer.copy(it)
                                                assignBoxRes.boxId?.let { packagingBoxId ->
                                                    boxListItem.box.id = packagingBoxId
                                                    outer.packagingBoxId = packagingBoxId
                                                }
                                                boxListItem.warehouseOrderItems.add(outer)
                                                postBoxListItemLiveDate()
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
        )
    }


    fun removeFromBox(warehouseOrderItem: WarehouseOrderItems) {
        compositeDisposable.add(
            packRepo.removeFromBox(warehouseOrderItem.id)
                .subscribeOn(Schedulers.io())
                .subscribe { baseDTO: BaseDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    baseDTO?.let {
                        if (baseDTO.status == 200) {
                            (boxListItemLiveData as MutableLiveData).value?.let {
                                warehouseOrderItem.packagingBoxNumber = null
                                for (boxListItem in it) {
                                    var removed = false
                                    for (index in boxListItem.warehouseOrderItems.indices) {
                                        if (boxListItem.warehouseOrderItems[index].id == warehouseOrderItem.id) {
                                            boxListItem.warehouseOrderItems.removeAt(index)
                                            removed = true
                                            break
                                        }
                                    }
                                    if(removed){
                                        break
                                    }
                                }
                            }
                            postBoxListItemLiveDate()
                        } else {
                            if (it.error.trim().isNotEmpty()) {
                                postAPIError(Throwable(it.error))
                            } else if (it.message.trim().isNotEmpty()){
                                postAPIError(Throwable(it.message))
                            }
                        }
                    }
                }
        )

    }

    fun deleteBox(boxListItem: BoxListItem) {
        compositeDisposable.add(
            packRepo.deleteBox(boxListItem.box.id)
                .subscribeOn(Schedulers.io())
                .subscribe { baseDTO: BaseDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    baseDTO?.let {
                        if (baseDTO.status == 200) {
                            (boxListItemLiveData as MutableLiveData).value?.let {
                                for (index in boxListItem.warehouseOrderItems.indices) {
                                    boxListItem.warehouseOrderItems[index].packagingBoxNumber = null;
                                }
                                it.remove(boxListItem)
                            }
                            postBoxListItemLiveDate()
                        } else {
                            if (it.error.trim().isNotEmpty()) {
                                postAPIError(Throwable(it.error))
                            } else if (it.message.trim().isNotEmpty()){
                                postAPIError(Throwable(it.message))
                            }
                        }
                    }
                }
        )
    }

    fun getNoOfItemsPacked() : Int{
        var totalNotPacked = 0
        var totalItems = 0
        for (warehouseOrderItem in selectedWarehouseOrder.warehouseOrderItems) {
            totalItems++
            if (warehouseOrderItem.packagingBoxNumber.isNullOrEmpty()) {
                totalNotPacked++
            }
        }
        return totalItems - totalNotPacked
    }

    fun getTotalItems() : Int{
        return selectedWarehouseOrder.warehouseOrderItems.size
    }

    private fun postBoxListItemLiveDate() {
        (boxListItemLiveData as MutableLiveData).postValue(boxListItemLiveData.value)
    }

}
