package com.blubirch.tus.ui.pick.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.BuildConfig
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.databinding.ItemPickItemListBinding
import com.blubirch.core.utility.copyTextToClipboardOnClick
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PickListAdapter : BaseQuickAdapter<WarehouseOrderItems, BaseViewHolder>(R.layout.item_pick_item_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrderItems>() {
            override fun areItemsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return false
            }

            override fun areContentsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return false
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: WarehouseOrderItems) {
        val binding = ItemPickItemListBinding.bind(holder.itemView)
        binding.txtTagNumber.text = item.tagNumber
        if (BuildConfig.DEBUG) {
            binding.txtTagNumber.copyTextToClipboardOnClick()
        }
        binding.txtBrand.text = item.details?.brand
        binding.txtLocation.text = item.aisleLocation
        binding.txtToatNumber.text = if(item.toatNumber == null) "-" else item.toatNumber
    }
}