package com.blubirch.tus.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.inward.GatePass
import com.blubirch.tus.databinding.ItemPendingRecieptBinding
import com.blubirch.core.utility.AppUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PendingReceiptListAdapter constructor(private val onItemClickListener: OnItemClickListener) :
    BaseQuickAdapter<GatePass, BaseViewHolder>(R.layout.item_pending_reciept) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<GatePass>() {
            override fun areItemsTheSame(
                    oldItem: GatePass,
                    newItem: GatePass
            ): Boolean {
                return oldItem.clientGatePassNumber == newItem.clientGatePassNumber
            }

            override fun areContentsTheSame(
                    oldItem: GatePass,
                    newItem: GatePass
            ): Boolean {
                return oldItem.clientGatePassNumber == newItem.clientGatePassNumber && oldItem.destinationCode == newItem.destinationCode
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: GatePass) {
        val binding = ItemPendingRecieptBinding.bind(holder.itemView)
        binding.txtSvnNumber.text = item.clientGatePassNumber
        binding.tvSource.text = item.sourceCode
        val count: Int = AppUtils.ageInDays(item.dispatchDate)
        binding.tvAgeing.text =
            context.resources.getQuantityString(R.plurals.days_d, count, count)
        holder.itemView.setOnClickListener {
            onItemClickListener.onItemClicked(item, getItemPosition(item))
        }
    }

    interface OnItemClickListener{
        fun onItemClicked(gatePass: GatePass, position: Int)
    }

}