package com.blubirch.tus.ui.dispatch

import android.os.Bundle
import android.view.*
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.databinding.FragmentDispatchListBinding
import com.blubirch.tus.ui.dispatch.adapters.DispatchListAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import javax.inject.Inject


class DispatchListFragment : BaseFragment<DispatchListViewModel>() {

    companion object {
        fun newInstance() = DispatchListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var dispatchListViewModel: DispatchListViewModel

    private lateinit var binding: FragmentDispatchListBinding

    private lateinit var dispatchListAdapter: DispatchListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navDispatch)
        val dispatchViewModel = ViewModelProvider(backStackEntry,viewModelFactory).get(DispatchViewModel::class.java)
        dispatchListViewModel = ViewModelProvider(this,viewModelFactory).get(DispatchListViewModel::class.java)
        dispatchListViewModel.setMainModel(dispatchViewModel)

        setHasOptionsMenu(dispatchViewModel.currentlySelectedWarehouseOrders.size > 0 ||
            dispatchViewModel.consignmentId > 0)
        dispatchListViewModel.setSelectedWarehouseOrders(arrayListOf())

        dispatchListAdapter = DispatchListAdapter {
            dispatchListViewModel.setSelectedWarehouseOrders(it)
            setHasOptionsMenu(it.size > 0 || dispatchViewModel.consignmentId > 0)
        }

        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvItemList.addItemDecoration(dividerItemDecoration)
        binding.rvItemList.adapter = dispatchListAdapter

        dispatchListViewModel.progressLiveData.value = true
        getViewModel().getList()


        binding.edtSearchVendor.addTextChangedListener(afterTextChanged = {
            it?.let {
                dispatchListViewModel.searchByVendorCode(it.toString())
            }
        })
    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDispatchListBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): DispatchListViewModel {
        return dispatchListViewModel;
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.check_only, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_ok -> navigate(DispatchListFragmentDirections.actionDispatchListFragmentToDispatchDetailFragment())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun attachLiveData() {
        getViewModel().dispatchListLiveData.observe(viewLifecycleOwner, Observer {
            dispatchListViewModel.progressLiveData.postValue(false)
            it?.let {
                if (it.isEmpty()) {
                    dispatchListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                    dispatchListAdapter.setNewInstance(null)
                } else {
                    if (dispatchListAdapter.data.size == 0) {
                        dispatchListAdapter.setNewInstance(it as ArrayList)
                    } else {
                        dispatchListAdapter.setDiffNewData(it as ArrayList)
                    }
                }
            }
        })
    }


}
