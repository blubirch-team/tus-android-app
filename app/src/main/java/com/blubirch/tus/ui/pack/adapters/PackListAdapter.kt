package com.blubirch.tus.ui.pack.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.databinding.ItemBoxListBinding
import com.blubirch.tus.utils.DividerItemDecoration
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PackListAdapter constructor(private val boxItemListener: BoxItemListener) : BaseQuickAdapter<BoxListItem, BaseViewHolder>(R.layout.item_box_list) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<BoxListItem>() {
            override fun areItemsTheSame(oldItem: BoxListItem, newItem: BoxListItem): Boolean {
                return oldItem.box.id == newItem.box.id
            }

            override fun areContentsTheSame(oldItem: BoxListItem, newItem: BoxListItem): Boolean {
                return oldItem.box.boxNumber == newItem.box.boxNumber &&
                        oldItem.warehouseOrderItems.size == newItem.warehouseOrderItems.size &&
                        areListItemSame(oldItem.warehouseOrderItems, newItem.warehouseOrderItems)
            }
        })
    }

    private fun areListItemSame(oldItems: ArrayList<WarehouseOrderItems>,
                                newItems: ArrayList<WarehouseOrderItems>) : Boolean{
        if (oldItems.size != newItems.size) {
            return false
        }
        for (index in oldItems.indices) {
            if(oldItems[index].tagNumber != newItems[index].tagNumber ||
                    oldItems[index].itemDescription != newItems[index].itemDescription){
                return false
            }
        }
        return true
    }

    override fun convert(holder: BaseViewHolder, boxListItem: BoxListItem) {
        val binding = ItemBoxListBinding.bind(holder.itemView)
        binding.txtBoxId.text = boxListItem.box.boxNumber
        binding.txtBoxCount.text = String.format(context.resources.getString(R.string._d_items),boxListItem.warehouseOrderItems.size)
        binding.imgDelete.setOnClickListener {
            boxItemListener.onBoxDelete(getItemPosition(boxListItem), boxListItem)
        }
        binding.imgPrint.setOnClickListener {
            boxItemListener.onPrint(boxListItem.box.boxNumber)
        }
        val packBoxItemAdapter = PackBoxItemAdapter(boxListItem.warehouseOrderItems,
            PackBoxItemAdapter.OnDeleteListener {
                position, warehouseOrderItem -> boxItemListener.onItemDelete(position, warehouseOrderItem, getItemPosition(boxListItem), boxListItem)
            }
        )
        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(binding.rvBoxItems.context)
        binding.rvBoxItems.addItemDecoration(dividerItemDecoration)
        binding.rvBoxItems.adapter = packBoxItemAdapter
    }

    public interface BoxItemListener{
        fun onItemDelete(itemPosition:Int, warehouseOrderItem: WarehouseOrderItems, parentPosition:Int, boxListItem: BoxListItem)
        fun onBoxDelete(position: Int, boxListItem: BoxListItem)
        fun onPrint(strToPrint: String)
    }
}