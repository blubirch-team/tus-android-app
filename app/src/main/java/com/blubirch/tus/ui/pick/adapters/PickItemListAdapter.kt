package com.blubirch.tus.ui.pick.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.databinding.ItemPickListItemBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PickItemListAdapter constructor(private val itemClickListener: ItemClickListener) :
    BaseQuickAdapter<WarehouseOrders, BaseViewHolder>(R.layout.item_pick_list_item) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrders>() {
            override fun areItemsTheSame(oldItem: WarehouseOrders, newItem: WarehouseOrders): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: WarehouseOrders, newItem: WarehouseOrders): Boolean {
                return oldItem.vendorName == newItem.vendorName &&
                        oldItem.vendorCode == newItem.vendorCode &&
                        oldItem.totalQuantity == newItem.totalQuantity
            }
        })
    }

    override fun convert(holder: BaseViewHolder, warehouseOrder: WarehouseOrders) {
        val binding = ItemPickListItemBinding.bind(holder.itemView)
        holder.itemView.setOnClickListener {
            itemClickListener.onItemClick(getItemPosition(warehouseOrder), warehouseOrder)
        }
        binding.txtVendorName.text = warehouseOrder.vendorName.orEmpty()
        binding.txtVendorCode.text = warehouseOrder.vendorCode
        binding.txtQuantity.text = warehouseOrder.totalQuantity.toString()
    }

    interface ItemClickListener{
        fun onItemClick(position: Int, warehouseOrder: WarehouseOrders)
    }
}