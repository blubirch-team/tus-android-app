package com.blubirch.tus.ui.dispatch.adapters

import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.repository.DispatchRepo
import com.blubirch.tus.ui.dispatch.DispatchViewModel
import com.blubirch.core.presentation.BaseViewModel
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DispatchGatepassConfirmViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var dispatchRepo: DispatchRepo

    private lateinit var dispatchViewModel: DispatchViewModel

    var deliveryRef: String
        get() { return dispatchViewModel.deliveryRef }
        set(value) { dispatchViewModel.deliveryRef = value }
    var invoiceNo: String
        get() { return dispatchViewModel.invoiceNo }
        set(value) { dispatchViewModel.invoiceNo = value }
    var transporter: String
        get() { return dispatchViewModel.transporter }
        set(value) { dispatchViewModel.transporter = value }
    var lorryRecNo: String
        get() { return dispatchViewModel.lorryRecNo }
        set(value) { dispatchViewModel.lorryRecNo = value }
    var vehicleNo: String
        get() { return dispatchViewModel.vehicleNo }
        set(value) { dispatchViewModel.vehicleNo = value }
    var drivePhNo: String
        get() { return dispatchViewModel.drivePhNo }
        set(value) { dispatchViewModel.drivePhNo = value }

    val dispatchConsignmentResponseLiveData by lazy { MutableLiveData<Boolean?>(null) }

    fun setMainModel(dispatchViewModel: DispatchViewModel) {
        DispatchDetailsViewModel@this.dispatchViewModel = dispatchViewModel
    }

    fun confirmDispatch(documents: ArrayList<AttachedDoc>) {
        progressLiveData.value = true
        compositeDisposable.add(
            dispatchRepo.dispatchConsignment(dispatchViewModel.consignmentId, documents)
                .subscribeOn(Schedulers.io())
                .subscribe { result, throwable ->
                    progressLiveData.postValue(true)
                    if (throwable == null) {
                        dispatchConsignmentResponseLiveData.postValue(true)
                    } else {
                        postAPIError(throwable)
                    }
                }
        )
    }

    fun getGatePassCount(): String {
        return dispatchViewModel.getAllSelectedOrders().size.toString()
    }

    fun getTotalQuantity(): String {
        var count = 0
        dispatchViewModel.getAllSelectedOrders().forEach {
            count += it.warehouseOrderItems.size
        }
        return count.toString()
    }


}