package com.blubirch.tus.ui.pack

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blubirch.tus.R
import com.blubirch.tus.databinding.DialogPackCompletionBinding
import com.blubirch.tus.databinding.DialogPackGatePassSummaryBinding
import com.blubirch.tus.ui.pack.adapters.PackGatePassAdapter
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseBottomSheetDialogFragment
import com.blubirch.core.utility.KotlinJavaDelegation
import com.google.android.material.bottomsheet.BottomSheetDialog
import javax.inject.Inject

class PackConfirmDialog : BaseBottomSheetDialogFragment<PackConfirmViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var packConfirmViewModelFactory: PackConfirmViewModel.Factory

    private lateinit var packConfirmViewModel: PackConfirmViewModel

    private lateinit var binding: DialogPackGatePassSummaryBinding


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val dialog = it as BottomSheetDialog

            dialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
            val containerLayout: FrameLayout =
                dialog.findViewById<FrameLayout>(com.google.android.material.R.id.container) as FrameLayout
            val frameButton = binding.frameButton
            val parent = frameButton.parent as ViewGroup
            parent.removeView(frameButton)
            containerLayout.addView(frameButton, containerLayout.childCount)
            (frameButton.layoutParams as FrameLayout.LayoutParams).gravity = Gravity.BOTTOM
        }
        return bottomSheetDialog
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        isCancelable = false

        val backStackEntry = navController.getBackStackEntry(R.id.navPack)
        val packViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(PackViewModel::class.java)
        packConfirmViewModel = packConfirmViewModelFactory.create(
            packViewModel.selectedWarehouseOrder,
            packViewModel.boxListItem
        )

        binding.txtSummary.text = String.format(requireContext().resources.getString(R.string.summary_s),
            packConfirmViewModel.selectedWarehouseOrder.orderNumber)
        binding.txtItemPacked.text = packConfirmViewModel.getNoOfItemsPacked().toString()
        binding.txtTotalBoxes.text = packConfirmViewModel.boxListItem?.size.toString()
        val packGatePassAdapter = PackGatePassAdapter()
        packGatePassAdapter.setNewInstance(packConfirmViewModel.boxListItem)

        val lm: LinearLayoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        binding.rvPackGatePass.isNestedScrollingEnabled = false;
        binding.rvPackGatePass.layoutManager = lm
        binding.rvPackGatePass.adapter = packGatePassAdapter

        binding.btnConfirmAndProceed.setOnClickListener {
            val completeBinding = DialogPackCompletionBinding.inflate(LayoutInflater.from(requireContext()))
            val completeDialog = KotlinJavaDelegation.createDialog(completeBinding.root)

            completeBinding.txtGatePass.text = packConfirmViewModel.selectedWarehouseOrder.gatepassNumber
            completeBinding.btnPrintBarcode.setOnClickListener {
                showPrintTagView(packConfirmViewModel.selectedWarehouseOrder.gatepassNumber?:"")
            }
            completeBinding.txtTotalInventory.text = packConfirmViewModel.getTotalItems().toString()
            completeBinding.txtTotalBoxes.text =  this@PackConfirmDialog.binding.txtTotalBoxes.text
            completeBinding.txtItemPacked.text = packConfirmViewModel.getNoOfItemsPacked().toString()
            completeBinding.txtNotPacked.text = packConfirmViewModel.getNoOfItemsNotPacked().toString()
            completeBinding.btnCancel.setOnClickListener {
                completeDialog.dismiss()
            }
            completeBinding.btnConfirm.setOnClickListener {
                completeDialog.dismiss()
                packConfirmViewModel.confirmDispatch()
            }
            completeDialog.show()
        }

        binding.btnCancel.setOnClickListener {
            dismiss()
        }

    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogPackGatePassSummaryBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): PackConfirmViewModel {
        return packConfirmViewModel
    }

    override fun attachLiveData() {
        packConfirmViewModel.dispatchResponseLiveData.observe(viewLifecycleOwner, Observer {
            it.let {
                showSnackBar(it)
                navigate(PackConfirmDialogDirections.actionPackConfirmDialogToMenuFragment())
            }
        })
    }

}
