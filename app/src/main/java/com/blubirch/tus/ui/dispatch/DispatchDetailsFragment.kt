package com.blubirch.tus.ui.dispatch

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.BuildConfig
import com.blubirch.tus.R
import com.blubirch.tus.databinding.FragmentDispatchDetailsBinding
import com.blubirch.tus.ui.dispatch.adapters.DispatchGatepassBoxAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.presentation.TakePictureActivity
import com.blubirch.core.presentation.customview.attachdoc.AttachDocView
import com.blubirch.core.utility.AppUtils
import javax.inject.Inject
import kotlin.random.Random


class DispatchDetailsFragment : BaseFragment<DispatchDetailsViewModel>(), TextWatcher {

    companion object {
        fun newInstance() = DispatchDetailsFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var dispatchDetailsViewModel: DispatchDetailsViewModel

    private lateinit var binding: FragmentDispatchDetailsBinding

    private lateinit var dispatchGatepassBoxAdapter: DispatchGatepassBoxAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navDispatch)
        val dispatchViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(DispatchViewModel::class.java)
        dispatchDetailsViewModel = ViewModelProvider(this, viewModelFactory).get(DispatchDetailsViewModel::class.java)
        dispatchDetailsViewModel.setMainModel(dispatchViewModel)

        val isConsignmentIdGenerated = dispatchViewModel.consignmentId > 0; // this will be > 0 if some items has been dispatched already

        setConfirmButtonState(isConsignmentIdGenerated)

        if (isConsignmentIdGenerated) {
            binding.edtDeliveryReference.setText(dispatchViewModel.deliveryRef)
            binding.edtInvoiceNo.setText(dispatchViewModel.invoiceNo)
            binding.edtTransporter.setText(dispatchViewModel.transporter)
            binding.edtLorryReceiptNo.setText(dispatchViewModel.lorryRecNo)
            binding.edtVehicleNo.setText(dispatchViewModel.vehicleNo)
            binding.edtDriverPhNo.setText(dispatchViewModel.drivePhNo)
            binding.edtDeliveryReference.isEnabled = false
            binding.edtInvoiceNo.isEnabled = false
            binding.edtTransporter.isEnabled = false
            binding.edtLorryReceiptNo.isEnabled = false
            binding.edtVehicleNo.isEnabled = false
            binding.edtDriverPhNo.isEnabled = false
            dispatchDetailsViewModel.setAttachedDocs(dispatchViewModel.getAttachedDocs())
            if(dispatchViewModel.currentlySelectedWarehouseOrders.size == 0){
                binding.attachDocView.isEnabled = false
            }
        } else {
            dispatchViewModel.clearVehicleDetails()
            binding.edtDeliveryReference.addTextChangedListener(this)
            binding.edtInvoiceNo.addTextChangedListener(this)
            binding.edtTransporter.addTextChangedListener(this)
            binding.edtLorryReceiptNo.addTextChangedListener(this)
            binding.edtVehicleNo.addTextChangedListener(this)
            binding.edtDriverPhNo.addTextChangedListener(this)
            if(BuildConfig.DEBUG && BuildConfig.FLAVOR.equals("dev")) {
                binding.edtDeliveryReference.setText(AppUtils.generateSafeToken())
                binding.edtInvoiceNo.setText(AppUtils.generateSafeToken())
                binding.edtTransporter.setText(AppUtils.generateSafeToken())
                binding.edtLorryReceiptNo.setText(AppUtils.generateSafeToken())
                binding.edtVehicleNo.setText(AppUtils.generateSafeToken())
                binding.edtDriverPhNo.setText((Random.nextInt(10000, 99999) * 100000).toString())
            }
        }

        binding.txtVendorCode.text = getViewModel().getVendorCode();

        dispatchGatepassBoxAdapter = DispatchGatepassBoxAdapter()
        dispatchGatepassBoxAdapter.setNewInstance(getViewModel().getAllSelectedOrders())
        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvGatepassAdapter.addItemDecoration(dividerItemDecoration)
        binding.rvGatepassAdapter.adapter = dispatchGatepassBoxAdapter

        binding.btnConfirmDispatch.setOnClickListener {
            if (binding.attachDocView.isEnabled) {
                val attachedDocs = dispatchDetailsViewModel.attachedDocLiveData.value
                if (attachedDocs == null || attachedDocs.isEmpty()) {
                    showSnackBar("Attach required documents.", binding.btnConfirmDispatch)
                    binding.attachDocView.requestFocus()
                } else {
                    dispatchDetailsViewModel.dispatchGatePass()
                }
            } else {
                openConfirmPage()
            }
        }

        dispatchDetailsViewModel.setDocTypes(getViewModel().getWarehouseOrderFileTypes())

        binding.attachDocView.setListener(object : AttachDocView.AttachDocListener {
            override fun startImageCapture() {
                Intent(activity, TakePictureActivity::class.java).let {
                    startActivityForResult(it, TakePictureActivity.INTENT_REQUEST_CODE_TAKE_PICTURE)
                }
            }
        })

        binding.attachDocView.setViewModel(viewLifecycleOwner, dispatchDetailsViewModel)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        binding.attachDocView.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDispatchDetailsBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): DispatchDetailsViewModel {
        return dispatchDetailsViewModel
    }

    override fun attachLiveData() {
        dispatchDetailsViewModel.dispatchGatePassResponseLiveData.observe(
            viewLifecycleOwner,
            Observer {
                it?.let {
                    try {
                        openConfirmPage()
                    } catch (e: Exception) {
                    }
                }
            })
    }

    private fun openConfirmPage() {
        navigate(DispatchDetailsFragmentDirections.actionDispatchDetailFragmentToDispatchDetailConfirmFragment())
    }

    override fun afterTextChanged(s: Editable?) {
        dispatchDetailsViewModel.deliveryRef = binding.edtDeliveryReference.text.toString()
        dispatchDetailsViewModel.invoiceNo = binding.edtInvoiceNo.text.toString()
        dispatchDetailsViewModel.transporter = binding.edtTransporter.text.toString()
        dispatchDetailsViewModel.lorryRecNo = binding.edtLorryReceiptNo.text.toString()
        dispatchDetailsViewModel.vehicleNo = binding.edtVehicleNo.text.toString()
        dispatchDetailsViewModel.drivePhNo = binding.edtDriverPhNo.text.toString()
        val phoneLength = binding.edtDriverPhNo.text.toString().trim().length
        if (phoneLength != 0 && phoneLength < 10) {
            binding.tilDriverPhNo.errorIconDrawable = null
            binding.tilDriverPhNo.error = "Invalid Phone Number"
        } else {
            binding.tilDriverPhNo.error = null
        }

        val isFormComplete = binding.edtDeliveryReference.text.toString()
            .isEmpty() || binding.edtInvoiceNo.text.toString().isEmpty() ||
                binding.edtTransporter.text.toString()
                    .isEmpty() || binding.edtLorryReceiptNo.text.toString().isEmpty() ||
                binding.edtVehicleNo.text.toString()
                    .isEmpty() || binding.edtDriverPhNo.text.toString().trim().length != 10
        setConfirmButtonState(!isFormComplete)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    private fun setConfirmButtonState(isEnabled: Boolean) {
        binding.btnConfirmDispatch.isEnabled = isEnabled
        binding.btnConfirmDispatch.setBackgroundColor(
            if (isEnabled) resources.getColor(R.color.colorPrimary)
            else resources.getColor(R.color.button_normal_text_color)
        )
    }


}
