package com.blubirch.tus.ui.pack.adapters

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.BuildConfig
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.tus.databinding.ItemPackItemStatusBinding
import com.blubirch.core.utility.copyTextToClipboardOnClick
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class PackItemStatusListAdapter : BaseQuickAdapter<WarehouseOrderItems, BaseViewHolder>(R.layout.item_pack_item_status) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<WarehouseOrderItems>() {
            override fun areItemsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: WarehouseOrderItems, newItem: WarehouseOrderItems): Boolean {
                return oldItem.tagNumber == newItem.tagNumber &&
                        oldItem.packagingBoxNumber == newItem.packagingBoxNumber
            }
        })
    }

    override fun convert(holder: BaseViewHolder, warehouseOrderItem: WarehouseOrderItems) {
        val binding = ItemPackItemStatusBinding.bind(holder.itemView)
        binding.tvTagNumber.text = warehouseOrderItem.tagNumber
        if (BuildConfig.DEBUG) {
            binding.tvTagNumber.copyTextToClipboardOnClick()
        }
        when {
            warehouseOrderItem.toatNumber.isNullOrBlank() -> {
                binding.tvBoxNumber.text = "-"
                binding.tvStatus.text = context.resources.getString(R.string.not_picked)
                binding.tvStatus.setTextColor(context.resources.getColor(R.color.text_color_E62626))
            }
            warehouseOrderItem.packagingBoxNumber.isNullOrBlank() -> {
                binding.tvBoxNumber.text = "-"
                binding.tvStatus.text = context.resources.getString(R.string.not_packed)
                binding.tvStatus.setTextColor(context.resources.getColor(R.color.text_color_E62626))
            }
            else -> {
                binding.tvBoxNumber.text = warehouseOrderItem.packagingBoxNumber
                binding.tvStatus.text = context.resources.getString(R.string.packed)
                binding.tvStatus.setTextColor(context.resources.getColor(R.color.text_color_219653))
            }
        }
    }
}