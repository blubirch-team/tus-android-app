package com.blubirch.tus.ui.regrading

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.blubirch.tus.testing.ui.TestActivity
import com.blubirch.tus.R
import com.blubirch.tus.data.api.dto.regrading.ReGradeInventory
import com.blubirch.tus.databinding.DialogReGradingInventoryInfoBinding
import com.blubirch.tus.ui.regrading.adapter.ReGradingListListAdapter
import com.blubirch.tus.utils.CustomTextWatcher
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import kotlinx.android.synthetic.main.fragment_regrading_list.*
import java.util.*
import javax.inject.Inject


class RegradingListFragment : BaseFragment<RegradingListViewModel>() {

    companion object {
        private const val TEST_REQUEST_CODE: Int = 1122
        fun newInstance() = RegradingListFragment()
    }

    private lateinit var descriptionDialog: MaterialDialog

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: RegradingListViewModel

    private var customTextWatcher: CustomTextWatcher? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel =
//            ViewModelProvider(this, viewModelFactory).get(RegradingListViewModel::class.java)
        val backStackEntry =
            navController.getBackStackEntry(R.id.navReGrading)
        viewModel = ViewModelProvider(backStackEntry, viewModelFactory)
            .get(RegradingListViewModel::class.java)
        viewModel.loadData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_regrading_list, container, false)
    }

    override fun getViewModel(): RegradingListViewModel {
        return viewModel
    }

    override fun onResume() {
        super.onResume()
        setListeners()
    }

    private fun setListeners() {
            customTextWatcher = object : CustomTextWatcher(edtSearchItem) {
                override fun textChanged(s: Editable?) {
                    viewModel.filterGatePass(edtSearchItem.text.toString())
                }
            }
        edtSearchItem.addTextChangedListener(customTextWatcher)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
    }


    override fun onPause() {
        super.onPause()
        if (customTextWatcher != null)
            edtSearchItem.removeTextChangedListener(customTextWatcher)
    }

    private fun setupList() {
        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())

        rvItemList.apply {
            addItemDecoration(dividerItemDecoration)
            rvItemList.adapter =
                ReGradingListListAdapter({ reGradeInventory, _ ->
                    viewModel.reGradeInventory = reGradeInventory
                    launchTesting(reGradeInventory)
                }, {
                    openInfo(it)
                })
        }
    }


    private fun launchTesting(reGradeInventory: ReGradeInventory) {
        val testInventory = reGradeInventory.toTestInventory()
        TestActivity.launch(this, testInventory, TEST_REQUEST_CODE)
    }

    override fun attachLiveData() {

        viewModel.apply {
            reGradingListLiveData.observe(viewLifecycleOwner, Observer {
                (rvItemList.adapter as ReGradingListListAdapter).apply {
                    setEmptyView(R.layout.layout_empty_list_msg_view)
                    if (data.size == 0) {
                        setNewInstance(it)
                    } else {
                        setDiffNewData(it)
                        notifyItemRangeChanged(0, it.size)
                    }
                }
            })
        }

    }

    private fun openInfo(reGradeInventory: ReGradeInventory) {
        val binding =
            DialogReGradingInventoryInfoBinding.inflate(LayoutInflater.from(requireContext()))
        descriptionDialog = MaterialDialog(requireContext(), BottomSheet(LayoutMode.WRAP_CONTENT))
            .show {
                val view = binding.root
                customView(view = view, noVerticalPadding = false, horizontalPadding = true)
                cornerRadius(16f)
            }
        reGradeInventory.apply {
            binding.hlvDesc.value = itemDescription
            binding.hlvSku.value = skuCode
            binding.hlvBrand.value = brand
            binding.hlvItemId.value = tagNumber
            binding.hlvLocation.value = aisleLocation
            binding.hlvSerialNumber.value = serialNumber
            val count: Int = AppUtils.ageInDays(createdAt)
            binding.hlvAge.value = resources.getQuantityString(R.plurals.days_d, count, count)
            binding.hlvCategoryL1.value = details.categoryL1
            binding.hlvCategoryL2.value = details.categoryL2
            binding.hlvSerialNumber2.value = serialNumber2

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (Activity.RESULT_OK == resultCode) {
            when (requestCode) {
                TEST_REQUEST_CODE -> {
                    data?.extras?.let {
                        updateTestResult()
                    }
//                    navigate(RegradingListFragmentDirections.actionReGradingFragmentToReGradingDispositionFragment())
                }
            }
        }
    }

    private fun updateTestResult() {
        viewModel.loadData()
        showSnackBar(getString(R.string.msg_regraded_successfully))
    }
}