package com.blubirch.tus.ui.pack

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.databinding.FragmentPackItemListBinding
import com.blubirch.tus.ui.pack.adapters.PackItemListAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import javax.inject.Inject


class PackItemListFragment : BaseFragment<PackItemListViewModel>() {

    companion object {
        fun newInstance() = PackItemListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var packItemListViewModel: PackItemListViewModel
    private lateinit var packViewModel: PackViewModel

    private lateinit var binding: FragmentPackItemListBinding

    private lateinit var packItemListAdapter: PackItemListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navPack)
        packViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(PackViewModel::class.java)
        packItemListViewModel = ViewModelProvider(this, viewModelFactory).get(PackItemListViewModel::class.java)

        packItemListAdapter = PackItemListAdapter(object : PackItemListAdapter.ItemClickListener{
            override fun onItemClick(position: Int, warehouseOrder: WarehouseOrders) {
                packViewModel.selectedWarehouseOrder = warehouseOrder
                navigate(PackItemListFragmentDirections.actionPackItemListFragmentToPackMainFragment())
            }
        })
        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvItemList.addItemDecoration(dividerItemDecoration)
        binding.rvItemList.adapter = packItemListAdapter

        packItemListViewModel.progressLiveData.value = true
        packItemListViewModel.getList()

        binding.edtSearchVendor.addTextChangedListener(afterTextChanged = {
            it?.let {
                packItemListViewModel.searchWarehouseByVendor(it.toString())
            }
        })

    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPackItemListBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): PackItemListViewModel {
        return packItemListViewModel;
    }

    override fun attachLiveData() {
        packItemListViewModel.warehouseOrdersLiveData.observe(viewLifecycleOwner, Observer {
            packItemListViewModel.progressLiveData.postValue(false)
            it.let {
                if (it.isEmpty()) {
                    packItemListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                    packItemListAdapter.setNewInstance(null)
                } else {
                   if (packItemListAdapter.data.size == 0) {
                        packItemListAdapter.setNewInstance(it as ArrayList)
                    } else {
                        packItemListAdapter.setDiffNewData(it as ArrayList)
                    }
                }
            }
        })
    }

}
