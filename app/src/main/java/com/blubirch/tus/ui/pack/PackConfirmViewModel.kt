package com.blubirch.tus.ui.pack

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.model.pack.BoxListItem
import com.blubirch.tus.data.repository.PackRepo
import com.blubirch.core.data.api.dto.BaseDTO
import com.blubirch.core.presentation.BaseViewModel
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import io.reactivex.schedulers.Schedulers

class PackConfirmViewModel @AssistedInject constructor(private val packRepo: PackRepo,
                                                       @Assisted val selectedWarehouseOrder : WarehouseOrders,
                                                       @Assisted val boxListItem : ArrayList<BoxListItem>?) : BaseViewModel() {

    @AssistedInject.Factory
    interface Factory {
        fun create(
                selectedWarehouseOrder: WarehouseOrders,
                boxListItem: ArrayList<BoxListItem>?
        ): PackConfirmViewModel
    }

    val dispatchResponseLiveData : LiveData<String> by lazy {
        MutableLiveData<String>()
    }

    fun getNoOfItemsPacked() : Int{
        var totalNotPacked = 0
        var totalItems = 0
        for (warehouseOrderItem in selectedWarehouseOrder.warehouseOrderItems) {
            totalItems++
            if (warehouseOrderItem.packagingBoxNumber.isNullOrEmpty()) {
                totalNotPacked++
            }
        }
        return totalItems - totalNotPacked
    }

    fun getNoOfItemsNotPacked() : Int{
        var totalNotPacked = 0
        for (warehouseOrderItem in selectedWarehouseOrder.warehouseOrderItems) {
            if (warehouseOrderItem.packagingBoxNumber.isNullOrEmpty()) {
                totalNotPacked++
            }
        }
        return totalNotPacked
    }

    fun getTotalItems() : Int{
        return selectedWarehouseOrder.warehouseOrderItems.size
    }

    fun confirmDispatch() {
        compositeDisposable.add(
            packRepo.confirmDispatch(selectedWarehouseOrder.id)
                .subscribeOn(Schedulers.io())
                .subscribe { baseDTO: BaseDTO?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    baseDTO?.let {
                        if (it.status == 200) {
                            (dispatchResponseLiveData as MutableLiveData).postValue(it.message)
                        } else {
                            if (it.error.trim().isNotEmpty()) {
                                postAPIError(Throwable(it.error))
                            } else if (it.message.trim().isNotEmpty()){
                                postAPIError(Throwable(it.message))
                            }
                        }
                    }
                }
        )
    }


}
