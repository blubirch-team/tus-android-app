package com.blubirch.tus.ui.regrading.adapter

import androidx.recyclerview.widget.DiffUtil
import com.blubirch.tus.R
import com.blubirch.tus.data.api.dto.regrading.ReGradeInventory
import com.blubirch.tus.databinding.ItemRegradingInventoryBinding
import com.blubirch.core.utility.AppUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class ReGradingListListAdapter constructor(
        private val onItemClick: (ReGradeInventory, Int) -> Unit,
        private val onInfoClick: (ReGradeInventory) -> Unit
) :
    BaseQuickAdapter<ReGradeInventory, BaseViewHolder>(R.layout.item_regrading_inventory) {

    init {
        setDiffCallback(object : DiffUtil.ItemCallback<ReGradeInventory>() {
            override fun areItemsTheSame(
                    oldItem: ReGradeInventory,
                    newItem: ReGradeInventory
            ): Boolean {
                return oldItem.skuCode == newItem.skuCode
            }

            override fun areContentsTheSame(
                    oldItem: ReGradeInventory,
                    newItem: ReGradeInventory
            ): Boolean {
                return oldItem.skuCode == newItem.skuCode && oldItem.inventoryId == newItem.inventoryId
            }
        })
    }

    override fun convert(holder: BaseViewHolder, item: ReGradeInventory) {
        val binding = ItemRegradingInventoryBinding.bind(holder.itemView)
        binding.tvItemDesc.text = item.itemDescription
        binding.tvItemId.text = item.tagNumber
        binding.tvILocation.text = item.aisleLocation
        val count: Int = AppUtils.ageInDays(item.createdAt)
        binding.tvItemAge.text =
            context.resources.getQuantityString(R.plurals.days_d, count, count)
        binding.ivInfo.setOnClickListener {
            onInfoClick(item)
        }
        holder.itemView.setOnClickListener {
            onItemClick(item, getItemPosition(item))
        }

    }
}