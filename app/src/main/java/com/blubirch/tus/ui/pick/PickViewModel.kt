package com.blubirch.tus.ui.pick

import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.tus.data.repository.PickRepo
import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject

class PickViewModel @Inject constructor(private val pickRepo: PickRepo) : BaseViewModel() {

    lateinit var selectedWarehouseOrder : WarehouseOrders

}
