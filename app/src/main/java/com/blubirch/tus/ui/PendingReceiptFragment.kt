package com.blubirch.tus.ui

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.data.model.inward.GatePass
import com.blubirch.tus.data.model.inward.GatePassInventory
import com.blubirch.tus.databinding.DialogPendingReceiptInfoBinding
import com.blubirch.tus.databinding.FragmentPendingReceiptBinding
import com.blubirch.tus.ui.adapters.PendingReceiptListAdapter
import com.blubirch.tus.ui.inward.GatePassSearchViewModel
import com.blubirch.tus.utils.CustomTextWatcher
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import com.blubirch.core.utility.KotlinJavaDelegation
import kotlinx.android.synthetic.main.fragment_pending_receipt.*
import javax.inject.Inject

class PendingReceiptFragment : BaseFragment<GatePassSearchViewModel>() {

    companion object {
        fun newInstance() = PendingReceiptFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: GatePassSearchViewModel

    private lateinit var binding: FragmentPendingReceiptBinding

    private lateinit var pendingReceiptListAdapter: PendingReceiptListAdapter

    private var customTextWatcher: CustomTextWatcher? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel =
            ViewModelProvider(this, viewModelFactory).get(GatePassSearchViewModel::class.java)

        getViewModel().loadData()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
    }

    override fun onResume() {
        super.onResume()
        setupListener()
    }

    private fun setupListener() {
        if (customTextWatcher == null) {
            customTextWatcher = object : CustomTextWatcher(edtSearchSTN) {
                override fun textChanged(s: Editable?) {
                    viewModel.filterGatePass(s.toString())
                }
            }
        }
        edtSearchSTN.addTextChangedListener(customTextWatcher)
    }

    override fun onPause() {
        super.onPause()
        if (customTextWatcher != null)
            edtSearchSTN.removeTextChangedListener(customTextWatcher)
    }

    private fun setupList() {
        pendingReceiptListAdapter =
            PendingReceiptListAdapter(object : PendingReceiptListAdapter.OnItemClickListener {
                override fun onItemClicked(gatePass: GatePass, position: Int) {
                    showGatePassRecieptDetails(gatePass)
                }
            })

        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        binding.rvItemList.addItemDecoration(dividerItemDecoration)
        binding.rvItemList.adapter = pendingReceiptListAdapter
    }

    private fun showGatePassRecieptDetails(gatePass: GatePass) {
        val binding =
            DialogPendingReceiptInfoBinding.inflate(LayoutInflater.from(requireContext()))
        val dialog = KotlinJavaDelegation.makeBottomSheet(binding.root)
        dialog.show()
        binding.imgClose.setOnClickListener {
            dialog.dismiss()
        }
        binding.txtSTNNumber.text = gatePass.clientGatePassNumber
        binding.txtTransferredTo.text = gatePass.destinationCode
        binding.txtTransferredFrom.text = gatePass.sourceCode
        var count = 0
        for (gatePassInventory: GatePassInventory in gatePass.gatePassInventories) {
            count += (gatePassInventory.quantity - gatePassInventory.inwardedQuantity)
        }
        binding.txtQuantity.text = count.toString()
        binding.txtAgeing.text = AppUtils.ageInDays(gatePass.dispatchDate).toString()
//                    binding.txtQuantity.text = gatePass.totalInventories.toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPendingReceiptBinding.inflate(inflater)
        return binding.root
    }

    override fun getViewModel(): GatePassSearchViewModel {
        return viewModel;
    }

    override fun attachLiveData() {
        getViewModel().gatePassesLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (pendingReceiptListAdapter.data.size == 0) {
                    pendingReceiptListAdapter.setNewInstance(it)
                } else {
                    pendingReceiptListAdapter.setDiffNewData(it)
                }
            }
        })
    }
}