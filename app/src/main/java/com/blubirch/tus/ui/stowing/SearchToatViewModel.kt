package com.blubirch.tus.ui.stowing

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.blubirch.tus.constants.APIResponseStatus
import com.blubirch.tus.data.api.dto.stowing.StowingItemListResDTO
import com.blubirch.tus.data.model.Inventory
import com.blubirch.tus.data.model.stowing.StowingItem
import com.blubirch.tus.data.repository.StowingRepo
import com.blubirch.core.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchToatViewModel @Inject constructor() : BaseViewModel() {

    lateinit var stowingViewModel: StowingViewModel

    @Inject
    lateinit var stowingRepo: StowingRepo

    private val toatList = ArrayList<StowingItem>()

    val toatListLiveData : LiveData<List<StowingItem>> by lazy {
        MutableLiveData<List<StowingItem>>()
    }

    val searchToatResponseLiveData by lazy {
        MutableLiveData<Int>()
    }

    var searchTerm : String = ""

    fun getStowingList() {
        progressLiveData.value = true
        compositeDisposable.add(
            stowingRepo.stowingList
                .subscribeOn(Schedulers.io())
                .subscribe { stowingItemListResDTO: StowingItemListResDTO?, throwable: Throwable? ->
                    throwable?.let {
                        postAPIError(it)
                        it.printStackTrace()
                    }

                    stowingItemListResDTO?.let {
                        if (postAPIErrorIfAny(stowingItemListResDTO).not()) {
                            toatList.clear()
                            toatList.addAll(stowingItemListResDTO.result)
                            postFilteredList()
                        }
                    }
                }
        )
    }

    fun filterList(toatNumber : String) {
        searchTerm = toatNumber
        postFilteredList()
    }

    private fun postFilteredList(){
        compositeDisposable.add(
            Observable.fromIterable(toatList)
                .filter{it.toatNumber.toLowerCase().contains(searchTerm.toLowerCase())}
                .toList()
                .subscribeOn(Schedulers.computation())
                .subscribe { filteredItems: MutableList<StowingItem>?, throwable: Throwable? ->
                    throwable?.let {
                        it.printStackTrace()
                        postAPIError(it)
                    }
                    filteredItems?.let {
                        progressLiveData.postValue(false)
                        (toatListLiveData as MutableLiveData).postValue(it)
                    }
                }
        )
    }

    fun searchToat(toatNumber: String) {
        stowingViewModel.selectedToatNumber = toatNumber
        progressLiveData.value = true
        compositeDisposable.add(
            stowingRepo.getStowingInventories(toatNumber)
                .subscribeOn(Schedulers.io())
                .subscribe { stowingInventoryDTO, throwable ->
                    if (throwable == null) {
                        if (stowingInventoryDTO.inventories == null || stowingInventoryDTO.inventories.isEmpty())
                            searchToatResponseLiveData.postValue(com.blubirch.tus.constants.APIResponseStatus.NOT_FOUND)
                        else {
                            saveInventoryList(stowingInventoryDTO.inventories)
                            searchToatResponseLiveData.postValue(com.blubirch.tus.constants.APIResponseStatus.FOUND)
                        }
                    } else {
                        searchToatResponseLiveData.postValue(com.blubirch.tus.constants.APIResponseStatus.ERROR)
                        postAPIError(throwable)
                    }
                })
    }

    private fun saveInventoryList(inventoryList: List<Inventory>) {
        stowingViewModel.selectedToatInventories.apply {
            clear()
            addAll(inventoryList)
            var count = 0
            forEach {
                it.location?.apply {
                    if (isNotEmpty())
                        count++
                }
            }
            (stowingViewModel.stowedCountLiveData as MutableLiveData).postValue(count)
        }
    }

}
