package com.blubirch.tus.ui.stowing

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blubirch.tus.R
import com.blubirch.tus.constants.APIResponseStatus
import com.blubirch.tus.data.model.stowing.StowingItem
import com.blubirch.tus.ui.stowing.adapter.StowingItemListAdapter
import com.blubirch.tus.utils.DividerItemDecoration
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseFragment
import com.blubirch.core.utility.AppUtils
import kotlinx.android.synthetic.main.fragment_search_toat.*
import javax.inject.Inject

class SearchToatFragment : BaseFragment<SearchToatViewModel>() {

    companion object {
        const val SCAN_TOAT_REQUEST_CODE = 6677
        fun newInstance() = SearchToatFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var searchToatViewModel: SearchToatViewModel

    private lateinit var stowingItemListAdapter: StowingItemListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backStackEntry = navController.getBackStackEntry(R.id.navStowing)
        val stowingViewModel = ViewModelProvider(backStackEntry, viewModelFactory).get(StowingViewModel::class.java)
        searchToatViewModel = ViewModelProvider(this, viewModelFactory).get(SearchToatViewModel::class.java)
        searchToatViewModel.stowingViewModel = stowingViewModel

        stowingItemListAdapter = StowingItemListAdapter{ position: Int, item: StowingItem ->
            AppUtils.hideSoftKeyboard(requireActivity())
            searchToatViewModel.searchToat(item.toatNumber)
        }

        val dividerItemDecoration = com.blubirch.tus.utils.DividerItemDecoration(requireContext())
        rvItemList.addItemDecoration(dividerItemDecoration)
        rvItemList.adapter = stowingItemListAdapter

        tilSearchToat.editText.addTextChangedListener(afterTextChanged = {
            it?.let {
                searchToatViewModel.filterList(it.toString())
            }
        })

        swipeRefresh.setOnRefreshListener {
            searchToatViewModel.getStowingList()
            swipeRefresh.post {
                swipeRefresh.isRefreshing = false
            }
        }

        searchToatViewModel.getStowingList()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_search_toat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tilSearchToat.initialize(this, SCAN_TOAT_REQUEST_CODE)
    }

    override fun onResume() {
        super.onResume()
        tilSearchToat.editText.setOnEditorActionListener { view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchToatViewModel.searchToat(tilSearchToat.text.toString())
                return@setOnEditorActionListener true
            }
            false
        }
    }

    override fun getViewModel(): SearchToatViewModel {
        return searchToatViewModel
    }

    override fun attachLiveData() {
        searchToatViewModel.searchToatResponseLiveData.observe(viewLifecycleOwner, Observer {
            searchToatViewModel.progressLiveData.value = false
            when (it) {
                com.blubirch.tus.constants.APIResponseStatus.NOT_FOUND -> {
                    showSnackBar(getString(R.string.no_inventory_in_toat))
                    searchToatViewModel.searchToatResponseLiveData.value = com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED

                }
                com.blubirch.tus.constants.APIResponseStatus.FOUND -> {
                    searchToatViewModel.searchToatResponseLiveData.value = com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED
                    navigate(SearchToatFragmentDirections.actionSearchToatFragmentToToatTagListFragment())
                }
            }
            if (it != com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED)
                searchToatViewModel.searchToatResponseLiveData.value = com.blubirch.tus.constants.APIResponseStatus.NOT_REQUESTED
        })

        getViewModel().toatListLiveData.observe(viewLifecycleOwner, Observer {
            searchToatViewModel.progressLiveData.postValue(false)
            it?.let {
                if (it.isEmpty()) {
                    stowingItemListAdapter.setEmptyView(R.layout.layout_empty_list_msg_view);
                    stowingItemListAdapter.setNewInstance(null)
                } else {
                    if (stowingItemListAdapter.data.size == 0) {
                        stowingItemListAdapter.setNewInstance(it as ArrayList)
                    } else {
                        stowingItemListAdapter.setDiffNewData(it as ArrayList)
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        tilSearchToat.onActivityResult(requestCode, resultCode, data)
    }

}