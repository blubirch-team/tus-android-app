package com.blubirch.tus.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

abstract class CustomTextWatcher(private val edt: EditText) : TextWatcher {

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        if (!edt.hasFocus())
            return
        textChanged(s)
    }

    abstract fun textChanged(s: Editable?)
}