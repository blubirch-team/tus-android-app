package com.blubirch.tus.data.api.interfaces;

import com.blubirch.tus.constants.EndPoints;
import com.blubirch.tus.data.api.dto.regrading.ReGradeInventoryDTO;
import com.blubirch.tus.data.api.dto.regrading.ReGradingRespDTO;
import com.blubirch.core.data.api.dto.BaseDTO;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ReGradingAPI {

    @GET(EndPoints.RE_GRADING_ITEMS)
    Single<ReGradingRespDTO> getList();

    @POST(EndPoints.SAVE_REGRADING)
    Single<BaseDTO> storeInventoryGrade(@Body ReGradeInventoryDTO reGradeInventoryDTO);
}
