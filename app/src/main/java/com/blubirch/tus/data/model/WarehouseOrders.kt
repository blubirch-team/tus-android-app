package com.blubirch.tus.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class WarehouseOrders(
        @Expose(serialize = false, deserialize = false)
    var isChecked: Boolean = false,
        @Expose
    @SerializedName("id")
    var id: Long,
        @Expose
    @SerializedName("order_number")
    val orderNumber: String,
        @Expose
    @SerializedName("vendor_code")
    val vendorCode: String,
        @Expose
    @SerializedName("vendor_name")
    val vendorName: String?,
        @Expose
    @SerializedName("partially_dispatched")
    val partiallyDispatched : Boolean,
        @Expose
    @SerializedName("status")
    val status: String,
        @Expose
    @SerializedName("distribution_center_id")
    var distributionCenterId: Long,
        @Expose
    @SerializedName("client_id")
    var clientId: Long,
        @Expose
    @SerializedName("status_id")
    var statusId: Long,
        @Expose
    @SerializedName("warehouse_gatepass_id")
    var warehouseGatepassId: Long?,
        @Expose
    @SerializedName("total_quantity")
    var totalQuantity: Long,
        @Expose
    @SerializedName("gatepass_number")
    var gatepassNumber: String?,
        @Expose
    @SerializedName("warehouse_order_items")
    var warehouseOrderItems: MutableList<WarehouseOrderItems>,
        @Expose
    @SerializedName("items_by_category")
    var itemsByCategory: ArrayList<ArrayList<String>>,
        @Expose
    @SerializedName("warehouse_order_file_types")
    var warehouseOrderFileTypes: ArrayList<ArrayList<String?>?>?,
        @Expose
    @SerializedName("warehouse_consignment_file_types")
    var warehouseConsignmentFileTypes: MutableList<MutableList<String?>?>?,
        @Expose
    @SerializedName("created_at")
    val createdAt: String,
        @Expose
    @SerializedName("updated_at")
    val updatedAt: String,
        @Expose(serialize = false, deserialize = false)
    var isDispatched: Boolean = false
){

    override fun equals(that: Any?): Boolean {
        if (this === that) return true
        if (that == null || javaClass != that.javaClass) return false
        val that: WarehouseOrders = that as WarehouseOrders
        return id == that.id
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }
}