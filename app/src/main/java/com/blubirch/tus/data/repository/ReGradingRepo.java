package com.blubirch.tus.data.repository;

import com.blubirch.tus.testing.data.models.TestSelection;
import com.blubirch.tus.data.api.dto.regrading.ReGradingRespDTO;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.core.data.database.dao.BaseDao;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Single;

public interface ReGradingRepo {

    Single<ReGradingRespDTO> loadRegradingItems();

    Single<BaseDTO> saveGradeAndDispositionResult(
            Long inventoryId, String toatNumber, HashMap<String, ArrayList<TestSelection>> testSelectionListMap,
            String grade, HashMap<String, String> gradingResult, String disposition, String workFlow, String policy);
}
