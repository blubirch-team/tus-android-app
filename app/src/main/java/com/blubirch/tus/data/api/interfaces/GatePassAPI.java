package com.blubirch.tus.data.api.interfaces;

import com.blubirch.tus.testing.data.api.EndPoint;
import com.blubirch.tus.constants.EndPoints;
import com.blubirch.tus.data.api.dto.inward.GatePassDTO;
import com.blubirch.tus.data.api.dto.inward.ProceedGRNRequestDTO;
import com.blubirch.tus.data.api.dto.inward.ProceedGRNResponseDTO;
import com.blubirch.tus.data.api.dto.inward.SKuResponseDTO;
import com.blubirch.tus.data.api.dto.inward.SaveTestedReturnDTO;
import com.blubirch.tus.data.api.dto.inward.TagDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateGRNRequestDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateGRNResponseDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateTagToatRequestDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateTagToatResponseDTO;
import com.blubirch.tus.data.api.dto.inward.VerifySerialDTO;
import com.blubirch.core.data.api.dto.BaseDTO;

import java.util.Date;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface GatePassAPI {

    @GET(EndPoints.GATE_PASS_LIST)
    Single<GatePassDTO> getList(@Query("date") Date date);

    @GET(EndPoints.GATE_PASS_SEARCH)
    Single<GatePassDTO> searchSTN(@Query("stn_number") String stnNumber);

    @GET(EndPoints.SKU_SEARCH)
    Single<SKuResponseDTO> searchSKU(@Query("sku_code") String skuCode);

    @GET(EndPoints.GENERATE_TAG)
    Single<TagDTO> generateTag();

    @POST(EndPoints.UPDATE_TAG_TOAT)
    Single<UpdateTagToatResponseDTO> updateTagToat(@Body UpdateTagToatRequestDTO updateTagToatRequestDTO);

    @POST(EndPoints.PROCEED_GRN)
    Single<ProceedGRNResponseDTO> proceedGRN(@Body ProceedGRNRequestDTO proceedGRNRequestDTO);

    @POST(EndPoints.UPDATE_GRN)
    Single<UpdateGRNResponseDTO> updateGRN(@Body UpdateGRNRequestDTO updateGRNRequestDTO);

    @Multipart
    @POST(EndPoints.SAVE_TESTED_RETURN)
    Single<SaveTestedReturnDTO> saveTestedReturn(@PartMap Map<String, RequestBody> body, @Part List<MultipartBody.Part> documentsPart);

    @POST(EndPoints.VERIFY_SERIAL_NUMBER)
    Single<BaseDTO> verifySerialNumber(@Body VerifySerialDTO verifySerialDTO);
}
