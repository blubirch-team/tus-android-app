package com.blubirch.tus.data.repository;

import com.blubirch.tus.testing.data.models.TestSelection;
import com.blubirch.tus.data.api.dto.inward.GatePassDTO;
import com.blubirch.tus.data.api.dto.inward.ProceedGRNResponseDTO;
import com.blubirch.tus.data.api.dto.inward.SKuResponseDTO;
import com.blubirch.tus.data.api.dto.inward.SaveTestedReturnDTO;
import com.blubirch.tus.data.api.dto.inward.TagDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateGRNResponseDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateTagToatResponseDTO;
import com.blubirch.tus.data.model.inward.GatePassInventory;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Single;

public interface GatePassRepo {

    Single<GatePassDTO> loadData();

    Single<GatePassDTO> searchGatePass(String gatePassNumber);

    Single<SKuResponseDTO> searchSKU(String sku);

    Single<BaseDTO> verifySerialNumber(String serialNumber, String serialNumber2);

    Single<TagDTO> generateTag();

    Single<UpdateTagToatResponseDTO> updateTagToat(long inventoryId, String tagId, String toatNumber);

    Single<ProceedGRNResponseDTO> proceedGRN(String stnNumber);

    Single<UpdateGRNResponseDTO> updateGRN(String stnNumber, String grnNumber);

    Single<SaveTestedReturnDTO> saveTestedReturn(
            String gatePassNumber, GatePassInventory gatePassInventory, String returnReason,
            ArrayList<AttachedDoc> documents, String serialNumber, String serialNumber2,
            String srNumber, String tagNumber, String toatNumber,String manualDisposition,
            HashMap<String, ArrayList<TestSelection>> testSelectionListMap, String grade,
            HashMap<String, String> gradingResult, String disposition, String workFlow,String policy);
}
