package com.blubirch.tus.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GatePass (
    @Expose
    @SerializedName("id")
    val id: Long,
    @Expose
    @SerializedName("distribution_center_id")
    val distributionCenterId: Long,
    @Expose
    @SerializedName("client_id")
    val clientId: Long,
    @Expose
    @SerializedName("user_id")
    val userId: Long,
    @Expose
    @SerializedName("status_id")
    val statusId: Long,
    @Expose
    @SerializedName("gatepass_number")
    val gatepassNumber: String,
    @Expose
    @SerializedName("created_at")
    val createdAt: String,
    @Expose
    @SerializedName("updated_at")
    val updatedAt: String
)