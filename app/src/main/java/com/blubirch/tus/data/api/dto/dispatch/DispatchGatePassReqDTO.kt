package com.blubirch.tus.data.api.dto.dispatch

import com.google.gson.annotations.SerializedName

class DispatchGatePassReqDTO(
    @SerializedName("id")
    var id: ArrayList<Long>,
    @SerializedName("delivery_reference_number")
    var deliveryReferenceNumber: String,
    @SerializedName("invoice_number")
    var invoiceNumber: String,
    @SerializedName("consignment_id")
    var consignmentId: Long,
    @SerializedName("transporter")
    var transporter: String,
    @SerializedName("lorry_receipt_number")
    var lorryReceiptNumber: String,
    @SerializedName("vehicle_number")
    var vehicleNumber: String,
    @SerializedName("driver_contact_number")
    var driverContactNumber: String,
    @SerializedName("driver_name")
    var driverName: String

)

