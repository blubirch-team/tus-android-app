package com.blubirch.tus.data.repository;

import com.blubirch.tus.data.api.dto.ItemInfoResDTO;
import com.blubirch.tus.data.api.interfaces.ItemInfoAPI;

import javax.inject.Inject;

import io.reactivex.Single;

public class ItemInfoRepoImpl implements ItemInfoRepo {

    private ItemInfoAPI itemInfoAPI;

    @Inject
    public ItemInfoRepoImpl(ItemInfoAPI itemInfoAPI) {
        this.itemInfoAPI = itemInfoAPI;
    }

    @Override
    public Single<ItemInfoResDTO> getInfo(String tagNumber) {
        return itemInfoAPI.getInfo(tagNumber);
    }

}
