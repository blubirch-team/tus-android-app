package com.blubirch.tus.data.model.stowing

import java.util.*

class DispositionItem (
    var dispositionLabel : String,
    var dispositionDate : Date
)
