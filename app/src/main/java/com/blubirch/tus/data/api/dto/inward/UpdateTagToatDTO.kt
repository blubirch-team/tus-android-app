package com.blubirch.tus.data.api.dto.inward

import com.google.gson.annotations.SerializedName

class UpdateTagToatRequestDTO(
    @SerializedName("inventory_id")
    var inventoryId: Long,
    @SerializedName("tag_number")
    var tagNumber: String,
    @SerializedName("toat_number")
    var toatNumber: String

)

class UpdateTagToatResponseDTO(@SerializedName("inventory_id") var inventoryId: Long)