package com.blubirch.tus.data.api.dto.inward


import com.blubirch.core.data.api.dto.BaseDTO
import com.google.gson.annotations.SerializedName

class SaveTestedReturnDTO(
    var result: GatePassInventoryResult
) : BaseDTO()

class GatePassInventoryResult(
    @SerializedName("brand")
    var brand: String,
    @SerializedName("client_category_id")
    var clientCategoryId: Int,
    @SerializedName("client_category_name")
    var clientCategoryName: String,
    @SerializedName("client_id")
    var clientId: Int,
    @SerializedName("created_at")
    var createdAt: String,
    @SerializedName("deleted_at")
    var deletedAt: Any,
    @SerializedName("distribution_center_id")
    var distributionCenterId: Long,
    @SerializedName("gate_pass_id")
    var gatePassId: Int,
    @SerializedName("id")
    var id: Long,
    @SerializedName("inwarded_quantity")
    var inwardedQuantity: Int,
    @SerializedName("item_description")
    var itemDescription: String,
    @SerializedName("map")
    var map: Double,
    @SerializedName("quantity")
    var quantity: Int,
    @SerializedName("sku_code")
    var skuCode: String,
    @SerializedName("status")
    var status: String,
    @SerializedName("status_id")
    var statusId: Int,
    @SerializedName("updated_at")
    var updatedAt: String,
    @SerializedName("user_id")
    var userId: Int
)