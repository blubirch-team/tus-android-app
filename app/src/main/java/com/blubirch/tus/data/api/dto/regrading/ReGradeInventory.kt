package com.blubirch.tus.data.api.dto.regrading


import com.blubirch.tus.testing.data.models.TestInventory
import com.google.gson.annotations.SerializedName
import java.util.*

data class ReGradeInventory(
    @SerializedName("id")
    var id: Long,
    @SerializedName("inventory_id")
    var inventoryId: Long,
    @SerializedName("item_description")
    var itemDescription: String,
    @SerializedName("sku_code")
    var skuCode: String,
    @SerializedName("client_category_id")
    var clientCategoryId: Long,
    @SerializedName("brand")
    var brand: String?,
    @SerializedName("tag_number")
    var tagNumber: String,
    @SerializedName("client_tag_number")
    var clientTagNumber: String,
    @SerializedName("toat_number")
    var toatNumber: String,
    @SerializedName("aisle_location")
    var aisleLocation: String,
    @SerializedName("serial_number")
    var serialNumber: String,
    @SerializedName("serial_number_2")
    var serialNumber2: String,
    @SerializedName("grade")
    var grade: String,
    @SerializedName("sr_number")
    var srNumber: String,
    @SerializedName("status")
    var status: String,
    @SerializedName("status_id")
    var statusId: Long,
    @SerializedName("created_at")
    var createdAt: Date,
    @SerializedName("updated_at")
    var updatedAt: Date,
    @SerializedName("distribution_center_id")
    var distributionCenterId: Long,
    @SerializedName("vendor_code")
    var vendorCode: String,
    @SerializedName("details")
    var details: Details
) {
    fun toTestInventory(): TestInventory {
        return TestInventory().also {
            it.id = inventoryId
            it.sku = skuCode
            it.desc = itemDescription
            it.categoryId = clientCategoryId
            it.brand = brand
            it.tagNumber = tagNumber
            it.serialNumber = serialNumber
            it.serialNumber2 = serialNumber2
            it.gradingType = status
            it.categoryL1 = details.categoryL1
            it.categoryL2 = details.categoryL2
            it.category = details.categoryL2
            it.status = status
            it.distributionCenterId = distributionCenterId
        }
    }
}