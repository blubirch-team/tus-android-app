package com.blubirch.tus.data.repository;

import com.blubirch.tus.testing.data.models.TestSelection;
import com.blubirch.tus.data.api.dto.regrading.ReGradeInventoryDTO;
import com.blubirch.tus.data.api.dto.regrading.ReGradingRespDTO;
import com.blubirch.tus.data.api.interfaces.ReGradingAPI;
import com.blubirch.core.data.api.dto.BaseDTO;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Single;

public class ReGradingRepoImpl implements ReGradingRepo {

    private ReGradingAPI reGradingAPI;

    @Inject
    public ReGradingRepoImpl(ReGradingAPI reGradingAPI) {
        this.reGradingAPI = reGradingAPI;
    }

    @Override
    public Single<ReGradingRespDTO> loadRegradingItems() {
        return reGradingAPI.getList();
    }

    @Override
    public Single<BaseDTO> saveGradeAndDispositionResult(
            Long inventoryId, String toatNumber, HashMap<String, ArrayList<TestSelection>> testSelectionListMap,
            String grade, HashMap<String, String> gradingResult, String disposition, String workFlow,String policy) {

        ReGradeInventoryDTO reGradeInventoryDTO = new ReGradeInventoryDTO(
                inventoryId, toatNumber, testSelectionListMap, gradingResult, grade, disposition, workFlow,policy);

        return reGradingAPI.storeInventoryGrade(reGradeInventoryDTO);
    }
}
