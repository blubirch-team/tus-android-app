package com.blubirch.tus.data.api.dto.dispatch

import com.google.gson.annotations.SerializedName

class DispatchConsignmentReqDTO(@SerializedName("consignment_id") var consignmentId: Long)

