package com.blubirch.tus.data.repository;

import com.blubirch.tus.data.api.dto.ItemInfoResDTO;

import io.reactivex.Single;

public interface ItemInfoRepo {

    Single<ItemInfoResDTO> getInfo(String tagNumber);

}
