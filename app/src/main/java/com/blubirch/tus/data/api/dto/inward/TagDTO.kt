package com.blubirch.tus.data.api.dto.inward

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class TagDTO(
    @SerializedName("tag_number")
    var tagNumber: String
) : Parcelable
