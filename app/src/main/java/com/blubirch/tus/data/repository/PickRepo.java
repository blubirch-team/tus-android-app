package com.blubirch.tus.data.repository;


import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.tus.data.api.dto.pick.UpdateToatResDTO;
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;

import io.reactivex.Single;

public interface PickRepo {

    Single<WarehouseOrdersResDTO> getList();

    Single<UpdateToatResDTO> updateToat(String id, String toatNumber);

    Single<BaseDTO> confirmPick(String id);

}
