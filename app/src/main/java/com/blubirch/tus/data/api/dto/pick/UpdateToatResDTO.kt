package com.blubirch.tus.data.api.dto.pick

import com.blubirch.tus.data.model.WarehouseOrderItems
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateToatResDTO(
    @Expose
    @SerializedName("warehouse_order_item")
    var warehouseOrderItem: WarehouseOrderItems
)