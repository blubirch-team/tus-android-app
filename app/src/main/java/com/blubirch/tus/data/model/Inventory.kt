package com.blubirch.tus.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
class Inventory : Parcelable {

    var id: Long = 0

    @SerializedName("sku_code")
    var skuCode: String = ""

    @SerializedName("item_description")
    var itemDesc: String = ""

    @SerializedName("tag_number")
    var tagNumber: String = ""

    @SerializedName("aisle_location")
    var location: String = ""

    @SerializedName("toat_number")
    var toatNumber: String = ""

    @SerializedName("created_at")
    var createdAt: Date = Date()

    @SerializedName("updated_at")
    var updatedAt: Date = Date()
}