package com.blubirch.tus.data.api.dto.inward


import com.blubirch.tus.data.model.inward.SKU
import com.blubirch.core.data.api.dto.BaseDTO
import com.google.gson.annotations.SerializedName

class SKuResponseDTO(
        @SerializedName("client_sku_masters")
    var SKUS: List<SKU>, message: String, status: Int, error: String
) : BaseDTO(message, status, error)