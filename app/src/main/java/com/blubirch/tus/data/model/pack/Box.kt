package com.blubirch.tus.data.model.pack

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.Objects

data class Box (
    @Expose
    @SerializedName("id")
    var id : Long,
    @Expose
    @SerializedName("distribution_center_id")
    val distributionCenterId : Long,
    @Expose
    @SerializedName("user_id")
    val userId : Long,
    @Expose
    @SerializedName("box_number")
    val boxNumber : String,
    @Expose
    @SerializedName("deleted_at")
    val deletedAt : String,
    @Expose
    @SerializedName("created_at")
    val createdAt : String,
    @Expose
    @SerializedName("updated_at")
    val updatedAt : String
){
    override fun equals(that: Any?): Boolean {
        if (this === that) return true
        if (that == null || javaClass != that.javaClass) return false
        val that: Box = that as Box
        return id == that.id && boxNumber == that.boxNumber
    }

    override fun hashCode(): Int {
        return Objects.hash(id, boxNumber)
    }
}