package com.blubirch.tus.data.api.dto.stowing

class UpdateLocationRequestDTO(var id: Long, var location: String)