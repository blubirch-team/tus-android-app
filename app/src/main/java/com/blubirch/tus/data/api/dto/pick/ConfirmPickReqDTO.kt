package com.blubirch.tus.data.api.dto.pick

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ConfirmPickReqDTO(
    @Expose
    @SerializedName("id")
    var id: String
)