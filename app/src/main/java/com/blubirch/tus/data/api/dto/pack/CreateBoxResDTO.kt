package com.blubirch.tus.data.api.dto.pack

import com.blubirch.tus.data.model.pack.Box
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CreateBoxResDTO (
    @Expose
    @SerializedName("box")
    val box : Box
)