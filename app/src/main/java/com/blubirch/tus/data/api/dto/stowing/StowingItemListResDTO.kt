package com.blubirch.tus.data.api.dto.stowing


import com.blubirch.tus.data.model.stowing.StowingItem
import com.blubirch.core.data.api.dto.BaseDTO
import com.google.gson.annotations.SerializedName

data class StowingItemListResDTO(
    @SerializedName("result")
    val result: List<StowingItem>
) : BaseDTO()