package com.blubirch.tus.data.repository;

import com.blubirch.tus.testing.data.models.TestSelection;
import com.blubirch.tus.data.api.dto.inward.GatePassDTO;
import com.blubirch.tus.data.api.dto.inward.InventoryDataDTO;
import com.blubirch.tus.data.api.dto.inward.ProceedGRNRequestDTO;
import com.blubirch.tus.data.api.dto.inward.ProceedGRNResponseDTO;
import com.blubirch.tus.data.api.dto.inward.SKuResponseDTO;
import com.blubirch.tus.data.api.dto.inward.SaveTestedReturnDTO;
import com.blubirch.tus.data.api.dto.inward.TagDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateGRNRequestDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateGRNResponseDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateTagToatRequestDTO;
import com.blubirch.tus.data.api.dto.inward.UpdateTagToatResponseDTO;
import com.blubirch.tus.data.api.dto.inward.VerifySerialDTO;
import com.blubirch.tus.data.api.interfaces.GatePassAPI;
import com.blubirch.tus.data.model.inward.GatePassInventory;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class GatePassRepoImpl implements GatePassRepo {

    private GatePassAPI gatePassAPI;

    @Inject
    public GatePassRepoImpl(GatePassAPI gatePassAPI) {
        this.gatePassAPI = gatePassAPI;
    }

    public Single<GatePassDTO> loadData() {
        return gatePassAPI.getList(null);
    }

    @Override
    public Single<GatePassDTO> searchGatePass(String gatePassNumber) {
        return gatePassAPI.searchSTN(gatePassNumber);
    }

    @Override
    public Single<SKuResponseDTO> searchSKU(String sku) {
        return gatePassAPI.searchSKU(sku);
    }

    @Override
    public Single<BaseDTO> verifySerialNumber(String serialNumber, String serialNumber2) {
        if (serialNumber == null)
            serialNumber = "";
        if (serialNumber2 == null)
            serialNumber2 = "";
        return gatePassAPI.verifySerialNumber(new VerifySerialDTO(serialNumber, serialNumber2));
    }

    @Override
    public Single<TagDTO> generateTag() {
        return gatePassAPI.generateTag();
    }

    @Override
    public Single<UpdateTagToatResponseDTO> updateTagToat(long inventoryId, String tagId, String toatNumber) {
        UpdateTagToatRequestDTO updateTagToatRequestDTO = new UpdateTagToatRequestDTO(inventoryId, tagId, toatNumber);
        return gatePassAPI.updateTagToat(updateTagToatRequestDTO);
    }

    @Override
    public Single<ProceedGRNResponseDTO> proceedGRN(String stnNumber) {
        ProceedGRNRequestDTO proceedGRNRequestDTO = new ProceedGRNRequestDTO(stnNumber);
        return gatePassAPI.proceedGRN(proceedGRNRequestDTO);
    }

    @Override
    public Single<UpdateGRNResponseDTO> updateGRN(String stnNumber, String grnNumber) {
        return gatePassAPI.updateGRN(new UpdateGRNRequestDTO(stnNumber, grnNumber));
    }

    @Override
    public Single<SaveTestedReturnDTO> saveTestedReturn(
            String gatePassNumber, GatePassInventory gatePassInventory, String returnReason,
            ArrayList<AttachedDoc> documents, String serialNumber, String serialNumber2,
            String srNumber, String tagNumber, String toatNumber, String manualDisposition,
            HashMap<String, ArrayList<TestSelection>> testSelectionListMap, String grade,
            HashMap<String, String> gradingResult, String disposition, String workFlow,String policy) {

        InventoryDataDTO inventoryDataDTO = new InventoryDataDTO(
                gatePassNumber, gatePassInventory.getId(), gatePassInventory.getSkuCode(),
                serialNumber, serialNumber2, srNumber, tagNumber, toatNumber, manualDisposition,
                testSelectionListMap, gradingResult, grade, returnReason, disposition, workFlow,policy);

        Map<String, RequestBody> bodyHashMap = new HashMap<>();
        bodyHashMap.put("inventory_data", RequestBody.create(MultipartBody.FORM, new Gson().toJson(inventoryDataDTO)));
        List<MultipartBody.Part> documentsPart = new ArrayList<>();
        int index = 0;
        for (AttachedDoc attachedDoc : documents) {
            bodyHashMap.put("documents[" + index + "][code]", RequestBody.create(MultipartBody.FORM, attachedDoc.getDocKey()));
            bodyHashMap.put("documents[" + index + "][reference_number]", RequestBody.create(MultipartBody.FORM, attachedDoc.getReferenceNo()));

            File file = new File(attachedDoc.getFileAbsolutePath());
            // Parsing any Media type file
            RequestBody fileRequestBody = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("documents[" + index + "][document]", file.getName(), fileRequestBody);

            documentsPart.add(fileToUpload);
            index++;
        }
        return gatePassAPI.saveTestedReturn(bodyHashMap, documentsPart);
    }


}
