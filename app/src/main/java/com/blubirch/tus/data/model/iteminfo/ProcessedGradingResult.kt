package com.blubirch.tus.data.model.iteminfo


import com.google.gson.annotations.SerializedName

data class ProcessedGradingResult(
    @SerializedName("Accessories")
    val accessories: String,
    @SerializedName("Functional")
    val functional: String,
    @SerializedName("Item Condition")
    val itemCondition: String,
    @SerializedName("Packaging")
    val packaging: String,
    @SerializedName("Physical")
    val physical: String
)