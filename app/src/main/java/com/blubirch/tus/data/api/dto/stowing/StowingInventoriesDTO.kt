package com.blubirch.tus.data.api.dto.stowing

import com.blubirch.tus.data.model.Inventory
import com.google.gson.annotations.SerializedName

class StowingInventoriesDTO(@SerializedName("inventories") var inventories: List<Inventory>)