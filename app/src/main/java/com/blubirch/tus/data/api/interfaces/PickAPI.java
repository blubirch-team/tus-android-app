package com.blubirch.tus.data.api.interfaces;


import com.blubirch.tus.constants.EndPoints;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.tus.data.api.dto.pick.ConfirmPickReqDTO;
import com.blubirch.tus.data.api.dto.pick.UpdateToatReqDTO;
import com.blubirch.tus.data.api.dto.pick.UpdateToatResDTO;
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PickAPI {

    @GET(EndPoints.GET_PICK_LIST)
    Single<WarehouseOrdersResDTO> getList();

    @POST(EndPoints.UPDATE_TOAT)
    Single<UpdateToatResDTO> updateToat(@Body UpdateToatReqDTO updateToatReqDTO);

    @POST(EndPoints.CONFIRM_PICK)
    Single<BaseDTO> confirmPick(@Body ConfirmPickReqDTO confirmPickReqDTO);

}
