package com.blubirch.tus.data.api.interfaces;


import com.blubirch.tus.constants.EndPoints;
import com.blubirch.tus.data.api.dto.ItemInfoResDTO;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ItemInfoAPI {

    @GET(EndPoints.ITEM_INFO)
    Single<ItemInfoResDTO> getInfo(@Query("tag_number") String tagNumber);

}
