package com.blubirch.tus.data.model.iteminfo


import com.google.gson.annotations.SerializedName
import java.util.*

data class ItemInfo(
        @SerializedName("aisle_location")
    val aisleLocation: Any?,
        @SerializedName("client_category_id")
    val clientCategoryId: Int,
        @SerializedName("client_id")
    val clientId: Int,
        @SerializedName("client_tag_number")
    val clientTagNumber: String,
        @SerializedName("created_at")
    val createdAt: Date,
        @SerializedName("deleted_at")
    val deletedAt: Date?,
        @SerializedName("details")
    val details: ItemDetails,
        @SerializedName("disposition")
    val disposition: String,
        @SerializedName("disptach_date")
    val disptachDate: Date?,
        @SerializedName("distribution_center_id")
    val distributionCenterId: Int,
        @SerializedName("gate_pass_id")
    val gatePassId: Int,
        @SerializedName("gate_pass_inventory_id")
    val gatePassInventoryId: String,
        @SerializedName("grade")
    val grade: String,
        @SerializedName("id")
    val id: Int,
        @SerializedName("insurance_created_at")
    val insuranceCreatedAt: Date?,
        @SerializedName("insurance_id")
    val insuranceId: String?,
        @SerializedName("insurance_status")
    val insuranceStatus: Any?,
        @SerializedName("insurance_updated_at")
    val insuranceUpdatedAt: Date?,
        @SerializedName("inventory_id")
    val inventoryId: Int,
        @SerializedName("item_description")
    val itemDescription: String,
        @SerializedName("item_inward_date")
    val itemInwardDate: Date?,
        @SerializedName("item_price")
    val itemPrice: Double,
        @SerializedName("liquidation_created_at")
    val liquidationCreatedAt: Date?,
        @SerializedName("liquidation_id")
    val liquidationId: String?,
        @SerializedName("liquidation_status")
    val liquidationStatus: String?,
        @SerializedName("liquidation_updated_at")
    val liquidationUpdatedAt: Date?,
        @SerializedName("markdown_created_at")
    val markdownCreatedAt: Date?,
        @SerializedName("markdown_id")
    val markdownId: String?,
        @SerializedName("markdown_status")
    val markdownStatus: String?,
        @SerializedName("markdown_updated_at")
    val markdownUpdatedAt: Date?,
        @SerializedName("quantity")
    val quantity: Int,
        @SerializedName("redeploy_created_at")
    val redeployCreatedAt: Date?,
        @SerializedName("redeploy_id")
    val redeployId: String?,
        @SerializedName("redeploy_status")
    val redeployStatus: String?,
        @SerializedName("redeploy_updated_at")
    val redeployUpdatedAt: Date?,
        @SerializedName("repair_created_at")
    val repairCreatedAt: Date?,
        @SerializedName("repair_id")
    val repairId: String?,
        @SerializedName("repair_status")
    val repairStatus: String?,
        @SerializedName("repair_updated_at")
    val repairUpdatedAt: Date?,
        @SerializedName("replacement_created_at")
    val replacementCreatedAt: Date?,
        @SerializedName("replacement_id")
    val replacementId: String?,
        @SerializedName("replacement_status")
    val replacementStatus: String?,
        @SerializedName("replacement_updated_at")
    val replacementUpdatedAt: Date?,
        @SerializedName("return_reason")
    val returnReason: String,
        @SerializedName("serial_number")
    val serialNumber: String,
        @SerializedName("serial_number_2")
    val serialNumber2: String,
        @SerializedName("sku_code")
    val skuCode: String,
        @SerializedName("sr_number")
    val srNumber: String,
        @SerializedName("status")
    val status: String,
        @SerializedName("status_id")
    val statusId: Int,
        @SerializedName("tag_number")
    val tagNumber: String,
        @SerializedName("toat_number")
    val toatNumber: String,
        @SerializedName("updated_at")
    val updatedAt: Date,
        @SerializedName("user_id")
    val userId: Int,
        @SerializedName("vendor_return_created_at")
    val vendorReturnCreatedAt: Date?,
        @SerializedName("vendor_return_id")
    val vendorReturnId: String?,
        @SerializedName("vendor_return_status")
    val vendorReturnStatus: Any?,
        @SerializedName("vendor_return_updated_at")
    val vendorReturnUpdatedAt: Date?
)