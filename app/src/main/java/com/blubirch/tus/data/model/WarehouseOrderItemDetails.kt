package com.blubirch.tus.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WarehouseOrderItemDetails(

		@field:SerializedName("category_l3")
	val categoryL3: String? = null,

		@field:SerializedName("stock_transfer_order_name")
	val stockTransferOrderName: String? = null,

		@field:SerializedName("inward_user_id")
	val inwardUserId: Int? = null,

		@field:SerializedName("grn_received_time")
	val grnReceivedTime: String? = null,

		@field:SerializedName("inwarding_disposition")
	val inwardingDisposition: String? = null,

		@field:SerializedName("category_l1")
	val categoryL1: String? = null,

		@field:SerializedName("category_l2")
	val categoryL2: String? = null,

		@field:SerializedName("manual_disposition")
	val manualDisposition: String? = null,

		@field:SerializedName("stn_number")
	val stnNumber: String? = null,

		@field:SerializedName("ean")
	val ean: String? = null,

		@field:SerializedName("grn_received_user_name")
	val grnReceivedUserName: String? = null,

		@field:SerializedName("inward_grading_time")
	val inwardGradingTime: String? = null,

		@field:SerializedName("grn_submitted_user_id")
	val grnSubmittedUserId: Int? = null,

		@field:SerializedName("dispatch_date")
	val dispatchDate: String? = null,

		@field:SerializedName("brand")
	val brand: String? = null,

		@field:SerializedName("stock_transfer_order_amount")
	val stockTransferOrderAmount: String? = null,

		@field:SerializedName("inward_user_name")
	val inwardUserName: String? = null,

		@field:SerializedName("issue_type")
	val issueType: String? = null,

		@field:SerializedName("grn_submitted_date")
	val grnSubmittedDate: String? = null,

		@field:SerializedName("stock_transfer_order_number")
	val stockTransferOrderNumber: String? = null,

		@field:SerializedName("processed_grading_result")
	val processedGradingResult: ProcessedGradingResult? = null,

		@field:SerializedName("destination_code")
	val destinationCode: String? = null,

		@field:SerializedName("grn_number")
	val grnNumber: String? = null,

		@field:SerializedName("grn_received_user_id")
	val grnReceivedUserId: Int? = null,

		@field:SerializedName("client_sku_master_id")
	val clientSkuMasterId: String? = null,

		@field:SerializedName("grn_submitted_user_name")
	val grnSubmittedUserName: String? = null,

		@field:SerializedName("own_label")
	val ownLabel: Boolean? = null,

		@field:SerializedName("source_code")
	val sourceCode: String? = null,

		@field:SerializedName("work_flow_name")
	val workFlowName: String? = null
) : Parcelable

@Parcelize
data class ProcessedGradingResult(

	@field:SerializedName("Functional")
	val functional: String? = null,

	@field:SerializedName("Physical")
	val physical: String? = null,

	@field:SerializedName("Accessories")
	val accessories: String? = null,

	@field:SerializedName("Item Condition")
	val itemCondition: String? = null,

	@field:SerializedName("Packaging")
	val packaging: String? = null,

	@field:SerializedName("Reason")
	val reason: String? = null
) : Parcelable
