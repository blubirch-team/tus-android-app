package com.blubirch.tus.data.repository;


import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;
import com.blubirch.tus.data.api.dto.pack.AssignBoxReqDTO;
import com.blubirch.tus.data.api.dto.pack.AssignBoxResDTO;
import com.blubirch.tus.data.api.dto.pack.ConfirmDispatchReqDTO;
import com.blubirch.tus.data.api.dto.pack.CreateBoxReqDTO;
import com.blubirch.tus.data.api.dto.pack.CreateBoxResDTO;
import com.blubirch.tus.data.api.dto.pack.DeleteBoxReqDTO;
import com.blubirch.tus.data.api.dto.pack.RemoveFromBoxReqDTO;
import com.blubirch.tus.data.api.interfaces.PackAPI;
import com.blubirch.core.data.api.dto.BaseDTO;

import javax.inject.Inject;

import io.reactivex.Single;

public class PackRepoImpl implements PackRepo {


    private PackAPI packAPI;

    @Inject
    public PackRepoImpl(PackAPI packAPI) {
        this.packAPI = packAPI;
    }


    @Override
    public Single<WarehouseOrdersResDTO> getList(){
        return packAPI.getList();
    }

    @Override
    public Single<CreateBoxResDTO> createBox(Long id){
        return packAPI.createBox(new CreateBoxReqDTO(String.valueOf(id)));
    }

    @Override
    public Single<BaseDTO> deleteBox(Long id) {
        return packAPI.deleteBox(new DeleteBoxReqDTO(String.valueOf(id)));
    }

    @Override
    public Single<AssignBoxResDTO> assignBox(Long warehouseOrderItemId, String boxNumber){
        return packAPI.assignBox(new AssignBoxReqDTO(String.valueOf(warehouseOrderItemId), boxNumber));
    }

    @Override
    public Single<BaseDTO> confirmDispatch(Long id) {
        return packAPI.confirmDispatch(new ConfirmDispatchReqDTO(String.valueOf(id)));
    }

    @Override
    public Single<BaseDTO> removeFromBox(Long id) {
        return packAPI.removeFromBox(new RemoveFromBoxReqDTO(String.valueOf(id)));
    }
}
