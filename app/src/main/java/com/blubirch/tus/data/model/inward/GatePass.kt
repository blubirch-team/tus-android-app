package com.blubirch.tus.data.model.inward


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class GatePass(
        var id: Long = 0L,

        @SerializedName("created_at")
    var createdAt: Date? = null,

        @SerializedName("updated_at")
    var updatedAt: Date? = null,

        @SerializedName("client_gatepass_number")
    var clientGatePassNumber: String = "",

        @SerializedName("source_address")
    var sourceAddress: String = "",

        @SerializedName("source_city")
    var sourceCity: String = "",

        @SerializedName("source_code")
    var sourceCode: String = "",

        @SerializedName("source_country")
    var sourceCountry: String = "",

        @SerializedName("source_pincode")
    var sourcePincode: String = "",

        @SerializedName("source_state")
    var sourceState: String = "",


        @SerializedName("destination_address")
    var destinationAddress: String = "",

        @SerializedName("destination_city")
    var destinationCity: String = "",

        @SerializedName("destination_code")
    var destinationCode: String = "",

        @SerializedName("destination_pincode")
    var destinationPinCode: String = "",

        @SerializedName("destination_state")
    var destinationState: String = "",

        @SerializedName("dispatch_date")
    var dispatchDate: Date = Date(),

        @SerializedName("sr_number")
    var srNumber: String = "",

        @SerializedName("status")
    var status: String = "",

        @SerializedName("gate_pass_inventories")
    var gatePassInventories: List<GatePassInventory> = arrayListOf(),

        @SerializedName("total_inventories")
    var totalInventories: Int = 0,

        @SerializedName("details")
    var details: Details? = null
) : Parcelable

@Parcelize
data class Details(
    @SerializedName("grn_submitted_date")
    var grnSubmittedDate: Date? = null,

    @SerializedName("grn_number")
    var grnNumber: String = ""
) : Parcelable