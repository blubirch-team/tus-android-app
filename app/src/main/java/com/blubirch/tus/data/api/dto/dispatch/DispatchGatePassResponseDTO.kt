package com.blubirch.tus.data.api.dto.dispatch

import com.google.gson.annotations.SerializedName

class DispatchGatePassResponseDTO(
    @SerializedName("created_at")
    var createdAt: String,
    @SerializedName("deleted_at")
    var deletedAt: Any,
    @SerializedName("driver_contact_number")
    var driverContactNumber: String,
    @SerializedName("driver_name")
    var driverName: String,
    @SerializedName("id")
    var id: Long,
    @SerializedName("consignment_id")
    var consignmentId: Long,
    @SerializedName("transporter")
    var transporter: String,
    @SerializedName("truck_receipt_number")
    var truckReceiptNumber: String,
    @SerializedName("updated_at")
    var updatedAt: String,
    @SerializedName("vehicle_number")
    var vehicleNumber: String
)