package com.blubirch.tus.data.model.iteminfo


import com.google.gson.annotations.SerializedName

data class ItemDetails(
        @SerializedName("brand")
    val brand: String,
        @SerializedName("category_l1")
    val categoryL1: String,
        @SerializedName("category_l2")
    val categoryL2: String,
        @SerializedName("client_sku_master_id")
    val clientSkuMasterId: String,
        @SerializedName("destination_code")
    val destinationCode: String,
        @SerializedName("dispatch_date")
    val dispatchDate: String?,
        @SerializedName("grn_submitted_date")
    val grnSubmittedDate: String,
        @SerializedName("grn_submitted_user_id")
    val grnSubmittedUserId: Int,
        @SerializedName("grn_submitted_user_name")
    val grnSubmittedUserName: String,
        @SerializedName("inward_grading_time")
    val inwardGradingTime: String,
        @SerializedName("inward_user_id")
    val inwardUserId: Int,
        @SerializedName("inward_user_name")
    val inwardUserName: String,
        @SerializedName("inwarding_disposition")
    val inwardingDisposition: String,
        @SerializedName("manual_disposition")
    val manualDisposition: String,
        @SerializedName("processed_grading_result")
    val processedGradingResult: ProcessedGradingResult,
        @SerializedName("source_code")
    val sourceCode: String,
        @SerializedName("stn_number")
    val stnNumber: String
)