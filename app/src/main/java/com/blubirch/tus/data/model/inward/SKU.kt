package com.blubirch.tus.data.model.inward


import com.google.gson.annotations.SerializedName

data class SKU(
    @SerializedName("client_category_id")
    var clientCategoryId: Long,
    @SerializedName("code")
    var code: String,
    @SerializedName("sku_description")
    var desc: String? = "",
    @SerializedName("id")
    var id: Int,
    var brand: String = "",
    @SerializedName("own_label")
    var ownLabel: Boolean = false
)