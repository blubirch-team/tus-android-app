package com.blubirch.tus.data.model.pack

import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.core.data.api.dto.BaseDTO

class BoxListItem(val box: Box) : BaseDTO() {
    val warehouseOrderItems = ArrayList<WarehouseOrderItems>()
}