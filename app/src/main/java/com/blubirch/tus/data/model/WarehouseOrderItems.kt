package com.blubirch.tus.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WarehouseOrderItems(
        @Expose
    @SerializedName("id")
    var id: Long,
        @Expose
    @SerializedName("warehouse_order_id")
    var warehouseOrderId: Long,
        @Expose
    @SerializedName("inventory_id")
    var inventoryId: Long,
        @Expose
    @SerializedName("client_category_id")
    var clientCategoryId: Long,
        @Expose
    @SerializedName("client_category_name")
    var clientCategoryName: String,
        @Expose
    @SerializedName("sku_master_code")
    var skuMasterCode: String,
        @Expose
    @SerializedName("item_description")
    var itemDescription: String,
        @Expose
    @SerializedName("tag_number")
    var tagNumber: String,
        @Expose
    @SerializedName("details")
    val details: WarehouseOrderItemDetails? = null,
        @Expose
    @SerializedName("serial_number")
    var serialNumber: String,
        @Expose
    @SerializedName("aisle_location")
    var aisleLocation: String,
        @Expose
    @SerializedName("quantity")
    var quantity: Long,
        @Expose
    @SerializedName("toat_number")
    var toatNumber: String?,
        @Expose
    @SerializedName("status_id")
    var statusId: Long,
        @Expose
    @SerializedName("packaging_box_number")
    var packagingBoxNumber: String?,
        @Expose
    @SerializedName("packaging_box_id")
    var packagingBoxId: Long?,
        @Expose
    @SerializedName("status")
    var status: String,
        @Expose
    @SerializedName("created_at")
    var createdAt: String,
        @Expose
    @SerializedName("updated_at")
    var updatedAt: String,
        @Expose
    @SerializedName("deleted_at")
    var deletedAt: String?
){
    fun copy(that: WarehouseOrderItems){
        this.id = that.id
        this.warehouseOrderId = that.warehouseOrderId
        this.inventoryId = that.inventoryId
        this.clientCategoryId = that.clientCategoryId
        this.clientCategoryName = that.clientCategoryName
        this.skuMasterCode = that.skuMasterCode
        this.itemDescription = that.itemDescription
        this.tagNumber = that.tagNumber
        this.serialNumber = that.serialNumber
        this.aisleLocation = that.aisleLocation
        this.quantity = that.quantity
        this.toatNumber = that.toatNumber
        this.statusId = that.statusId
        this.packagingBoxNumber = that.packagingBoxNumber
        this.status = that.status
        this.createdAt = that.createdAt
        this.updatedAt = that.updatedAt
        this.deletedAt = that.deletedAt
    }
}