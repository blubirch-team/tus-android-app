package com.blubirch.tus.data.repository;


import com.blubirch.tus.data.model.WarehouseOrders;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;
import com.blubirch.tus.data.api.dto.dispatch.DispatchGatePassResponseDTO;
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc;

import java.util.ArrayList;

import io.reactivex.Single;

public interface DispatchRepo {

    Single<WarehouseOrdersResDTO> getList();

    Single<DispatchGatePassResponseDTO> dispatchGatePass(
            ArrayList<WarehouseOrders> selectedWarehouseOrders, String deliveryRefNo, String invoiceNumber, Long consignmentId, String transporter, String lorryReceiptNumber,
            String vehicleNumber, String driverContactNumber, String driverName, ArrayList<AttachedDoc> documents);

    Single<BaseDTO> dispatchConsignment(long consignmentId, ArrayList<AttachedDoc> documents);

}
