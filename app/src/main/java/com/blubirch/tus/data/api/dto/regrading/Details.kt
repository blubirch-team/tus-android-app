package com.blubirch.tus.data.api.dto.regrading


import com.google.gson.annotations.SerializedName

data class Details(
    @SerializedName("brand")
    var brand: String,
    @SerializedName("category_l1")
    var categoryL1: String,
    @SerializedName("category_l2")
    var categoryL2: String,
    @SerializedName("client_category_id")
    var clientCategoryId: Long,
    @SerializedName("destination_code")
    var destinationCode: String,
    @SerializedName("dispatch_date")
    var dispatchDate: String,
    @SerializedName("disposition")
    var disposition: String,
    @SerializedName("grn_number")
    var grnNumber: String,
    @SerializedName("grn_received_time")
    var grnReceivedTime: String,
    @SerializedName("grn_received_user_id")
    var grnReceivedUserId: Int,
    @SerializedName("grn_received_user_name")
    var grnReceivedUserName: String,
    @SerializedName("grn_submitted_date")
    var grnSubmittedDate: String,
    @SerializedName("grn_submitted_user_id")
    var grnSubmittedUserId: Int,
    @SerializedName("grn_submitted_user_name")
    var grnSubmittedUserName: String,
    @SerializedName("inward_grading_time")
    var inwardGradingTime: String,
    @SerializedName("inward_user_id")
    var inwardUserId: Int,
    @SerializedName("inward_user_name")
    var inwardUserName: String,
    @SerializedName("pending_approval_remark")
    var pendingApprovalRemark: String,
    @SerializedName("pending_initiation_remark")
    var pendingInitiationRemark: String,
    @SerializedName("pending_quotation_remark")
    var pendingQuotationRemark: String,
    @SerializedName("return_reason")
    var returnReason: String,
    @SerializedName("sku_code")
    var skuCode: String,
    @SerializedName("source_code")
    var sourceCode: String,
    @SerializedName("stn_number")
    var stnNumber: String,
    @SerializedName("work_flow_name")
    var workFlowName: String
)