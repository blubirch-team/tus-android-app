package com.blubirch.tus.data.api.interfaces;

import com.blubirch.tus.constants.EndPoints;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;
import com.blubirch.tus.data.api.dto.dispatch.DispatchGatePassResponseDTO;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface DispatchAPI {

    @GET(EndPoints.DISPATCH_GET_LIST)
    Single<WarehouseOrdersResDTO> getList();

    @Multipart
    @POST(EndPoints.INITIATE_DISPATCH)
    Single<DispatchGatePassResponseDTO> initiateDispatch(@PartMap Map<String, RequestBody> body, @Part List<MultipartBody.Part> documentsPart);

    @Multipart
    @POST(EndPoints.COMPLETE_DISPATCH)
    Single<BaseDTO> completeDispatch(@PartMap Map<String, RequestBody> body, @Part List<MultipartBody.Part> documentsPart);

}
