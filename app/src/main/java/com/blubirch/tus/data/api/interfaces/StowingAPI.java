package com.blubirch.tus.data.api.interfaces;


import com.blubirch.tus.constants.EndPoints;
import com.blubirch.tus.data.api.dto.stowing.CompleteStowingRequestDTO;
import com.blubirch.tus.data.api.dto.stowing.StowingInventoriesDTO;
import com.blubirch.tus.data.api.dto.stowing.StowingItemListResDTO;
import com.blubirch.tus.data.api.dto.stowing.UpdateLocationRequestDTO;
import com.blubirch.core.data.api.dto.BaseDTO;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface StowingAPI {

    @GET(EndPoints.URL_STOWING_LIST)
    Single<StowingInventoriesDTO> getToatInventoryList(@Query(value = "toat_number") String toatNumber);

    @POST(EndPoints.SAVE_AISLE_LOCATION)
    Single<BaseDTO> updateAisleLocation(@Body UpdateLocationRequestDTO updateLocationRequestDTO);

    @POST(EndPoints.COMPLETE_STOWING)
    Single<BaseDTO> completeStowing(@Body CompleteStowingRequestDTO completeStowingRequestDTO);

    @GET(EndPoints.GET_STOWING_ITEMS)
    Single<StowingItemListResDTO> getStowingList();
}
