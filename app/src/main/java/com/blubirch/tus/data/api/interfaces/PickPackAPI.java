package com.blubirch.tus.data.api.interfaces;


import com.blubirch.tus.constants.EndPoints;
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;
import com.blubirch.tus.data.api.dto.pack.AssignBoxReqDTO;
import com.blubirch.tus.data.api.dto.pack.AssignBoxResDTO;
import com.blubirch.tus.data.api.dto.pack.ConfirmDispatchReqDTO;
import com.blubirch.tus.data.api.dto.pack.CreateBoxReqDTO;
import com.blubirch.tus.data.api.dto.pack.CreateBoxResDTO;
import com.blubirch.tus.data.api.dto.pack.DeleteBoxReqDTO;
import com.blubirch.tus.data.api.dto.pack.RemoveFromBoxReqDTO;
import com.blubirch.core.data.api.dto.BaseDTO;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PickPackAPI {

    @GET(EndPoints.GET_PICK_PACK_ORDER_LIST)
    Single<WarehouseOrdersResDTO> getList();

    @POST(EndPoints.PICK_PACK_CREATE_BOX)
    Single<CreateBoxResDTO> createBox(@Body CreateBoxReqDTO createBoxReqDTO);

    @POST(EndPoints.PICK_PACK_DELETE_BOX)
    Single<BaseDTO> deleteBox(@Body DeleteBoxReqDTO deleteBoxReqDTO);

    @POST(EndPoints.PICK_PACK_ASSIGN_BOX)
    Single<AssignBoxResDTO> assignBox(@Body AssignBoxReqDTO assignBoxReqDTO);

    @POST(EndPoints.PICK_PACK_REMOVE_FROM_BOX)
    Single<BaseDTO> removeFromBox(@Body RemoveFromBoxReqDTO removeFromBoxReqDTO);

    @POST(EndPoints.PICK_PACK_DISPATCH_CONFIRM)
    Single<BaseDTO> confirmDispatch(@Body ConfirmDispatchReqDTO confirmDispatchReqDTO);


}
