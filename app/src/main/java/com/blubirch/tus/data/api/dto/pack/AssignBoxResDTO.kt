package com.blubirch.tus.data.api.dto.pack

import com.blubirch.tus.data.model.GatePass
import com.blubirch.tus.data.model.WarehouseOrderItems
import com.blubirch.core.data.api.dto.BaseDTO
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AssignBoxResDTO(
        @Expose
    @SerializedName("warehouse_order_item")
    val warehouseOrderItem: WarehouseOrderItems,
        @Expose
    @SerializedName("gatepass")
    val gatePass: GatePass?,
        @Expose
    @SerializedName("packaging_box_id")
    val boxId: Long?
) : BaseDTO()