package com.blubirch.tus.data.api.dto.pack

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CreateBoxReqDTO (
    @Expose
    @SerializedName("id")
    val id : String
)