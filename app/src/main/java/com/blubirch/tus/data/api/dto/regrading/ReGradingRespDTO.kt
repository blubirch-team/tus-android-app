package com.blubirch.tus.data.api.dto.regrading

import com.google.gson.annotations.SerializedName

class ReGradingRespDTO(
        @SerializedName("liquidation")
    var liquidations: List<ReGradeInventory>,
        @SerializedName("repair")
    var repairs: List<ReGradeInventory>
)