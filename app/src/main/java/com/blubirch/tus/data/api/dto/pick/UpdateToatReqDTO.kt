package com.blubirch.tus.data.api.dto.pick

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateToatReqDTO(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("toat_number")
    var toatNumber: String
)