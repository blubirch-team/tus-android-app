package com.blubirch.tus.data.repository

import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO
import com.blubirch.tus.data.api.dto.pack.AssignBoxResDTO
import com.blubirch.tus.data.api.dto.pack.CreateBoxResDTO
import com.blubirch.core.data.api.dto.BaseDTO
import io.reactivex.Single

interface PickPackRepo{

    fun getList(): Single<WarehouseOrdersResDTO>

    fun createBox(id: Long): Single<CreateBoxResDTO>

    fun deleteBox(id: Long): Single<BaseDTO>

    fun assignBox(warehouseOrderItemId: Long, boxId: String): Single<AssignBoxResDTO>

    fun removeFromBox(id: Long): Single<BaseDTO>

    fun confirmDispatch(id: Long): Single<BaseDTO>


}