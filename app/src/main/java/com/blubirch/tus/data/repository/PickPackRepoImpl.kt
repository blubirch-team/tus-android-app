package com.blubirch.tus.data.repository

import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO
import com.blubirch.tus.data.api.dto.pack.*
import com.blubirch.tus.data.api.interfaces.PickPackAPI
import com.blubirch.core.data.api.dto.BaseDTO
import com.blubirch.tus.data.api.dto.pack.*
import io.reactivex.Single
import javax.inject.Inject

class PickPackRepoImpl @Inject constructor(private val pickPackAPI : PickPackAPI) : PickPackRepo {

    override fun getList(): Single<WarehouseOrdersResDTO> {
        return pickPackAPI.list
    }

    override fun createBox(id: Long): Single<CreateBoxResDTO> {
        return pickPackAPI.createBox(CreateBoxReqDTO(id.toString()))
    }

    override fun deleteBox(id: Long): Single<BaseDTO> {
        return pickPackAPI.deleteBox(DeleteBoxReqDTO(id.toString()))
    }

    override fun assignBox(warehouseOrderItemId: Long, boxNumber: String): Single<AssignBoxResDTO> {
        return pickPackAPI.assignBox(AssignBoxReqDTO(warehouseOrderItemId.toString(), boxNumber))
    }

    override fun confirmDispatch(id: Long): Single<BaseDTO> {
        return pickPackAPI.confirmDispatch(ConfirmDispatchReqDTO(id.toString()))
    }

    override fun removeFromBox(id: Long): Single<BaseDTO> {
        return pickPackAPI.removeFromBox(RemoveFromBoxReqDTO(id.toString()))
    }

}