package com.blubirch.tus.data.repository;


import com.blubirch.tus.data.model.WarehouseOrders;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;
import com.blubirch.tus.data.api.dto.dispatch.DispatchConsignmentReqDTO;
import com.blubirch.tus.data.api.dto.dispatch.DispatchGatePassResponseDTO;
import com.blubirch.tus.data.api.dto.dispatch.DispatchGatePassReqDTO;
import com.blubirch.tus.data.api.interfaces.DispatchAPI;
import com.blubirch.core.presentation.customview.attachdoc.AttachedDoc;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DispatchRepoImpl implements DispatchRepo {


    private DispatchAPI dispatchAPI;

    @Inject
    public DispatchRepoImpl(DispatchAPI dispatchAPI) {
        this.dispatchAPI = dispatchAPI;
    }

    @Override
    public Single<WarehouseOrdersResDTO> getList() {
        return dispatchAPI.getList();
    }

    @Override
    public Single<DispatchGatePassResponseDTO> dispatchGatePass(
            ArrayList<WarehouseOrders> selectedWarehouseOrders, String deliveryRefNo,
            String invoiceNumber, Long consignmentId, String transporter, String lorryReceiptNumber,
            String vehicleNumber, String driverContactNumber, String driverName, ArrayList<AttachedDoc> documents) {

        ArrayList<Long> gatePassIds = new ArrayList<>();
        for (WarehouseOrders selectedWarehouseOrder : selectedWarehouseOrders) {
            gatePassIds.add(selectedWarehouseOrder.getId());
        }

        DispatchGatePassReqDTO dispatchGatePassReqDTO = new DispatchGatePassReqDTO(gatePassIds, deliveryRefNo, invoiceNumber, consignmentId, transporter,
                lorryReceiptNumber, vehicleNumber, driverContactNumber, driverName);
        Map<String, RequestBody> bodyHashMap = new HashMap<>();
        bodyHashMap.put("dispatch_initiate_data", RequestBody.create(MultipartBody.FORM, new Gson().toJson(dispatchGatePassReqDTO)));
        List<MultipartBody.Part> documentsPart = getDocumentPart(bodyHashMap, documents);
        return dispatchAPI.initiateDispatch(bodyHashMap, documentsPart);
    }



    @Override
    public Single<BaseDTO> dispatchConsignment(long consignmentId, ArrayList<AttachedDoc> documents) {
        DispatchConsignmentReqDTO dispatchConsignmentReqDTO = new DispatchConsignmentReqDTO(consignmentId);
        Map<String, RequestBody> bodyHashMap = new HashMap<>();
        bodyHashMap.put("dispatch_complete_data", RequestBody.create(MultipartBody.FORM, new Gson().toJson(dispatchConsignmentReqDTO)));
        List<MultipartBody.Part> documentsPart = getDocumentPart(bodyHashMap, documents);
        return dispatchAPI.completeDispatch(bodyHashMap, documentsPart);
    }


    private List<MultipartBody.Part> getDocumentPart(Map<String, RequestBody> bodyHashMap, ArrayList<AttachedDoc> documents) {
        List<MultipartBody.Part> documentsPart = new ArrayList<>();
        int index = 0;
        for (AttachedDoc attachedDoc : documents) {
            bodyHashMap.put("documents[" + index + "][code]", RequestBody.create(MultipartBody.FORM, attachedDoc.getDocKey()));
//            bodyHashMap.put("documents[" + index + "][code]", RequestBody.create(MultipartBody.FORM, attachedDoc.getDocName()));
            bodyHashMap.put("documents[" + index + "][reference_number]", RequestBody.create(MultipartBody.FORM, attachedDoc.getReferenceNo()));

            File file = new File(attachedDoc.getFileAbsolutePath());
            // Parsing any Media type file
            RequestBody fileRequestBody = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("documents[" + index + "][document]", file.getName(), fileRequestBody);

            documentsPart.add(fileToUpload);
            index++;
        }
        return documentsPart;
    }


}
