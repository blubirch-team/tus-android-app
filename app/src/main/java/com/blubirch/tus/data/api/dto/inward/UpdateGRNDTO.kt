package com.blubirch.tus.data.api.dto.inward

import com.blubirch.core.data.api.dto.BaseDTO
import com.google.gson.annotations.SerializedName

class UpdateGRNRequestDTO(
    @SerializedName("stn_number") var stnNumber: String,
    @SerializedName("grn_number") var grnNumber: String
)

class UpdateGRNResponseDTO : BaseDTO()