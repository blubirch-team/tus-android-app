package com.blubirch.tus.data.repository;

import com.blubirch.tus.data.api.dto.stowing.StowingItemListResDTO;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.tus.data.api.dto.stowing.CompleteStowingRequestDTO;
import com.blubirch.tus.data.api.dto.stowing.StowingInventoriesDTO;
import com.blubirch.tus.data.api.dto.stowing.UpdateLocationRequestDTO;
import com.blubirch.tus.data.api.interfaces.StowingAPI;

import javax.inject.Inject;

import io.reactivex.Single;

public class StowingRepoImpl implements StowingRepo {

    private StowingAPI stowingAPI;

    @Inject
    public StowingRepoImpl(StowingAPI stowingAPI) {
        this.stowingAPI = stowingAPI;
    }

    @Override
    public Single<StowingInventoriesDTO> getStowingInventories(String toatNumber) {
        return stowingAPI.getToatInventoryList(toatNumber);
    }

    @Override
    public Single<BaseDTO> updateLocationDTO(long id, String location) {
        UpdateLocationRequestDTO updateLocationRequestDTO = new UpdateLocationRequestDTO(id, location);
        return stowingAPI.updateAisleLocation(updateLocationRequestDTO);
    }

    @Override
    public Single<BaseDTO> completeStowing(String toatNumber) {
        CompleteStowingRequestDTO completeStowingRequestDTO = new CompleteStowingRequestDTO(toatNumber);
        return stowingAPI.completeStowing(completeStowingRequestDTO);
    }

    @Override
    public Single<StowingItemListResDTO> getStowingList() {
        return stowingAPI.getStowingList();
    }

}
