package com.blubirch.tus.data.api.dto.inward

import com.blubirch.core.data.api.dto.BaseDTO
import com.google.gson.annotations.SerializedName

class ProceedGRNRequestDTO(@SerializedName("stn_number") var stnNumber: String)

class ProceedGRNResponseDTO : BaseDTO()
