package com.blubirch.tus.data.api.dto.inward

import com.google.gson.annotations.SerializedName

class VerifySerialDTO(
    @SerializedName("serial_number")
    val serialNumber: String,
    @SerializedName("serial_number_2")
    val serialNumber2: String
)