package com.blubirch.tus.data.model.stowing


import com.google.gson.annotations.SerializedName

data class StowingItem(
    @SerializedName("last_updated_at")
    val lastUpdatedAt: String,
    @SerializedName("quantity")
    val quantity: Int,
    @SerializedName("toat_number")
    val toatNumber: String
)