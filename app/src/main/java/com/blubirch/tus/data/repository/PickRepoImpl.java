package com.blubirch.tus.data.repository;


import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.tus.data.api.dto.pick.ConfirmPickReqDTO;
import com.blubirch.tus.data.api.dto.pick.UpdateToatReqDTO;
import com.blubirch.tus.data.api.dto.pick.UpdateToatResDTO;
import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;
import com.blubirch.tus.data.api.interfaces.PickAPI;

import javax.inject.Inject;

import io.reactivex.Single;

public class PickRepoImpl implements PickRepo{


    private PickAPI pickAPI;

    @Inject
    public PickRepoImpl(PickAPI pickAPI) {
        this.pickAPI = pickAPI;
    }

    @Override
    public Single<WarehouseOrdersResDTO> getList(){
        return pickAPI.getList();
    }

    @Override
    public Single<UpdateToatResDTO> updateToat(String id, String toatNumber) {
        UpdateToatReqDTO updateToatReqDTO = new UpdateToatReqDTO(id, toatNumber);
        return pickAPI.updateToat(updateToatReqDTO);
    }

    @Override
    public Single<BaseDTO> confirmPick(String id) {
        return pickAPI.confirmPick(new ConfirmPickReqDTO(id));
    }

}
