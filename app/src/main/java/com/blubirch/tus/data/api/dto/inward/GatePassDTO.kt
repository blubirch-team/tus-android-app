package com.blubirch.tus.data.api.dto.inward


import com.blubirch.tus.data.model.inward.GatePass
import com.google.gson.annotations.SerializedName

data class GatePassDTO(
        @SerializedName("gate_passes")
    var gatePasses: List<GatePass>,
        var meta: Meta
)

class Meta(val message: String)