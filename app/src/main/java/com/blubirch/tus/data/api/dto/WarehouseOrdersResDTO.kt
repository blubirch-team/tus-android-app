package com.blubirch.tus.data.api.dto

import com.blubirch.tus.data.model.WarehouseOrders
import com.blubirch.core.data.api.dto.BaseDTO
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WarehouseOrdersResDTO(
    @Expose
    @SerializedName("warehouse_orders")
    var warehouseOrders: List<WarehouseOrders>
) : BaseDTO()