package com.blubirch.tus.data.model.inward


import android.os.Parcelable
import com.blubirch.tus.testing.data.models.TestInventory
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*


@Parcelize
data class GatePassInventory(
    var id: Long = 0,

    @SerializedName("created_at")
    var createdAt: Date? = null,

    @SerializedName("updated_at")
    var updatedAt: Date? = null,

    @SerializedName("distribution_center_id")
    var distributionCenterId: Long = 0,

    @SerializedName("inwarded_quantity")
    var inwardedQuantity: Int = 0,

    @SerializedName("item_description")
    var itemDescription: String = "dasdasdasd",

    @SerializedName("map")
    var map: Double = 0.0,

    @SerializedName("quantity")
    var quantity: Int = 0,

    @SerializedName("sku_code")
    var skuCode: String = "dsadas",

    @SerializedName("ean")
    var ean: String = "dsadas",

    @SerializedName("item_code")
    var itemCode: String = "",

    @SerializedName("client_category_id")
    var clientCategoryId: Long = 0,

    @SerializedName("client_category")
    var clientCategory: String = "",

    var brand: String = "",
    var model: String = "",
    var status: String = "",

    var details: Detail? = null
) : Parcelable {


    fun toTestInventory(): TestInventory {
        return TestInventory().also {
            it.id = id
            it.sku = skuCode
            it.itemCode = itemCode
            it.desc = itemDescription
            it.categoryId = clientCategoryId
            it.category = clientCategory
            it.brand = brand
            it.model = model
            it.returnReasonRequired = true
            it.ownLabel = details!!.ownLabel
            when (details!!.ownLabel) {
                true -> it.gradingType = "Own Label"
                false -> it.gradingType = "Non Own Label"
            }
        }
    }
}

@Parcelize
data class Detail(
    @SerializedName("own_label")
    var ownLabel: Boolean = false
) : Parcelable