package com.blubirch.tus.data.repository;

import com.blubirch.tus.data.api.dto.stowing.StowingItemListResDTO;
import com.blubirch.core.data.api.dto.BaseDTO;
import com.blubirch.tus.data.api.dto.stowing.StowingInventoriesDTO;

import io.reactivex.Single;

public interface StowingRepo {

    Single<StowingInventoriesDTO> getStowingInventories(String toatNumber);

    Single<BaseDTO> updateLocationDTO(long id, String location);

    Single<BaseDTO> completeStowing(String toatNumber);

    Single<StowingItemListResDTO> getStowingList();

}
