package com.blubirch.tus.data.repository;


import com.blubirch.tus.data.api.dto.WarehouseOrdersResDTO;
import com.blubirch.tus.data.api.dto.pack.AssignBoxResDTO;
import com.blubirch.tus.data.api.dto.pack.CreateBoxResDTO;
import com.blubirch.core.data.api.dto.BaseDTO;

import io.reactivex.Single;

public interface PackRepo {

    Single<WarehouseOrdersResDTO> getList();

    Single<CreateBoxResDTO> createBox(Long id);

    Single<BaseDTO> deleteBox(Long id);

    Single<AssignBoxResDTO> assignBox(Long warehouseOrderItemId, String boxId);

    Single<BaseDTO> confirmDispatch(Long id);

    Single<BaseDTO> removeFromBox(Long id);


}
