package com.blubirch.tus.data.api.dto.inward


import com.blubirch.tus.testing.data.models.TestSelection
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class InventoryDataDTO(
    @SerializedName("client_gate_pass_number")
    var clientGatePassNumber: String,
    @SerializedName("gatepass_inventory_id")
    var gatepassInventoryId: Long,
    @SerializedName("sku_code")
    var skuCode: String,
    @SerializedName("serial_number")
    var serialNumber: String?,
    @SerializedName("serial_number_2")
    var serialNumber2: String?,
    @SerializedName("sr_number")
    var srNumber: String?,
    @SerializedName("tag_number")
    var tagNumber: String,
    @SerializedName("toat_number")
    var toatNumber: String,
    @SerializedName("manual_disposition")
    var manualDisposition: String?,

    @SerializedName("final_grading_result")
    var finalGradingResult: HashMap<String, ArrayList<TestSelection>>?,
    @SerializedName("processed_grading_result")
    var processedGradingResult: HashMap<String, String>,
    @SerializedName("grade")
    var grade: String,

    @SerializedName("return_reason")
    var returnReason: String,
    @SerializedName("disposition")
    var disposition: String?,
    @SerializedName("work_flow_name")
    var workFlowName: String?,
    @SerializedName("policy_type")
    var policyTpe: String?

)

