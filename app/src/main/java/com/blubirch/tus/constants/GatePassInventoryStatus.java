package com.blubirch.tus.constants;

public interface GatePassInventoryStatus {
    String PENDING_RECEIPT = "Pending Receipt";
    String PART_RECEIVED = "Part Received";
    String FULLY_RECEIVED = "Fully Received";
    String EXCESS_RECEIVED = "Excess Received";

    interface UIStatus {
        String PENDING_RECEIPT = "In-Transit";
        String PART_RECEIVED = "In-Transit";
        String FULLY_RECEIVED = "Received";
        String EXCESS_RECEIVED = "Excess";
    }
}


