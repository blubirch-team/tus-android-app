package com.blubirch.tus.constants;

import com.blubirch.tus.data.api.interfaces.DispatchAPI;
import com.blubirch.tus.data.api.interfaces.GatePassAPI;
import com.blubirch.tus.data.api.interfaces.ItemInfoAPI;
import com.blubirch.tus.data.api.interfaces.PackAPI;
import com.blubirch.tus.data.api.interfaces.PickAPI;
import com.blubirch.tus.data.api.interfaces.PickPackAPI;
import com.blubirch.tus.data.api.interfaces.ReGradingAPI;
import com.blubirch.tus.data.api.interfaces.StowingAPI;

public interface EndPoints {

    /**
     * {@link GatePassAPI}
     */
    String GATE_PASS_LIST = "warehouse/wms/gate_passes";
    String GATE_PASS_SEARCH = "warehouse/wms/gate_passes/stn_search";
    String SKU_SEARCH = "warehouse/wms/gate_passes/search_sku";
    String GENERATE_TAG = "warehouse/wms/gate_passes/generate_tag";
    String UPDATE_TAG_TOAT = "warehouse/wms/gate_passes/update_inventory";
    String PROCEED_GRN = "warehouse/wms/gate_passes/proceed_grn";
    String UPDATE_GRN = "warehouse/wms/gate_passes/update_grn";
    String SAVE_TESTED_RETURN = "warehouse/wms/gate_passes/create_inventories";
    String VERIFY_SERIAL_NUMBER = "warehouse/wms/gate_passes/serial_verification";
    /**
     * {@link StowingAPI}
     */
    String URL_STOWING_LIST = "warehouse/wms/stowing/fetch_inventories";
    String SAVE_AISLE_LOCATION = "warehouse/wms/stowing/set_location";
    String COMPLETE_STOWING = "warehouse/wms/stowing/complete_stowing";
    String GET_STOWING_ITEMS = "warehouse/wms/stowing/unstowed_items";

    /**
     * {@link PickPackAPI}
     */
    String GET_PICK_PACK_ORDER_LIST = "warehouse/wms/pick/fetch_orders";
    String PICK_PACK_CREATE_BOX = "warehouse/wms/pack/create_box";
    String PICK_PACK_ASSIGN_BOX = "warehouse/wms/pack/assign_box";
    String PICK_PACK_DELETE_BOX = "warehouse/wms/pack/delete_box";
    String PICK_PACK_REMOVE_FROM_BOX = "warehouse/wms/pack/remove_item";
    String PICK_PACK_DISPATCH_CONFIRM = "warehouse/wms/pack/dispatch_confirm";


    /**
     * {@link PickAPI}
     */
    String GET_PICK_LIST = "warehouse/wms/pick/fetch_orders";
    String UPDATE_TOAT = "warehouse/wms/pick/update_toat";
    String CONFIRM_PICK = "warehouse/wms/pick/pick_confirm";

    /**
     * {@link PackAPI}
     */
    String PACK_GET_LIST = "warehouse/wms/pack/fetch_orders";
    String PACK_CREATE_BOX = "warehouse/wms/pack/create_box";
    String PACK_DELETE_BOX = "warehouse/wms/pack/delete_box";
    String PACK_ASSIGN_BOX = "warehouse/wms/pack/assign_box";
    String PACK_REMOVE_FROM_BOX = "warehouse/wms/pack/remove_item";
    String PACK_DISPATCH_CONFIRM = "warehouse/wms/pack/dispatch_confirm";

    /**
     * {@link DispatchAPI}
     */
    String DISPATCH_GET_LIST = "warehouse/wms/dispatch/fetch_orders";
    String INITIATE_DISPATCH = "warehouse/wms/dispatch/dispatch_initiate_new";
    String COMPLETE_DISPATCH = "warehouse/wms/dispatch/dispatch_complete";

    String GET_CONSIGNMENT_LIST = "consignemnts";

    /**
     * {@link ReGradingAPI}
     */
    String RE_GRADING_ITEMS = "gradings/fetch_regrade_inventories";
    String SAVE_REGRADING = "gradings/store_grade";


    /** {@link ItemInfoAPI} */
    String ITEM_INFO = "warehouse/item_informations/search";

}
