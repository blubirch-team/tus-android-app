package com.blubirch.tus.constants;

public interface GatePassStatus {
    String CREATED = "Created";
    String IN_TRANSIT = "In-Transit";
    String RECEIVED = "Received";
    String COMPLETED = "Completed";
    String CLOSED = "Closed";

    int GRN_NOT_SUBMITTED = 0;
    int GRN_SUBMITTED = 1;
    int GRN_NUMBER_UPDATED = 2;
}
