package com.blubirch.tus.constants;

public interface SearchStatus {
    int NOT_SEARCHED = -1;
    int NOT_FOUND = 1;
    int FOUND = 2;
    int ERROR = 3;

}
