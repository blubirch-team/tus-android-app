package com.blubirch.tus.constants;

public interface MockEndPoints {

    String GET_PICK_LIST = "warehouse/pick_item";

    String URL_GET_PICK_ITEMS = "warehouse/pick_item/get_inventories";

    String GET_CONSIGNMENT_LIST = "consignemnts";

    /*GatePass*/
    String URL_GATE_PASS_LIST="gate_passes";

}
