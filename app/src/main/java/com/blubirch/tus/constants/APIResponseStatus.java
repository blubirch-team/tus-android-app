package com.blubirch.tus.constants;

public interface APIResponseStatus {
    int NOT_REQUESTED = -1;
    int NOT_FOUND = 0;
    int FOUND = 1;
    int ERROR = 2;
    int SUCCESS = 3;

}
