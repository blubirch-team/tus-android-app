package com.blubirch.tus

import com.blubirch.core.presentation.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor() : BaseViewModel()
