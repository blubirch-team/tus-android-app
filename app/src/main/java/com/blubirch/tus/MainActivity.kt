package com.blubirch.tus

import android.view.MenuItem
import com.blubirch.core.di.ViewModelFactory
import com.blubirch.core.presentation.BaseActivity
import com.blubirch.tus.R
import javax.inject.Inject

class MainActivity : BaseActivity<MainViewModel>() {

    @Inject
    lateinit var mViewModelFactory: ViewModelFactory

    override fun attachLiveData() {

    }

    override fun setupNavGraph(navController: androidx.navigation.NavController) {
        navController.graph = navController.navInflater.inflate(R.navigation.main_navigation)
    }

    override fun getViewModelFactory(): ViewModelFactory {
        return mViewModelFactory
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        setupUI()
    }

    private fun setupUI() {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.bsfLogout,
                R.id.menuFragment -> {
                    supportActionBar?.setDisplayShowHomeEnabled(true)
                    supportActionBar?.setLogo(R.drawable.ic_blubirch_logo)
                    supportActionBar?.setDisplayUseLogoEnabled(true)
                }
                else -> {
                    supportActionBar?.setDisplayShowHomeEnabled(false)
                    supportActionBar?.setLogo(null)
                    supportActionBar?.setDisplayUseLogoEnabled(false)
                }
            }
        }

    }
}
