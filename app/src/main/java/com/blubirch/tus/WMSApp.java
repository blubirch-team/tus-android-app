package com.blubirch.tus;

import com.blubirch.tus.di.component.DaggerApplicationComponent;
import com.blubirch.core.utility.AppPreferences;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class WMSApp extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        AppPreferences.initPreferences(this);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.factory().create(WMSApp.this);
    }

}